# PeePyPoo

PeePyPoo is a Python library for the simulation of wastewater treatment systems.

Please note that this package is still under development, any contributions are highly appreciated, 
see below how to contribute.

The documentation is hosted on: https://datinfo.gitlab.io/PeePyPoo

## Authors

This package has been created at [Eawag](https://www.eawag.ch) by:

- [Florian Wenk](https://www.eawag.ch/de/ueber-uns/portraet/organisation/mitarbeitende/profile/florian-wenk/show/)
- [Andreas Frömelt](https://www.eawag.ch/de/ueber-uns/portraet/organisation/mitarbeitende/profile/andreas-froemelt/show/)

## Contributing

Pull requests are welcome in general. Further, everyone adding a new system is highly encouraged to provide it using 
a pull request for adding it to the model library.

For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[GPLv3.0](https://choosealicense.com/licenses/gpl-3.0/)
