.. _extension reference:

Extension Reference
===================

This chapter describes the different classes and methods that are necessary for extending the functionality of this
library. This can mainly be done by adding new systems and signal types to the library. Each of those has a section in
the following.

Systems
-------

In this library, the overall system to be simulated is considered as a connection of multiple subsystems which are
connected through inputs and outputs, i.e. each system can produce outputs, which are then fed as inputs into other
systems. Thus each system is considered as connection of different blocks. Each system can perform arbitrary
computations limited only by the capability of python, therefore they can represent classical model parts which evaluate
equations producing an output. On the other hand they can also mainly be used to have side-effects e.g. displaying
something or communicating to hardware.

Therefore, all functionality of this library is within the systems, while the rest of the code manages the connection
and evaluation of these systems. Thus, for extending the functionality of this library, adding new systems is crucial.
The following part is documenting existing code to facilitate adding new systems.

For adding a new system, one has to consider, that every system has to inherit from the system class and at least
implement its :meth:`peepypoo.Systems.System.get_output` function. For implementing this function however, the helper
functions which are already implemented can be used, especially :meth:`peepypoo.Systems.System._get_inputs` is useful,
which provides the inputs of the system at a given time. (Is documented directly in the code as only needed for
extending the library). However, more specialized systems to inherit from exist to facilitate the implementation of
dynamic systems and systems that can be converted to julia.

Callbacks
+++++++++
For extending the library, especially for functionality concerning the whole block diagram , it can be necessary that
the systems respond to events. For example, the :class:`peepypoo.Systems.BDWrapper` uses them to dynamically add/remove
new systems if they are connected to a system that is in the wrapper.

For such functionality, each system supports a number of callbacks which can be added to react to certain events.
Currently, the following events are supported:

* ``input_connection_addition``
* ``output_connection_addition``
* ``on_connection_addition``
* ``input_connection_removal``
* ``output_connection_removal``
* ``on_connection_removal``
* ``input_connection_change``
* ``output_connection_change``
* ``on_connection_change``
* ``input_connection_tree_change``
* ``output_connection_tree_change``
* ``connection_tree_change``
* ``system_deletion``

To add a callback to an event, simply a function reference has to be added to the vector of the system named
``on_EVENT_NAME_callbacks``, where ``EVENT_NAME`` is any of the events listed above, i.e. the event name is
prepended by ``on_`` and ``_callbacks`` is appended to it. This yields e.g. ``on_system_deletion_callbacks``.

.. warning::

    It should always be added to the callback vectors and never directly set, as they are used for some internal
    functionality as well, which could otherwise be destroyed.

JuliaModelingToolkitConvertibleSystem
+++++++++++++++++++++++++++++++++++++

The abstract class :class:`peepypoo.Systems.JuliaModelingToolkitConvertibleSystem`, is to be inherited by every system
that supports conversion to ModelingToolkit.jl for simulation there. It defines the interface needed for converting the
system to Julia. For this, the method :meth:`peepypoo.Systems.JuliaModelingToolkitConvertibleSystem._compile_julia_system`
must be overridden. See the documentation of this function directly in the code for the functionality needed to be
implemented when overriding this method (it is not documented here, as it is private and only intended for use within
the library itself).

ContinuousTimeDynamicalSystem
+++++++++++++++++++++++++++++

The :class:`peepypoo.Systems.Dynamic.ContinuousTimeDynamicalSystem` is useful for implementing continuous time dynamical
systems to use within this framework. It can be used to implement generic dynamical systems of the form

.. math::

    \dot{x} = f(x, u, p, t)\\
    y = \text{calculate_output}(x, u, p, t).

For these systems, it is possible to simply inherit from this class and to provide the dynamics function
:meth:`peepypoo.Systems.Dynamic.DynamicalSystem.f` as well as the output function
:meth:`peepypoo.Systems.Dynamic.DynamicalSystem.calculate_output` and then the base system takes care of
integrating the state through time. Further, it is recommended to provide the dynamics and output equations
as well in symbolic form in the variables :attr:`peepypoo.Systems.Dynamic.DynamicalSystem.dynamics_formula`,
resp. :attr:`peepypoo.Systems.Dynamic.DynamicalSystem.output_formula`, which enable the dynamical system to
be automatically converted to Julia and simulating it there using
`ModelingToolkit.jl <https://docs.sciml.ai/ModelingToolkit/stable/>`_.

DiscreteTimeDynamicalSystem
+++++++++++++++++++++++++++

The :class:`peepypoo.Systems.Dynamic.DiscreteTimeDynamicalSystem` is useful for implementing discrete time dynamical
systems to use within this framework. It can be used to implement generic dynamical systems of the form

.. math::

    x_{k+1} = f(x_k, u_k, p_k, t_{k+1})\\
    y_{k+1} = \text{calculate_output}(x_{k+1}, u_k, p_k, t_{k+1}).

For these systems, it is possible to simply inherit from this class and to provide the dynamics function
:meth:`peepypoo.Systems.Dynamic.DynamicalSystem.f` as well as the output function
:meth:`peepypoo.Systems.Dynamic.DynamicalSystem.calculate_output` and then the base system takes care of
stepping through time and keeping track of the state.

.. warning::

    It is possible to provide the dynamics and output equations
    as well in symbolic form in the variables :attr:`peepypoo.Systems.Dynamic.DynamicalSystem.dynamics_formula`,
    resp. :attr:`peepypoo.Systems.Dynamic.DynamicalSystem.output_formula`, which enable the dynamical system to
    be automatically converted to Julia and simulating it there using
    `ModelingToolkit.jl <https://docs.sciml.ai/ModelingToolkit/stable/>`_. However hereby there is an error, which
    leads to the system being integrated instead of stepped in discrete steps through time.
    This is due to an apparent limitation in ModelingToolkit.jl, see
    `this github issue <https://github.com/SciML/ModelingToolkit.jl/issues/894>`_.

JuliaModelingToolkitConvertibleStaticSystem
+++++++++++++++++++++++++++++++++++++++++++

The abstract class :class:`peepypoo.Systems.Static.JuliaModelingToolkitConvertibleStaticSystem`, is an abstract helper class
which helps in writing static systems which can be converted to ModelingToolkit.jl. For this functionality, the
relation between in- and output has to be provided in symbolic form in
:attr:`peepypoo.Systems.Static.JuliaModelingToolkitConvertibleStaticSystem.output_formula` which is then used to directly convert the
system to julia. However, the usual :meth:`peepypoo.Systems.System.get_output` function has still to be overridden.

Signal Types
------------

Signal types are used for checking the types of the signals exchanged between systems. Thus, for each type that is to
be sent from one system to another, a corresponding signal type has to exists. Thus to extend this package to support
another variable type to pass, a new ``SignalType`` has to be added. Each signal type has to implement several methods:

* :meth:`supports_value<peepypoo.SignalTypes.SignalType.supports_value>`: Returns if the given value is supported by
    this ``SignalType``
* :meth:`get_zero<peepypoo.SignalTypes.SignalType.get_zero>`: Returns the neutral/zero element of this signal type. It
    is used for getting the default input to a system if nothing is connected.
* :meth:`is_compatible<peepypoo.SignalTypes.SignalType.is_compatible>`: Returns if a port of another ``SignalType`` is
    compatible to this one s.t. it can be connected to a port of this type.

Adding stoichiometric matrices
------------------------------

A special class of systems in this library are the reactors. They are defined such that the stoichiometric matrices
are read from tabular files. Some standard stoichiometries have already been defined and are available in the library.
However, when simulating reactors, it is well probable that other than the standard reactions are needed,
such that other stoichiometries have to be defined. For this purpose all reactors accept not only the
names of library models as their ``stoichiometriy_file`` parameters but as well paths to any valid
tabular file (tested are ``.csv``, ``.ods`` and ``.xlsx`` but theoretically all formats supported by
pandas should work), containing a stoichiometric matrix can be provided. When doing this, the file must
be containing a stoichiometric matrix with the following properties:

* "Stoichiometric Matrix" or "Stoichiometry" (all capitals can also be
  lowercase) is written in the *top left corner*
* The *top row* (without the "Stoichiometric Matrix" or equivalent label)
  consists of the character "i" followed by the increasing list of integers
  (starting with 1) denoting the compound number and finally an empty cell.
* In the *second row*, the state variables are named
* In the *third row*, the units of the state variables are added
* Then, the following rows consist of numbers or numeric expressions which
  define the stoichiometry.
* The *most left column* (again without the "Stoichiometric Matrix" or
  equivalent label) consists first of an empty cell, then the character "j"
  and the increasing list of process numbers (again starting with 1).
* In the *most right column* below the empty cell after the compound
  numbers, the process rates are listed, all in the same row as the
  corresponding process
* Below this stoichiometric matrix, the composition matrix **can** be added
  which is structured as follows:

  * In the *first column*, directly below the highest process number, the
    character "k" denotes that the composition matrix follows. Afterwards,
    the increasing numbers of the conservatives follow (again starting from
    1).
  * Right of this, in the column of the corresponding states, the
    composition matrix is added.

* Below this stoichiometric matrix and the optional composition matrix, a row **can** be added indicating whether
  the state dynamics are applied or if the state is to be kept constantly at a value, given as parameter.
  This functionality is currently used for keeping the dissolved oxygen at a constant level to mimic aeration.

  If not given, all states are assumed to use the dynamics. If the state is to be kept constant, the system will hold
  the initial value. This row is structured as follows:

  * In the *first column* directly below the highest process number or the highest number of conservatives,
    "is_constant" is added.
  * Right of this, in the columns of the corresponding states, it is added whether the state is to be kept at a
    constant value. Empty or "0" indicates that the dynamics are used, "1" implies that the state is to be kept
    constant.

Note that the stoichiometric matrix does **not** have to be the only thing
in the dataframe. It is just necessary that the top left corner, the top
row as well as the most left column correspond to the requirements
mentioned above and that no other entry except the top left corner of the
matrix matches the ``[sS]toichiometr(y|ic [mM]atrix)`` regular expression. Otherwise, an
error is thrown.

Examples of such matrices can be found in the model library (found under
``peepypoo/Systems/Dynamic/Continuous/StoichiometricMatrices/``).

Further, all parameters used in the stoichiometric matrix have to be defined separately in the following form:

* It consists of two columns with headers "Parameters" (or "Parameter",
  "Params", "Param" or all letters being lowercase) and "Values" (or
  "Value", or with lowercase "v"). The "Values" column must be directly
  right of the "Parameters" column, i.e. the cell right of "Parameters"
  must contain "Values".
* The "Parameters" column contain each a parameter, the "Values" column the
  value of the parameters on its left. If the cell is left empty, the
  parameter is considered to be provided at runtime.
* The columns are terminated by an empty cell.

Similar to the stoichiometric matrix, also other entries might be in the
dataframe. The only condition is that none of them match neither of the regular expressions
``[pP]aram(eter)?s?`` nor ``[vV]alues?``.

.. note::

    The parameters can be defined in an arbitrary tabular file as the stoichiometric matrix,
    the only requirement is that the path to it is given as ``parameters_file`` parameter. If
    this is not given, it is assumed that the parameters are defined in the same file as the
    stoichiometry. Providing the parameters as single file is especially useful for providing
    fixed parameters to library models, such that the parameters do not have to be given at
    simulation time.

Examples of parameter matrices can be found as well in the model library (in the same files as the
stoichiometries).