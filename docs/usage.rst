Usage
=====

This chapter aims to serve as a reference for using the PeePyPoo package for modeling of wastewater treatment plants.
Note that it describes only the API for using the framework, for extending the package see the
:ref:`extension reference<extension reference>`.

The package enables modeling and simulation of potentially arbitrary systems by mainly providing an interface that can
be used to connect systems and evaluate a connection of those. It features a modular design that enables adding new
systems easily. However, this package is primarily built for the simulation of wastewater treatment plants and thus
currently only systems from this domain are included in the library.

To build a model using this package, one proceeds as follows:

#.  The systems are :ref:`created<usage:Creating Systems>`
#.  The systems are :ref:`connected<usage:Connecting Systems>`
#.  The resulting blockdiagram is :ref:`simulated<usage:Simulating Systems>`, either

    *  In :ref:`Python<usage:In Python>`, or
    *  In :ref:`Julia<usage:In Julia>` using the BDWrapper.

Additionally, the full blockdiagram can be :ref:`analyzed<usage:Analyzing the Blockdiagram>` using the
:class:`BDWrapper<peepypoo.BDWrapper>`.

Below, every of these steps is explained using a very simple example, before two more advanced
:ref:`usage examples<examples:Examples>` follow. The :ref:`first example<examples:Disinfection with Ozone>` is a small example which
provides a quick overview of how to perform the same steps for a system of reactors and recommended as a quick start
into the package. Complementary to that, the :ref:`second example<examples:BSM1>` is more advanced and shows the simulation of a
larger system closer to real-world applications.

Creating Systems
----------------
Systems are the basic building blocks of everything in this package. Each system is an entity on its
own and has a number of inputs and outputs and defines the input-output relation between those.

For such systems, currently the following are available in different categories:

Reactors
    - :class:`Batch Reactor<peepypoo.Systems.Dynamic.Continuous.Reactors.BatchReactor>`
    - :class:`Continuously Stirred Tank Reactors (CSTR)<peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR>`
    - :class:`Sequencing Batch Reactor (SBR, Always stirred)<peepypoo.Systems.Dynamic.Continuous.Reactors.BatchReactor>`

Settlers
    - :class:`Ideal Settler<peepypoo.Systems.Static.FlowElements.IdealClarifier>`: Represents an ideal clarifier
      fully separating the solid parts in the flow
    - :class:`Takács Settler Model<peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs>`: A more realistic model
      of a clarifier

Flow Elements
    - :class:`Flow Unifier<peepypoo.Systems.Static.FlowElements.FlowUnifier>`: Combines an arbitrary number of flows
    - :class:`Flow Separator<peepypoo.Systems.Static.FlowElements.FlowSeparator>`: Separates a singe flow into a
      arbitrary number of flows

General systems
    - :class:`Constant<peepypoo.Systems.Static.Constant>`: Simply outputting a given constant, helpful to be used as
      inflow
    - :class:`PiecewiseLinearInterpolation<peepypoo.Systems.Static.PiecewiseLinearInterpolation>`: Outputting a piecwise
      linear interpolation of given values, helpful to be used as inflow
    - :class:`Gain<peepypoo.Systems.Static.Gain>`: Multiplying the input by a given gain
    - :class:`Cast<peepypoo.Systems.Static.Cast>`: Casting the input signal to a new type
    - :class:`Continuous Integrator<peepypoo.Systems.Dynamic.Continuous.Integrator>`: Integrating the input in
      continuous time
    - :class:`Discrete Integrator<peepypoo.Systems.Dynamic.Discrete.Integrator>`: Integrating the input in
      discrete time

On how to create these systems and which options they support, the reader is referred to the specific documentation of
the systems.

For our small example of multiplying and integrating two constant values, we need to declare two constant and an integrator system
integrating two inputs (after importing `peepypoo`):

.. code-block:: python

    import peepypoo

    const1 = peepypoo.Systems.Static.Constant([1.0], name="Const1")  # Constant with a value of 1.0
    const2 = peepypoo.Systems.Static.Constant([2.0], name="Const2")  # Constant with a value of 2.0
    gain = peepypoo.Systems.Static.Gain([2.0, 2.0], name="Gain")  # A gain multiplying two inputs each with 2
    int1 = peepypoo.Systems.Dynamic.Continuous.Integrator(initial_state=0.0, name="int1")  # Integrator for the first signal with initial state being 0.0.
    int2 = peepypoo.Systems.Dynamic.Continuous.Integrator(initial_state=0.0, name="int2")  # Integrator for the second signal with initial state being 0.0.

.. note::

    The example could equivalently be constructed using array values and combining some of the systems. However, here
    we stick to this notation to be able to later show different methods of connecting the systems.

Connecting Systems
------------------

However, most systems are not interesting on their own but mainly in the interaction with other systems. For this, the
systems can be connected using two different methods of each system:

1.  :meth:`add_input_connection<peepypoo.Systems.System.add_input_connection>`
2.  :meth:`add_output_connection<peepypoo.Systems.System.add_output_connection>`

Both of these functions lead ultimately to the same result, but look at it from a different viewpoint:
While the first one looks back and adds another system to provide the input for the current one, the second one looks
forward adding the output of the current system as input of another one.

To demonstrate this difference we connect the systems in our example all from the view of the gain in the middle. First,
we look backwards and add the constants as input systems to the gain (the constant's outputs are inputs to the gain):

.. code-block:: python

    # First add the output of the first constant to the first input of the gain:
    gain.add_input_connection(const1, [0], [0]) # Connect const1 output signal 0 to gain input signal 0
    # Then add the output of the second constant to the second input of the gain:
    gain.add_input_connection(const2, [1], [0]) # Connect const2 output signal 0 to gain input signal 1

Hereby, the arrays in the function arguments specify which input/output signals are to be connected. The first specifies
which **input** ports (of the gain system) and the second one which **output** ports (of the constant) are connected.

Now looking forwards from the gain system, we add the integrators as output systems of the gain system (the gain's
outputs are inputs for the integrators):

.. code-block:: python

    # Add the first output of the gain system as input to the first integrator
    gain.add_output_connection(int1, [0], [0]) # Connect gain output signal 0 to int1 input signal 0
    # Add the second output of the gain system as input to the second integrator
    gain.add_output_connection(int2, [0], [1]) # Connect gain output signal 1 to int2 input signal 0

Equal as above, the arrays in the function arguments specify which input/output signals are to be connected. The first
specifies which **input** ports (of the gain system) and the second one which **output** ports (of the constant) are
connected (same order for the ports for both functions, only the systems are swapped).

In this example one sees that the signals to be connected have to be selected by 0-based indexes. This might be quite
confusing and seem like magic to know which numbers to put there. However, the order of signals can be inferred from
the documentation of each system, which provides a table for the system explaining which port has which number and what
the specific ports do.

Further it is important to note that, while each system input port can only be driven a single signal, each system
output can drive arbitrarily many inputs.

.. warning::

    There is a source of error that can occur when trying to connect systems and is particularly unintuitive for users
    of Python or similar languages which are dynamically typed and no datatypes need to be specifically assigned:

    **The package is very strict about datatypes, for example distinguishing between integer and floating point values.**
    Thus our example would produce an error if any of the values in the constants, the gain or the initial conditions of
    the integrators would miss the `.0` in the end!


Simulating Systems
------------------

Finally the connected systems can all together be simulated, either directly in python or they can automatically be
converted to Julia and be simulated there providing much better performance especially for large systems.
This automatic translation to Julia has the advantage of better performance and applicability to large systems
however, it comes at the price of some time overhead needed to convert the systems to julia.

In Python
+++++++++

To simulate the system in python, one can very easily get the output of the specific systems at a specific timestep.
For example, to get the output of the first integrator after `10` time units one uses:

.. code-block:: python

    int1.get_output(10) # Get the output of int1 after 10 time units
    # Returns 20

and similarly for the second integrator:

.. code-block:: python

    int2.get_output(10) # Get the output of int2 after 10 time units
    # Returns 40

.. invisible-code-block: python

    # Check outputs for doctesting
    assert int1.get_output(10)[0] - 20 <= 1e-4
    assert int2.get_output(10)[0] - 40 <= 1e-4

However, this might be annoying to do at every timestep if the signal value over a certain time is required.
To improve this, there is a :class:`Signal Logger<peepypoo.Systems.LoggingAndVisualization.SignalLogger>` system,
which takes care of exactly that. For example for getting the output of the two integrators at every `0.1` timesteps,
one can use this system as follows:

.. code-block:: python

    # Create the logging system
    logger = peepypoo.Systems.LoggingAndVisualization.SignalLogger(0.1) #  0.1 is the timestep between the logging.
    # Add the signals to log (the output signals 0 of both integrators)
    logger.add_logging_signal(int1, [0])
    logger.add_logging_signal(int2, [0])
    # Log the data for 10 timesteps
    logger.log_data(0, 10)
    # Get the logged data
    out = logger.get_logged_data()

Where one gets back a pandas dataframe with index being the timestep and the columns being named after the name of the
system and the output port number.

.. invisible-code-block: python

    assert all(out.to_numpy()[-1] - [20, 40] <= 1e-4)

.. warning::

    The python simulation is implemented quite simple: For getting the output of a system and thus solving the
    corresponding equations, each system queries its input at every time needed for the integration routine.
    Supposing that our system has loops (e.g. recirculation flows) this will lead to having the systems infinitely
    calling their predecessors (the system it is called for will eventually be called again resulting in the loop) and
    with this in an ``RecursionError: maximum recursion depth exceeded`` error. To avoid this, set the argument
    ``discretize_dt=STEP`` for at least one dynamical system in the loop to break it (but it is recommended to set it
    for all systems in the loop). Hereby ``STEP`` is the timestep of discretization. Increase this timestep (or
    introduce it for more systems in the loop) if you still get this error. Note that this will introduce a "delay" in
    the system and the larger it is, the more inaccuracies will be in the solution.
    As the solution in Julia (see :ref:`usage:In Julia`) handles these cases better and without this
    discretization it is advised to switch to this one instead. Further the solution in julia generally shows a better
    performance for such complex systems.

However, this still has issues if the systems get more complicated or if they have algebraic loops:
Then, this method of solving is very slow or is infeasible. To overcome as well this issue, it is possible
to convert the whole system tree (if all individual systems support it) to the Julia library
`ModelingToolkit.jl <https://docs.sciml.ai/ModelingToolkit/stable/>`_, which enables as well faster computations
at the cost of it taking some time to convert the systems. How to do this is explained in the next
:ref:`subsection<usage:In Julia>`.

In Julia
++++++++

To simulate the system in julia, the :class:`BDWrapper<peepypoo.BDWrapper>` class is used, which is responsible for every functionality
involving the whole blockdiagram ans which will as well be used for the analyses in the corresponding
:ref:`section<usage:Analyzing the Blockdiagram>`.

.. note::

    Currently, the logger system cannot be used when simulating in Julia. Thus it has to be removed if the last part of
    the explanations have been followed. To do so, use

    .. code-block:: python

        logger.remove_connections()

    Please note, that this is not a limitation of functionality, as the simulation in Julia directly has this feature
    built in and always outputs a dataframe with the outputs of each system at every evaluated timestep.

Therefore, at first all systems need to be added to the :class:`BDWrapper<peepypoo.BDWrapper>` as follows:

.. code-block:: python

    # The argument is a list of systems to be contained in the wrapper. Note that for each system always all
    # other systems connected to it are added as well. Thus it suffices to provide it with, for example, the gain.
    wrapper = peepypoo.BDWrapper([gain])

Then, the whole blockdiagram can be evaluated in julia, where directly the times to evaluate the output at can be
provided:

.. code-block:: python

    # Evaluate the blockdiagram in julia. Arguments are starting and end time as well as the times
    # to output the result at.
    out = wrapper.evaluate_blockdiagram_julia(0, 10, [i/10 for i in range(0, 101)])

The resulting output is again a pandas dataframe with again the index being time, but the columns are now directly the
systems (enabling indexing the dataframe directly by the system object) and each entry is now a ``numpy.array``
containing all values of the outputs of the system at the specific time.

If one prefers the output of the logger system, there is a utility function in the wrapper to convert the dataframe
(or only certain columns of it) to this format:

.. code-block:: python

    # Convert only the outputs of the integrators to the other format
    out_conv = wrapper.convert_df_systems_to_df_signals(out[[int1, int2]])

.. invisible-code-block: python

    assert all(out_conv.to_numpy()[-1] - [20, 40] <= 1e-4)

.. warning::

    If you simulate using the Julia translation and have issues with the tolerances (indicated e.g. by unexpected
    oscillations introduced by the numerics), you can set the accuracies by passing the ``atol`` and ``rtol`` keywords
    to the call of :meth:`evaluate_blockdiagram_julia <peepypoo.BDWrapper.evaluate_blockdiagram_julia>`. See the docs
    there for details.

Analyzing the Blockdiagram
--------------------------

Perform analyses on the full blockdiagram, one uses again the :class:`BDWrapper<peepypoo.BDWrapper>`.
Here we reuse the wrapper from the last step, so if this hasn't been followed, the creation has to be done as shown
there. Once the wrapper is created one can perform the analyses on the whole diagram.

Currently, only two checks are implemented, namely if cycles or algebraic loops exist in the system:

.. code-block:: python

    wrapper.has_cycles() # Are there cyclic connections?
    wrapper.has_algebraic_loop() # Are there algebraic loops (cyclic connections without any dynamic system in it)?

.. invisible-code-block: python

    assert not wrapper.has_cycles()
    assert not wrapper.has_algebraic_loop()

Additionally, it is supported to have a look at the differential equations solved by the system using

.. code-block:: python

    wrapper.get_equations()

which outputs a string with the latex equations of the system. Typesetting the result from
this example in LaTeX yields

.. math::

    \begin{align}
    \frac{\mathrm{d} \mathrm{int1_{+}x}\left( t \right)}{\mathrm{d}t} =& \mathrm{Gain_{+}y0}\left( t \right) \\
    \frac{\mathrm{d} \mathrm{int2_{+}x}\left( t \right)}{\mathrm{d}t} =& \mathrm{int2_{+}u}\left( t \right)
    \end{align}

