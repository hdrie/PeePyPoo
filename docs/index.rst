.. PeePyPoo documentation master file, created by
    sphinx-quickstart on Thu Oct 27 15:45:53 2022.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

PeePyPoo
========

PeePyPoo is a package providing a modeling framework designed for modeling water systems,
with the option of using hybrid models in mind. However it can as well be used for arbitrary other systems. It has
the basic functionality included for solving the models in python, but can as well automatically translate the given
systems to the Julia `ModelingToolkit.jl <https://docs.sciml.ai/ModelingToolkit/stable/>`_, which can be used for
significantly improved simulation performance.

.. note::

    This package is still under development, so if you find any bugs or have any suggestions, please
    post an issue report or even better start a pull request. See `Contributing`_


Authors
-------

This package has been created at `Eawag <https://www.eawag.ch>`_ by:

- `Florian Wenk <https://www.eawag.ch/de/ueber-uns/portraet/organisation/mitarbeitende/profile/florian-wenk/show/>`_
- `Andreas Frömelt <https://www.eawag.ch/de/ueber-uns/portraet/organisation/mitarbeitende/profile/andreas-froemelt/show/>`_

If you use PeePyPoo in your work please cite it using the following

.. code-block::

    @misc{2023peepypoo,
        title = {{PeePyPoo}: An Open-Source Framework for Hybrid Modelling of Water Systems},
        url = {https://gitlab.com/datinfo/PeePyPoo},
        author = {Florian Wenk and Andreas Froemelt},
        year = {2023},
    }

Feature Summary
---------------

PeePyPoo is a modeling package intended for modeling water systems. It thus mainly provides a framework for
creating reactors and combining them arbitrarily. The reactors can simulate arbitrary reactions,
provided as stoichiometric matrices in a tabular file (e.g. .odf, .csv or .xslx formats amongst others).
These reactors can then be combined with other reactors as well as settlers and flow components.

With this, models of full water resource recovery facilities can be created by simply combining the corresponding
reactors and other flow elements.

There is already a library containing some reactors, settlers etc., however it can be extended if necessary
and, if done, every contribution of new systems is welcome and can be incorporated by pull-requests.

Feature List
------------

Reactors
    - :class:`Batch Reactor<peepypoo.Systems.Dynamic.Continuous.Reactors.BatchReactor>`
    - :class:`Continuously Stirred Tank Reactors (CSTR)<peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR>`
    - :class:`Sequencing Batch Reactor (SBR, Always stirred)<peepypoo.Systems.Dynamic.Continuous.Reactors.BatchReactor>`

Stoichiometries
    - `ozonation`: Disinfection with Ozon
    - `ASM1`: Activated Sludge Model Nr. 1
    - `ASM1_constO`: Activated Sludge Model Nr. 1, where the dissolved oxygen is kept constant at the initial value
    - `ASM3`: Activated Sludge Model Nr. 3
    - `ASM3_constO`: Activated Sludge Model Nr. 3, where the dissolved oxygen is kept constant at the initial value
    - `ASM3_BioP`: Activated Sludge Model Nr. 3 with the Bio-P Module

Settlers
    - :class:`Ideal Settler<peepypoo.Systems.Static.FlowElements.IdealClarifier>`
    - :class:`Takács Settler Model<peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs>`

Flow Elements
    - :class:`Flow Unifier<peepypoo.Systems.Static.FlowElements.FlowUnifier>`
    - :class:`Flow Separator<peepypoo.Systems.Static.FlowElements.FlowSeparator>`

General systems
    - :class:`Constant<peepypoo.Systems.Static.Constant>`: Simply outputting a given constant
    - :class:`PiecewiseLinearInterpolation<peepypoo.Systems.Static.PiecewiseLinearInterpolation>`: Outputting a piecwise
      linear interpolation of given values
    - :class:`Gain<peepypoo.Systems.Static.Gain>`: Multiplying the input by a given gain
    - :class:`Cast<peepypoo.Systems.Static.Cast>`: Casting the input signal to a new type
    - :class:`Continuous Integrator<peepypoo.Systems.Dynamic.Continuous.Integrator>`: Integrating the input in
      continuous time
    - :class:`Discrete Integrator<peepypoo.Systems.Dynamic.Discrete.Integrator>`: Integrating the input in
      discrete time

Helper Systems
    - :class:`Signal Logger<peepypoo.Systems.LoggingAndVisualization.SignalLogger>`: Logs signals when simulating using
      Python

Analysis
    - :class:`Block Diagram Wrapper<peepypoo.BDWrapper>`: Wraps a number of systems to a block diagram and allows for
      analysis of the whole diagram and simulation of it in Julia for better performance.

Contributing
------------

Pull requests are welcome in general. Further, everyone adding a new system is highly encouraged to provide it using
a pull request for adding it to the model library.

For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

Contents
--------

.. toctree::
    :maxdepth: 2

    installation
    usage
    examples
    extensionReference

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
