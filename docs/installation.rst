Installation
============

This chapter describes how to install the package for usage.
As the package internally uses Julia to speed up calculations, the setup requires installing both Python and Julia.
Note however, that the installation of Julia is done automatically once the package is imported and does thus not
require any manual action.

Instructions
------------

This project uses Python as well as internally Julia to speed up the calculations. However, Julia is automatically
installed when importing the package.

For the installation, it is recommended to use a virtual environment. To install the package withing a virtual
environment it proceed as follows:

1. Install python using your favorite package manager or from the `web <https://www.python.org/>`_.
   (This can be skipped if Python is already installed)
2. Create a new virtual environment and activate it
    `bash <https://en.wikipedia.org/wiki/Bash_(Unix_shell)>`_ (Unix, including MacOS)
        - Creation: ``python -m venv <venv>``
        - Activation: ``source <venv>/bin/activate``

        where ``<venv>`` is the path where the virtual environment is to be located
    Windows (in the CMD)
        - Creation: ``python -m venv <venv>``
        - Activation: ``.\<venv>\Scripts\activate``

        where ``<venv>`` is the path where the virtual environment is to be located
3. Install ``PeePyPoo`` directly from git using pip in the virtual environment: ``pip install git+https://gitlab.com/datinfo/PeePyPoo``
4. **OPTIONAL:** Install Julia and the Julia requirements (otherwise this will be automatically done when importing
   ``PeePyPoo`` for the first time):

    1. Start the Python REPL (``python`` with the virtual environment activated)
    2. Import the module ``PeePyPoo``: ``import peepypoo``

    .. note::
        This step is listed here as the first import of the package might take a substantial amount of time.
        This is due to the fact, that in this step Julia and all needed julia dependencies are automatically installed
        within the virtual environment (The actual time needed for this varies depending on the setup and if julia is
        already installed).

5. Start using the **PeePyPoo** package: ``import peepypoo`` within the virtual environment

.. warning::
    If you installed python from the Microsoft Store, the installation of PeePyPoo will currently not work.
    This is because all apps installed from the Microsoft Store are sandboxed which is why errors occur when installing
    Julia within it, as the default julia version needs some shared libraries from the system which are not accessible
    due to the sandboxing. **It is recommended to use the Python version from the official
    `webpage <https://www.python.org/>`_ instead.**

