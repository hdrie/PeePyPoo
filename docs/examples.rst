Examples
========

In the following, there are multiple additional examples which might be helpful for using the framework. As this
package is mainly developed for modeling wastewater treatment plants, the examples are from this area, but it should be
possible to be extend it to other areas as well.

Disinfection with Ozone
-----------------------

As a small example, we consider the following code, which implements a simple connection of two
continuously stirred tank reactors (CSTR) as commonly used in wastewater treatment. It serves as a quick example
to see how it works. For more detailed explanations, it is referred to the :ref:`usage<usage:Usage>` and the
:ref:`API Reference<autoapi/index:API Reference>`.

This example implements two reactors used for ozonation of water. In the first reactor, the concentration of ozone
is kept constant, to simulate a supply of ozone in the beginning. The stoichiometric matrix for the ozonation process
is defined in the library and accessible under the name ``ozonation``.
For building up this system, it is proceeded as follows:

First, the needed packages are imported and  constants defined:

.. code-block:: python

    # Include Packages
    import peepypoo
    import numpy as np

    # Define the needed constants
    v = 1000.0/2    # Reactor volume
    q = 10000.0/24  # Flow rate through the reactor
    k_o3 = 10.0 / 24  # Reaction rate for the decay of ozone
    k_d = 1500.0 / 24  # Reaction rate for the decay of the bacteria

Then, the inflow and the reactor systems are created:

.. code-block:: python

    # Assemble all used systems
    # The Inflow, which is assumed to be constant. Generates a system with two outputs,
    # first the flow rate q and then the concentrations. The ozone concentration is set to 0.5 to
    # keep this level in the first reactor (Consider the ozone to be supplied before the first reactor)
    # and the bacteria concentration is set to 1, such that one can interpret the bacteria in the effluent
    # as the percentage of bacteria that are remaining.
    #                                                   S_O3| X_B
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])

    # The two CSTRs. Each one is a system with three inputs, first the flow, then the
    # concentrations in the flow and finally the parameters. The systems then have two
    # outputs, which are the output flow rate and the output concentrations in the effluent
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        # The volume of the reactor     The initial condition
        #                                        S_O3|X_B
        v,                              np.array([0.5, 0]),
        # The stoichiometric matrix to use      A name for the system
        stoichiometry_file='ozonation',         name="First CSTR"
    )
    r1 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        # The volume of the reactor     The initial condition
        #                                        S_O3|X_B
        v,                              np.array([0.5, 0]),
        # The stoichiometric matrix to use      A name for the system
        stoichiometry_file='ozonation',         name="Second CSTR"
    )

Further, the stoichiometry for the ozonation has two parameters:
``k_o3`` (decay of ozone) and ``k_d`` (decay of bacteria with ozone). Thus, the reactors need to know them as well
and systems are created to connect them to the reactors. The values of the constants were defined above, but we change
the value of ``k_o3`` to ``0`` in the first reactor to simulate the supply there (initial condition and inflow are equal
and no decay, thus the ozone level is constant in the reactor):

.. code-block:: python

    # The parameters for the first and the second CSTR
    params_first = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params = peepypoo.Systems.Static.Constant([np.array([k_o3, k_d])])

Then, the systems are connected:

.. code-block:: python

    # Connect the systems
    # The flow rate and concentrations
    inf.add_output_connection(r, [0, 1], [0, 1])  # From the inflow to the first reactor
    r.add_output_connection(r1, [0, 1], [0, 1])  # From the first to the second reactor
    # Connect the parameters to the reactors. The parameters are always at input port 2.
    params_first.add_output_connection(r, [2], [0])
    params.add_output_connection(r1, [2], [0])

Then, the system is ready to be evaluated. For this, there are multiple ways, each with its own advantages and
disadvantages. The simplest and easiest way is to do it purely in Python:

.. code-block:: python

    # Get the output after 50 time units
    out = r1.get_output(50)

.. invisible-code-block: python
    # Compare the output to the reference what it should be
    assert out[0] - q <= 1e-6 \
           and np.all(np.abs(out[1] - np.array([0.333, 9.99e-4]))
                      <= [0.5e-3, 1e-6])

This version has the advantage that it is called very fast and easily, but on the other
side it has quite bad performance and yields only the output of one system at a single given time.
Thus to have it at a series of times or for multiple systems, it has to be looped over this method or similar.
Thus there is an alternative using the :class:`SignalLogger<peepypoo.Systems.LoggingAndVisualization.SignalLogger>`,
which solves the issue of logging multiple systems and a full time series:

.. code-block:: python

    # Create the logging system
    logger = peepypoo.Systems.LoggingAndVisualization.SignalLogger(0.1) #  0.1 is the timestep between the logging.
    # Add the signals to log (the outputs of both reactors)
    logger.add_logging_signal(r, [0, 1])
    logger.add_logging_signal(r1, [0, 1])
    # Log the data
    logger.log_data(0, 50) # Log for 50 timesteps
    # Get the logged data
    out = logger.get_logged_data()

.. invisible-code-block: python
    # Compare the output to the reference what it should be
    assert out[str(r1) + ": Output 0"].iloc[-1] - q <= 1e-6 \
           and np.all(np.abs(out[str(r1) + ": Output 1"].iloc[-1]
                      - np.array([0.333, 9.99e-4])) <= [0.5e-3, 1e-6])

However, this still has issues if the systems get more complicated or if they have algebraic loops, as
then, this method of solving is very slow or is infeasible. To overcome as well this issue, it is possible
to convert the whole system tree (if all individual systems support it) to the Julia library
`ModelingToolkit.jl <https://docs.sciml.ai/ModelingToolkit/stable/>`_, which enables faster computations.
On the other hand, this has the disadvantage that it takes some time to convert the systems.

However, before converting the system to Julia, the signal logger is removed from the system using:

.. code-block:: python

    logger.remove_connections()

This is necessary because the signal logger is currently not supported for conversion to julia. Note however, that
this does not pose a restriction of functionality, as when simulating using julia always all signals are reported.

Then, the system can be converted to Julia and evaluated there. To do so, it is necessary to use the
:class:`BDWrapper<peepypoo.BDWrapper>` class as follows:

.. code-block:: python

    # Arg is a list of systems to be contained in the wrapper. Note that for each system always all
    # other systems connected to it are added as well.
    wrapper = peepypoo.BDWrapper([r, r1])
    # Evaluate the blockdiagram in julia. Arguments are starting and end time as well as the times
    # to output the result at.
    out = wrapper.evaluate_blockdiagram_julia(0, 50, np.arange(0, 50.01, 0.1))

.. invisible-code-block: python
    # Compare the output to the reference what it should be
    out = out[r1].iloc[-1]
    assert out[0] - q <= 1e-6 \
           and np.all(np.abs(out[1] - np.array([0.333, 9.99e-4]))
                      <= [0.5e-3, 1e-6])

Finally, one can as well have a look at the underlying equations of this system. To do this, we get a
LaTeX string of them using

.. code-block:: python

    wrapper.get_equations()

Rendering the resulting string then yields

.. math::

    \begin{align}
    \frac{\mathrm{d} \mathrm{First CSTR_{+}S\_O3}\left( t \right)}{\mathrm{d}t} =& 0.83333 \left( 0.5 - \mathrm{First CSTR_{+}S\_O3}\left( t \right) \right) \\
    \frac{\mathrm{d} \mathrm{First CSTR_{+}X\_B}\left( t \right)}{\mathrm{d}t} =& 0.83333 \left( 1 - \mathrm{First CSTR_{+}X\_B}\left( t \right) \right) - 62.5 \mathrm{First CSTR_{+}X\_B}\left( t \right) \mathrm{First CSTR_{+}S\_O3}\left( t \right) \\
    \frac{\mathrm{d} \mathrm{Second CSTR_{+}S\_O3}\left( t \right)}{\mathrm{d}t} =& 0.83333 \left( \mathrm{First CSTR_{+}S\_O3}\left( t \right) - \mathrm{Second CSTR_{+}S\_O3}\left( t \right) \right) - 0.41667 \mathrm{Second CSTR_{+}S\_O3}\left( t \right) \\
    \frac{\mathrm{d} \mathrm{Second CSTR_{+}X\_B}\left( t \right)}{\mathrm{d}t} =& 0.83333 \left( \mathrm{First CSTR_{+}X\_B}\left( t \right) - \mathrm{Second CSTR_{+}X\_B}\left( t \right) \right) - 62.5 \mathrm{Second CSTR_{+}S\_O3}\left( t \right) \mathrm{Second CSTR_{+}X\_B}\left( t \right)
    \end{align}.

Note that this is a simplified version of the whole system and represents the minimal number of equations needed to
describe the system with all connections directly plugged in: The constant inflow and parameters have been directly
input into the equations as well as the connections between the two reactors are directly accounted for, which can be
seen for example in the equations for the second CSTR, where the input concentrations are directly the concentrations in
the first CSTR.

BSM1
----

This example presents a more complex use of the PeePyPoo package for modeling wastewater treatment plants.
For this, it is presented how one could proceed to implement the so-called BSM1 model from IWA-MIA
(`<http://iwa-mia.org/benchmarking/#BSM1>`_).

.. note::

    This example shows the open-loop assessment of the BSM1 described in the corresponding paper,
    with two differences:

    - The rain weather influent is used instead of the dry weather influent:
      The influent thus consists of the 100 day stabilisation period followed by the rain weather influent.
    - The aeration of the reactors is implemented by keeping the dissolved oxygen in them constant at ``2 g(-COD)/m^3``.
      This is done because this package currently does not support ozonation using ``k_l*a`` as used in the BSM1.

The first step is of course to import the package and any other required packages.
Here, we additionally import numpy and pandas, which will be used as well for handling arrays and tables.

.. code-block:: python

    import numpy as np
    import peepypoo
    import pandas as pd

Then, the model parameters have to be defined:

.. code-block:: python

    # Stoichiometric parameters
    Y_A = 0.24
    Y_H = 0.67
    f_P = 0.08
    i_XB = 0.08
    i_XP = 0.06
    # Kinetic parameters
    mu_H = 4.0
    K_S = 10.0
    K_OH = 0.2
    K_NO = 0.5
    b_H = 0.3
    eta_g = 0.8
    eta_h = 0.8
    k_h = 3.0
    K_X = 0.1
    mu_A = 0.5
    K_NH = 1.0
    b_A = 0.05
    K_OA = 0.4
    k_a = 0.05

The volumes of the reactors are given as 1000m³ for the compartments 1 and 2 while the other, aerated,
tanks have a volume of 1333m³:

.. code-block:: python

    V_na = 1000.0
    V_a = 1333.0

Then the dynamic influent is assembled in a table. For this, first the stabilisation influent is defined,
then the rain weather influent obtained from `<http://iwa-mia.org/wp-content/uploads/2019/04/Inf_rain_2006.txt>`_
is loaded and added to the table:

.. code-block:: python

    # The stabilisation influent
    inf_stab_data = pd.DataFrame([[0, 30, 69.50, 51.2, 202.32, 28.17, 0, 0, 0, 0, 31.56, 6.95, 10.59, 7.0, 18446.0],
                                  [100, 30, 69.50, 51.2, 202.32, 28.17, 0, 0, 0, 0, 31.56, 6.95, 10.59, 7.0, 18446.0]],
                                 columns=['t', 'Si', 'Ss', 'Xi', 'Xs', 'Xbh', 'Xba', 'Xp',
                                          'So', 'Sno', 'Snh', 'Snd', 'Xnd', 'Salk', 'Q'])
    # load the Rain influent data from BSM1
    inf_rain_data = pd.read_csv('http://iwa-mia.org/wp-content/uploads/2019/04/Inf_rain_2006.txt',
                                sep='\t', header=0, dtype=float)

    # Assemble the time series to have 100 days of stabilisation before applying the dynamic rain influent
    # Shift the Rain influent to start after 100 days
    inf_rain_data.t += 100
    # Add the stabilisation period to assemble the full influent time series for stabilisation and rain weather
    inf_data = pd.concat([inf_stab_data, inf_rain_data])

The next step is then to build up all single model components which are needed to connect to the
full model. For the reactors, it is necessary to define the stoichiometry. This can be done either
manually in a tabular file (CSV, ODS, XSLX etc.) or one of the predefined stoichiometries can be used.
As here the ASM1 model is used we can take it from the library. As initial concentration in all tanks
the simply ``1.0`` is taken, as it can be arbitrary (for the BSM1, the stabilization period leads to
a uniform initial condition for the rain influent).

For the influent, we use piecewise linear interpolation using the now loaded data.
We first create all systems except the reactors:

.. code-block:: python

    # Inflow, split into flow rate and concentrations
    inflow = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [inf_data.t.to_numpy()],  # Time vector
        [inf_data.Q.to_numpy(), inf_data.drop(['t', 'Q'], axis=1).to_numpy().T], # Input vector as [Q, C]
        name="Inflow")

    # Constant parameters for the recirculations and the clarifier sludge output
    # Clarifier underflow rate (Qu = Qr + Qw), values are from the BSM1 paper for the open-loop assessment
    Qu_rate = peepypoo.Systems.Static.Constant([18446.0 + 385.0], name="Qu_rate")
    # Wastage Flow rate Qw, values are from the BSM1 paper for the open-loop assessment
    Qw_rate = peepypoo.Systems.Static.Constant([385.0], name="Qw_rate")
    # Internal recycle flow rate Qa, values are from the BSM1 paper for the open-loop assessment
    Qa_rate = peepypoo.Systems.Static.Constant([55338.0], name="Qa_rate")

    # Flow elements
    # Flow unifier, unites inflow, internal and external recycle
    unifier = peepypoo.Systems.Static.FlowElements.FlowUnifier(
        num_materials=13,
        num_flows=3,
        name='Unifier'
    )
    # Flow separator for the wastage pump
    wastage_pump = peepypoo.Systems.Static.FlowElements.FlowSeparator(
        num_materials=13,
        num_flows=2,
        use_percentages=False,
        name='Wastage Pump'
    )
    # Flow separator for the internal recycle
    internal_recyc = peepypoo.Systems.Static.FlowElements.FlowSeparator(
        num_materials=13,
        num_flows=2,
        use_percentages=False,
        name='Internal Recycle'
    )

    # Clarifier
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=13,
        #               | S_I | S_S  | X_I | X_S | X_BH| X_BA| X_P | S_O  |S_NO  |S_NH  |S_ND  | X_ND| S_ALK|
        solid_materials=[False, False, True, True, True, True, True, False, False, False, False, True, False],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75*sum(x[2:7])),  # Formula to calculate TSS from the states according to ASM1
        feedlayer=5,  # Stating to count at 0, thus this is the 6th layer
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.000576, rp=0.00286, fns=0.00228, Xt=3000,
        initial_state=np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]*10),
        name='Clarifier'
    )

    # System Parameters
    params = peepypoo.Systems.Static.Constant(
        [np.array([Y_H, Y_A, f_P, i_XB, i_XP, mu_H, K_S, K_OH, K_NO, b_H, mu_A, K_NH, K_OA, b_A, eta_g, k_a, k_h, K_X, eta_h])]
    )

Then, the first two, non-aerated, units are defined:

.. code-block:: python

    # Non-aerated reactors
    unit1 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v=V_na,
        #                      |S_I| S_S| X_I| X_S|X_BH|X_BA| X_P| S_O|S_NO|S_NH|S_ND|X_ND|S_ALK
        initial_state=np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
        stoichiometry_file='ASM1',
        name='Unit 1'
    )
    unit2 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v=V_na,
        #                      |S_I| S_S| X_I| X_S|X_BH|X_BA| X_P| S_O|S_NO|S_NH|S_ND|X_ND|S_ALK
        initial_state=np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
        stoichiometry_file='ASM1',
        name='Unit 2'
    )

And finally the aerated reactors. Currently the only supported type of aeration is that the dissolved oxygen is kept
constant at the initial condition, with is defined in the ``ASM1_constO`` stoichiometry. There, the dissolved oxygen
corresponds to the 8th state (counting from 1), thus the corresponding entry in the initial condition has to be set to
the desired value. Here we chose ``2.0 g(-COD)/m^3``:

.. code-block:: python

    unit3 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v=V_a,
        #                      |S_I| S_S| X_I| X_S|X_BH|X_BA| X_P| S_O|S_NO|S_NH|S_ND|X_ND|S_ALK
        initial_state=np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
        stoichiometry_file='ASM1_constO',   # Keep dissolved oxygen constant
        name='Unit 3'
    )
    unit4 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v=V_a,
        #                      |S_I| S_S| X_I| X_S|X_BH|X_BA| X_P| S_O|S_NO|S_NH|S_ND|X_ND|S_ALK
        initial_state=np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
        stoichiometry_file='ASM1_constO',  # Keep dissolved oxygen constant
        name='Unit 4'
    )
    unit5 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v=V_a,
        #                      |S_I| S_S| X_I| X_S|X_BH|X_BA| X_P| S_O|S_NO|S_NH|S_ND|X_ND|S_ALK
        initial_state=np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
        stoichiometry_file='ASM1_constO',  # Keep dissolved oxygen constant
        name='Unit 5'
    )

Then, all subsystems can be connected to form the overall model:

.. code-block:: python

    # First, connect all parameters
    # Connect the ASM params to all reactors
    unit1.add_input_connection(params, [2], [0])
    unit2.add_input_connection(params, [2], [0])
    unit3.add_input_connection(params, [2], [0])
    unit4.add_input_connection(params, [2], [0])
    unit5.add_input_connection(params, [2], [0])
    # Clarifier underflow flow rate Qu_rate to the clarifier (to know how much sludge is taken out)
    Qu_rate.add_output_connection(clarifier, [2], [0])
    # Wastage flow rate Qw_rate to the wastage pump (to know how much flow to pump out)
    Qw_rate.add_output_connection(wastage_pump, [2], [0])
    # Internal recycle flow rate Qa_rate to the internal recycle (to know how much flow to recycle)
    Qa_rate.add_output_connection(internal_recyc, [2], [0])

    # Connect the forward flows
    inflow.add_output_connection(unifier, [0, 1], [0, 1])  # Inflow to unifier
    unifier.add_output_connection(unit1, [0, 1], [0, 1])  # Unifier to Unit 1
    unit1.add_output_connection(unit2, [0, 1], [0, 1])  # Unit 1 to Unit 2
    unit2.add_output_connection(unit3, [0, 1], [0, 1])  # Unit 2 to Unit 3
    unit3.add_output_connection(unit4, [0, 1], [0, 1])  # Unit 3 to Unit 4
    unit4.add_output_connection(unit5, [0, 1], [0, 1])  # Unit 4 to Unit 5
    unit5.add_output_connection(internal_recyc, [0, 1], [0, 1])  # Unit 5 to internal recycle
    internal_recyc.add_output_connection(clarifier, [0, 1], [0, 1])  # internal recycle to clarifier

    # Connect the recycles
    # internal recycle
    internal_recyc.add_output_connection(unifier, [4, 5], [2, 3])
    # external recycle
    clarifier.add_output_connection(wastage_pump, [0, 1], [2, 3])  # clarifier to wastage pump
    wastage_pump.add_output_connection(unifier, [2, 3], [0, 1])  # external recycle

Then, the full model is assembled and can be transferred to Julia to solve it there.
(As it is a quite complex model, execution in python would take way too long.)

.. code-block:: python

    # Get the wrapper
    wrapper = peepypoo.BDWrapper([inflow]) # Passing one system is enough, the whole connected tree gets added automatically to the wrapper

    # Simulate the whole system in julia for 114 days (100 days stabilisation + 14 days rain weather)
    out = wrapper.evaluate_blockdiagram_julia(0, 114, np.arange(0, 114, 1/24))

Then, the whole output is stored in the variable ``out`` which is a pandas dataframe, with column
indices being the systems and the rows corresponding to the evaluated timesteps.

.. note::

    If you would like to simulate the whole system in Python instead of translating it to Julia, the issue due to cycles
    described in a warning in :ref:`this subsection<usage:In Python>` applies here. In short, the keyword
    ``discretize_dt=STEP`` (with timestep to be filled in ``STEP``) has to be added to at least one
    (but recommended to all) of the systems in the model.
    See :ref:`the section on simulation in Python<usage:In Python>` for details.
