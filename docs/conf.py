# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# Add path to code
import os
import sys
sys.path.insert(0, os.path.abspath('../'))
sys.setrecursionlimit(1500)

# Import typing for more readable typehints (without all paths...)
import typing
import numpy as np
import pandas as pd

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'PeePyPoo'
copyright = '2023, Florian Wenk, Andreas Froemelt'
author = 'Florian Wenk, Andreas Froemelt'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc',
              'autoapi.extension',
              'sphinx.ext.intersphinx',
              'sphinx.ext.mathjax',
              'sphinx.ext.autosectionlabel']

# Auto API options
autoapi_type = 'python'
autodoc_typehints = 'description'
autoapi_dirs = ['../peepypoo']
autoapi_options = ['members', 'undoc-members', 'show-inheritance',
                   'show-module-summary', 'special-members',
                   'imported-members']
autoapi_keep_files = False

# Autosectionlabel options
autosectionlabel_prefix_document = True

# Intersphinx mapping
intersphinx_mapping = {
    "scipy": ("https://docs.scipy.org/doc/scipy/reference/", None),
    "pandas": ("https://pandas.pydata.org/docs/", None)
}

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output
html_theme = 'piccolo_theme'

html_theme_options = {
    "source_url": 'https://gitlab.com/datinfo/PeePyPoo',
    "source_icon": "gitlab"
}
