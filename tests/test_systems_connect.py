import pytest
import peepypoo


@pytest.fixture
def single_const_and_gain():
    """
    Provides a constant (with output [10, 15]) and a gain (multiplier [0.5, 2])
    system for testing
    """
    const = peepypoo.Systems.Static.Constant([10.0, 15])
    gain = peepypoo.Systems.Static.Gain([0.5, 2])
    return {"const": const,
            "gain": gain}


@pytest.fixture
def two_const_and_gain():
    """
    Provides two constants (outputs [10] and [15] respectively) and a gain
    (multiplier [0.5, 2]) systems for testing
    """
    const1 = peepypoo.Systems.Static.Constant([10.0])
    const2 = peepypoo.Systems.Static.Constant([15])
    gain = peepypoo.Systems.Static.Gain([0.5, 2])
    return {"const1": const1,
            "const2": const2,
            "gain": gain}


@pytest.fixture
def output_connect_single(single_const_and_gain):
    """
    Connects the constant and the gain from the single_const_and_gain fixture
    using the add_output_connection function.
    """
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [0, 1], [0, 1]
    )
    return single_const_and_gain


@pytest.fixture
def input_connect_single(single_const_and_gain):
    """
    Connects the constant and the gain from the single_const_and_gain fixture
    using the add_input_connection function.
    """
    single_const_and_gain["gain"].add_input_connection(
        single_const_and_gain["const"], [0, 1], [0, 1]
    )
    return single_const_and_gain


@pytest.fixture
def two_connected(two_const_and_gain):
    """
    Connects the two constant systems to the gain and provides them as a
    fixture
    """
    two_const_and_gain["const1"].add_output_connection(
        two_const_and_gain["gain"], [0], [0]
    )
    two_const_and_gain["const2"].add_output_connection(
        two_const_and_gain["gain"], [1], [0]
    )
    return two_const_and_gain


@pytest.fixture
def connected_expected_output():
    return [5, 30]


@pytest.fixture
def first_disconnected_expected_output():
    return [0, 30]


@pytest.fixture
def second_disconnected_expected_output():
    return [5, 0]


@pytest.fixture
def disconnected_expected_output():
    return [0, 0]


def test_output_connect(single_const_and_gain,
                        connected_expected_output):
    """
    Tests connecting two systems with an output connection
    """
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [0, 1], [0, 1]
    )

    assert single_const_and_gain["gain"].get_output(654) \
           == connected_expected_output


def test_input_connect(single_const_and_gain,
                       connected_expected_output):
    """
    Tests connecting two systems with an input connection
    """
    single_const_and_gain["gain"].add_input_connection(
        single_const_and_gain["const"], [0, 1], [0, 1]
    )

    assert single_const_and_gain["gain"].get_output(87) \
           == connected_expected_output


def test_output_connect_non_int(single_const_and_gain):
    """
    Tests connecting two systems with an output connection providing floating
    point ports.
    """
    with pytest.raises(TypeError):
        single_const_and_gain["const"].add_output_connection(
            single_const_and_gain["gain"], [0, 1.2], [0.2, 1]
        )


def test_input_connect_non_int(single_const_and_gain):
    """
    Tests connecting two systems with an input connection providing floating
    point ports.
    """
    with pytest.raises(TypeError):
        single_const_and_gain["gain"].add_input_connection(
            single_const_and_gain["const"], [0, 1.2], [0.2, 1]
        )


def test_output_connect_duplicate_input(single_const_and_gain):
    """
    Tests connecting two systems with an output connection providing duplicate
    input ports
    """
    with pytest.raises(ValueError):
        single_const_and_gain["const"].add_output_connection(
            single_const_and_gain["gain"], [0, 0], [0, 1]
        )


def test_input_connect_duplicate_input(single_const_and_gain):
    """
    Tests connecting two systems with an input connection providing duplicate
    input ports
    """
    with pytest.raises(ValueError):
        single_const_and_gain["gain"].add_input_connection(
            single_const_and_gain["const"], [0, 0], [0, 1]
        )


def test_output_connect_duplicate_output(single_const_and_gain,
                                         connected_expected_output):
    """
    Tests connecting two systems with an output connection providing duplicate
    output ports
    """
    with pytest.raises(TypeError):
        single_const_and_gain["const"].add_output_connection(
            single_const_and_gain["gain"], [0, 1], [0, 0]
        )


def test_input_connect_duplicate_output(single_const_and_gain,
                                        connected_expected_output):
    """
    Tests connecting two systems with an input connection providing duplicate
    output ports
    """
    with pytest.raises(TypeError):
        single_const_and_gain["gain"].add_input_connection(
            single_const_and_gain["const"], [0, 1], [0, 0]
        )


def test_output_connect_wrong_input_port(single_const_and_gain):
    """
    Tests if connecting two systems with an output connection on wrong port
    fails.
    """
    with pytest.raises(ValueError):
        single_const_and_gain["const"].add_output_connection(
            single_const_and_gain["gain"], [0, 2], [0, 1]
        )


def test_input_connect_wrong_input_port(single_const_and_gain):
    """
    Tests if connecting two systems with an input connection on wrong port
    fails.
    """
    with pytest.raises(ValueError):
        single_const_and_gain["gain"].add_input_connection(
            single_const_and_gain["const"], [0, 2], [0, 1]
        )


def test_output_connect_wrong_output_port(single_const_and_gain):
    """
    Tests if connecting two systems with an output connection on wrong port
    fails.
    """
    with pytest.raises(ValueError):
        single_const_and_gain["const"].add_output_connection(
            single_const_and_gain["gain"], [0, 1], [-1, 1]
        )


def test_input_connect_wrong_output_port(single_const_and_gain):
    """
    Tests if connecting two systems with an input connection on wrong port
    fails.
    """
    with pytest.raises(ValueError):
        single_const_and_gain["gain"].add_input_connection(
            single_const_and_gain["const"], [0, 1], [0, 2]
        )


def test_output_connect_already_connected(input_connect_single):
    """
    Tests if connecting two systems with an output connection on already
    connected port fails.
    """
    with pytest.raises(ValueError):
        input_connect_single["const"].add_output_connection(
            input_connect_single["gain"], [0, 1], [0, 1]
        )


def test_input_connect_already_connected(input_connect_single):
    """
    Tests if connecting two systems with an input connection on already
    connected port fails.
    """
    with pytest.raises(ValueError):
        input_connect_single["gain"].add_input_connection(
            input_connect_single["const"], [0, 1], [0, 1]
        )


def test_output_connect_single_steps(
        single_const_and_gain,
        connected_expected_output):
    """
    Tests connecting two systems one input after other with an output
    connection.
    """
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [0], [0]
    )
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [1], [1]
    )

    assert single_const_and_gain["gain"].get_output(54) \
           == connected_expected_output


def test_input_connect_single_steps(
        single_const_and_gain,
        connected_expected_output):
    """
    Tests connecting two systems one input after other with an input
    connection.
    """
    single_const_and_gain["gain"].add_input_connection(
        single_const_and_gain["const"], [0], [0]
    )
    single_const_and_gain["gain"].add_input_connection(
        single_const_and_gain["const"], [1], [1]
    )

    assert single_const_and_gain["gain"].get_output(54) \
           == connected_expected_output


def test_remove_input_con_through_input(input_connect_single,
                                        disconnected_expected_output):
    """
    Tests removal of a connection connected through input connection using the
    remove_input_connection function.
    """
    input_connect_single["gain"].remove_input_connection([0, 1])

    assert input_connect_single["gain"].get_output(61.24) \
           == disconnected_expected_output


def test_remove_output_con_through_input(input_connect_single,
                                         disconnected_expected_output):
    """
    Tests removal of a connection connected through input connection using the
    remove_output_connection function.
    """
    input_connect_single["const"].remove_output_connection([0, 1])

    assert input_connect_single["gain"].get_output(-4.24) \
           == disconnected_expected_output


def test_remove_input_con_through_output(output_connect_single,
                                         disconnected_expected_output):
    """
    Tests removal of a connection connected through output connection using the
    remove_input_connection function.
    """
    output_connect_single["gain"].remove_input_connection([0, 1])

    assert output_connect_single["gain"].get_output(61.24) \
           == disconnected_expected_output


def test_remove_output_con_through_output(output_connect_single,
                                          disconnected_expected_output):
    """
    Tests removal of a connection connected through output connection using the
    remove_output_connection function.
    """
    output_connect_single["const"].remove_output_connection([0, 1])

    assert output_connect_single["gain"].get_output(-4.24) \
           == disconnected_expected_output


def test_remove_input_con_through_input_single_steps(
        input_connect_single,
        disconnected_expected_output):
    """
    Tests removal of a connection connected through input connection using the
    remove_input_connection function for two inputs individually.
    """
    input_connect_single["gain"].remove_input_connection([0])
    input_connect_single["gain"].remove_input_connection([1])

    assert input_connect_single["gain"].get_output(61.24) \
           == disconnected_expected_output


def test_remove_output_con_through_input_single_steps(
        input_connect_single,
        disconnected_expected_output):
    """
    Tests removal of a connection connected through input connection using the
    remove_output_connection function for two inputs individually.
    """
    input_connect_single["const"].remove_output_connection([0])
    input_connect_single["const"].remove_output_connection([1])

    assert input_connect_single["gain"].get_output(-4.24) \
           == disconnected_expected_output


def test_remove_input_con_through_output_single_steps(
        output_connect_single,
        disconnected_expected_output):
    """
    Tests removal of a connection connected through output connection using the
    remove_input_connection function for two inputs individually.
    """
    output_connect_single["gain"].remove_input_connection([0])
    output_connect_single["gain"].remove_input_connection([1])

    assert output_connect_single["gain"].get_output(61.24) \
           == disconnected_expected_output


def test_remove_output_con_through_output_single_steps(
        output_connect_single,
        disconnected_expected_output):
    """
    Tests removal of a connection connected through output connection using the
    remove_output_connection function for two inputs individually.
    """
    output_connect_single["const"].remove_output_connection([0])
    output_connect_single["const"].remove_output_connection([1])

    assert output_connect_single["gain"].get_output(-4.24) \
           == disconnected_expected_output


def test_remove_first_input_through_output(
        input_connect_single,
        first_disconnected_expected_output):
    """
    Tests removal of first of two connections connected through input
    connection using the remove_output_connection function.
    """
    input_connect_single["const"].remove_output_connection([0])

    assert input_connect_single["gain"].get_output(243.213) \
           == first_disconnected_expected_output


def test_remove_first_input_through_input(
        input_connect_single,
        first_disconnected_expected_output):
    """
    Tests removal of first of two connections connected through input
    connection using the remove_input_connection function.
    """
    input_connect_single["gain"].remove_input_connection([0])

    assert input_connect_single["gain"].get_output(243.213) \
           == first_disconnected_expected_output


def test_remove_second_input_through_output(
        input_connect_single,
        second_disconnected_expected_output):
    """
    Tests removal of second of two connections connected through input
    connection using the remove_output_connection function.
    """
    input_connect_single["const"].remove_output_connection([1])

    assert input_connect_single["gain"].get_output(243.213) \
           == second_disconnected_expected_output


def test_remove_second_input_through_input(
        input_connect_single,
        second_disconnected_expected_output):
    """
    Tests removal of second of two connections connected through input
    connection using the remove_input_connection function.
    """
    input_connect_single["gain"].remove_input_connection([1])

    assert input_connect_single["gain"].get_output(243.213) \
           == second_disconnected_expected_output


def test_remove_connections_output_conn(
        input_connect_single,
        disconnected_expected_output):
    """
    Tests if the removal of all connections of the system removes outgoing
    connections.
    """
    input_connect_single["const"].remove_connections()

    assert input_connect_single["gain"].get_output(65.754) \
           == disconnected_expected_output


def test_remove_connections_input_conn(
        input_connect_single,
        disconnected_expected_output):
    """
    Tests if the removal of all connections of the system removes ingoing
    connections.
    """
    input_connect_single["gain"].remove_connections()

    assert input_connect_single["gain"].get_output(65.754) \
           == disconnected_expected_output


def test_remove_nonexistent_output_conn(input_connect_single):
    """
    Tests if the removal of a nonexistent output connection fails
    """
    with pytest.raises(ValueError):
        input_connect_single["const"].remove_output_connection([2])


def test_remove_nonexistent_input_conn(input_connect_single):
    """
    Tests if the removal of a nonexistent input connection fails
    """
    with pytest.raises(ValueError):
        input_connect_single["gain"].remove_input_connection([2])


def test_remove_not_connected_port_output_conn(single_const_and_gain):
    """
    Tests removal of connection on not connected output port
    """
    single_const_and_gain["const"].remove_output_connection([0])


def test_remove_not_connected_port_input_conn(single_const_and_gain):
    """
    Tests removal of connection on not connected input port
    """
    single_const_and_gain["gain"].remove_input_connection([0])


def test_remove_not_connected_system_output_conn(input_connect_single):
    """
    Tests removal of connection on output port to not connected system
    """
    with pytest.raises(ValueError):
        input_connect_single["const"].remove_output_connection(
            [0],
            [input_connect_single["const"]]
        )


def test_output_remove_empty_port(
        input_connect_single,
        connected_expected_output):
    """
    Tests removal of connection on output port with empty ports list
    """
    input_connect_single["const"].remove_output_connection(
        [],
        [input_connect_single["const"]]
    )

    assert input_connect_single["gain"].get_output(354.35) \
           == connected_expected_output


def test_output_remove_empty_system(
        input_connect_single,
        connected_expected_output):
    """
    Tests removal of connection on output port with empty systems list
    """
    input_connect_single["const"].remove_output_connection(
        [0],
        []
    )

    assert input_connect_single["gain"].get_output(354.35) \
           == connected_expected_output


def test_input_remove_empty_port(
        input_connect_single,
        connected_expected_output):
    """
    Tests removal of connection on input port with empty ports list
    """
    input_connect_single["gain"].remove_input_connection([])

    assert input_connect_single["gain"].get_output(354.35) \
           == connected_expected_output


def test_connection_two_systems(
        two_connected,
        connected_expected_output):
    """
    Tests the output of the two constants connected to the gain.
    """
    assert two_connected["gain"].get_output(54.654) \
           == connected_expected_output


def test_two_systems_remove_first_output(
        two_connected,
        first_disconnected_expected_output):
    """
    Tests the removal of the first of two constants connected to the gain from
    the constant system.
    """
    two_connected["const1"].remove_connections()

    assert two_connected["gain"].get_output(54.654) \
           == first_disconnected_expected_output


def test_two_systems_remove_first_input(
        two_connected,
        first_disconnected_expected_output):
    """
    Tests the removal of the first of two constants connected to the gain from
    the gain system.
    """
    two_connected["gain"].remove_input_connection([0])

    assert two_connected["gain"].get_output(54.654) \
           == first_disconnected_expected_output


def test_two_systems_remove_second_output(
        two_connected,
        second_disconnected_expected_output):
    """
    Tests the removal of the second of two constants connected to the gain from
    the constant system.
    """
    two_connected["const2"].remove_connections()

    assert two_connected["gain"].get_output(54.654) \
           == second_disconnected_expected_output


def test_two_systems_remove_second_input(
        two_connected,
        second_disconnected_expected_output):
    """
    Tests the removal of the second of two constants connected to the gain from
    the gain system.
    """
    two_connected["gain"].remove_input_connection([1])

    assert two_connected["gain"].get_output(54.654) \
           == second_disconnected_expected_output


def test_two_systems_remove_both_input(
        two_connected,
        disconnected_expected_output):
    """
    Tests disconnecting both systems from the input side.
    """
    two_connected["gain"].remove_connections()

    assert two_connected["gain"].get_output(657.657) \
           == disconnected_expected_output


def test_two_systems_remove_both_fist_second_input(
        two_connected,
        disconnected_expected_output):
    """
    Tests disconnecting both systems from the input side. Individually
    disconnecting first the first one, then the second one.
    """
    two_connected["gain"].remove_input_connection([0])
    two_connected["gain"].remove_input_connection([1])

    assert two_connected["gain"].get_output(657.657) \
           == disconnected_expected_output


def test_two_systems_remove_both_second_first_input(
        two_connected,
        disconnected_expected_output):
    """
    Tests disconnecting both systems from the input side. Individually
    disconnecting first the second one, then the first one.
    """
    two_connected["gain"].remove_input_connection([1])
    two_connected["gain"].remove_input_connection([0])

    assert two_connected["gain"].get_output(657.657) \
           == disconnected_expected_output


def test_two_systems_remove_both_fist_second_output(
        two_connected,
        disconnected_expected_output):
    """
    Tests disconnecting both systems from the output side. Individually
    disconnecting first the first one, then the second one.
    """
    two_connected["const1"].remove_output_connection([0])
    two_connected["const2"].remove_output_connection([0])

    assert two_connected["gain"].get_output(657.657) \
           == disconnected_expected_output


def test_two_systems_remove_both_second_first_output(
        two_connected,
        disconnected_expected_output):
    """
    Tests disconnecting both systems from the output side. Individually
    disconnecting first the second one, then the first one.
    """
    two_connected["const2"].remove_output_connection([0])
    two_connected["const1"].remove_output_connection([0])

    assert two_connected["gain"].get_output(657.657) \
           == disconnected_expected_output


def test_connection_type_checking_output(
        single_const_and_gain):
    """
    Checks if connecting systems with incompatible types throws error when
    adding an output connection
    """
    with pytest.raises(TypeError):
        single_const_and_gain["const"].add_output_connection(
            single_const_and_gain["gain"],
            [0, 1],
            [1, 0]
        )


def test_connection_type_checking_input(
        single_const_and_gain):
    """Checks if connecting systems with incompatible types throws error"""
    with pytest.raises(TypeError):
        single_const_and_gain["gain"].add_input_connection(
            single_const_and_gain["const"],
            [0, 1],
            [1, 0]
        )


def test_input_port_removal_addition(single_const_and_gain):
    """
    Tests removal and adding back an input port of a system
    """
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [0, 1], [0, 1]
    )
    # Save port types
    port_type = single_const_and_gain["gain"].input_types[0]
    # Remove input port
    single_const_and_gain["gain"]._remove_input_port([0])
    # Add port and connection back
    single_const_and_gain["gain"]._add_input_port(1, port_type)
    single_const_and_gain["gain"].add_input_connection(
        single_const_and_gain["const"], [1], [0]
    )

    assert single_const_and_gain["gain"].get_output(654) \
           == [7.5, 20]


def test_output_port_removal_addition(single_const_and_gain):
    """
    Tests removal and adding back an output port of a system
    """
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [0, 1], [0, 1]
    )
    # Save port types
    port_type = single_const_and_gain["const"].output_types[0]
    # Remove output port
    single_const_and_gain["const"]._remove_output_port([0])
    # Add port and connection back
    single_const_and_gain["const"]._add_output_port(1, port_type)
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [0], [1]
    )

    assert single_const_and_gain["gain"].get_output(654) \
           == [7.5, 20]


def test_input_port_addition_no_type(single_const_and_gain,
                                     connected_expected_output):
    """
    Tests adding an input port to a system without type
    """
    single_const_and_gain["const"]._add_input_port(1)
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [0, 1], [0, 1]
    )

    assert single_const_and_gain["const"].num_inputs == 1 and \
           single_const_and_gain["gain"].get_output(654) \
           == connected_expected_output


def test_input_port_addition_invalid_type(single_const_and_gain):
    """
    Tests adding an input port to a system with invalid signal type number
    """
    with pytest.raises(ValueError):
        single_const_and_gain["const"]._add_input_port(
            1, [peepypoo.SignalTypes.get_signal_class(None)]*2
        )


def test_input_port_addition_zero_ports(single_const_and_gain,
                                        connected_expected_output):
    """
    Tests adding zero input ports to a system (i.e. no port)
    """
    single_const_and_gain["const"]._add_input_port(0)
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [0, 1], [0, 1]
    )

    assert single_const_and_gain["const"].num_inputs == 0 and \
           single_const_and_gain["gain"].get_output(654) \
           == connected_expected_output


def test_output_port_addition_no_type(single_const_and_gain,
                                      connected_expected_output):
    """
    Tests adding an output port to a system without type
    """
    single_const_and_gain["gain"]._add_output_port(1)
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [0, 1], [0, 1]
    )

    assert single_const_and_gain["gain"].num_outputs == 3 and \
           single_const_and_gain["gain"].get_output(654) \
           == connected_expected_output


def test_output_port_addition_invalid_type(single_const_and_gain):
    """
    Tests adding an output port to a system with invalid signal type number
    """
    with pytest.raises(ValueError):
        single_const_and_gain["gain"]._add_output_port(
            1, [peepypoo.SignalTypes.get_signal_class(None)]*2
        )


def test_output_port_addition_zero_ports(single_const_and_gain,
                                         connected_expected_output):
    """
    Tests adding zero input ports to a system (i.e. no port)
    """
    single_const_and_gain["gain"]._add_output_port(0)
    single_const_and_gain["const"].add_output_connection(
        single_const_and_gain["gain"], [0, 1], [0, 1]
    )

    assert single_const_and_gain["gain"].num_outputs == 2 and \
           single_const_and_gain["gain"].get_output(654) \
           == connected_expected_output


def test_system_deletion(
        input_connect_single,
        disconnected_expected_output):
    """
    Tests the deletion of the system
    """
    input_connect_single["const"].delete()

    assert input_connect_single["gain"].get_output(65.754) \
           == disconnected_expected_output
