import pytest
import peepypoo
import numpy as np


def test_settlertakacs_nothingsettleable1():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([18446.0, np.array([1.0, 2.0, 3.0])])
    sludge_rate = peepypoo.Systems.Static.Constant([1000.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[False, False, True],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=5,
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.000576, rp=0.00286, fns=1, Xt=3000,
        initial_state=np.array([1, 2, 3] * 10),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    steady_state = clarifier.get_output(200)
    assert steady_state[0] - 17446 <= float_tol and steady_state[2] - 1000 <= float_tol \
           and np.all(abs(steady_state[1] - np.array([1, 2, 3])) <= float_tol) \
           and np.all(steady_state[3] - np.array([1, 2, 3]) <= float_tol)


def test_settlertakacs_nothingsettleable2():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([18446.0, np.array([1.0, 2.0, 3.0])])
    sludge_rate = peepypoo.Systems.Static.Constant([1000.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[True, True, True],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=5,
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.000576, rp=0.00286, fns=1, Xt=3000,
        initial_state=np.array([1] * 10),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    steady_state = clarifier.get_output(200)
    assert steady_state[0] - 17446 <= float_tol and steady_state[2] - 1000 <= float_tol \
           and np.all(steady_state[1] - np.array([1, 2, 3]) <= float_tol) \
           and np.all(steady_state[3] - np.array([1, 2, 3]) <= float_tol)


def test_settlertakacs_nothingsettleable3():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([18446.0, np.array([1.0, 2.0, 3.0])])
    sludge_rate = peepypoo.Systems.Static.Constant([1000.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[False, True, False],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=5,
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.000576, rp=0.00286, fns=1, Xt=3000,
        initial_state=np.array([1, 2, 3] * 10),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    steady_state = clarifier.get_output(200)
    assert steady_state[0] - 17446 <= float_tol and steady_state[2] - 1000 <= float_tol \
           and np.all(steady_state[1] - np.array([1, 2, 3]) <= float_tol) \
           and np.all(steady_state[3] - np.array([1, 2, 3]) <= float_tol)


def test_settlertakacs_nothingsettleable4():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([18446.0, np.array([1.0, 2.0, 3.0])])
    sludge_rate = peepypoo.Systems.Static.Constant([1000.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[True, False, False],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=5,
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.000576, rp=0.00286, fns=1, Xt=3000,
        initial_state=np.array([1, 2, 3] * 10),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    steady_state = clarifier.get_output(200)
    assert steady_state[0] - 17446 <= float_tol and steady_state[2] - 1000 <= float_tol \
           and np.all(steady_state[1] - np.array([1, 2, 3]) <= float_tol) \
           and np.all(steady_state[3] - np.array([1, 2, 3]) <= float_tol)


def test_settlertakacs_rhequalsrp():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([18446.0, np.array([1.0, 2.0, 3.0])])
    sludge_rate = peepypoo.Systems.Static.Constant([1000.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[True, False, False],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=5,
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.00286, rp=0.00286, fns=0.00228, Xt=3000,
        initial_state=np.array([1, 2, 3] * 10),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    steady_state = clarifier.get_output(200)
    assert steady_state[0] - 17446 <= float_tol and steady_state[2] - 1000 <= float_tol \
           and np.all(steady_state[1] - np.array([1, 2, 3]) <= float_tol) \
           and np.all(steady_state[3] - np.array([1, 2, 3]) <= float_tol)


def test_settlertakacs_threelayers():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([3000.0, np.array([1.0, 2.0, 4/3])])
    sludge_rate = peepypoo.Systems.Static.Constant([1500.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[False, False, True],
        provided_outflow='sludge',
        num_layers=3,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=1,
        A=1500, z=1, v0_max=1, v0=100000, rh=0.000576, rp=0.00286, fns=0, Xt=3000,
        initial_state=np.array([1, 1, 1] * 3),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    steady_state = clarifier.get_output(200)
    assert steady_state[0] - 1500 <= float_tol and steady_state[2] - 1500 <= float_tol \
           and np.all(steady_state[1][0:2] - np.array([1, 2]) <= float_tol) \
           and steady_state[1][2] - 2/5/0.75 <= float_tol \
           and np.all(steady_state[3][0:2] - np.array([1, 2]) <= float_tol) \
           and steady_state[3][2] - 8/5/0.75 <= float_tol


def test_settlertakacs_nothingsettleable1_jl():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([18446.0, np.array([1.0, 2.0, 3.0])])
    sludge_rate = peepypoo.Systems.Static.Constant([1000.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[False, False, True],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=5,
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.000576, rp=0.00286, fns=1, Xt=3000,
        initial_state=np.array([1, 2, 3] * 10),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    wrapper = peepypoo.BDWrapper([clarifier])
    steady_state = np.array(wrapper.evaluate_blockdiagram_julia(0, 200)[clarifier])[-1]
    assert steady_state[0] - 17446 <= float_tol and steady_state[2] - 1000 <= float_tol \
           and np.all(abs(steady_state[1] - np.array([1, 2, 3])) <= float_tol) \
           and np.all(steady_state[3] - np.array([1, 2, 3]) <= float_tol)


def test_settlertakacs_nothingsettleable2_jl():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([18446.0, np.array([1.0, 2.0, 3.0])])
    sludge_rate = peepypoo.Systems.Static.Constant([1000.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[True, True, True],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=5,
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.000576, rp=0.00286, fns=1, Xt=3000,
        initial_state=np.array([1] * 10),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    wrapper = peepypoo.BDWrapper([clarifier])
    steady_state = np.array(wrapper.evaluate_blockdiagram_julia(0, 200)[clarifier])[-1]
    assert steady_state[0] - 17446 <= float_tol and steady_state[2] - 1000 <= float_tol \
           and np.all(steady_state[1] - np.array([1, 2, 3]) <= float_tol) \
           and np.all(steady_state[3] - np.array([1, 2, 3]) <= float_tol)


def test_settlertakacs_nothingsettleable3_jl():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([18446.0, np.array([1.0, 2.0, 3.0])])
    sludge_rate = peepypoo.Systems.Static.Constant([1000.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[False, True, False],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=5,
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.000576, rp=0.00286, fns=1, Xt=3000,
        initial_state=np.array([1, 2, 3] * 10),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    wrapper = peepypoo.BDWrapper([clarifier])
    steady_state = np.array(wrapper.evaluate_blockdiagram_julia(0, 200)[clarifier])[-1]
    assert steady_state[0] - 17446 <= float_tol and steady_state[2] - 1000 <= float_tol \
           and np.all(steady_state[1] - np.array([1, 2, 3]) <= float_tol) \
           and np.all(steady_state[3] - np.array([1, 2, 3]) <= float_tol)


def test_settlertakacs_nothingsettleable4_jl():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([18446.0, np.array([1.0, 2.0, 3.0])])
    sludge_rate = peepypoo.Systems.Static.Constant([1000.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[True, False, False],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=5,
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.000576, rp=0.00286, fns=1, Xt=3000,
        initial_state=np.array([1, 2, 3] * 10),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    wrapper = peepypoo.BDWrapper([clarifier])
    steady_state = np.array(wrapper.evaluate_blockdiagram_julia(0, 200)[clarifier])[-1]
    assert steady_state[0] - 17446 <= float_tol and steady_state[2] - 1000 <= float_tol \
           and np.all(steady_state[1] - np.array([1, 2, 3]) <= float_tol) \
           and np.all(steady_state[3] - np.array([1, 2, 3]) <= float_tol)


def test_settlertakacs_rhequalsrp_jl():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([18446.0, np.array([1.0, 2.0, 3.0])])
    sludge_rate = peepypoo.Systems.Static.Constant([1000.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[True, False, False],
        provided_outflow='sludge',
        num_layers=10,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=5,
        A=1500, z=0.4, v0_max=250, v0=474, rh=0.00286, rp=0.00286, fns=0.00228, Xt=3000,
        initial_state=np.array([1, 2, 3] * 10),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    wrapper = peepypoo.BDWrapper([clarifier])
    steady_state = np.array(wrapper.evaluate_blockdiagram_julia(0, 200)[clarifier])[-1]
    assert steady_state[0] - 17446 <= float_tol and steady_state[2] - 1000 <= float_tol \
           and np.all(steady_state[1] - np.array([1, 2, 3]) <= float_tol) \
           and np.all(steady_state[3] - np.array([1, 2, 3]) <= float_tol)


def test_settlertakacs_threelayers_jl():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([3000.0, np.array([1.0, 2.0, 4/3])])
    sludge_rate = peepypoo.Systems.Static.Constant([1500.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[False, False, True],
        provided_outflow='sludge',
        num_layers=3,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=1,
        A=1500, z=1, v0_max=1, v0=100000, rh=0.000576, rp=0.00286, fns=0, Xt=3000,
        initial_state=np.array([1, 1, 1] * 3),
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    wrapper = peepypoo.BDWrapper([clarifier])
    steady_state = np.array(wrapper.evaluate_blockdiagram_julia(0, 200)[clarifier])[-1]
    assert steady_state[0] - 1500 <= float_tol and steady_state[2] - 1500 <= float_tol \
           and np.all(steady_state[1][0:2] - np.array([1, 2]) <= float_tol) \
           and steady_state[1][2] - 2/5/0.75 <= float_tol \
           and np.all(steady_state[3][0:2] - np.array([1, 2]) <= float_tol) \
           and steady_state[3][2] - 8/5/0.75 <= float_tol


def test_settlertakacs_threelayers_noic_jl():
    float_tol = 1e-2
    inflow = peepypoo.Systems.Static.Constant([3000.0, np.array([1.0, 2.0, 4/3])])
    sludge_rate = peepypoo.Systems.Static.Constant([1500.0])
    clarifier = peepypoo.Systems.Dynamic.Continuous.Settlers.SettlerTakacs(
        num_materials=3,
        solid_materials=[False, False, True],
        provided_outflow='sludge',
        num_layers=3,
        Xf=(lambda x: 0.75 * x[2]),
        feedlayer=1,
        A=1500, z=1, v0_max=1, v0=100000, rh=0.000576, rp=0.00286, fns=0, Xt=3000,
        name='Clarifier'
    )

    inflow.add_output_connection(clarifier, [0, 1], [0, 1])
    sludge_rate.add_output_connection(clarifier, [2], [0])

    wrapper = peepypoo.BDWrapper([clarifier])
    steady_state = np.array(wrapper.evaluate_blockdiagram_julia(0, 200)[clarifier])[-1]
    assert steady_state[0] - 1500 <= float_tol and steady_state[2] - 1500 <= float_tol \
           and np.all(steady_state[1][0:2] - np.array([1, 2]) <= float_tol) \
           and steady_state[1][2] - 2/5/0.75 <= float_tol \
           and np.all(steady_state[3][0:2] - np.array([1, 2]) <= float_tol) \
           and steady_state[3][2] - 8/5/0.75 <= float_tol

