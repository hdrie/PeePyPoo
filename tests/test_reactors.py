import pathlib

import pytest
import peepypoo
import peepypoo.Systems.Dynamic.Continuous.\
    StoichiometricMatrices.readStoichiometricMatrix
import numpy as np


def test_reading_stoichiometric_matrix_xlsx():
    states, params, rates, rates_form, params_plugged_in, is_const = peepypoo.Systems.\
        Dynamic.Continuous.StoichiometricMatrices.readStoichiometricMatrix.\
        get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'StoichiometricMatrix.xlsx'),
                           str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'Parameters.xlsx'))

    assert len(states) == 5 and len(params[0]) == 3 \
           and states == ['S_O2', 'S_S', 'S_NH4', 'S_HCO3', 'X_H'] \
           and params == [['mu_m', 'K_S', 'b']] \
           and len(params_plugged_in) == 0 \
           and rates([1]*5, [[1]*3]) == [-1.345, -0.845, 0.044, 0.00315, -0.5]


def test_reading_stoichiometric_matrix_ods():
    states, params, rates, rates_form, params_plugged_in, is_const = peepypoo.Systems.\
        Dynamic.Continuous.StoichiometricMatrices.readStoichiometricMatrix.\
        get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'StoichiometricMatrix.ods'),
                           str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'Parameters.ods'))

    assert len(states) == 5 and len(params[0]) == 3 \
           and states == ['S_O2', 'S_S', 'S_NH4', 'S_HCO3', 'X_H'] \
           and params == [['mu_m', 'K_S', 'b']] \
           and len(params_plugged_in) == 0 \
           and rates([1]*5, [[1]*3]) == [-1.345, -0.845, 0.044, 0.00315, -0.5]


def test_reading_stoichiometric_matrix_csv():
    states, params, rates, rates_form, params_plugged_in, is_const = peepypoo.Systems.\
        Dynamic.Continuous.StoichiometricMatrices.readStoichiometricMatrix.\
        get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'StoichiometricMatrix.csv'),
                           str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'Parameters.csv'))

    assert len(states) == 5 and len(params[0]) == 2 \
           and states == ['S_O2', 'S_S', 'S_NH4', 'S_HCO3', 'X_H'] \
           and params == [['mu_m', 'b']] \
           and len(params_plugged_in) == 1 \
           and [*params_plugged_in.keys()] == ['K_S'] \
           and params_plugged_in['K_S'] == 0 \
           and rates([1]*5, [[1]*2]) == [-1.69, -1.69, 0, 0, 0]


def test_reading_stoichiometric_matrix_both_csv():
    states, params, rates, rates_form, params_plugged_in, is_const = peepypoo.Systems.\
        Dynamic.Continuous.StoichiometricMatrices.readStoichiometricMatrix.\
        get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'Stoichiometry_Params_both.csv'))

    assert len(states) == 5 and len(params[0]) == 3 \
           and states == ['S_O2', 'S_S', 'S_NH4', 'S_HCO3', 'X_H'] \
           and params == [['mu_m', 'K_S', 'b']] \
           and len(params_plugged_in) == 0 \
           and rates([1]*5, [[1]*3]) == [-1.345, -0.845, 0.044, 0.00315, -0.5]


def test_reading_stoichiometric_matrix_one_undef_param():
    states, params, rates, rates_form, params_plugged_in, is_const = peepypoo.Systems.\
        Dynamic.Continuous.StoichiometricMatrices.readStoichiometricMatrix.\
        get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'StoichiometricMatrix.ods'),
                           str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'Parameters_single_variable.xlsx'))

    assert len(states) == 5 and len(params[0]) == 1 \
           and states == ['S_O2', 'S_S', 'S_NH4', 'S_HCO3', 'X_H'] \
           and params == [['K_S']] \
           and len(params_plugged_in) == 2 \
           and [*params_plugged_in.keys()] == ['mu_m', 'b'] \
           and params_plugged_in['mu_m'] == 1 \
           and params_plugged_in['b'] == 1 \
           and rates([1]*5, [[1]]) == [-1.345, -0.845, 0.044, 0.00315, -0.5]


def test_reading_stoichiometric_matrix_all_params_def():
    states, params, rates, rates_form, params_plugged_in, is_const = peepypoo.Systems.\
        Dynamic.Continuous.StoichiometricMatrices.readStoichiometricMatrix.\
        get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'StoichiometricMatrix.ods'),
                           str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'Parameters_all_def.xlsx'))

    assert len(states) == 5 and len(params) == 0 \
           and states == ['S_O2', 'S_S', 'S_NH4', 'S_HCO3', 'X_H'] \
           and params == [] \
           and len(params_plugged_in) == 3 \
           and [*params_plugged_in.keys()] == ['mu_m', 'K_S', 'b'] \
           and params_plugged_in['mu_m'] == 1 \
           and params_plugged_in['K_S'] == 1 \
           and params_plugged_in['b'] == 1 \
           and rates([1]*5, []) == [-1.345, -0.845, 0.044, 0.00315, -0.5]


def test_reading_stoichiometric_matrix_single_state():
    states, params, rates, rates_form, params_plugged_in, is_const = peepypoo.Systems.\
        Dynamic.Continuous.StoichiometricMatrices.readStoichiometricMatrix.\
        get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'StoichiometricMatrix_single_state.xlsx'))

    assert len(states) == 1 and len(params[0]) == 3 \
           and states == ['S_O2'] \
           and params == [['mu_m', 'K_S', 'b']] \
           and len(params_plugged_in) == 0 \
           and rates([1], [[1]*3])[0] - (-1.69) < 1e-4


def test_reading_stoichiometric_matrix_isconst():
    states, params, rates, rates_form, params_plugged_in, is_const = peepypoo.Systems.\
        Dynamic.Continuous.StoichiometricMatrices.readStoichiometricMatrix.\
        get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'StoichiometricMatrix_isconst.ods'))

    assert len(states) == 5 and len(params[0]) == 3 \
           and states == ['S_O2', 'S_S', 'S_NH4', 'S_HCO3', 'X_H'] \
           and params == [['mu_m', 'K_S', 'b']] \
           and len(params_plugged_in) == 0 \
           and len(is_const) == 5 \
           and is_const[0] \
           and not is_const[1] and not is_const[2] and not is_const[3] and not is_const[4]


def test_reading_stoichiometric_matrix_invalid1():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix_invalid1.ods'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Stoichiometry_Params_both.csv'))


def test_reading_stoichiometric_matrix_invalid2():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix_invalid2.ods'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Stoichiometry_Params_both.csv'))


def test_reading_stoichiometric_matrix_invalid3():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix_invalid3.ods'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Stoichiometry_Params_both.csv'))


def test_reading_stoichiometric_matrix_invalid4():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix_invalid4.ods'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Stoichiometry_Params_both.csv'))


def test_reading_stoichiometric_matrix_invalid5():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix_invalid5.ods'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Stoichiometry_Params_both.csv'))


def test_reading_stoichiometric_matrix_invalid6():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix_invalid6.xlsx'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Stoichiometry_Params_both.csv'))


def test_reading_stoichiometric_matrix_invalid7():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix_invalid7.xlsx'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Stoichiometry_Params_both.csv'))


def test_reading_stoichiometric_matrix_invalid8():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix_invalid8.xlsx'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Stoichiometry_Params_both.csv'))


def test_reading_stoichiometric_matrix_invalid9():
    with pytest.raises(ValueError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix_invalid9.xlsx'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Stoichiometry_Params_both.csv'))


def test_reading_parameters_invalid1():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix.xlsx'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Parameters_invalid1.ods'))


def test_reading_parameters_invalid2():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix.xlsx'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Parameters_invalid2.ods'))


def test_reading_parameters_invalid3():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix.xlsx'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Parameters_invalid3.ods'))


def test_reading_parameters_invalid4():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix.xlsx'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Parameters_invalid4.ods'))


def test_reading_parameters_invalid5():
    with pytest.raises(SyntaxError):
        peepypoo.Systems.Dynamic.Continuous.\
            StoichiometricMatrices.readStoichiometricMatrix.\
            get_reaction_rates(str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'StoichiometricMatrix.xlsx'),
                               str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'Parameters_invalid5.xlsx'))


def test_cstr_ozonation():
    """
    Tests a single CSTR for disinfection with ozone. Ozone concentration is
    constant.
    """
    float_tol = 0.5e-3
    v = 1000.0
    q = 10000.0/24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First", stoichiometry_file='ozonation'
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params.add_output_connection(r, [2], [0])

    out = r.get_output(24)
    assert out[0] - q <= float_tol \
           and np.all(abs(out[1] - np.array([0.5, 0.013])) <= float_tol)


def test_two_cstr_ozonation():
    """
    Tests a two CSTR chain for disinfection with ozone. Ozone concentration is
    constant in the first CSTR.
    """
    float_tol = [0.5e-3, 1e-6]
    v = 1000.0/2
    q = 10000.0/24
    k_o3 = 10.0 / 24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First", stoichiometry_file='ozonation'
    )
    r1 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="Second", stoichiometry_file='ozonation'
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params_first = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params = peepypoo.Systems.Static.Constant([np.array([k_o3, k_d])])
    params_first.add_output_connection(r, [2], [0])
    params.add_output_connection(r1, [2], [0])
    r.add_output_connection(r1, [0, 1], [0, 1])

    out = r1.get_output(50)
    assert out[0] - q <= min(float_tol) \
           and np.all(np.abs(out[1] - np.array([0.333, 9.99e-4])) <= float_tol)


def test_two_cstr_ozonation_faster():
    """
    Tests a two CSTR chain for disinfection with ozone. Ozone concentration is
    constant in the first CSTR. This version evaluates the first reactor first
    for checking if it is faster with caching.
    """
    float_tol = [0.5e-3, 2e-6]
    v = 1000.0/2
    q = 10000.0/24
    k_o3 = 10.0 / 24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First", stoichiometry_file='ozonation'
    )
    r1 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="Second", stoichiometry_file='ozonation'
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params_first = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params = peepypoo.Systems.Static.Constant([np.array([k_o3, k_d])])
    params_first.add_output_connection(r, [2], [0])
    params.add_output_connection(r1, [2], [0])
    r.add_output_connection(r1, [0, 1], [0, 1])

    # Evaluate the first reactor for caching
    r.get_output(50)

    out = r1.get_output(50)
    assert out[0] - q <= min(float_tol) \
           and np.all(np.abs(out[1] - np.array([0.333, 9.99e-4])) <= float_tol)


def test_two_cstr_ozonation_faster2():
    """
    Tests a two CSTR chain for disinfection with ozone. Ozone concentration is
    constant in the first CSTR. This version evaluates the first reactor first
    for checking if it is faster with caching. Uses the ozonation file with constant O
    """
    float_tol = [0.5e-3, 2e-6]
    v = 1000.0/2
    q = 10000.0/24
    k_o3 = 10.0 / 24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First",
        stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'ozonation_constO.ods')
    )
    r1 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="Second", stoichiometry_file='ozonation'
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([2, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params_first = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params = peepypoo.Systems.Static.Constant([np.array([k_o3, k_d])])
    params_first.add_output_connection(r, [2], [0])
    params.add_output_connection(r1, [2], [0])
    r.add_output_connection(r1, [0, 1], [0, 1])

    # Evaluate the first reactor for caching
    r.get_output(50)

    out = r1.get_output(50)
    assert out[0] - q <= min(float_tol) \
           and np.all(np.abs(out[1] - np.array([0.333, 9.99e-4])) <= float_tol)


def test_two_cstr_ozonation_discrete():
    """
    Tests a two CSTR chain for disinfection with ozone. Ozone concentration is
    constant in the first CSTR. For this test, the input of the two systems is
    considered discrete.
    """
    float_tol = [0.5e-3, 1e-5]
    v = 1000.0/2
    q = 10000.0/24
    k_o3 = 10.0 / 24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First", stoichiometry_file='ozonation',
        discretize_dt=0.25
    )
    r1 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="Second", stoichiometry_file='ozonation',
        discretize_dt=0.25
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params_first = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params = peepypoo.Systems.Static.Constant([np.array([k_o3, k_d])])
    params_first.add_output_connection(r, [2], [0])
    params.add_output_connection(r1, [2], [0])
    r.add_output_connection(r1, [0, 1], [0, 1])

    out = r1.get_output(50)
    assert out[0] - q <= min(float_tol) \
           and np.all(np.abs(out[1] - np.array([0.333, 9.99e-4])) <= float_tol)


def test_4_cstr_ozonation():
    """
    Tests a 4 CSTR chain for disinfection with ozone. Ozone concentration is
    constant in the first CSTR.
    """
    float_tol = [0.5e-3, 2e-6]
    n_react = 4
    v = 1000.0/n_react
    q = 10000.0/24
    k_o3 = 10.0 / 24
    k_d = 1500.0 / 24
    r = []
    for i in range(n_react):
        r.append(peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5, 0]), name="{}".format(i),
            stoichiometry_file='ozonation'
        ))
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r[0], [0, 1], [0, 1])
    params_first = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params_first.add_output_connection(r[0], [2], [0])
    params = peepypoo.Systems.Static.Constant([np.array([k_o3, k_d])])
    for i in range(1, n_react):
        params.add_output_connection(r[i], [2], [0])
        r[i].add_input_connection(r[i-1], [0, 1], [0, 1])

    # Cache the outputs
    for i in range(n_react - 1):
        r[i].get_output(50)

    out = r[-1].get_output(50)
    assert out[0] - q <= min(float_tol) \
           and np.all(np.abs(out[1] - np.array([0.256, 2.30e-5])) <= float_tol)


@pytest.mark.julia
def test_30_cstr_ozonation_julia():
    """
    Tests a 30 CSTR chain for disinfection with ozone. Ozone concentration is
    constant in the first CSTR. Evaluation is done with conversion to Julia for
    speedup.
    """
    float_tol = [0.5e-3, 1e-14]
    n_react = 30
    v = 1000.0/n_react
    q = 10000.0/24
    k_o3 = 10.0 / 24
    k_d = 1500.0 / 24
    r = []
    for i in range(n_react):
        r.append(peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5, 0]), name="{}".format(i),
            stoichiometry_file='ozonation'
        ))
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r[0], [0, 1], [0, 1])
    params_first = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params_first.add_output_connection(r[0], [2], [0])
    params = peepypoo.Systems.Static.Constant([np.array([k_o3, k_d])])
    for i in range(1, n_react):
        params.add_output_connection(r[i], [2], [0])
        r[i].add_input_connection(r[i-1], [0, 1], [0, 1])

    wrapper = peepypoo.BDWrapper(r)

    # Set solver tolerance appropriately (output is very small, so we need to be very accurate)
    out = wrapper.evaluate_blockdiagram_julia(0, 50, rtol=1e-6, atol=1e-14)[r[-1]].iloc[-1]
    assert out[0] - q <= min(float_tol) \
           and np.all(np.abs(out[1] - np.array([0.193, 4.6e-13])) <= float_tol)


@pytest.mark.julia
def test_30_cstr_ozonation_julia2():
    """
    Tests a 30 CSTR chain for disinfection with ozone. Ozone concentration is
    constant in the first CSTR. Evaluation is done with conversion to Julia for
    speedup. Uses the stoichiometry with constant O for the first reactor.
    """
    float_tol = [0.5e-3, 1e-14]
    n_react = 30
    v = 1000.0/n_react
    q = 10000.0/24
    k_o3 = 10.0 / 24
    k_d = 1500.0 / 24
    r = [peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5, 0]), name="{}".format(0),
            stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices' /
                                   'ozonation_constO.ods')
        )]
    for i in range(1, n_react):
        r.append(peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5, 0]), name="{}".format(i),
            stoichiometry_file='ozonation'
        ))
    inf = peepypoo.Systems.Static.Constant([q, np.array([2, 1.0])])
    inf.add_output_connection(r[0], [0, 1], [0, 1])
    params_first = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params_first.add_output_connection(r[0], [2], [0])
    params = peepypoo.Systems.Static.Constant([np.array([k_o3, k_d])])
    for i in range(1, n_react):
        params.add_output_connection(r[i], [2], [0])
        r[i].add_input_connection(r[i-1], [0, 1], [0, 1])

    wrapper = peepypoo.BDWrapper(r)

    # Set solver tolerance appropriately (output is very small, so we need to be very accurate)
    out = wrapper.evaluate_blockdiagram_julia(0, 50, rtol=1e-6, atol=1e-14)[r[-1]].iloc[-1]
    assert out[0] - q <= min(float_tol) \
           and np.all(np.abs(out[1] - np.array([0.193, 4.6e-13])) <= float_tol)


def test_4_cstr_ozonation_discrete():
    """
    Tests a 4 CSTR chain for disinfection with ozone. Ozone concentration is
    constant in the first CSTR. For this test, the input of the systems is
    considered discrete.
    """
    float_tol = [0.5e-3, 1e-6]
    n_react = 4
    v = 1000.0/n_react
    q = 10000.0/24
    k_o3 = 10.0 / 24
    k_d = 1500.0 / 24
    r = []
    for i in range(n_react):
        r.append(peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5, 0]), name="{}".format(i),
            stoichiometry_file='ozonation', discretize_dt=0.25
        ))
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r[0], [0, 1], [0, 1])
    params_first = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params_first.add_output_connection(r[0], [2], [0])
    params = peepypoo.Systems.Static.Constant([np.array([k_o3, k_d])])
    for i in range(1, n_react):
        params.add_output_connection(r[i], [2], [0])
        r[i].add_input_connection(r[i-1], [0, 1], [0, 1])

    # Cache the outputs
    for i in range(n_react - 1):
        r[i].get_output(50)

    out = r[-1].get_output(50)
    assert out[0] - q <= min(float_tol) \
           and np.all(abs(out[1] - np.array([0.256, 2.30e-5])) <= float_tol)


def test_two_cstr_ozonation_mixed_discrete():
    """
    Tests a two CSTR chain for disinfection with ozone. Ozone concentration is
    constant in the first CSTR. For this test, the input of the second system
    is considered discrete while the one of the first one continuous.
    """
    float_tol = [0.5e-3, 1e-5]
    v = 1000.0/2
    q = 10000.0/24
    k_o3 = 10.0 / 24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First", stoichiometry_file='ozonation'
    )
    r1 = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="Second",
        stoichiometry_file='ozonation', discretize_dt=0.25
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params_first = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params = peepypoo.Systems.Static.Constant([np.array([k_o3, k_d])])
    params_first.add_output_connection(r, [2], [0])
    params.add_output_connection(r1, [2], [0])
    r.add_output_connection(r1, [0, 1], [0, 1])

    out = r1.get_output(50)
    assert out[0] - q <= min(float_tol) \
           and np.all(abs(out[1] - np.array([0.333, 9.99e-4])) <= float_tol)


def test_cstr_single_state():
    """
    Tests a two CSTR chain for disinfection with ozone. Ozone concentration is
    constant in the first CSTR.
    """
    float_tol = 1e-4
    v = 1000.0/2
    q = 10000.0/24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([1]), name="First",
        stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'StoichiometricMatrix_single_state.xlsx')
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([1])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params = peepypoo.Systems.Static.Constant([np.array([1.0]*3)])
    params.add_output_connection(r, [2], [0])

    out = r.get_output(40)
    assert out[0] - q <= float_tol \
           and np.all(out[1] - np.array([-0.109]) < float_tol)


def test_cstr_stoichiometry_path():
    """
    Tests a single CSTR providing the file for the stoichiometry matrix.
    """
    float_tol = 0.5e-3
    v = 1000.0
    q = 10000.0/24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First",
        stoichiometry_file=str(pathlib.Path(__file__).parents[1].absolute() /
                               'peepypoo/Systems/Dynamic/Continuous/'
                               'StoichiometricMatrices/ozonation.ods')
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params.add_output_connection(r, [2], [0])

    out = r.get_output(24)
    assert out[0] - q <= float_tol \
           and np.all(abs(out[1] - np.array([0.5, 0.013])) <= float_tol)


def test_cstr_parameters_path():
    """
    Tests a single CSTR for disinfection with ozone. Ozone concentration is
    constant.
    """
    float_tol = 0.5e-3
    v = 1000.0
    q = 10000.0/24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First",
        stoichiometry_file='ozonation',
        parameters_file=str(pathlib.Path(__file__).parents[1].absolute() /
                            'peepypoo/Systems/Dynamic/Continuous/'
                            'StoichiometricMatrices/ozonation.ods')
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params.add_output_connection(r, [2], [0])

    out = r.get_output(24)
    assert out[0] - q <= float_tol \
           and np.all(abs(out[1] - np.array([0.5, 0.013])) <= float_tol)


def test_cstr_invalid_stoichiometry_path():
    """
    Tests a single CSTR with an invalid stoichiometry path
    """
    v = 1000.0
    with pytest.raises(FileNotFoundError):
        peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5, 0]), name="First",
            stoichiometry_file=str(pathlib.Path(__file__).parents[1]
                                   .absolute() / 'peepypoo/Systems/Continuous/'
                                                 'StoichiometricMatrices/'
                                                 'ozonation.ods')
        )


def test_cstr_invalid_parameters_path():
    """
    Tests a single CSTR with an invalid parameters path
    """
    v = 1000.0
    with pytest.raises(FileNotFoundError):
        peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5, 0]), name="First",
            stoichiometry_file='ozonation',
            parameters_file='../peepypoo/Systems/Continuous/'
                               'StoichiometricMatrices/ozonation.ods'
        )


def test_cstr_no_stoichiometry():
    """
    Tests a single CSTR without stoichiometry path
    """
    v = 1000.0
    with pytest.raises(FileNotFoundError):
        peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5, 0]), name="First"
        )


def test_cstr_multiple_stoichiometry_matches():
    """
    Tests a single CSTR with a stoichiometry file which matches multiple
    library ones
    """
    v = 1000.0
    with pytest.raises(FileNotFoundError):
        peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5, 0]), stoichiometry_file='ASM', name="First"
        )


def test_cstr_invalid_discretization():
    """
    Tests a single CSTR with a stoichiometry file which matches multiple
    library ones
    """
    v = 1000.0
    with pytest.raises(ValueError):
        peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5, 0]), stoichiometry_file='ozonation',
            name="First", discretize_dt=-12.5
        )


def test_cstr_reading_asm3():
    """
    Tests a single CSTR with the ASM3 stoichiometry to test with composition.
    """
    v = 1000.0
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0]*13), stoichiometry_file='ASM3', name="First"
    )

    assert r.num_inputs == 3 and r.num_outputs == 2 and r._num_states == 1


def test_cstr_reading_asm3_bio_p():
    """
    Tests a single CSTR with the ASM3_BioP stoichiometry.
    """
    v = 1000.0
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0]*17), stoichiometry_file='ASM3_BioP', name="First"
    )

    assert r.num_inputs == 3 and r.num_outputs == 2 and r._num_states == 1


def test_cstr_wrong_ic_size():
    """
    Tests a single CSTR with an initial condition which does not match the size
    """
    v = 1000.0
    with pytest.raises(ValueError):
        peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
            v, np.array([0.5]), stoichiometry_file='ozonation', name="First"
        )


def test_batch_reactor():
    """
    Tests the batch reactor system
    """
    float_tol = 1e-4
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.BatchReactor(
        initial_state=np.array([350.0, 2.0]),
        stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices/'
                               'Batch_exp_biomass_and_substrate.csv')
    )

    assert np.all(abs(r.get_output(48)[0] - np.array([0, 208.5]))
                  <= float_tol)


@pytest.mark.julia
def test_batch_reactor_julia():
    """
    Tests the batch reactor system in julia
    """
    float_tol = 1e-4
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.BatchReactor(
        initial_state=np.array([350.0, 2.0]),
        stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices/'
                               'Batch_exp_biomass_and_substrate.csv')
    )

    wrapper = peepypoo.BDWrapper([r])

    assert np.all(abs(wrapper.evaluate_blockdiagram_julia(0, 48)[r].iloc[-1][0]
                      - np.array([0, 208.5])) <= float_tol)


def test_batch_reactor_invalid_ic():
    """
    Tests the batch reactor system with an invalid initial condition
    """
    with pytest.raises(ValueError):
        peepypoo.Systems.Dynamic.Continuous.Reactors.BatchReactor(
            initial_state=np.array([350.0, 2.0, 54]),
            stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices/'
                                   'Batch_exp_biomass_and_substrate.csv')
        )


def test_sbr_reactor():
    """
    Tests the SBR system
    """
    float_tol = 1e-4
    const = peepypoo.Systems.Static.Constant([0.0, np.array([0.0, 0.0]), 0.0])
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.SBR(
        initial_state=np.array([1.0, 350.0, 2.0]),
        stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices/'
                               'Batch_exp_biomass_and_substrate.csv')
    )
    const.add_output_connection(r, [0, 1, 2], [0, 1, 2])

    outp = r.get_output(48)
    assert outp[0] - 1 <= float_tol \
           and outp[1] - 0.0 <= float_tol \
           and np.all(abs(outp[2] - np.array([0, 208.5])) <= float_tol)


@pytest.mark.julia
def test_sbr_reactor_julia():
    """
    Tests the SBR system
    """
    float_tol = 1e-4
    const = peepypoo.Systems.Static.Constant([0.0, np.array([0.0, 0.0]), 0.0])
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.SBR(
        initial_state=np.array([1.0, 350.0, 2.0]),
        stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'Batch_exp_biomass_and_substrate.csv')
    )
    const.add_output_connection(r, [0, 1, 2], [0, 1, 2])

    wrapper = peepypoo.BDWrapper([r])

    outp = wrapper.evaluate_blockdiagram_julia(0, 48)[r].iloc[-1]
    assert outp[0] - 1 <= float_tol \
           and outp[1] - 0.0 <= float_tol \
           and np.all(abs(outp[2] - np.array([0, 208.5])) <= float_tol)


def test_sbr_reactor_var_vol():
    """
    Tests the SBR system with variable volume
    """
    float_tol = 1e-4
    # Get systems
    q_in = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([0, 0.2, 0.2, 1])],
        [np.array([20.0, 20.0, 0.0, 0.0])],
        extrapolation="periodic"
    )
    q_out = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([0, 0.6, 0.6, 0.8, 0.8, 1])],
        [np.array([0, 0.0, 20.0, 20.0, 0, 0])],
        extrapolation="periodic"
    )
    c_in = peepypoo.Systems.Static.Constant([np.array([100])])
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.SBR(
        initial_state=np.array([5, 5]),
        stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'SBR_test_simple_degradation.xlsx'),
        integrator_kwargs={'atol': 1e-12, 'rtol': 1e-12}
    )
    # Connect systems
    q_in.add_output_connection(r, [0], [0])
    c_in.add_output_connection(r, [1], [0])
    q_out.add_output_connection(r, [2], [0])

    # Add logger for logging
    logger = peepypoo.Systems.LoggingAndVisualization.SignalLogger(0.1)
    logger.add_logging_signal(r, [0, 1, 2])
    logger.log_data(0, 3)
    outp = logger.get_logged_data(0, 2.95)
    
    # reference solution
    sol = np.array(
        [[5, np.array([5])],
         [6.9999999999999964, np.array([15.59441419])],
         [8.999999999997982, np.array([14.21552603])],
         [8.999999999997982, np.array([3.1719126])],
         [8.999999999997982, np.array([0.70774937])],
         [8.999999999997982, np.array([0.15792023])],
         [8.999999999965421, np.array([0.03523677])],
         [6.999999999965413, np.array([0.00786239])],
         [5.000000000005206, np.array([0.00175434])],
         [5.000000000005206, np.array([0.00039145])],
         [5.000000000005206, np.array([8.73432109e-05])],
         [7.0000000000002025, np.array([14.79753468])],
         [8.999999999988232, np.array([14.07723103])],
         [8.999999999988232, np.array([3.14105481])],
         [8.999999999988232, np.array([0.70086406])],
         [8.999999999988232, np.array([0.15638391])],
         [8.999999999957883, np.array([0.03489397])],
         [6.999999999957871, np.array([0.0077859])],
         [5.000000000020455, np.array([0.00173727])],
         [5.000000000020455, np.array([0.00038764])],
         [5.000000000020801, np.array([8.6493504e-05])],
         [7.000000000020803, np.array([14.79753454])],
         [9.000000000008798, np.array([14.07723101])],
         [9.000000000008798, np.array([3.14105481])],
         [9.000000000008798, np.array([0.70086406])],
         [9.000000000008798, np.array([0.15638391])],
         [8.99999999997845, np.array([0.03489397])],
         [6.9999999999784475, np.array([0.0077859])],
         [5.000000000041027, np.array([0.00173727])],
         [5.000000000041027, np.array([0.00038764])]], dtype=object
    )

    assert np.all(np.abs(outp.to_numpy()[:, [0, 2]] - sol) < float_tol)


@pytest.mark.julia
def test_sbr_reactor_julia_var_vol():
    """
    Tests the SBR system with variable volume in julia
    """
    float_tol = 1e-4
    # Get systems
    q_in = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([0, 0.2, 0.2, 1])],
        [np.array([20.0, 20.0, 0.0, 0.0])],
        extrapolation="periodic"
    )
    q_out = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([0, 0.6, 0.6, 0.8, 0.8, 1])],
        [np.array([0, 0.0, 20.0, 20.0, 0, 0])],
        extrapolation="periodic"
    )
    c_in = peepypoo.Systems.Static.Constant([np.array([100])])
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.SBR(
        initial_state=np.array([5, 5]),
        stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                               'test_stoichiometric_matrices' /
                               'SBR_test_simple_degradation.xlsx')
    )
    # Connect systems
    q_in.add_output_connection(r, [0], [0])
    c_in.add_output_connection(r, [1], [0])
    q_out.add_output_connection(r, [2], [0])

    wrapper = peepypoo.BDWrapper([r])
    outp = wrapper.evaluate_blockdiagram_julia(0, 3, np.arange(0, 3, 0.1),
                                               rtol=1e-7, atol=1e-7)

    # reference solution
    sol = np.array(
        [[5, np.array([5])],
         [6.9999999999999964, np.array([15.59441419])],
         [8.999999999997982, np.array([14.21552603])],
         [8.999999999997982, np.array([3.1719126])],
         [8.999999999997982, np.array([0.70774937])],
         [8.999999999997982, np.array([0.15792023])],
         [8.999999999965421, np.array([0.03523677])],
         [6.999999999965413, np.array([0.00786239])],
         [5.000000000005206, np.array([0.00175434])],
         [5.000000000005206, np.array([0.00039145])],
         [5.000000000005206, np.array([8.73432109e-05])],
         [7.0000000000002025, np.array([14.79753468])],
         [8.999999999988232, np.array([14.07723103])],
         [8.999999999988232, np.array([3.14105481])],
         [8.999999999988232, np.array([0.70086406])],
         [8.999999999988232, np.array([0.15638391])],
         [8.999999999957883, np.array([0.03489397])],
         [6.999999999957871, np.array([0.0077859])],
         [5.000000000020455, np.array([0.00173727])],
         [5.000000000020455, np.array([0.00038764])],
         [5.000000000020801, np.array([8.6493504e-05])],
         [7.000000000020803, np.array([14.79753454])],
         [9.000000000008798, np.array([14.07723101])],
         [9.000000000008798, np.array([3.14105481])],
         [9.000000000008798, np.array([0.70086406])],
         [9.000000000008798, np.array([0.15638391])],
         [8.99999999997845, np.array([0.03489397])],
         [6.9999999999784475, np.array([0.0077859])],
         [5.000000000041027, np.array([0.00173727])],
         [5.000000000041027, np.array([0.00038764])]], dtype=object
    )

    assert np.all(np.abs(np.array(outp[r].to_list())[:, [0, 2]] - sol) < float_tol)


def test_sbr_invalid_ic():
    """
    Tests the SBR system with an invalid initial condition
    """
    with pytest.raises(ValueError):
        peepypoo.Systems.Dynamic.Continuous.Reactors.SBR(
            initial_state=np.array([350.0, 2.0]),
            stoichiometry_file=str(pathlib.Path(__file__).parent.absolute() /
                                   'test_stoichiometric_matrices/'
                                   'Batch_exp_biomass_and_substrate.csv')
        )
