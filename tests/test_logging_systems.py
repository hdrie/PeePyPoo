import numpy as np
import peepypoo
import pytest
import typing


@pytest.fixture
def logger_connected() \
        -> typing.Tuple[
            peepypoo.Systems.Static.Constant,
            peepypoo.Systems.Dynamic.Continuous.Integrator,
            peepypoo.Systems.Dynamic.Continuous.Integrator,
            peepypoo.Systems.LoggingAndVisualization.SignalLogger
        ]:
    """
    Fixture providing a logger which logs the signals of two inputs
    :return:
    """
    const = peepypoo.Systems.Static.Constant([1])
    integr1 = peepypoo.Systems.Dynamic.Continuous.Integrator(0,
                                                             name='FirstInt')
    integr2 = peepypoo.Systems.Dynamic.Continuous.Integrator(0,
                                                             name='SecondInt')
    logger = peepypoo.Systems.LoggingAndVisualization.SignalLogger(0.1)
    const.add_output_connection(integr1, [0], [0])
    integr1.add_output_connection(integr2, [0], [0])
    logger.add_input_connection(integr1, [0], [0])
    logger.add_logging_signal(integr2, [0])

    return const, integr1, integr2, logger


def test_logger_input_connect():
    """
    Tests the logging system with the input connection functions
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([1])
    integr1 = peepypoo.Systems.Dynamic.Continuous.Integrator(0,
                                                             name='FirstInt')
    integr2 = peepypoo.Systems.Dynamic.Continuous.Integrator(0,
                                                             name='SecondInt')
    logger = peepypoo.Systems.LoggingAndVisualization.SignalLogger(0.1)
    const.add_output_connection(integr1, [0], [0])
    integr1.add_output_connection(integr2, [0], [0])
    logger.add_input_connection(integr1, [-1234], [0])
    logger.add_logging_signal(integr2, [0])
    logger.log_data(10, 11)
    data = logger.get_logged_data()

    assert np.all(data.index.to_numpy() - np.linspace(10, 11, 11)
                  <= float_tol) \
           and np.all(data.to_numpy()
                      - np.array([np.linspace(10, 11, 11),
                                  np.square(np.linspace(10, 11, 11))/2]).T
                      <= float_tol) \
           and data.columns.to_numpy()[0] == (str(integr1) + ": Output 0") \
           and data.columns.to_numpy()[1] == (str(integr2) + ": Output 0")


def test_logger_output_connect():
    """
    Tests the logging system with the output connection functions
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([1])
    integr1 = peepypoo.Systems.Dynamic.Continuous.Integrator(0,
                                                             name='FirstInt')
    integr2 = peepypoo.Systems.Dynamic.Continuous.Integrator(0,
                                                             name='SecondInt')
    logger = peepypoo.Systems.LoggingAndVisualization.SignalLogger(0.1)
    const.add_output_connection(integr1, [0], [0])
    integr1.add_output_connection(integr2, [0], [0])
    integr1.add_output_connection(logger, [0], [0])
    integr2.add_output_connection(logger, [1], [0])
    logger.log_data(10, 11)
    data = logger.get_logged_data()

    assert np.all(data.index.to_numpy() - np.linspace(10, 11, 11)
                  <= float_tol) \
           and np.all(data.to_numpy()
                      - np.array([np.linspace(10, 11, 11),
                                  np.square(np.linspace(10, 11, 11))/2]).T
                      <= float_tol) \
           and data.columns.to_numpy()[0] == (str(integr1) + ": Output 0") \
           and data.columns.to_numpy()[1] == (str(integr2) + ": Output 0")


def test_logger_output(logger_connected):
    """
    Tests the output of the logger system.
    :param logger_connected:
    :return:
    """
    assert logger_connected[3].get_output(12) == []


def test_logger_no_data(logger_connected):
    """
    Tests the logging system output without logging data
    """
    integr1 = logger_connected[1]
    integr2 = logger_connected[2]
    logger = logger_connected[3]
    data = logger.get_logged_data()

    assert len(data.index.to_numpy()) == 0 \
           and len(data.to_numpy()) == 0 \
           and data.columns.to_numpy()[0] == (str(integr1) + ": Output 0") \
           and data.columns.to_numpy()[1] == (str(integr2) + ": Output 0")


def test_logger_multiple_queries(logger_connected):
    """
    Tests the logging system with multiple logging queries
    """
    float_tol = 1e-6
    integr1 = logger_connected[1]
    integr2 = logger_connected[2]
    logger = logger_connected[3]

    logger.log_data(10, 11)
    logger.log_data(12, 15)
    data = logger.get_logged_data()

    t = np.concatenate([np.linspace(10, 11, 11), np.linspace(12, 15, 31)])

    assert np.all(data.index.to_numpy() - t <= float_tol) \
           and np.all(data.to_numpy() - np.array([t, np.square(t)/2]).T
                      <= float_tol) \
           and data.columns.to_numpy()[0] == (str(integr1) + ": Output 0") \
           and data.columns.to_numpy()[1] == (str(integr2) + ": Output 0")


def test_logger_overlapping_queries(logger_connected):
    """
    Tests the logging system with two overlapping logging queries
    """
    float_tol = 1e-6
    integr1 = logger_connected[1]
    integr2 = logger_connected[2]
    logger = logger_connected[3]

    logger.log_data(10, 11)
    logger.log_data(8, 15)
    data = logger.get_logged_data()

    t = np.linspace(8, 15, 71)

    assert np.all(data.index.to_numpy() - t <= float_tol) \
           and np.all(data.to_numpy() - np.array([t, np.square(t)/2]).T
                      <= float_tol) \
           and data.columns.to_numpy()[0] == (str(integr1) + ": Output 0") \
           and data.columns.to_numpy()[1] == (str(integr2) + ": Output 0")


def test_logger_adjacent_queries(logger_connected):
    """
    Tests the logging system with two adjacent logging queries
    """
    float_tol = 1e-6
    integr1 = logger_connected[1]
    integr2 = logger_connected[2]
    logger = logger_connected[3]

    logger.log_data(10, 11)
    logger.log_data(11, 15)
    data = logger.get_logged_data()

    t = np.linspace(10, 15, 51)

    assert np.all(data.index.to_numpy() - t <= float_tol) \
           and np.all(data.to_numpy() - np.array([t, np.square(t)/2]).T
                      <= float_tol) \
           and data.columns.to_numpy()[0] == (str(integr1) + ": Output 0") \
           and data.columns.to_numpy()[1] == (str(integr2) + ": Output 0")


def test_logger_continuing_query(logger_connected):
    """
    Tests the logging system for continuing a query
    """
    float_tol = 1e-6
    integr1 = logger_connected[1]
    integr2 = logger_connected[2]
    logger = logger_connected[3]

    logger.log_data(10, 11)
    logger.log_data(t_final=15)
    data = logger.get_logged_data()

    t = np.linspace(10, 15, 51)

    assert np.all(data.index.to_numpy() - t <= float_tol) \
           and np.all(data.to_numpy() - np.array([t, np.square(t)/2]).T
                      <= float_tol) \
           and data.columns.to_numpy()[0] == (str(integr1) + ": Output 0") \
           and data.columns.to_numpy()[1] == (str(integr2) + ": Output 0")


def test_logger_preceding_query(logger_connected):
    """
    Tests the logging system for a preceding a query
    """
    float_tol = 1e-6
    integr1 = logger_connected[1]
    integr2 = logger_connected[2]
    logger = logger_connected[3]

    logger.log_data(10, 11)
    logger.log_data(t_init=8)
    data = logger.get_logged_data()

    t = np.linspace(8, 11, 31)

    assert np.all(data.index.to_numpy() - t <= float_tol) \
           and np.all(data.to_numpy() - np.array([t, np.square(t)/2]).T
                      <= float_tol) \
           and data.columns.to_numpy()[0] == (str(integr1) + ": Output 0") \
           and data.columns.to_numpy()[1] == (str(integr2) + ": Output 0")


def test_logger_continuing_query_none_yet(logger_connected):
    """
    Tests the logging system for continuing a query without recording yet
    """
    logger = logger_connected[3]

    with pytest.raises(ValueError):
        logger.log_data(t_final=15)


def test_logger_preceding_query_none_yet(logger_connected):
    """
    Tests the logging system for preceding a query without recording yet
    """
    logger = logger_connected[3]

    with pytest.raises(ValueError):
        logger.log_data(t_init=15)


def test_logger_no_args(logger_connected):
    """
    Tests the logging system for logging with no args without prior recording
    """
    logger = logger_connected[3]

    with pytest.raises(ValueError):
        logger.log_data()


def test_logger_no_args_with_recording(logger_connected):
    """
    Tests the logging system for logging with no args with prior recording
    """
    float_tol = 1e-6
    integr1 = logger_connected[1]
    integr2 = logger_connected[2]
    logger = logger_connected[3]

    logger.log_data(8, 11)
    logger.log_data()
    data = logger.get_logged_data()

    t = np.linspace(8, 11, 31)

    assert np.all(data.index.to_numpy() - t <= float_tol) \
           and np.all(data.to_numpy() - np.array([t, np.square(t)/2]).T
                      <= float_tol) \
           and data.columns.to_numpy()[0] == (str(integr1) + ": Output 0") \
           and data.columns.to_numpy()[1] == (str(integr2) + ": Output 0")


def test_logger_start_inf_query(logger_connected):
    """
    Tests the logging system with a query range starting at -inf
    """
    logger = logger_connected[3]

    with pytest.raises(ValueError):
        logger.log_data(-np.inf, 15)


def test_logger_stop_inf_query(logger_connected):
    """
    Tests the logging system with a query range stopping at inf
    """
    logger = logger_connected[3]

    with pytest.raises(ValueError):
        logger.log_data(0, np.inf)


def test_logger_both_inf_query(logger_connected):
    """
    Tests the logging system with a query range from (-inf, inf)
    """
    logger = logger_connected[3]

    with pytest.raises(ValueError):
        logger.log_data(-np.inf, np.inf)


def test_logger_start_nan_query(logger_connected):
    """
    Tests the logging system with a query range starting at nan
    """
    logger = logger_connected[3]

    with pytest.raises(ValueError):
        logger.log_data(np.nan, 15)


def test_logger_stop_nan_query(logger_connected):
    """
    Tests the logging system with a query range stopping at nan
    """
    logger = logger_connected[3]

    with pytest.raises(ValueError):
        logger.log_data(0, np.nan)


def test_logger_both_nan_query(logger_connected):
    """
    Tests the logging system with a query range from (nan, nan)
    """
    logger = logger_connected[3]

    with pytest.raises(ValueError):
        logger.log_data(-np.nan, np.nan)
