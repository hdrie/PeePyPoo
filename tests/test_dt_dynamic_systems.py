import pytest
import peepypoo
import numpy as np


def test_dt_integrator_const():
    """
    Tests the discrete time integrator system with a constant input.
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([1.0])
    integr = peepypoo.Systems.Dynamic.Discrete.Integrator(0.0, dt=0.1,
                                                          tol=float_tol)
    const.add_output_connection(integr, [0], [0])

    assert integr.get_output(20)[0] - 20 <= float_tol


def test_dt_integrator_np():
    """
    Tests the discrete time integrator system with a numpy array.
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([np.array([1.0, 2.0])])
    integr = peepypoo.Systems.Dynamic.Discrete.Integrator(
        np.array([2.0, 0.25]), dt=0.1, tol=float_tol)
    const.add_output_connection(integr, [0], [0])

    assert np.all(integr.get_output(20)[0]
                  - np.array([22.0, 40.25]) <= float_tol)


def test_dt_double_integrator_const():
    """
    Tests the discrete time integrator system twice in a row with a constant
    input.
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([1.0])
    integr1 = peepypoo.Systems.Dynamic.Discrete.Integrator(0.0, dt=0.1,
                                                           tol=float_tol)
    integr2 = peepypoo.Systems.Dynamic.Discrete.Integrator(0.0, dt=0.1,
                                                           tol=float_tol)
    const.add_output_connection(integr1, [0], [0])
    integr1.add_output_connection(integr2, [0], [0])

    assert integr2.get_output(20)[0] - 400 <= float_tol


def test_dt_integrator_time_forward():
    """
    Tests the discrete time integrator system with increasing times.
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([np.array([1.0, 2.0])])
    integr = peepypoo.Systems.Dynamic.Discrete.Integrator(
        np.array([2.0, 0.25]), dt=0.1, tol=float_tol)
    const.add_output_connection(integr, [0], [0])

    for t in range(0, 200):
        assert np.all(integr.get_output(t/10)[0]
                      - np.array([2.0+t/10, 0.25+2*t/10]) <= float_tol)


def test_dt_integrator_time_backwards():
    """
    Tests the discrete time integrator system with decreasing times.
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([np.array([1.0, 2.0])])
    integr = peepypoo.Systems.Dynamic.Discrete.Integrator(
        np.array([2.0, 0.25]), dt=0.1, tol=float_tol)
    const.add_output_connection(integr, [0], [0])

    for t in range(200, 0, -1):
        assert np.all(integr.get_output(t/10)[0]
                      - np.array([2.0+t/10, 0.25+2*t/10]) <= float_tol)


def test_dt_integrator_reset_on_input_change():
    """
    Tests the discrete time integrator system if it resets on input change.
    """
    float_tol = 1e-6
    const1 = peepypoo.Systems.Static.Constant([1.0])
    const2 = peepypoo.Systems.Static.Constant([2.0])
    integr = peepypoo.Systems.Dynamic.Discrete.Integrator(0.0, dt=0.1,
                                                          tol=float_tol)
    const1.add_output_connection(integr, [0], [0])
    integr.get_output(20)
    const1.remove_connections()
    const2.add_output_connection(integr, [0], [0])

    assert integr.get_output(20)[0] - 40 <= float_tol


def test_dt_integrator_reset__change_in_input_tree():
    """
    Tests the discrete time integrator system if it resets on change in the
    input tree.
    """
    float_tol = 1e-6
    const1 = peepypoo.Systems.Static.Constant([1.0])
    const2 = peepypoo.Systems.Static.Constant([2.0])
    gain = peepypoo.Systems.Static.Gain([2.0])
    integr = peepypoo.Systems.Dynamic.Discrete.Integrator(0.0, dt=0.1,
                                                          tol=float_tol)
    gain.add_output_connection(integr, [0], [0])
    const1.add_output_connection(gain, [0], [0])
    integr.get_output(20)
    const1.remove_connections()
    const2.add_output_connection(gain, [0], [0])

    assert integr.get_output(20)[0] - 80 <= float_tol


def test_dt_integrator_negative_output_time():
    """
    Tests the discrete time integrator system if the requested time is before
    the initial time.
    """
    float_tol = 1e-6
    const1 = peepypoo.Systems.Static.Constant([1.0])
    integr = peepypoo.Systems.Dynamic.Discrete.Integrator(0.0, dt=0.1,
                                                          tol=float_tol)
    const1.add_output_connection(integr, [0], [0])

    assert integr.get_output(-20)[0] - 0.0 <= float_tol


def test_dt_integrator_none_ic():
    """
    Tests the discrete time integrator system if the initial state is none
    """
    float_tol = 1e-6
    const1 = peepypoo.Systems.Static.Constant([None])
    integr = peepypoo.Systems.Dynamic.Discrete.Integrator(None, dt=0.1,
                                                          tol=float_tol)
    const1.add_output_connection(integr, [0], [0])

    with pytest.raises(TypeError):
        integr.get_output(20)
