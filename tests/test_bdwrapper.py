import pandas as pd
import pytest
import sympy

import peepypoo
import numpy as np


@pytest.fixture
def system_collection():
    """
    This fixture provides a collection of systems for testing the BDWrapper.
    """
    # Different constants
    c1 = peepypoo.Systems.Static.Constant([1.0])
    c2 = peepypoo.Systems.Static.Constant([2.0])
    c3 = peepypoo.Systems.Static.Constant([1.0, 2.0])

    # Different gains
    g1 = peepypoo.Systems.Static.Gain([2.0])
    g2 = peepypoo.Systems.Static.Gain([2.0, 4.0])
    g3 = peepypoo.Systems.Static.Gain([3.0, 3.0])

    # Different Integrators
    intc1 = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    intc2 = peepypoo.Systems.Dynamic.Continuous.Integrator(
        np.array([1.0, 2.0]))
    intd1 = peepypoo.Systems.Dynamic.Discrete.Integrator(0.0)
    intd2 = peepypoo.Systems.Dynamic.Discrete.Integrator(np.array([1.0, 2.0]))

    # A signal logger
    logger = peepypoo.Systems.LoggingAndVisualization.SignalLogger(0.1)

    return c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger


def test_bdwrapper_initialization_single_tree(system_collection):
    """
    Test the initialization of a BDWrapper with a single tree
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(g2, [0], [0])
    c2.add_output_connection(g2, [1], [0])

    g2.add_output_connection(g3, [0], [0])
    g3.add_output_connection(intc1, [0], [0])
    intc1.add_output_connection(g3, [1], [0])

    g3.add_output_connection(intd1, [0], [1])

    bdwrapper = peepypoo.BDWrapper([g2])

    assert len(bdwrapper._all_systems) == 7 \
           and all([a in bdwrapper._all_systems
                    for a in [c1, c2, g1, g2, g3, intc1, intd1]]) \
           and len(bdwrapper._heads) == 1 \
           and all([a in bdwrapper._heads for a in [intd1]]) \
           and len(bdwrapper._tails) == 2 \
           and all([a in bdwrapper._tails for a in [c1, c2]])


def test_bdwrapper_initialization_three_trees(system_collection):
    """
    Test the initialization of a BDWrapper with three trees
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    c3.add_output_connection(g1, [0], [0])
    g1.add_output_connection(g2, [0], [0])
    c3.add_output_connection(g2, [1], [1])

    c1.add_output_connection(g3, [0], [0])
    g3.add_output_connection(intc1, [0], [0])
    intc1.add_output_connection(g3, [1], [0])
    g3.add_output_connection(intd1, [0], [1])

    bdwrapper = peepypoo.BDWrapper([g2, intc1, intd2])

    assert len(bdwrapper._all_systems) == 8 \
           and all([a in bdwrapper._all_systems
                    for a in [c1, c3, g1, g2, g3, intc1, intd1, intd2]]) \
           and len(bdwrapper._heads) == 3 \
           and all([a in bdwrapper._heads for a in [intd1, g2, intd2]]) \
           and len(bdwrapper._tails) == 3 \
           and all([a in bdwrapper._tails for a in [c1, c3, intd2]])


def test_bdwrapper_growing_tree(system_collection):
    """
    Test the BDWrapper with a growing tree (new systems being added)
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([g2])

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(g2, [0], [0])
    c2.add_output_connection(g2, [1], [0])

    g2.add_output_connection(g3, [0], [0])
    g3.add_output_connection(intc1, [0], [0])
    intc1.add_output_connection(g3, [1], [0])

    g3.add_output_connection(intd1, [0], [1])

    assert len(bdwrapper._all_systems) == 7 \
           and all([a in bdwrapper._all_systems
                    for a in [c1, c2, g1, g2, g3, intc1, intd1]]) \
           and len(bdwrapper._heads) == 1 \
           and all([a in bdwrapper._heads for a in [intd1]]) \
           and len(bdwrapper._tails) == 2 \
           and all([a in bdwrapper._tails for a in [c1, c2]])


def test_bdwrapper_connection_removal(system_collection):
    """
    Test the BDWrapper while removing a connection
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([g2])

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(g2, [0], [0])
    c2.add_output_connection(g2, [1], [0])

    g2.add_output_connection(g3, [0], [0])
    g3.add_output_connection(intc1, [0], [0])
    intc1.add_output_connection(g3, [1], [0])

    g3.add_output_connection(intd1, [0], [1])
    intc1.remove_connections()

    assert len(bdwrapper._all_systems) == 7 \
           and all([a in bdwrapper._all_systems
                    for a in [c1, c2, g1, g2, g3, intc1, intd1]]) \
           and len(bdwrapper._heads) == 2 \
           and all([a in bdwrapper._heads for a in [intd1, intc1]]) \
           and len(bdwrapper._tails) == 3 \
           and all([a in bdwrapper._tails for a in [c1, c2, intc1]])


def test_bdwrapper_tree_addition_and_removal(system_collection):
    """
    Test the addition and removal of trees to/from a BDWrapper
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    c3.add_output_connection(g1, [0], [0])
    g1.add_output_connection(g2, [0], [0])
    c3.add_output_connection(g2, [1], [1])

    c1.add_output_connection(g3, [0], [0])
    g3.add_output_connection(intc1, [0], [0])
    intc1.add_output_connection(g3, [1], [0])
    g3.add_output_connection(intd1, [0], [1])

    bdwrapper = peepypoo.BDWrapper([intc1, intd2])
    bdwrapper.add_systems_tree([g1])
    bdwrapper.remove_systems_tree([intc1])

    assert len(bdwrapper._all_systems) == 4 \
           and all([a in bdwrapper._all_systems
                    for a in [c3, g1, g2, intd2]]) \
           and len(bdwrapper._heads) == 2 \
           and all([a in bdwrapper._heads for a in [g2, intd2]]) \
           and len(bdwrapper._tails) == 2 \
           and all([a in bdwrapper._tails for a in [c3, intd2]])


def test_bdwrapper_system_deletion(system_collection):
    """
    Test the BDWrapper with a system being deleted
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([g2])

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(g2, [0], [0])
    c2.add_output_connection(g2, [1], [0])

    g2.add_output_connection(g3, [0], [0])
    g3.add_output_connection(intc1, [0], [0])
    intc1.add_output_connection(g3, [1], [0])

    g3.add_output_connection(intd1, [0], [1])

    g3.delete()
    del g3

    assert len(bdwrapper._all_systems) == 6 \
           and all([a in bdwrapper._all_systems
                    for a in [c1, c2, g1, g2, intc1, intd1]]) \
           and len(bdwrapper._heads) == 3 \
           and all([a in bdwrapper._heads for a in [g2, intc1, intd1]]) \
           and len(bdwrapper._tails) == 4 \
           and all([a in bdwrapper._tails for a in [c1, c2, intc1, intd1]])


def test_bdwrapper_cycle_detection(system_collection):
    """
    Test the BDWrapper cycle detection with cycle but not algebraic loop
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([g2])

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(g2, [0], [0])
    c2.add_output_connection(g2, [1], [0])

    g2.add_output_connection(g3, [0], [0])
    g3.add_output_connection(intc1, [0], [0])
    intc1.add_output_connection(g3, [1], [0])

    g3.add_output_connection(intd1, [0], [1])

    assert bdwrapper.has_cycles() and not bdwrapper.has_algebraic_loop()


def test_bdwrapper_algebraic_loop_detection(system_collection):
    """
    Test the BDWrapper cycle detection with algebraic loop
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([g2])

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(g2, [0], [0])
    c2.add_output_connection(g2, [1], [0])

    g2.add_output_connection(g3, [0], [0])
    g3.add_output_connection(g3, [1], [0])

    g3.add_output_connection(intd1, [0], [1])

    assert bdwrapper.has_cycles() and bdwrapper.has_algebraic_loop()


def test_bdwrapper_cycle_detection_no_cycle(system_collection):
    """
    Test the BDWrapper cycle detection without cycle
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([g2])

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(g2, [0], [0])
    c2.add_output_connection(g2, [1], [0])

    g2.add_output_connection(g3, [0], [0])

    g3.add_output_connection(intd1, [0], [1])

    assert not bdwrapper.has_cycles() and not bdwrapper.has_algebraic_loop()


@pytest.mark.julia
def test_bdwrapper_julia_conversion(system_collection):
    """
    Test the BDWrapper julia conversion
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([c1])

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(intc1, [0], [0])

    simulation_result = bdwrapper.evaluate_blockdiagram_julia(0, 10)
    assert abs(simulation_result[intc1].iloc[-1] - 20) < 1e-6 and \
           all(simulation_result[c1].to_numpy() == 1)


@pytest.mark.julia
def test_bdwrapper_julia_conversion_fail_not_supported(system_collection):
    """
    Test the BDWrapper julia conversion failing due to not supported system.
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([intc1])
    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(intc1, [0], [0])
    intc1.add_output_connection(logger, [0], [0])

    with pytest.raises(ValueError):
        bdwrapper.evaluate_blockdiagram_julia(0, 10)


@pytest.mark.julia
def test_bdwrapper_julia_conversion_fail_not_connected(system_collection):
    """
    Test the BDWrapper julia conversion failing due to not connected.
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([intc2])

    with pytest.raises(ValueError):
        bdwrapper.evaluate_blockdiagram_julia(0, 10)


@pytest.mark.julia
def test_bdwrapper_julia_conversion_fail_non_complete(system_collection):
    """
    Test the BDWrapper julia conversion failing due to not full tree in wrapper
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([c1])
    c1.add_output_connection(intc1, [0], [0])

    # Private function call as otherwise not possible to have incomplete system
    # tree
    bdwrapper._remove_systems([c1])

    with pytest.raises(ValueError):
        bdwrapper.evaluate_blockdiagram_julia(0, 10)


@pytest.mark.julia
def test_bdwrapper_julia_conversion_fail_diff_indep_vars(system_collection):
    """
    Test the BDWrapper julia conversion failing due to different independent
    variables
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    # Change independent variable of the integrator and reassign all other
    # variables to have proper change in all parts
    intc1.t = sympy.Symbol('tau')
    intc1.outputs = intc1.outputs
    intc1.inputs = intc1.inputs
    intc1.states = intc1.states
    intc1.params = intc1.params
    intc1.output_formula = intc1.output_formula
    intc1.dynamics_formula = intc1.dynamics_formula

    bdwrapper = peepypoo.BDWrapper([c1])
    c1.add_output_connection(intc1, [0], [0])

    with pytest.raises(ValueError):
        bdwrapper.evaluate_blockdiagram_julia(0, 10)


@pytest.mark.julia
def test_bdwrapper_julia_output_conversion(system_collection):
    """
    Test the BDWrapper conversion of julia output
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([c1])

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(intc1, [0], [0])

    simulation_result = bdwrapper.evaluate_blockdiagram_julia(0, 10)
    simulation_result_transf = bdwrapper.convert_df_systems_to_df_signals(simulation_result)
    assert (set(simulation_result_transf.columns) == {'Gain: Output 0', 'Constant: Output 0',
                                                      'Continuous integrator: Output 0'}  # All column names match
            and all([isinstance(x, float) for x in simulation_result_transf['Constant: Output 0'].tolist()])
            and all([isinstance(x, float) for x in simulation_result_transf['Gain: Output 0'].tolist()])
            and all([isinstance(x, np.ndarray)
                     for x in simulation_result_transf['Continuous integrator: Output 0'].tolist()]))


def test_bdwrapper_df_conversion_twice_same_name(system_collection):
    """
    Test the BDWrapper conversion of julia output failing for having the wrong number of signals
    """

    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection
    simulation_result = pd.DataFrame([[[1.0], [2.0], [3.0]], [[4.0], [5.0], [6.0]]], columns=[c1, c2, g1])
    simulation_result_transf = peepypoo.BDWrapper.convert_df_systems_to_df_signals(simulation_result)
    assert (set(simulation_result_transf.columns.tolist()) == {'Constant: Output 0', 'Constant1: Output 0',
                                                               'Gain: Output 0'}  # Compare column names
            and all([isinstance(x, float) for x in simulation_result_transf['Constant: Output 0'].tolist()])
            and simulation_result_transf['Constant: Output 0'].tolist() == [1, 4]
            and all([isinstance(x, float) for x in simulation_result_transf['Constant1: Output 0'].tolist()])
            and simulation_result_transf['Constant1: Output 0'].tolist() == [2, 5]
            and all([isinstance(x, float) for x in simulation_result_transf['Gain: Output 0'].tolist()])
            and simulation_result_transf['Gain: Output 0'].tolist() == [3, 6])


def test_bdwrapper_df_conversion_fail_no_sys():
    """
    Test the BDWrapper conversion of julia output failing for having no system as columns
    """

    simulation_result = pd.DataFrame([[1, 2, 3], [4, 5, 6]])
    with pytest.raises(ValueError):
        peepypoo.BDWrapper.convert_df_systems_to_df_signals(simulation_result)


def test_bdwrapper_df_conversion_fail_wrong_format(system_collection):
    """
    Test the BDWrapper conversion of julia output failing for having the wrong format
    """

    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection
    simulation_result = pd.DataFrame([[1, 2, 3], [4, 5, 6]], columns=[c1, c2, c3])
    with pytest.raises(ValueError):
        peepypoo.BDWrapper.convert_df_systems_to_df_signals(simulation_result)


def test_bdwrapper_df_conversion_fail_wrong_num_outputs(system_collection):
    """
    Test the BDWrapper conversion of julia output failing for having the wrong number of signals
    """

    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection
    simulation_result = pd.DataFrame([[[1], [2], [3]], [[4], [5], [6]]], columns=[c1, c2, c3])
    with pytest.raises(ValueError):
        peepypoo.BDWrapper.convert_df_systems_to_df_signals(simulation_result)


@pytest.mark.julia
def test_bdwrapper_julia_equations(system_collection):
    """
    Test the BDWrapper get_equations
    """
    c1, c2, c3, g1, g2, g3, intc1, intc2, intd1, intd2, logger \
        = system_collection

    bdwrapper = peepypoo.BDWrapper([c1])

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(intc1, [0], [0])

    equations = bdwrapper.get_equations()

    assert equations == ('\\begin{align}\n\\frac{\\mathrm{d} \\mathrm{' + intc1.name + '_{+}x}\\left( t \\right)}'
                         '{\\mathrm{d}t} =& 2\n\\end{align}\n')


@pytest.mark.julia
def test_bdwrapper_julia_equations_samename(system_collection):
    """
    Test the BDWrapper get_equations with two systems having the same name
    """
    c1 = peepypoo.Systems.Static.Constant([1.0], name="test")
    g1 = peepypoo.Systems.Static.Gain([2.0], name="test")
    intc1 = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0, name="test")

    bdwrapper = peepypoo.BDWrapper([c1])

    g1.add_output_connection(intc1, [0], [0])
    c1.add_output_connection(g1, [0], [0])

    equations = bdwrapper.get_equations()

    assert equations == ('\\begin{align}\n\\frac{\\mathrm{d} \\mathrm{' + intc1.name + '_{' + str(id(intc1)) +
                         '}_{+}x}\\left( t \\right)}{\\mathrm{d}t} =& 2\n\\end{align}\n')


@pytest.mark.julia
def test_bdwrapper_julia_equations_numeric():
    """
    Test the BDWrapper get_equations with system names being numeric
    """
    c1 = peepypoo.Systems.Static.Constant([1.0], name="0")
    g1 = peepypoo.Systems.Static.Gain([2.0], name="1")
    intc1 = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0, name="2")

    bdwrapper = peepypoo.BDWrapper([c1])

    c1.add_output_connection(g1, [0], [0])
    g1.add_output_connection(intc1, [0], [0])

    equations = bdwrapper.get_equations()

    assert equations == ('\\begin{align}\n\\frac{\\mathrm{d} {' + intc1.name + '_{+}x}\\left( t \\right)}'
                         '{\\mathrm{d}t} =& 2\n\\end{align}\n')
