import numpy as np
import peepypoo
import pytest


def test_ideal_clarifier_half():
    """
    Tests the ideal clarifier for halving the flow
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([10.0, np.array([0.1, 0.1]), 5.0])
    clar = peepypoo.Systems.Static.FlowElements.IdealClarifier(
        2, [False, True], provided_outflow='sludge'
    )
    const.add_output_connection(clar, [0, 1, 2], [0, 1, 2])

    outp = clar.get_output(54.2)
    assert outp[0] - 5.0 < float_tol \
           and np.all(np.abs(outp[1] - np.array([0.1, 0.0])) < float_tol) \
           and outp[2] - 5.0 < float_tol \
           and np.all(np.abs(outp[3] - np.array([0.1, 0.2])) < float_tol)


def test_ideal_clarifier_half_non_set():
    """
    Tests the ideal clarifier for halving the flow
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([10.0, np.array([0.1, 0.1]), 5.0])
    clar = peepypoo.Systems.Static.FlowElements.IdealClarifier(
        2, [False, True], provided_outflow='sludge', f_ns=1
    )
    const.add_output_connection(clar, [0, 1, 2], [0, 1, 2])

    outp = clar.get_output(54.2)
    assert outp[0] - 5.0 < float_tol \
           and np.all(np.abs(outp[1] - np.array([0.1, 0.2])) < float_tol) \
           and outp[2] - 5.0 < float_tol \
           and np.all(np.abs(outp[3] - np.array([0.1, 0.0])) < float_tol)


def test_ideal_clarifier_half_part_non_set():
    """
    Tests the ideal clarifier for halving the flow
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([10.0, np.array([0.1, 0.1]), 5.0])
    clar = peepypoo.Systems.Static.FlowElements.IdealClarifier(
        2, [False, True], provided_outflow='sludge', f_ns=0.25
    )
    const.add_output_connection(clar, [0, 1, 2], [0, 1, 2])

    outp = clar.get_output(54.2)
    assert outp[0] - 5.0 < float_tol \
           and np.all(np.abs(outp[1] - np.array([0.1, 0.05])) < float_tol) \
           and outp[2] - 5.0 < float_tol \
           and np.all(np.abs(outp[3] - np.array([0.1, 0.15])) < float_tol)


def test_ideal_clarifier_third1():
    """
    Tests the ideal clarifier for outputting a third of the flow as sludge
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([12.0, np.array([0.1, 0.1]), 4.0])
    clar = peepypoo.Systems.Static.FlowElements.IdealClarifier(
        2, [False, True], provided_outflow='sludge'
    )
    const.add_output_connection(clar, [0, 1, 2], [0, 1, 2])

    outp = clar.get_output(54.2)
    assert outp[0] - 8.0 < float_tol \
           and np.all(outp[1] - np.array([0.1, 0.0]) < float_tol) \
           and outp[2] - 4.0 < float_tol \
           and np.all(outp[3] - np.array([0.1, 0.3]) < float_tol)


def test_ideal_clarifier_third2():
    """
    Tests the ideal clarifier for outputting a third of the flow as sludge
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([12.0, np.array([0.1, 0.1]), 8.0])
    clar = peepypoo.Systems.Static.FlowElements.IdealClarifier(
        2, [False, True], provided_outflow='liquid'
    )
    const.add_output_connection(clar, [0, 1, 2], [0, 1, 2])

    outp = clar.get_output(54.2)
    assert outp[0] - 8.0 < float_tol \
           and np.all(outp[1] - np.array([0.1, 0.0]) < float_tol) \
           and outp[2] - 4.0 < float_tol \
           and np.all(outp[3] - np.array([0.1, 0.3]) < float_tol)


@pytest.mark.julia
def test_ideal_clarifier_half_jl():
    """
    Tests the ideal clarifier for halving the flow in julia
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([10.0, np.array([0.1, 0.1]), 5.0])
    clar = peepypoo.Systems.Static.FlowElements.IdealClarifier(
        2, [False, True], provided_outflow='sludge'
    )
    const.add_output_connection(clar, [0, 1, 2], [0, 1, 2])

    wrap = peepypoo.BDWrapper([clar])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[clar].to_numpy()[0]
    assert outp[0] - 5.0 < float_tol \
           and np.all(outp[1] - np.array([0.1, 0.0]) < float_tol) \
           and outp[2] - 5.0 < float_tol \
           and np.all(outp[3] - np.array([0.1, 0.2]) < float_tol)


@pytest.mark.julia
def test_ideal_clarifier_half_non_set_jl():
    """
    Tests the ideal clarifier for halving the flow in julia
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([10.0, np.array([0.1, 0.1]), 5.0])
    clar = peepypoo.Systems.Static.FlowElements.IdealClarifier(
        2, [False, True], provided_outflow='sludge', f_ns=1
    )
    const.add_output_connection(clar, [0, 1, 2], [0, 1, 2])

    wrap = peepypoo.BDWrapper([clar])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[clar].to_numpy()[0]
    assert outp[0] - 5.0 < float_tol \
           and np.all(outp[1] - np.array([0.1, 0.2]) < float_tol) \
           and outp[2] - 5.0 < float_tol \
           and np.all(outp[3] - np.array([0.1, 0.0]) < float_tol)


@pytest.mark.julia
def test_ideal_clarifier_half_part_non_set_jl():
    """
    Tests the ideal clarifier for halving the flow in julia
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([10.0, np.array([0.1, 0.1]), 5.0])
    clar = peepypoo.Systems.Static.FlowElements.IdealClarifier(
        2, [False, True], provided_outflow='sludge', f_ns=0.25
    )
    const.add_output_connection(clar, [0, 1, 2], [0, 1, 2])

    wrap = peepypoo.BDWrapper([clar])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[clar].to_numpy()[0]
    assert outp[0] - 5.0 < float_tol \
           and np.all(outp[1] - np.array([0.1, 0.05]) < float_tol) \
           and outp[2] - 5.0 < float_tol \
           and np.all(outp[3] - np.array([0.1, 0.15]) < float_tol)


@pytest.mark.julia
def test_ideal_clarifier_third1_jl():
    """
    Tests the ideal clarifier for outputting a third of the flow as sludge in
    julia
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([12.0, np.array([0.1, 0.1]), 4.0])
    clar = peepypoo.Systems.Static.FlowElements.IdealClarifier(
        2, [False, True], provided_outflow='sludge'
    )
    const.add_output_connection(clar, [0, 1, 2], [0, 1, 2])

    wrap = peepypoo.BDWrapper([clar])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[clar].to_numpy()[0]
    assert outp[0] - 8.0 < float_tol \
           and np.all(outp[1] - np.array([0.1, 0.0]) < float_tol) \
           and outp[2] - 4.0 < float_tol \
           and np.all(outp[3] - np.array([0.1, 0.3]) < float_tol)


@pytest.mark.julia
def test_ideal_clarifier_third2_jl():
    """
    Tests the ideal clarifier for outputting a third of the flow as sludge in
    julia
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([12.0, np.array([0.1, 0.1]), 8.0])
    clar = peepypoo.Systems.Static.FlowElements.IdealClarifier(
        2, [False, True], provided_outflow='liquid'
    )
    const.add_output_connection(clar, [0, 1, 2], [0, 1, 2])

    wrap = peepypoo.BDWrapper([clar])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[clar].to_numpy()[0]
    assert outp[0] - 8.0 < float_tol \
           and np.all(outp[1] - np.array([0.1, 0.0]) < float_tol) \
           and outp[2] - 4.0 < float_tol \
           and np.all(outp[3] - np.array([0.1, 0.3]) < float_tol)


def test_ideal_clarifier_invalid_matdef():
    """
    Tests the ideal clarifier fails if the material definition (if soluble or
    solid) is wrong
    """
    with pytest.raises(ValueError):
        peepypoo.Systems.Static.FlowElements.IdealClarifier(
            2, [False, True, False], provided_outflow='sludge'
        )


def test_ideal_clarifier_invalid_f_ns():
    """
    Tests the ideal clarifier fails if the fraction of unsettlable solids is invalid
    """
    with pytest.raises(ValueError):
        peepypoo.Systems.Static.FlowElements.IdealClarifier(
            2, [False, True, False], provided_outflow='sludge', f_ns=2
        )


def test_flow_unifier_2eq():
    """
    Tests the flow unifier for two equal flows
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 0.1])])
    unif = peepypoo.Systems.Static.FlowElements.FlowUnifier(2, 2)
    const.add_output_connection(unif, [0, 1, 2, 3], [0, 1, 0, 1])

    outp = unif.get_output(54.2)
    assert (outp[0] - 4) < float_tol \
           and np.all(outp[1] - np.array([0.1, 0.1]) < float_tol)


def test_flow_unifier_2eqFl():
    """
    Tests the flow unifier for two flows with equal rates
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 0.1])])
    const1 = peepypoo.Systems.Static.Constant([2.0, np.array([0.3, 0])])
    unif = peepypoo.Systems.Static.FlowElements.FlowUnifier(2, 2)
    const.add_output_connection(unif, [0, 1], [0, 1])
    const1.add_output_connection(unif, [2, 3], [0, 1])

    outp = unif.get_output(54.2)
    assert (outp[0] - 4) < float_tol \
           and np.all(outp[1] - np.array([0.2, 0.05]) < float_tol)


def test_flow_unifier_2fl():
    """
    Tests the flow unifier for two flows
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 1.2])])
    const1 = peepypoo.Systems.Static.Constant([3.0, np.array([0.6, 0.2])])
    unif = peepypoo.Systems.Static.FlowElements.FlowUnifier(2, 2)
    const.add_output_connection(unif, [0, 1], [0, 1])
    const1.add_output_connection(unif, [2, 3], [0, 1])

    outp = unif.get_output(54.2)
    assert (outp[0] - 5) < float_tol \
           and np.all(outp[1] - np.array([0.4, 0.6]) < float_tol)


def test_flow_unifier_3fl():
    """
    Tests the flow unifier for three flows
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 1.2, 0.3])])
    const1 = peepypoo.Systems.Static.Constant([3.0, np.array([0.6, 0.2, 0.8])])
    const2 = peepypoo.Systems.Static.Constant([5.0, np.array([0.8, 0.6, 0.0])])
    unif = peepypoo.Systems.Static.FlowElements.FlowUnifier(3, 3)
    const.add_output_connection(unif, [0, 1], [0, 1])
    const1.add_output_connection(unif, [2, 3], [0, 1])
    const2.add_output_connection(unif, [4, 5], [0, 1])

    outp = unif.get_output(54.2)
    assert (outp[0] - 10) < float_tol \
           and np.all(outp[1] - np.array([0.6, 0.6, 0.3]) < float_tol)


def test_flow_unifier_8fl():
    """
    Tests the flow unifier for three flows
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 1.2])])
    const1 = peepypoo.Systems.Static.Constant([3.0, np.array([0.6, 0.2])])
    const2 = peepypoo.Systems.Static.Constant([5.0, np.array([0.8, 0.6])])
    const3 = peepypoo.Systems.Static.Constant([8.0, np.array([0.4, 3.6])])
    const4 = peepypoo.Systems.Static.Constant([13.0, np.array([0.7, 0.8])])
    const5 = peepypoo.Systems.Static.Constant([21.0, np.array([0.4, 5.4])])
    const6 = peepypoo.Systems.Static.Constant([34.0, np.array([0.3, 0.4])])
    const7 = peepypoo.Systems.Static.Constant([55.0, np.array([0.2, 0.6])])
    unif = peepypoo.Systems.Static.FlowElements.FlowUnifier(2, 8)
    const.add_output_connection(unif, [0, 1], [0, 1])
    const1.add_output_connection(unif, [2, 3], [0, 1])
    const2.add_output_connection(unif, [4, 5], [0, 1])
    const3.add_output_connection(unif, [6, 7], [0, 1])
    const4.add_output_connection(unif, [8, 9], [0, 1])
    const5.add_output_connection(unif, [10, 11], [0, 1])
    const6.add_output_connection(unif, [12, 13], [0, 1])
    const7.add_output_connection(unif, [14, 15], [0, 1])

    outp = unif.get_output(54.2)
    assert (outp[0] - 141) < float_tol \
           and np.all(outp[1] - np.array([0.33971631, 1.45531915]) < float_tol)


@pytest.mark.julia
def test_flow_unifier_2eq_jl():
    """
    Tests the flow unifier for two equal flows
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 0.1])])
    unif = peepypoo.Systems.Static.FlowElements.FlowUnifier(2, 2)
    const.add_output_connection(unif, [0, 1, 2, 3], [0, 1, 0, 1])

    wrap = peepypoo.BDWrapper([unif])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[unif].to_numpy()[0]

    assert (outp[0] - 4) < float_tol \
           and np.all(outp[1] - np.array([0.1, 0.1]) < float_tol)


@pytest.mark.julia
def test_flow_unifier_2eqFl_jl():
    """
    Tests the flow unifier for two flows with equal rates
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 0.1])])
    const1 = peepypoo.Systems.Static.Constant([2.0, np.array([0.3, 0])])
    unif = peepypoo.Systems.Static.FlowElements.FlowUnifier(2, 2)
    const.add_output_connection(unif, [0, 1], [0, 1])
    const1.add_output_connection(unif, [2, 3], [0, 1])

    wrap = peepypoo.BDWrapper([unif])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[unif].to_numpy()[0]
    assert (outp[0] - 4) < float_tol \
           and np.all(outp[1] - np.array([0.2, 0.05]) < float_tol)


@pytest.mark.julia
def test_flow_unifier_2fl_jl():
    """
    Tests the flow unifier for two flows
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 1.2])])
    const1 = peepypoo.Systems.Static.Constant([3.0, np.array([0.6, 0.2])])
    unif = peepypoo.Systems.Static.FlowElements.FlowUnifier(2, 2)
    const.add_output_connection(unif, [0, 1], [0, 1])
    const1.add_output_connection(unif, [2, 3], [0, 1])

    wrap = peepypoo.BDWrapper([unif])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[unif].to_numpy()[0]
    assert (outp[0] - 5) < float_tol \
           and np.all(outp[1] - np.array([0.4, 0.6]) < float_tol)


@pytest.mark.julia
def test_flow_unifier_3fl_jl():
    """
    Tests the flow unifier for three flows
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 1.2, 0.3])])
    const1 = peepypoo.Systems.Static.Constant([3.0, np.array([0.6, 0.2, 0.8])])
    const2 = peepypoo.Systems.Static.Constant([5.0, np.array([0.8, 0.6, 0.0])])
    unif = peepypoo.Systems.Static.FlowElements.FlowUnifier(3, 3)
    const.add_output_connection(unif, [0, 1], [0, 1])
    const1.add_output_connection(unif, [2, 3], [0, 1])
    const2.add_output_connection(unif, [4, 5], [0, 1])

    wrap = peepypoo.BDWrapper([unif])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[unif].to_numpy()[0]
    assert (outp[0] - 10) < float_tol \
           and np.all(outp[1] - np.array([0.6, 0.6, 0.3]) < float_tol)


@pytest.mark.julia
def test_flow_unifier_8fl_jl():
    """
    Tests the flow unifier for three flows
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 1.2])])
    const1 = peepypoo.Systems.Static.Constant([3.0, np.array([0.6, 0.2])])
    const2 = peepypoo.Systems.Static.Constant([5.0, np.array([0.8, 0.6])])
    const3 = peepypoo.Systems.Static.Constant([8.0, np.array([0.4, 3.6])])
    const4 = peepypoo.Systems.Static.Constant([13.0, np.array([0.7, 0.8])])
    const5 = peepypoo.Systems.Static.Constant([21.0, np.array([0.4, 5.4])])
    const6 = peepypoo.Systems.Static.Constant([34.0, np.array([0.3, 0.4])])
    const7 = peepypoo.Systems.Static.Constant([55.0, np.array([0.2, 0.6])])
    unif = peepypoo.Systems.Static.FlowElements.FlowUnifier(2, 8)
    const.add_output_connection(unif, [0, 1], [0, 1])
    const1.add_output_connection(unif, [2, 3], [0, 1])
    const2.add_output_connection(unif, [4, 5], [0, 1])
    const3.add_output_connection(unif, [6, 7], [0, 1])
    const4.add_output_connection(unif, [8, 9], [0, 1])
    const5.add_output_connection(unif, [10, 11], [0, 1])
    const6.add_output_connection(unif, [12, 13], [0, 1])
    const7.add_output_connection(unif, [14, 15], [0, 1])

    wrap = peepypoo.BDWrapper([unif])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[unif].to_numpy()[0]
    assert (outp[0] - 141) < float_tol \
           and np.all(outp[1] - np.array([0.33971631, 1.45531915]) < float_tol)


def test_flow_separator_2fl_abs():
    """
    Tests the flow separator for two flows with absolute flow
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 1.2]), 1.2])
    sep = peepypoo.Systems.Static.FlowElements.FlowSeparator(
        2, 2, use_percentages=False
    )
    const.add_output_connection(sep, [0, 1, 2], [0, 1, 2])

    outp = sep.get_output(54.2)
    assert (outp[0] - 0.8) < float_tol \
           and np.all(outp[1] - np.array([0.1, 1.2]) < float_tol) \
           and (outp[2] - 1.2) < float_tol \
           and np.all(outp[3] - np.array([0.1, 1.2]) < float_tol)


def test_flow_separator_2fl_rel():
    """
    Tests the flow separator for two flows with relative flow
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 1.2]), 0.75])
    sep = peepypoo.Systems.Static.FlowElements.FlowSeparator(
        2, 2, use_percentages=True
    )
    const.add_output_connection(sep, [0, 1, 2], [0, 1, 2])

    outp = sep.get_output(54.2)
    assert (outp[0] - 0.5) < float_tol \
           and np.all(outp[1] - np.array([0.1, 1.2]) < float_tol) \
           and (outp[2] - 1.5) < float_tol \
           and np.all(outp[3] - np.array([0.1, 1.2]) < float_tol)


def test_flow_separator_23fl_abs():
    """
    Tests the flow separator for two flows with absolute flow
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant(
        [2687.0, np.array([0.1, 1.2]), *[2.9+i for i in range(1, 23)]]
    )
    sep = peepypoo.Systems.Static.FlowElements.FlowSeparator(
        2, 23, use_percentages=False
    )
    const.add_output_connection(sep, [*range(24)], [*range(24)])

    outp = sep.get_output(54.2)
    assert np.all(np.array(outp[0::2])
                  - np.array([2687 - sum([2.9+i for i in range(1, 23)]),
                              *[2.9+i for i in range(1, 23)]]) < float_tol) \
           and np.all(np.array(outp[1::2]) - np.array([0.1, 1.2]) < float_tol)


def test_flow_separator_23fl_rel():
    """
    Tests the flow separator for two flows with relative flow
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant(
        [2687.0, np.array([0.1, 1.2]), *[i/100 for i in range(1, 24)]]
    )
    sep = peepypoo.Systems.Static.FlowElements.FlowSeparator(
        2, 23, use_percentages=True
    )
    const.add_output_connection(sep, [*range(24)], [*range(24)])

    outp = sep.get_output(54.2)
    assert np.all(np.array(outp[0::2])
                  - np.array([(1 - sum([i/100 for i in range(1, 23)])),
                              *[i/100 for i in range(1, 23)]])*2687
                  < float_tol) \
           and np.all(np.array(outp[1::2]) - np.array([0.1, 1.2]) < float_tol)


@pytest.mark.julia
def test_flow_separator_2fl_abs_jl():
    """
    Tests the flow separator for two flows with absolute flow in julia
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 1.2]), 1.2])
    sep = peepypoo.Systems.Static.FlowElements.FlowSeparator(
        2, 2, use_percentages=False
    )
    const.add_output_connection(sep, [0, 1, 2], [0, 1, 2])

    wrap = peepypoo.BDWrapper([sep])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[sep].to_numpy()[0]
    assert (outp[0] - 0.8) < float_tol \
           and np.all(outp[1] - np.array([0.1, 1.2]) < float_tol) \
           and (outp[2] - 1.2) < float_tol \
           and np.all(outp[3] - np.array([0.1, 1.2]) < float_tol)


@pytest.mark.julia
def test_flow_separator_2fl_rel_jl():
    """
    Tests the flow separator for two flows with relative flow in julia
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([2.0, np.array([0.1, 1.2]), 0.75])
    sep = peepypoo.Systems.Static.FlowElements.FlowSeparator(
        2, 2, use_percentages=True
    )
    const.add_output_connection(sep, [0, 1, 2], [0, 1, 2])

    wrap = peepypoo.BDWrapper([sep])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[sep].to_numpy()[0]
    assert (outp[0] - 0.5) < float_tol \
           and np.all(outp[1] - np.array([0.1, 1.2]) < float_tol) \
           and (outp[2] - 1.5) < float_tol \
           and np.all(outp[3] - np.array([0.1, 1.2]) < float_tol)


@pytest.mark.julia
def test_flow_separator_23fl_abs_jl():
    """
    Tests the flow separator for two flows with absolute flow in julia
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant(
        [2687.0, np.array([0.1, 1.2]), *[2.9+i for i in range(1, 23)]]
    )
    sep = peepypoo.Systems.Static.FlowElements.FlowSeparator(
        2, 23, use_percentages=False
    )
    const.add_output_connection(sep, [*range(24)], [*range(24)])

    wrap = peepypoo.BDWrapper([sep])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[sep].to_numpy()[0]
    assert np.all(np.array(outp[0::2])
                  - np.array([2687 - sum([2.9+i for i in range(1, 23)]),
                              *[2.9+i for i in range(1, 23)]]) < float_tol) \
           and np.all(np.array(outp[1::2].tolist())
                      - np.array([0.1, 1.2]) < float_tol)


@pytest.mark.julia
def test_flow_separator_23fl_rel_jl():
    """
    Tests the flow separator for two flows with relative flow in julia
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant(
        [2687.0, np.array([0.1, 1.2]), *[i/100 for i in range(1, 24)]]
    )
    sep = peepypoo.Systems.Static.FlowElements.FlowSeparator(
        2, 23, use_percentages=True
    )
    const.add_output_connection(sep, [*range(24)], [*range(24)])

    wrap = peepypoo.BDWrapper([sep])
    outp = wrap.evaluate_blockdiagram_julia(0, 54.2, [54.2])
    outp = outp[sep].to_numpy()[0]
    assert np.all(np.array(outp[0::2])
                  - np.array([(1 - sum([i/100 for i in range(1, 23)])),
                              *[i/100 for i in range(1, 23)]])*2687
                  < float_tol) \
           and np.all(np.array(outp[1::2].tolist())
                      - np.array([0.1, 1.2]) < float_tol)
