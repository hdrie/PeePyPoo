import pytest
import peepypoo
import numpy as np


def test_scipy_integrate_ivp_const_rk45():
    """
    Tests the scipy.integrate.solve_ivp integration wrapper with RK45 and a
    constant input
    """
    float_tol = 1e-6
    integr = peepypoo.Integrators.Python.SciPySolveIVP(lambda x, t: 1)
    sol, fcn = integr.integrate(np.array([0]), 0, 10)

    assert sol - 10 <= float_tol \
           and fcn(5) - 5 <= float_tol


def test_scipy_integrate_ivp_const_rk23():
    """
    Tests the scipy.integrate.solve_ivp integration wrapper with RK23 and a
    constant input
    """
    float_tol = 1e-6
    integr = peepypoo.Integrators.Python.SciPySolveIVP(lambda x, t: 1,
                                                       method='RK23')
    sol, fcn = integr.integrate(np.array([0]), 0, 10)

    assert sol - 10 <= float_tol \
           and fcn(5) - 5 <= float_tol


def test_scipy_integrate_ivp_vec_rk45():
    """
    Tests the scipy.integrate.solve_ivp integration wrapper with RK45 and a
    vectorial input
    """
    float_tol = 5e-4
    integr = peepypoo.Integrators.Python.SciPySolveIVP(
        lambda x, t: np.array([x[0], t, x[2]*t])
    )
    sol, fcn = integr.integrate(np.array([1, 0, 1]), 0, 1)

    assert np.all(sol - np.array([np.exp(1), 0.5, np.exp(1/2)]) <= float_tol) \
           and np.all(fcn(0.5) - np.array([np.exp(0.5), 0.125, np.exp(0.125)])
                      <= float_tol)


def test_scipy_integrate_ivp_vec_rk23():
    """
    Tests the scipy.integrate.solve_ivp integration wrapper with RK23 and a
    vectorial input
    """
    float_tol = 5e-4
    integr = peepypoo.Integrators.Python.SciPySolveIVP(
        lambda x, t: np.array([x[0], t, x[2]*t]),
        method='RK23'
    )
    sol, fcn = integr.integrate(np.array([1, 0, 1]), 0, 1)

    assert np.all(sol - np.array([np.exp(1), 0.5, np.exp(1/2)]) <= float_tol) \
           and np.all(fcn(0.5) - np.array([np.exp(0.5), 0.125, np.exp(0.125)])
                      <= float_tol)


def test_scipy_integrate_ivp_tol():
    """
    Tests the scipy.integrate.solve_ivp integration wrapper with RK45 and a
    constant input
    """
    float_tol = 1e-6
    integr = peepypoo.Integrators.Python.SciPySolveIVP(
        lambda x, t: np.array([x[0], t, x[2]*t]),
        atol=1e-8,
        rtol=1e-7
    )
    sol, fcn = integr.integrate(np.array([1, 0, 1]), 0, 1)

    assert np.all(sol - np.array([np.exp(1), 0.5, np.exp(1/2)]) <= float_tol) \
           and np.all(fcn(0.5) - np.array([np.exp(0.5), 0.125, np.exp(0.125)])
                      <= float_tol)


