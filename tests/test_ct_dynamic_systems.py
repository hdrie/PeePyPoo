import pytest
import peepypoo
import numpy as np


def test_ct_integrator_const():
    """
    Tests the continuous time integrator system with a constant input.
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([1.0])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    const.add_output_connection(integr, [0], [0])

    assert integr.get_output(20)[0] - 20 <= float_tol


def test_ct_integrator_np():
    """
    Tests the continuous time integrator system with a numpy array.
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([np.array([1.0, 2.0])])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(
        np.array([2.0, 0.25])
    )
    const.add_output_connection(integr, [0], [0])

    assert np.all(integr.get_output(20)[0]
                  - np.array([22.0, 40.25]) <= float_tol)


def test_ct_double_integrator_const():
    """
    Tests the continuous time integrator system twice in a row with a constant
    input.
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([1.0])
    integr1 = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    integr2 = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    const.add_output_connection(integr1, [0], [0])
    integr1.add_output_connection(integr2, [0], [0])

    assert integr2.get_output(20)[0] - 400 <= float_tol


def test_ct_integrator_time_forward():
    """
    Tests the continuous time integrator system with increasing times.
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([np.array([1.0, 2.0])])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(
        np.array([2.0, 0.25])
    )
    const.add_output_connection(integr, [0], [0])

    for t in range(0, 200):
        assert np.all(integr.get_output(t/10)[0]
                      - np.array([2.0+t/10, 0.25+2*t/10]) <= float_tol)


def test_ct_integrator_time_backwards():
    """
    Tests the continuous time integrator system with decreasing times.
    """
    float_tol = 1e-6
    const = peepypoo.Systems.Static.Constant([np.array([1.0, 2.0])])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(
        np.array([2.0, 0.25])
    )
    const.add_output_connection(integr, [0], [0])

    for t in range(200, 0, -1):
        assert np.all(integr.get_output(t/10)[0]
                      - np.array([2.0+t/10, 0.25+2*t/10]) <= float_tol)


def test_ct_integrator_reset_on_input_change():
    """
    Tests the continuous time integrator if it resets on input change
    """
    float_tol = 1e-6
    const1 = peepypoo.Systems.Static.Constant([1.0])
    const2 = peepypoo.Systems.Static.Constant([2.0])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    const1.add_output_connection(integr, [0], [0])
    integr.get_output(20)
    const1.remove_connections()
    const2.add_output_connection(integr, [0], [0])

    assert integr.get_output(20)[0] - 40 <= float_tol


def test_ct_integrator_reset_on_change_in_input_tree():
    """
    Tests the continuous time integrator if it resets on change in the input
    tree.
    """
    float_tol = 1e-6
    const1 = peepypoo.Systems.Static.Constant([1.0])
    const2 = peepypoo.Systems.Static.Constant([2.0])
    gain = peepypoo.Systems.Static.Gain([2.0])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    gain.add_output_connection(integr, [0], [0])
    const1.add_output_connection(gain, [0], [0])
    integr.get_output(20)
    const1.remove_connections()
    const2.add_output_connection(gain, [0], [0])

    assert integr.get_output(20)[0] - 80 <= float_tol


def test_ct_integrator_negative_output_time():
    """
    Tests the continuous time integrator system if the requested time is before
    the initial time.
    """
    float_tol = 1e-6
    const1 = peepypoo.Systems.Static.Constant([1.0])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    const1.add_output_connection(integr, [0], [0])

    assert integr.get_output(-10)[0] - 0.0 <= float_tol


def test_system_input_port_removal_addition():
    """
    Tests removal and adding back a system input port of a dynamical system
    """
    const1 = peepypoo.Systems.Static.Constant([1.0])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    const1.add_output_connection(integr, [0], [0])

    # Save port types
    port_type = integr.input_types[0]
    # Remove input port
    integr._remove_system_input_port([0])
    # Add port and connection back
    integr._add_system_input_port(1, port_type)
    integr.add_input_connection(
        const1, [0], [0]
    )

    assert integr.get_output(654)[0] - 654 <= 1e-6


def test_system_input_port_removal_addition_with_params():
    """
    Tests removal and adding back a system input port of a dynamical system
    which has already connected parameters
    """
    float_tol = 0.5e-3
    v = 1000.0
    q = 10000.0/24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First", stoichiometry_file='ozonation'
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params.add_output_connection(r, [2], [0])

    inp_type = r.input_types[0:2]
    r._remove_system_input_port([0, 1])
    r._add_system_input_port(2, inp_type)
    r.add_input_connection(inf, [0, 1], [0, 1])

    out = r.get_output(24)
    assert out[0] - q <= float_tol \
           and np.all(abs(out[1] - np.array([0.5, 0.013])) <= float_tol)


def test_system_input_port_addition_no_type():
    """
    Tests adding an input port to a system without type
    """
    const1 = peepypoo.Systems.Static.Constant([1.0])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    const1.add_output_connection(integr, [0], [0])

    integr._add_system_input_port(1)

    assert integr.num_inputs == 2 and \
           integr._num_system_inputs == 2 and \
           integr._num_params == 0 and \
           integr.get_output(654)[0] - 654 <= 1e-6


def test_system_input_port_addition_zero_ports():
    """
    Tests adding zero system input ports to a system (i.e. no port)
    """
    const1 = peepypoo.Systems.Static.Constant([1.0])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    const1.add_output_connection(integr, [0], [0])

    integr._add_system_input_port(0)

    assert integr.num_inputs == 1 and \
           integr._num_system_inputs == 1 and \
           integr._num_params == 0 and \
           integr.get_output(654)[0] - 654 <= 1e-6


def test_param_port_removal_addition():
    """
    Tests removal and adding back a system input port of a dynamical system
    """
    float_tol = 0.5e-3
    v = 1000.0
    q = 10000.0 / 24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First", stoichiometry_file='ozonation'
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params.add_output_connection(r, [2], [0])

    inp_type = r.input_types[2]
    r._remove_param_port([0])
    r._add_param_port(1, inp_type)
    r.add_input_connection(params, [2], [0])

    out = r.get_output(24)
    assert out[0] - q <= float_tol \
           and np.all(abs(out[1] - np.array([0.5, 0.013])) <= float_tol)


def test_param_port_addition_no_type():
    """
    Tests adding an input port to a system without type
    """
    const1 = peepypoo.Systems.Static.Constant([1.0])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    const1.add_output_connection(integr, [0], [0])

    integr._add_param_port(1)

    assert integr.num_inputs == 2 and \
           integr._num_system_inputs == 1 and \
           integr._num_params == 1 and \
           integr.get_output(654)[0] - 654 <= 1e-6


def test_param_port_addition_zero_ports():
    """
    Tests adding zero system input ports to a system (i.e. no port)
    """
    const1 = peepypoo.Systems.Static.Constant([1.0])
    integr = peepypoo.Systems.Dynamic.Continuous.Integrator(0.0)
    const1.add_output_connection(integr, [0], [0])

    integr._add_param_port(0)

    assert integr.num_inputs == 1 and \
           integr._num_system_inputs == 1 and \
           integr._num_params == 0 and \
           integr.get_output(654)[0] - 654 <= 1e-6


def test_port_removal_addition():
    """
    Tests removal and adding back a system input port of a dynamical system
    """
    float_tol = 0.5e-3
    v = 1000.0
    q = 10000.0 / 24
    k_d = 1500.0 / 24
    r = peepypoo.Systems.Dynamic.Continuous.Reactors.CSTR(
        v, np.array([0.5, 0]), name="First", stoichiometry_file='ozonation'
    )
    inf = peepypoo.Systems.Static.Constant([q, np.array([0.5, 1.0])])
    inf.add_output_connection(r, [0, 1], [0, 1])
    params = peepypoo.Systems.Static.Constant([np.array([0, k_d])])
    params.add_output_connection(r, [2], [0])

    inp_type = r.input_types[0:2]
    param_type = r.input_types[2]
    r._remove_input_port([0, 1, 2])
    r._add_input_port(1, param_type)
    r._add_system_input_port(2, inp_type)
    r.add_input_connection(params, [2], [0])
    r.add_input_connection(inf, [0, 1], [0, 1])

    out = r.get_output(24)
    assert out[0] - q <= float_tol \
           and np.all(abs(out[1] - np.array([0.5, 0.013])) <= float_tol)
