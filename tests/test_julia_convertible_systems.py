import pytest
import peepypoo
from peepypoo.Systems import JuliaModelingToolkitConvertibleSystem as JlSys


@pytest.mark.julia
def test_julia_const_t():
    """
    Tests the constant system with an integer requesting the julia time
    variable.
    """
    const = peepypoo.Systems.Static.Constant([54])

    assert const.t_jl == const.jl_macro_vars(const.jl, 't', "var")


@pytest.mark.julia
def test_julia_const_inputs():
    """
    Tests the constant system with an integer requesting the julia input
    variables.
    """
    const = peepypoo.Systems.Static.Constant([54])

    assert const.inputs_jl == []


@pytest.mark.julia
def test_julia_const_outputs():
    """
    Tests the constant system with an integer requesting the julia output
    variables
    """
    const = peepypoo.Systems.Static.Constant([54])

    assert const.outputs_jl == [const.jl_macro_vars(const.jl, 'u',
                                                    "var", [const.t_jl])]


@pytest.mark.julia
def test_julia_const_ic():
    """
    Tests the constant system with an integer requesting the julia initial
    condition variable
    """
    const = peepypoo.Systems.Static.Constant([54])

    assert const.initial_condition_jl == const.jl.Dict()


@pytest.mark.julia
def test_julia_const_tstops():
    """
    Tests the constant system with an integer requesting the julia tstops
    variable
    """
    const = peepypoo.Systems.Static.Constant([54])

    assert const.tstops_jl == []


@pytest.mark.julia
def test_julia_const_d_disc():
    """
    Tests the constant system with an integer requesting the julia
    d_discontinuities variable
    """
    const = peepypoo.Systems.Static.Constant([54])

    assert const.d_discontinuities_jl == []


@pytest.mark.julia
def test_julia_const_disc_period():
    """
    Tests the constant system with an integer requesting the julia
    discontinuities_period_jl variable
    """
    const = peepypoo.Systems.Static.Constant([54])

    assert const.discontinuities_period_jl == []


@pytest.mark.julia
def test_julia_const_getequations():
    """
    Tests the constant system get_equations function
    """
    const = peepypoo.Systems.Static.Constant([54])

    assert const.get_equations() == '\\begin{align}\n\\mathrm{y0}\\left( t \\right) =& 54\n\\end{align}\n'


@pytest.mark.julia
def test_julia_var():
    """
    Tests the Julia variable-generation function
    """
    dep1 = JlSys.jl.seval("@variables x")[0]
    dep2 = JlSys.jl.seval("@variables y")[0]
    assert JlSys.jl_macro_vars(JlSys.jl, 'var', "var", [dep1, dep2]) == \
           JlSys.jl.seval("@variables var(x, y)")[0]


@pytest.mark.julia
def test_julia_param():
    """
    Tests the Julia param-generation function
    """

    assert JlSys.jl_macro_vars(JlSys.jl, 'par', "param") == \
           JlSys.jl.seval("@parameters par")[0]


@pytest.mark.julia
def test_julia_const():
    """
    Tests the Julia constant-generation function
    """

    assert JlSys.jl_macro_vars(JlSys.jl, 'c', "const", val=12) == \
           JlSys.jl.seval("@constants c = 12")[0]


@pytest.mark.julia
def test_julia_const_no_value():
    """
    Tests the Julia constant-generation function
    """
    with pytest.raises(ValueError):
        JlSys.jl_macro_vars(JlSys.jl, 'c', "const")


@pytest.mark.julia
def test_julia_invalid_type():
    """
    Tests the Julia variable-generation function with an invalid type
    """

    with pytest.raises(ValueError):
        JlSys.jl_macro_vars(JlSys.jl, 'c', "lskda", val=12)


@pytest.mark.julia
def test_julia_symvec_padding():
    """
    Tests the Julia symbolic vector padding
    """

    t = JlSys.jl_macro_vars(JlSys.jl, 'tau', "var")
    symvec = [JlSys.jl_macro_vars(JlSys.jl, 'y0', "var", t),
              JlSys.jl_macro_vars(JlSys.jl, 'y1', "var", t),
              [JlSys.jl_macro_vars(JlSys.jl, 'u0', "var", t),
               JlSys.jl_macro_vars(JlSys.jl, 'u1', "var", t)]]
    formvec = [0, [0, 1, 2, [0, 1, 2], 4], [0, 1]]
    pad_vec = JlSys.jl_symvec_pad_vectorial(JlSys.jl, symvec, formvec, t)

    pad_vec_ref = [JlSys.jl_macro_vars(JlSys.jl, 'y0', "var", t),
                   [JlSys.jl_macro_vars(JlSys.jl, 'y1_0', "var", t),
                    JlSys.jl_macro_vars(JlSys.jl, 'y1_1', "var", t),
                    JlSys.jl_macro_vars(JlSys.jl, 'y1_2', "var", t),
                    [JlSys.jl_macro_vars(JlSys.jl, 'y3_0', "var", t),
                     JlSys.jl_macro_vars(JlSys.jl, 'y3_1', "var", t),
                     JlSys.jl_macro_vars(JlSys.jl, 'y3_2', "var", t)],
                    JlSys.jl_macro_vars(JlSys.jl, 'y1_4', "var", t)],
                   [JlSys.jl_macro_vars(JlSys.jl, 'u0', "var", t),
                    JlSys.jl_macro_vars(JlSys.jl, 'u1', "var", t)]]

    assert pad_vec == pad_vec_ref


@pytest.mark.julia
def test_julia_symvec_padding_invalid_shape1():
    """
    Tests the Julia symbolic vector padding with invalid shape
    """

    t = JlSys.jl_macro_vars(JlSys.jl, 'tau', "var")
    symvec = [JlSys.jl_macro_vars(JlSys.jl, 'y0', "var", t),
              [JlSys.jl_macro_vars(JlSys.jl, 'u0', "var", t),
               JlSys.jl_macro_vars(JlSys.jl, 'u1', "var", t)]]
    formvec = [0, [0, 1, 2, [0, 1, 2], 4]]
    with pytest.raises(ValueError):
        JlSys.jl_symvec_pad_vectorial(JlSys.jl, symvec, formvec, t)


@pytest.mark.julia
def test_julia_symvec_padding_invalid_shape2():
    """
    Tests the Julia symbolic vector padding with invalid shape
    """

    t = JlSys.jl_macro_vars(JlSys.jl, 'tau', "var")
    symvec = [JlSys.jl_macro_vars(JlSys.jl, 'y0', "var", t),
              [JlSys.jl_macro_vars(JlSys.jl, 'u0', "var", t),
               JlSys.jl_macro_vars(JlSys.jl, 'u1', "var", t)]]
    formvec = [0, 1]
    with pytest.raises(ValueError):
        JlSys.jl_symvec_pad_vectorial(JlSys.jl, symvec, formvec, t)

