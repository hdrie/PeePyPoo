import pytest
from peepypoo import SignalTypes
import numpy as np
import pandas as pd


def test_get_signal_class_int():
    """
    Tests the get_signal_class function with an integer.
    """
    assert type(SignalTypes.get_signal_class(1)) is SignalTypes.SignalType

def test_get_signal_class_list():
    """
    Tests the get_signal_class function with a list.
    """
    assert type(SignalTypes.get_signal_class([1, "Hi", np.zeros(12)])) \
           is SignalTypes.ListSignals


def test_get_signal_class_np():
    """
    Tests the get_signal_class function with a numpy array.
    """
    assert type(SignalTypes.get_signal_class(np.array([1, 2, 3]))) \
           is SignalTypes.NumpySignals


def test_get_signal_class_dict():
    """
    Tests the get_signal_class function with a dict.
    """
    assert type(SignalTypes.get_signal_class({"key": "val", 1: 2, "t": 2})) \
           is SignalTypes.DictSignals


def test_get_signal_class_pd_df():
    """
    Tests the get_signal_class function with a pandas DF.
    """
    assert type(SignalTypes.get_signal_class(pd.DataFrame([0, 1, 2]))) \
           is SignalTypes.PandasDFSignals


def test_get_signal_class_incompatible():
    """
    Tests the get_signal_class for a not implemented type.
    Currently, the incompatible type is type itself, exchange it if it gets
    implemented.
    """
    with pytest.raises(TypeError):
        SignalTypes.get_signal_class(int)


def test_list_compatible_size():
    """
    Tests the compatibility of signals of numpy type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class([1, "Hi", pd.DataFrame([1, 2, 3]),
                                         np.array([1, 2, 3]), [1, 2.0, 3]])
    typ2 = SignalTypes.get_signal_class([25, "World", pd.DataFrame([9, 5, 13]),
                                         np.array([88, 22, 1]), [21, 3.5, 2]])

    assert typ1.is_compatible(typ2)


def test_list_incompatible_size():
    """
    Tests the compatibility of signals of numpy type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class([1, 2, 3])
    typ2 = SignalTypes.get_signal_class([4, 5])

    assert not typ1.is_compatible(typ2)


def test_list_incompatible_dtype():
    """
    Tests the compatibility of signals of numpy type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class([1, "Hi", 3.2])
    typ2 = SignalTypes.get_signal_class([4, "World", 6])

    assert not typ1.is_compatible(typ2)


def test_list_zero():
    """
    Tests the zero element of list signals.
    """
    typ = SignalTypes.get_signal_class([1, "Hi", np.array([1, 2, 3])])

    assert all([a == b if isinstance(a == b, bool) else all(a == b)
                for a, b in zip(typ.get_zero(), [0, "", np.zeros(3)])])


def test_np_class_compatible_size():
    """
    Tests the compatibility of signals of numpy type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class(np.array([1, 2, 3]))
    typ2 = SignalTypes.get_signal_class(np.array([4, 5, 6]))

    assert typ1.is_compatible(typ2)


def test_np_class_incompatible_size():
    """
    Tests the compatibility of signals of numpy type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class(np.array([1, 2, 3]))
    typ2 = SignalTypes.get_signal_class(np.array([4, 5]))

    assert not typ1.is_compatible(typ2)


def test_np_class_incompatible_dtype():
    """
    Tests the compatibility of signals of numpy type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class(np.array([1, 2, 3]))
    typ2 = SignalTypes.get_signal_class(np.array([4, 5, 6], dtype=float))

    assert not typ1.is_compatible(typ2)


def test_np_zero():
    """
    Tests the compatibility of signals of numpy type depending on their size.
    """
    typ = SignalTypes.get_signal_class(np.array([1, 2, 3]))

    assert np.array_equal(typ.get_zero(), np.zeros(3))


def test_dict_class_compatible():
    """
    Tests the compatibility of signals of dict type.
    """
    typ1 = SignalTypes.get_signal_class({"key": "val", 1: 2, "t": 3})
    typ2 = SignalTypes.get_signal_class({"key": "sd", 1: 54, "t": 65})

    assert typ1.is_compatible(typ2)


def test_dict_class_incompatible_size():
    """
    Tests the compatibility of signals of dict type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class({"key": "val", 1: 2, "t": 3})
    typ2 = SignalTypes.get_signal_class({"key": "sd", "t": 65})

    assert not typ1.is_compatible(typ2)


def test_dict_class_incompatible_type():
    """
    Tests the compatibility of signals of dict type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class({"key": "val", 1: 2, "t": 3})
    typ2 = SignalTypes.get_signal_class({"key": "val", 1: 2, "t": "Hi"})

    assert not typ1.is_compatible(typ2)


def test_dict_class_incompatible_keys():
    """
    Tests the compatibility of signals of dict type depending on their keys.
    """
    typ1 = SignalTypes.get_signal_class({"key": "val", 1: 2, "t": 3})
    typ2 = SignalTypes.get_signal_class({"key": "val", 2: 2, "t": 3})

    assert not typ1.is_compatible(typ2)


def test_dict_zero():
    """
    Tests the zero of dict signals
    """
    typ = SignalTypes.get_signal_class({"key": "val", 1: 2, "t": 3.2})

    assert typ.get_zero() == {"key": "", 1: 0, "t": 0.0}


def test_pd_df_class_compatible():
    """
    Tests the compatibility of signals of pandas dataframe type.
    """
    typ1 = SignalTypes.get_signal_class(pd.DataFrame([1, 2, 3], columns=["a"],
                                                     index=["d", "e", "f"]))
    typ2 = SignalTypes.get_signal_class(pd.DataFrame([4, 5, 6], columns=["a"],
                                                     index=["d", "e", "f"]))

    assert typ1.is_compatible(typ2)


def test_pd_df_class_incompatible_size():
    """
    Tests the compatibility of signals of pd df type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class(pd.DataFrame([1, 2, 3]))
    typ2 = SignalTypes.get_signal_class(pd.DataFrame([[1, 2, 3], [1, 2, 3]]))

    assert not typ1.is_compatible(typ2)


def test_pd_df_class_incompatible_size2():
    """
    Tests the compatibility of signals of pd df type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class(pd.DataFrame([1, 2, 3]))
    typ2 = SignalTypes.get_signal_class(pd.DataFrame([1, 2, 3, 4]))

    assert not typ1.is_compatible(typ2)


def test_pd_df_class_incompatible_type():
    """
    Tests the compatibility of signals of pd df type depending on their size.
    """
    typ1 = SignalTypes.get_signal_class(pd.DataFrame([[1, 2, 3],
                                                      [2, 3, 4]]))
    typ2 = SignalTypes.get_signal_class(pd.DataFrame([[1, 2, 3],
                                                      ["a", "b", "c"]]))

    assert not typ1.is_compatible(typ2)


def test_pd_df_class_incompatible_col():
    """
    Tests the compatibility of signals of pd df type depending on their columns
    """
    typ1 = SignalTypes.get_signal_class(pd.DataFrame([1, 2, 3], columns=["a"]))
    typ2 = SignalTypes.get_signal_class(pd.DataFrame([1, 2, 3], columns=["b"]))

    assert not typ1.is_compatible(typ2)


def test_pd_df_class_incompatible_index():
    """
    Tests the compatibility of signals of pd df type depending on their index.
    """
    typ1 = SignalTypes.get_signal_class(pd.DataFrame([1, 2, 3],
                                                     index=["a", "b", "c"]))
    typ2 = SignalTypes.get_signal_class(pd.DataFrame([1, 2, 3]))

    assert not typ1.is_compatible(typ2)


def test_pd_df_zero():
    """
    Tests the zero of pd df signals
    """
    typ = SignalTypes.get_signal_class(pd.DataFrame([1, 2, 3, 4]))

    assert typ.get_zero().equals(pd.DataFrame([0, 0, 0, 0]))


def test_str_int():
    """
    Tests the string representation of int signals
    """
    typ = SignalTypes.get_signal_class(1)

    assert str(typ) == "<class \'int\'>"


def test_str_float():
    """
    Tests the string representation of float signals
    """
    typ = SignalTypes.get_signal_class(1.0)

    assert str(typ) == "<class \'float\'>"


def test_str_list():
    """
    Tests the string representation of float signals
    """
    typ = SignalTypes.get_signal_class([1.0, 1, [1, 1.2]])

    assert str(typ) == ("<class \'list\'>[<class \'float\'>, <class \'int\'>, "
                        "<class \'list\'>[<class \'int\'>, <class \'float\'>]]")


def test_str_np():
    """
    Tests the string representation of np signals
    """
    typ = SignalTypes.get_signal_class(np.array([1.0, 1, 1.2]))

    assert str(typ) == "<class 'numpy.ndarray'>(shape=(3,), dtype=float64)"


def test_str_dict():
    """
    Tests the string representation of dict signals
    """
    typ = SignalTypes.get_signal_class({"key": "sd", 1: 54, "t": {"a": 1}})

    assert str(typ) == "<class 'dict'>{key: <class 'str'>, 1: <class 'int'>, t: <class 'dict'>{a: <class 'int'>}}"


def test_str_pd_df():
    """
    Tests the string representation of pd df signals
    """
    typ = SignalTypes.get_signal_class(pd.DataFrame([1, 2, 3], index=["a", "b", "c"]))

    assert str(typ) == ("<class 'pandas.core.frame.DataFrame'>(shape=(3, 1), "
                        "index=['a', 'b', 'c'], columns=[0], types=[dtype('int64')])")
