import pytest
import peepypoo


@pytest.fixture
def system_collection():
    """
    This fixture provides a collection of systems for testing the system event
    callbacks.
    """
    # A constant
    c = peepypoo.Systems.Static.Constant([1.0])

    # Different gains
    g1 = peepypoo.Systems.Static.Gain([2.0])
    g2 = peepypoo.Systems.Static.Gain([3.0])
    g3 = peepypoo.Systems.Static.Gain([4.0])

    return c, g1, g2, g3


def test_input_connection_addition(system_collection):
    """
    Tests if the input connection addition callbacks get executed
    """
    c, g1, g2, g3 = system_collection
    tmp_var = []
    g1.on_input_connection_addition_callbacks.append(
        lambda sys, evt: tmp_var.append("Executed")
    )
    tmp_var1 = []
    g1.on_input_connection_change_callbacks.append(
        lambda sys, evt: tmp_var1.append("Executed")
    )

    c.add_output_connection(g1, [0], [0])
    assert tmp_var and tmp_var1


def test_output_connection_addition(system_collection):
    """
    Tests if the output connection addition callbacks get executed
    """
    c, g1, g2, g3 = system_collection
    tmp_var = []
    c.on_output_connection_addition_callbacks.append(
        lambda sys, evt: tmp_var.append("Executed")
    )
    tmp_var1 = []
    c.on_output_connection_change_callbacks.append(
        lambda sys, evt: tmp_var1.append("Executed")
    )

    c.add_output_connection(g1, [0], [0])
    assert tmp_var and tmp_var1


def test_connection_addition(system_collection):
    """
    Tests if the connection addition callbacks get executed
    """
    c, g1, g2, g3 = system_collection
    tmp_var1 = []
    tmp_var2 = []
    c.on_connection_addition_callbacks.append(
        lambda sys, evt: tmp_var1.append("Executed")
    )
    g1.on_connection_addition_callbacks.append(
        lambda sys, evt: tmp_var2.append("Executed")
    )
    tmp_var3 = []
    tmp_var4 = []
    c.on_connection_change_callbacks.append(
        lambda sys, evt: tmp_var3.append("Executed")
    )
    g1.on_connection_change_callbacks.append(
        lambda sys, evt: tmp_var4.append("Executed")
    )

    c.add_output_connection(g1, [0], [0])
    assert tmp_var1 and tmp_var2 and tmp_var3 and tmp_var4


def test_input_connection_removal(system_collection):
    """
    Tests if the input connection removal callbacks get executed
    """
    c, g1, g2, g3 = system_collection
    tmp_var = []
    g1.on_input_connection_removal_callbacks.append(
        lambda sys, evt: tmp_var.append("Executed")
    )
    tmp_var1 = []
    g1.on_input_connection_change_callbacks.append(
        lambda sys, evt: tmp_var1.append("Executed")
    )

    c.add_output_connection(g1, [0], [0])
    c.remove_connections()
    assert tmp_var and tmp_var1


def test_output_connection_removal(system_collection):
    """
    Tests if the output connection removal callbacks get executed
    """
    c, g1, g2, g3 = system_collection
    tmp_var = []
    c.on_output_connection_removal_callbacks.append(
        lambda sys, evt: tmp_var.append("Executed")
    )
    tmp_var1 = []
    c.on_output_connection_change_callbacks.append(
        lambda sys, evt: tmp_var1.append("Executed")
    )

    c.add_output_connection(g1, [0], [0])
    c.remove_connections()
    assert tmp_var and tmp_var1


def test_connection_removal(system_collection):
    """
    Tests if the connection removal callbacks get executed
    """
    c, g1, g2, g3 = system_collection
    tmp_var1 = []
    tmp_var2 = []
    c.on_connection_removal_callbacks.append(
        lambda sys, evt: tmp_var1.append("Executed")
    )
    g1.on_connection_removal_callbacks.append(
        lambda sys, evt: tmp_var2.append("Executed")
    )
    tmp_var3 = []
    tmp_var4 = []
    c.on_connection_change_callbacks.append(
        lambda sys, evt: tmp_var3.append("Executed")
    )
    g1.on_connection_change_callbacks.append(
        lambda sys, evt: tmp_var4.append("Executed")
    )

    c.add_output_connection(g1, [0], [0])
    c.remove_connections()
    assert tmp_var1 and tmp_var2 and tmp_var3 and tmp_var4


def test_connection_tree_change(system_collection):
    """
    Tests if the connection tree change callbacks get executed
    """
    c, g1, g2, g3 = system_collection
    c.add_output_connection(g1, [0], [0])
    g1.add_output_connection(g2, [0], [0])
    g2.add_output_connection(g3, [0], [0])
    tmp_var1 = []
    c.on_output_connection_tree_change_callbacks.append(
        lambda sys, evt: tmp_var1.append("Executed")
    )
    tmp_var2 = []
    g1.on_output_connection_tree_change_callbacks.append(
        lambda sys, evt: tmp_var2.append("Executed")
    )
    tmp_var3 = []
    g2.on_input_connection_tree_change_callbacks.append(
        lambda sys, evt: tmp_var3.append("Executed")
    )
    tmp_var4 = []
    g3.on_input_connection_tree_change_callbacks.append(
        lambda sys, evt: tmp_var4.append("Executed")
    )
    tmp_var5 = []
    c.on_connection_tree_change_callbacks.append(
        lambda sys, evt: tmp_var5.append("Executed")
    )
    tmp_var6 = []
    g1.on_connection_tree_change_callbacks.append(
        lambda sys, evt: tmp_var6.append("Executed")
    )
    tmp_var7 = []
    g2.on_connection_tree_change_callbacks.append(
        lambda sys, evt: tmp_var7.append("Executed")
    )
    tmp_var8 = []
    g3.on_connection_tree_change_callbacks.append(
        lambda sys, evt: tmp_var8.append("Executed")
    )

    g1.remove_output_connection([0])
    g1.add_output_connection(g2, [0], [0])

    assert len(tmp_var1) == 2\
           and len(tmp_var2) == 2 \
           and len(tmp_var3) == 2 \
           and len(tmp_var4) == 2 \
           and len(tmp_var5) == 2 \
           and len(tmp_var6) == 2 \
           and len(tmp_var7) == 2 \
           and len(tmp_var8) == 2
