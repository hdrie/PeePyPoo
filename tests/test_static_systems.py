import pytest
import sympy

import peepypoo
import numpy as np
import pandas as pd

def test_const_int():
    """
    Tests the constant system with an integer.
    """
    const = peepypoo.Systems.Static.Constant([54])

    assert const.get_output(687) == [54]


def test_const_list():
    """
    Tests the constant system with a list.
    """
    const = peepypoo.Systems.Static.Constant([[21, 24]])

    assert const.get_output(98.24) == [[21, 24]]


def test_const_different_types():
    """
    Tests the constant system with different types.
    """
    const = peepypoo.Systems.Static.Constant([[21, 24], 54])

    assert const.get_output(98.24) == [[21, 24], 54]


def test_const_numpy():
    """
    Tests the constant system with a numpy array.
    """
    const = peepypoo.Systems.Static.Constant([np.array([1, 1, 2, 3, 5, 8, 15])])

    assert np.all(const.get_output(98.24)[0]
                  == np.array([1, 1, 2, 3, 5, 8, 15]))


def test_const_pandas():
    """
    Tests the constant system with a pandas dataframe array.
    """
    const = peepypoo.Systems.Static.Constant([pd.DataFrame([1, 2, 3])])

    assert const.get_output(98.24)[0].equals(pd.DataFrame([1, 2, 3]))


def test_const_type():
    """
    Tests the costant system with an arbitrary object (PytestWarning).
    """
    const = peepypoo.Systems.Static.Constant([pytest.PytestWarning()])

    out = const.get_output(98.24)
    assert type(out[0]) == pytest.PytestWarning and len(out) == 1


def test_const_empty():
    """
    Tests the constant system without any input.
    """
    const = peepypoo.Systems.Static.Constant()

    assert const.get_output(654.2) == []


def test_gain_no_input():
    """
    Tests the gain function without any input
    """
    gain = peepypoo.Systems.Static.Gain([21, 12.05, np.array([1, 2])])

    assert all([a == b
                if not isinstance(a, np.ndarray) else np.array_equal(a, b)
                for a, b in zip(gain.get_output(58),
                                [0, 0.0, np.array([0, 0])])])


def test_gain_empty():
    """
    Tests the gain system with an empty gain
    """
    gain = peepypoo.Systems.Static.Gain()

    assert gain.get_output(38.12) == [] \
           and gain.output_formula == sympy.Tuple()


def test_gain_input():
    """
    Tests the gain function with corresponding input from a const block
    """
    const = peepypoo.Systems.Static.Constant([2, 0.5, np.array([2, 3, 4])])
    gain = peepypoo.Systems.Static.Gain([21, 12.05, np.array([1, 2, 3])])

    const.add_output_connection(gain, [0, 1, 2], [0, 1, 2])

    assert all([np.array_equal(a, b) if isinstance(a, np.ndarray) else a == b
               for a, b in zip(gain.get_output(58),
                               [42, 6.025, np.array([2, 6, 12])])])


def test_cast_empty():
    """
    Tests the cast system instantiated without input
    """
    cast = peepypoo.Systems.Static.Cast()

    assert cast.get_output(54.2) == []


def test_cast_int_to_float():
    """
    Tests the cast system casting an into to float
    """
    const = peepypoo.Systems.Static.Constant([1])
    cast = peepypoo.Systems.Static.Cast(
        [peepypoo.SignalTypes.get_signal_class(1)],
        [peepypoo.SignalTypes.get_signal_class(1.0)])

    cast.add_input_connection(const, [0], [0])

    assert cast.get_output(54.2)[0] == 1.0


def test_cast_non_matching_lengths():
    """
    Tests the cast system with not matching input and output type lengths
    """
    with pytest.raises(ValueError):
        cast = peepypoo.Systems.Static.Cast(
            [peepypoo.SignalTypes.get_signal_class(1)])


def test_piecewise_lin_zero():
    """
    Tests the peicewise linear interpolation system with zero extrapolation.
    """
    tol = 1e-6
    s = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([1, 2, 3, 4])], [np.array([0, 10, 2, 5]),
                                   np.arange(15, 23).reshape(2, -1)],
        extrapolation="zero")

    out = []
    for i, val in enumerate(range(0, 9)):
        out.append(s.get_output(val+0.5))
    out = np.array(out, dtype=object)

    assert np.all(out[:, 0] - [0, 5, 6, 3.5, 0, 0, 0, 0, 0] < tol) \
        and np.all(np.array([o.tolist() for o in out[:, 1]])
                   - [[0, 0],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [0, 0],
                      [0, 0],
                      [0, 0],
                      [0, 0],
                      [0, 0]] < tol)


def test_piecewise_lin_const():
    """
    Tests the peicewise linear interpolation system with constant extrapolation
    """
    tol = 1e-6
    s = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([1, 2, 3, 4])], [np.array([0, 10, 2, 5]),
                                   np.arange(15, 23).reshape(2, -1)],
        extrapolation="const")

    out = []
    for i, val in enumerate(range(0, 9)):
        out.append(s.get_output(val+.5))
    out = np.array(out, dtype=object)

    assert np.all(out[:, 0] - [0, 5, 6, 3.5, 5, 5, 5, 5, 5] < tol) \
        and np.all(np.array([o.tolist() for o in out[:, 1]])
                   - [[15, 19],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [18, 22],
                      [18, 22],
                      [18, 22],
                      [18, 22],
                      [18, 22]] < tol)


def test_piecewise_lin_linear():
    """
    Tests the peicewise linear interpolation system with linear extrapolation
    """
    tol = 1e-6
    s = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([1, 2, 3, 4])], [np.array([0, 10, 2, 5]),
                                   np.arange(15, 23).reshape(2, -1)],
        extrapolation="linear")

    out = []
    for i, val in enumerate(range(0, 9)):
        out.append(s.get_output(val+0.5))
    out = np.array(out, dtype=object)

    assert np.all(out[:, 0] - [-5, 5, 6, 3.5, 6.5, 9.5, 12.5, 15.5, 18.5]
                  < tol) \
        and np.all(np.array([o.tolist() for o in out[:, 1]])
                   - [[14.5, 18.5],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [18.5, 22.5],
                      [19.5, 23.5],
                      [20.5, 24.5],
                      [21.5, 25.5],
                      [22.5, 26.5]] < tol)


def test_piecewise_lin_periodic():
    """
    Tests the peicewise linear interpolation system with periodic extrapolation
    """
    tol = 1e-6
    s = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([1, 2, 3, 4], dtype=int)],
        [np.array([0, 10, 2, 5]), np.arange(15, 23).reshape(2, -1)],
        extrapolation="periodic")

    out = []
    for i, val in enumerate(range(0, 9)):
        out.append(s.get_output(val+0.5))
    out = np.array(out, dtype=object)

    assert np.all(out[:, 0] - [3.5, 5, 6, 3.5, 5, 6, 3.5, 5, 6] < tol) \
        and np.all(np.array([o.tolist() for o in out[:, 1]])
                   - [[17.5, 21.5],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [15.5, 19.5],
                      [16.5, 20.5]] < tol)


@pytest.mark.julia
def test_piecewise_lin_zero_jl():
    """
    Tests the peicewise linear interpolation system with zero extrapolation
    in julia.
    """
    tol = 1e-6
    s = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([1, 2, 3, 4])], [np.array([0, 10, 2, 5]),
                                   np.arange(15, 23).reshape(2, -1)],
        extrapolation="zero")

    wrapper = peepypoo.BDWrapper([s])
    out = wrapper.evaluate_blockdiagram_julia(0, 8, np.arange(0.5, 9, 1))
    out = np.array(out[s].to_list())

    assert np.all(out[:, 0] - [0, 5, 6, 3.5, 0, 0, 0, 0, 0] < tol) \
        and np.all(np.array([o.tolist() for o in out[:, 1]])
                   - [[0, 0],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [0, 0],
                      [0, 0],
                      [0, 0],
                      [0, 0],
                      [0, 0]] < tol)


@pytest.mark.julia
def test_piecewise_lin_const_jl():
    """
    Tests the peicewise linear interpolation system with constant extrapolation
    in julia.
    """
    tol = 1e-6
    s = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([1, 2, 3, 4])], [np.array([0, 10, 2, 5]),
                                   np.arange(15, 23).reshape(2, -1)],
        extrapolation="const")

    wrapper = peepypoo.BDWrapper([s])
    out = wrapper.evaluate_blockdiagram_julia(0, 8, np.arange(0.5, 9, 1))
    out = np.array(out[s].to_list())

    assert np.all(out[:, 0] - [0, 5, 6, 3.5, 5, 5, 5, 5, 5] < tol) \
        and np.all(np.array([o.tolist() for o in out[:, 1]])
                   - [[15, 19],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [18, 22],
                      [18, 22],
                      [18, 22],
                      [18, 22],
                      [18, 22]] < tol)


@pytest.mark.julia
def test_piecewise_lin_linear_jl():
    """
    Tests the peicewise linear interpolation system with linear extrapolation
    in julia.
    """
    tol = 1e-6
    s = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([1, 2, 3, 4])], [np.array([0, 10, 2, 5]),
                                   np.arange(15, 23).reshape(2, -1)],
        extrapolation="linear")

    wrapper = peepypoo.BDWrapper([s])
    out = wrapper.evaluate_blockdiagram_julia(0, 8, np.arange(0.5, 9, 1))
    out = np.array(out[s].to_list())

    assert np.all(out[:, 0] - [-5, 5, 6, 3.5, 6.5, 9.5, 12.5, 15.5, 18.5]
                  < tol) \
        and np.all(np.array([o.tolist() for o in out[:, 1]])
                   - [[14.5, 18.5],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [18.5, 22.5],
                      [19.5, 23.5],
                      [20.5, 24.5],
                      [21.5, 25.5],
                      [22.5, 26.5]] < tol)


@pytest.mark.julia
def test_piecewise_lin_periodic_jl():
    """
    Tests the peicewise linear interpolation system with periodic extrapolation
    in julia.
    """
    tol = 1e-6
    s = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([1, 2, 3, 4], dtype=int)], [np.array([0, 10, 2, 5]),
                                   np.arange(15, 23).reshape(2, -1)],
        extrapolation="periodic")

    wrapper = peepypoo.BDWrapper([s])
    out = wrapper.evaluate_blockdiagram_julia(0, 8, np.arange(0.5, 9, 1))
    out = np.array(out[s].to_list())

    assert np.all(out[:, 0] - [3.5, 5, 6, 3.5, 5, 6, 3.5, 5, 6] < tol) \
        and np.all(np.array([o.tolist() for o in out[:, 1]])
                   - [[17.5, 21.5],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [15.5, 19.5],
                      [16.5, 20.5]] < tol)


def test_piecewise_lin_invalid_sizes_1():
    """
    Tests if the PiecewiseLinearInterpolation system throws an error with
    incompatible sizes of time vector list and data vector list
    """
    with pytest.raises(ValueError):
        peepypoo.Systems.Static.PiecewiseLinearInterpolation(
            [np.array([1, 2, 3, 4], dtype=int),
             np.array([1, 2, 3, 4], dtype=int)],
            [np.array([0, 10, 2, 5]),
             np.array([0, 10, 2, 5]),
             np.arange(15, 23).reshape(2, -1)],
            extrapolation="periodic")


def test_piecewise_lin_invalid_sizes_2():
    """
    Tests if the PiecewiseLinearInterpolation system throws an error with
    incompatible sizes of time vector and corresponding data vector
    """
    with pytest.raises(ValueError):
        peepypoo.Systems.Static.PiecewiseLinearInterpolation(
            [np.array([1, 2, 3, 4, 5], dtype=int),
             np.array([1, 2, 3, 4], dtype=int)],
            [np.array([0, 10, 2, 5]),
             np.arange(15, 25).reshape(2, -1)],
            extrapolation="periodic")


def test_piecewise_lin_multi_dim_t():
    """
    Tests the peicewise linear interpolation system with periodic extrapolation
    in julia.
    """
    tol = 1e-6
    s = peepypoo.Systems.Static.PiecewiseLinearInterpolation(
        [np.array([1, 2, 3, 4], dtype=int).reshape(1, -1)],
        [np.array([0, 10, 2, 5]), np.arange(15, 23).reshape(2, -1)],
        extrapolation="periodic")

    wrapper = peepypoo.BDWrapper([s])
    out = wrapper.evaluate_blockdiagram_julia(0, 8, np.arange(0.5, 9, 1))
    out = np.array(out[s].to_list())

    assert np.all(out[:, 0] - [3.5, 5, 6, 3.5, 5, 6, 3.5, 5, 6] < tol) \
        and np.all(np.array([o.tolist() for o in out[:, 1]])
                   - [[17.5, 21.5],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [15.5, 19.5],
                      [16.5, 20.5],
                      [17.5, 21.5],
                      [15.5, 19.5],
                      [16.5, 20.5]] < tol)
