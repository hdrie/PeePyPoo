import math
import numpy as np
import pandas as pd
import typing
import warnings

from peepypoo.Systems._System import System

__all__ = ['SignalLogger']


class SignalLogger(System):
    """
    A system to log signals.

    This system records the signals at each sampling time and records it. In
    the end, the data can be read out as a pandas dataframe.
    This system has a speciality in the input ports, and that is that they are
    not fixed, i.e. in the beginning it has zero inputs but on each connection
    of a system a new input port gets added and the requested signal is
    connected to it. Similarly, on removal of the connection the input port
    gets removed.

    :param dt: The sampling time to record at
    :param name: The name of the system
    """

    dt: float
    """ Sampling time for logging the signal """
    t: np.array
    """ Array with the recorded times """
    states: np.ndarray
    """ Array with the recorded states """
    state_list: typing.List[typing.Tuple[System, int]]
    """ List of the states which are recorded (system and port) """
    __adding_logging_signal: bool
    """ If currently a logging signal is added """
    __removing_logging_signal: bool
    """ If currently a logging signal is removed """

    def __init__(self, dt: float, name: str = "Logger"):
        super().__init__(num_inputs=0, num_outputs=0, name=name)

        self.dt = dt
        self.t = np.empty((0,), dtype=float)
        self.states = np.empty((0, 0), dtype=object)
        self.state_list = []
        self.__adding_logging_signal = False
        self.__removing_logging_signal = False

        # Reset the system if the input connection tree changes
        self.on_input_connection_tree_change_callbacks\
            .append(lambda sys, event: self.reset())

    def get_output(self, t: float) -> typing.List[typing.Any]:
        """
        This system has no output. The logged signal is handled otherwise.

        :param t: Time to get the output at
        :return: The output at the given time (Empty for this system)
        """
        return []

    def add_logging_signal(self, system: System, ports: typing.List[int]) \
            -> None:
        """
        Add a signal to be logged.

        This function adds signals to be logged to this system. Note that this
        resets all saved states.

        :param system: The system of which the output is to be logged
        :param ports: The output ports of the system which are to be logged
        :return: None
        """
        if not self.__adding_logging_signal:
            self.__adding_logging_signal = True
            self.state_list.extend([(system, port) for port in ports])
            types = [system.output_types[i] for i in ports]
            self._add_input_port(len(types), types)
            super().add_input_connection(
                system,
                [*range(self.num_inputs-len(ports), self.num_inputs)],
                ports
            )

            self.__adding_logging_signal = False

    def remove_logging_signal(self, ports: typing.List[int]) -> None:
        """
        Remove a logged signal.

        This removes a logged signal. Note that this resets all saved states.

        :param ports: The ports of the signals to be removed.
        :return: None
        """
        if not self.__removing_logging_signal:
            self.__removing_logging_signal = True
            self.state_list = [self.state_list[i]
                               for i in range(self.num_inputs)
                               if i not in ports]
            super().remove_input_connection(ports)
            self._remove_input_port(ports)

            self.__removing_logging_signal = False

    def add_input_connection(self, system: 'System',
                             input_ports: typing.List[int],
                             output_ports: typing.List[int]) -> None:
        """
        Wrapper to add a logging signal on adding input connections.
        This enables logging a signal by adding an output connection to this
        system.

        :param system: The system providing the output to be logged
        :param input_ports: Ignored
        :param output_ports: The output ports to be logged
        :return: None
        """
        self.add_logging_signal(system, output_ports)

    def remove_input_connection(self, input_ports: typing.List[int]) -> None:
        """
        Wrapper to remove a logging signal on removing input connections.
        This enables removing the logging of a signal by removing the output
        connection from another system.

        :param input_ports: The input ports to remove
        :return: None
        """
        self.remove_logging_signal(input_ports)

    def reset(self) -> None:
        """
        Resets the signal logger saved states.

        :return: None
        """
        self.t = np.empty((0,), dtype=float)
        self.states = np.empty((self.num_inputs, 0), dtype=object)

    def log_data(self, t_init: float = None, t_final: float = None) -> None:
        """
        Log the incoming data between the given times. If any of the times is
        ``None``, the maximum, resp. minimum, time already recorded is taken as
        beginning, resp. ending. If any limit is ``None`` but no data was
        logged yet, an error is thrown.

        :param t_init: Time to start logging. If ``None``, the maximum time
            recorded is taken. Defaults to ``None``
        :param t_final: Time to stop the logging. If ``None``, the minimum time
            recorded is taken. Defaults to ``None``
        :return: None

        :raises ValueError: If one of the times is not finite or if one of the
            limits is to be inferred, but no data is recorded yet.
        """
        # Set current limit if
        if t_init is None:
            t_init = self.t.max(initial=-np.inf)
            if np.isinf(t_init):
                raise ValueError("SignalLogger: Cannot infer initial time if "
                                 "no data recorded yet.")
        if t_final is None:
            t_final = self.t.min(initial=np.inf)
            if np.isinf(t_final):
                raise ValueError("SignalLogger: Cannot infer final time if "
                                 "no data recorded yet.")
        if not np.isfinite(t_init) or not np.isfinite(t_final):
            raise ValueError("Input and output times must be finite (neither "
                             "NaN nor +-Inf). Provided values are: t_init={}, "
                             "t_final={}.".format(t_init, t_final))
        if t_final < t_init:
            return
        # Get vector of requested times
        numel = math.ceil((t_final - t_init)/self.dt) + 1
        t_req = np.linspace(t_init, t_final, numel)
        # Filter out already recorded times
        t_req = t_req[np.bitwise_not(np.isin(t_req, self.t))]
        # Get number of times to calculate and the index of the first one
        numel = len(t_req)
        idx = len(self.t)
        # Add time to time vector
        self.t = np.append(self.t, t_req)
        # Preallocate memory for states
        self.states = np.append(
            self.states,
            np.empty((self.states.shape[0], numel), dtype=object),
            axis=1
        )
        # Get the state at each time
        for i in range(idx, idx + numel):
            t = self.t[i]
            self.states[:, i] = self._get_inputs(t)
        # Sort for ascending t
        idxs = self.t.argsort()
        self.t = self.t[idxs]
        self.states = self.states[:, idxs]
        # Make times unique
        mask = np.append([True], np.diff(self.t) != 0)
        self.t = self.t[mask]
        self.states = self.states[:, mask]

    def get_logged_data(self, t_start: float = -np.inf,
                        t_end: float = np.inf) -> pd.DataFrame:
        """
        Returns the logged data.

        This system returns the logged data between the given time intervals.

        :param t_start: Start time of the interval. -Inf if no minimum
        :param t_end: End time of the interval. +Inf if no maximum
        :return: Pandas dataframe with the logged data. Each column contains
            the logged data of one input port.
        """
        idx = np.where(np.bitwise_and(self.t >= t_start, self.t <= t_end))
        sys = list(set([s[0] for s in self.state_list]))  # Get all unique systems
        sys_name_dict = dict([(s, s.name) for s in sys])  # Get system names, dtype=object for non-fixed length
        sys = np.array([*sys_name_dict.keys()])
        sys_names = np.array([*sys_name_dict.values()])
        for i in range(len(sys)):  # Append the number of previous occurrences to the system name
            if np.sum(sys_names[:i] == sys_names[i]) > 0:
                warnings.warn("There are multiple systems named '{n}' in the logger. Enumerating them which might not "
                              "be consistent over different runs.".format(n=sys_names[i]))
                sys_name_dict[sys[i]] = sys_names[i] + str(np.sum(sys_names[:i] == sys_names[i]))

        return pd.DataFrame(
            self.states[:, idx[0]].T, index=self.t[idx],
            columns=[sys_name_dict[state[0]] + ": Output " + str(state[1])
                     for state in self.state_list]
        )
