import abc
import copy
import math

import juliacall
import numpy as np
import sympy
import typing

import peepypoo.Utilities as Utilities

from peepypoo.SignalTypes import get_signal_class, SignalType
from peepypoo.Systems._System import System, \
    JuliaModelingToolkitConvertibleSystem

__all__ = ['JuliaModelingToolkitConvertibleStaticSystem',
           'Constant', 'PiecewiseLinearInterpolation', 'Gain', 'Cast']


#######################################################
# Julia Convertible Static system
#######################################################
class JuliaModelingToolkitConvertibleStaticSystem(
    JuliaModelingToolkitConvertibleSystem, abc.ABC
):
    """
    An abstract class which allows inheriting static systems to just define the
    symbolic equations describing the input-output relationship to
    automatically transform it to julia.
    For this functionality to be enabled, the inheriting system must set the
    ``output_formula`` variable.

    :param num_inputs: The number of inputs of the dynamical system
    :param num_outputs: The number of outputs of the dynamical system
    :param input_types: The types of the input signals
    :param output_types: The types of the output signals
    :param t_sym: Sympy symbol denoting the time variable. Defaults to t.
    :param inputs: Sympy symbols for the system inputs. Defaults to u0 to un,
        where n is the given number of inputs.
    :param outputs: Sympy symbols for the system outputs. Defaults to y0 to yn,
        where n is the given number of outputs.
    :param name: The name of the system
    """

    # Symbolic values
    _t_sym: sympy.Symbol
    """ Sympy symbol for time """
    @property
    def t(self) -> sympy.Symbol:
        """ Sympy symbol for time """
        return self._t_sym

    @t.setter
    def t(self, value: sympy.Symbol) -> None:
        self._t_sym = value

    _inputs: typing.List[sympy.Array]
    """ The names of the inputs """

    @property
    def inputs(self) -> typing.List[sympy.Array]:
        """ The names of the inputs """
        return self._inputs

    @inputs.setter
    def inputs(self, value: typing.List[sympy.Array]) -> None:
        self._inputs = value

    _outputs: typing.List[sympy.Array]
    """ The names of the outputs """

    @property
    def outputs(self) -> typing.List[sympy.Array]:
        """ The names of the outputs """
        return self._outputs

    @outputs.setter
    def outputs(self, value: typing.List[sympy.Array]) -> None:
        self._outputs = value

    _output_formula: sympy.Tuple
    """ The symbolic output formula """
    _output_lambda: typing.Callable[[typing.List, float], np.ndarray]
    """ Lambda of the symbolic output formula """

    @property
    def output_formula(self) -> sympy.Tuple:
        """
        Sympy equations for calculating the derivative/next timestep depending
        on the current state, parameters and input
        """
        return self._output_formula

    @output_formula.setter
    def output_formula(self, value: sympy.Tuple) -> None:
        if not value.free_symbols.issubset({self.t}.union(
                *[v.free_symbols for v in self.inputs])):
            raise ValueError("The symbolic output equation contains symbols "
                             "which are not in the state, input or parameter "
                             "vector nor time.")
        self._output_lambda = sympy.lambdify([self.inputs, self.t], value, modules=None)
        self._output_formula = value

    def __init__(self,
                 num_inputs: int = 0,
                 num_outputs: int = 0,
                 input_types: typing.List[SignalType] = None,
                 output_types: typing.List[SignalType] = None,
                 t_sym: sympy.Symbol = sympy.symbols('t'),
                 inputs: typing.List[sympy.Symbol] | None = None,
                 outputs: typing.List[sympy.Symbol] | None = None,
                 name: str = "Julia Convertible Static System"):
        if input_types is None:
            input_types = [get_signal_class(None)] * num_inputs
        if output_types is None:
            output_types = [get_signal_class(None)] * num_outputs
        if inputs is None:
            inputs = sympy.Array([sympy.Symbol('u'+str(i))
                                  for i in range(num_inputs)])
        if outputs is None:
            outputs = sympy.Array([sympy.Symbol('y'+str(i))
                                   for i in range(num_outputs)])
        System.__init__(self, num_inputs, num_outputs, name=name)
        self.input_types = input_types
        self.output_types = output_types
        self.t = t_sym
        self.inputs = inputs
        self.outputs = outputs

    def _compile_julia_system(self) -> None:
        """
        Get the Julia ODESystem of the Static System.

        Compiles the Julia ODESystem representing this static system and the
        two vectors for the inputs to and outputs from the systems.
        :return: None
        """
        # Call superclass method for basic definitions
        super()._compile_julia_system()
        # Get the julia t, inputs and outputs
        t_jl = self.jl_macro_vars(self.jl, str(self._t_sym), "var")
        inp_jl = Utilities.apply_nested_list(
            lambda x, t:
            self.jl_macro_vars(self.jl, str(x), "var", [t_jl])
            if x else [],
            self.inputs
        )
        system_outp_vec = Utilities.apply_nested_list(
            lambda x, t:
            self.jl_macro_vars(self.jl, str(x), "var", [t_jl])
            if x else [],
            self.outputs
        )
        # Get the outputs system
        outp_jl = self._output_lambda(inp_jl, t_jl)

        # Make multiple output variables if not yet
        system_outp_vec = self.jl_symvec_pad_vectorial(
            self.jl, system_outp_vec, outp_jl, t_jl)

        eqs = [self.jl.Equation(y, outp)
               for y, outp in zip(Utilities.flatten(system_outp_vec),
                                  Utilities.flatten(outp_jl))]
        eqs = juliacall.convert(self.jl.Vector, eqs)
        sys = self.jl.ODESystem(
            eqs, t_jl,
            name=self.jl.Symbol(self.name + "_" + str(id(self)))
        )

        # Assign to variables
        self._t_jl = t_jl
        self._inputs_jl = Utilities.apply_nested_list(
            lambda x, t: getattr(sys, str(x)) if x else [], self.inputs
        )
        self._outputs_jl = Utilities.apply_nested_list(  # Get output variables
            lambda x, t: getattr(sys, str(x)[:-2 - len(str(t_jl))])
            if x else [],
            system_outp_vec
        )
        # Rearrange output variables to match the output definition
        self._outputs_jl = [*Utilities.flatten(self._outputs_jl)]
        self._outputs_jl = Utilities.apply_nested_list(
            lambda x, t: self._outputs_jl[
                Utilities.nested_idx_to_linear_idx(self.outputs, t)
            ],
            self.outputs
        )
        self._system_jl = sys
        self._ic_jl = self.jl.Dict()


#######################################################
# A named constant block
#######################################################
class Constant(JuliaModelingToolkitConvertibleSystem):
    """
    A system which outputs a constant. The output constants can be named.

    This system simply outputs a user-defined constant output. It can have
    arbitrary type.

    The resulting system has the following outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    |Output| 0     | y0      | ?                | The first element of the    |
    |      |       |         |                  | provided list with the data |
    |      |       |         |                  | to output.                  |
    +      +-------+---------+------------------+-----------------------------+
    |      | ...   | yi      | ?                | Repeated until all elements |
    |      |       |         |                  | of the list with the data   |
    |      |       |         |                  | to output has its port.     |
    +------+-------+---------+------------------+-----------------------------+

    :param output: A list with the constant output of the system. Each element gets its own output port.
    :param output_names: The names of the output of the system. A list with the same length as the output.
    :param name: The name of the system
    """

    __output: typing.List[typing.Any]
    """ The output of the constant block"""
    __output_names: typing.List[str]
    """ The names of the constant block output """

    def __init__(self,
                 output: typing.List[typing.Any] = None,
                 output_names: typing.List[str] = None,
                 name: str = "Constant"):
        if output is None:
            output = []
        if output_names is None:
            output_names = ['y' + str(i) for i in range(len(output))]
        super().__init__(num_inputs=0, num_outputs=len(output), name=name)
        self.__output_names = output_names
        self.__output = output
        self.output_types = [get_signal_class(out)
                             for out in self.__output]

    def get_output(self, t: float) -> typing.List[typing.Any]:
        """
        Get the constant output of this constant block.

        :param t: Time to get the output at. Ignored as the output is constant
        :return: The constant output of the system.
        """
        return copy.deepcopy(self.__output)

    def _compile_julia_system(self) -> None:
        # Call superclass method for basic definitions
        super()._compile_julia_system()
        outputs_sp = Utilities.apply_nested_list(
            lambda x, t: sympy.Symbol(self.__output_names[t[0]]
                                      + ('_' + '_'.join([str(i)
                                                         for i in t[1:]])
                                         if len(t) > 1 else
                                         '')
                                      ),
            self.__output
        )
        t_jl = self.jl_macro_vars(self.jl, 't', "var")
        output_jl = [self.jl_macro_vars(self.jl, str(outp), "var", [t_jl])
                     for outp in Utilities.flatten(outputs_sp)]

        eqs = [self.jl.Equation(y, juliacall.convert(self.jl.Any, outp))
               for y, outp in zip(output_jl, [*Utilities.flatten(self.__output)])]
        eqs = juliacall.convert(self.jl.Vector, eqs)
        sys = self.jl.ODESystem(
            eqs, t_jl,
            name=self.jl.Symbol(self.name + "_" + str(id(self)))
        )

        outputs_jl = Utilities.apply_nested_list(
            lambda x, t: getattr(sys, str(x)),
            outputs_sp
        )

        self._system_jl = sys
        self._t_jl = t_jl
        self._inputs_jl = []
        self._outputs_jl = outputs_jl
        self._ic_jl = self.jl.Dict()


#######################################################
# A named block for piecewise linear functions
#######################################################
class PiecewiseLinearInterpolation(JuliaModelingToolkitConvertibleSystem):
    """
    A system for providing input from a linear interpolation of data in a list.

    The resulting system has the following outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    |Output| 0     | y0      | float or         | The provided data to output |
    |      |       |         | np.array[float]  | interpolated to the given t.|
    +      +-------+---------+------------------+-----------------------------+
    |      | ...   | yi      | ?                | Repeated until all provided |
    |      |       |         |                  | data has its port.          |
    +------+-------+---------+------------------+-----------------------------+

    :param t: List of numpy arrays with the times of the timeseries to
        interpolate. Must either contain one numpy array for all data or one
        for each data list.
    :param y: List of numpy arrays containing each the data to interpolate
        inbetween. Must either be a one dimensional array of the same length as
        the corresponding time vector or have the same number of columns (2nd
        dimension) as the time vector has entries.
    :param extrapolation: The extrapolation method to use if the given time is
        outside the interval of the time vector. Available options:

        zero
            Output zero if outside the interval

        constant
            Output the first value if earlier than the first time or the last
            value if later than the last time.

        linear
            Extrapolate using the closest interval, i.e. the line between the
            first and the second time if earlier than the first time or the
            line between the last two times if later than the last time.

        periodic
            Repeat the given sequence periodically. The last time already
            contains the first value of the series.

    :param output_names: The names of the output of the system
    :param name: The name of the system
    """

    _t: typing.List[np.ndarray]
    """ The time values of the piecewise function """
    _y: typing.List[np.ndarray]
    """ The values to return at the respective time """
    _extrapolation: typing.Literal["zero", "const", "linear", "periodic"]
    """ The type of extrapolation """
    __output_names: typing.List[str]
    """ The names of the piecewise linear output """

    # Import DataInterpolations for interpolation
    JuliaModelingToolkitConvertibleSystem.jl.seval("using DataInterpolations")

    def __init__(self, t: typing.List[np.ndarray], y: typing.List[np.ndarray],
                 extrapolation: typing.Literal["zero", "const", "linear",
                 "periodic"] = "zero",
                 output_names: typing.List[str] = None,
                 name: str = "PiecewiseLinearInterpolation"):
        if output_names is None:
            output_names = ['y' + str(i) for i in range(len(y))]
        if len(t) != len(y) and len(t) != 1:
            raise ValueError("PiecewiseLinearInterpolation error: The time "
                             "input must either have a single time vector for "
                             "each output or the same number of time vectors "
                             "and outputs.")
        if len(t) == 1 and len(y) > 1:
            t = t*len(y)
        # Ensure that t is 1d array and that y is at least 2D
        for i in range(len(t)):
            if t[i].ndim != 1:
                t[i].shape = (-1,)
            if y[i].ndim == 1:
                y[i].shape = (1, -1)
        if any([len(tv) != yv.shape[1] for tv, yv in zip(t, y)]):
            raise ValueError("PiecewiseLinearInterpolation error: The time "
                             "vector for each output must have the same "
                             "number values as columns in the value vector.")
        super().__init__(num_inputs=0, num_outputs=len(y), name=name)
        # Sort all t and y in increasing order
        for i in range(len(t)):
            idx = np.argsort(t[i])
            t[i] = np.take_along_axis(t[i], idx, axis=0)
            y[i] = np.take_along_axis(
                y[i],
                np.reshape(idx, (1, -1, *np.ones((y[i].ndim-2), dtype=int))),
                axis=1
            )
        self._t = t
        self._y = y
        self._extrapolation = extrapolation
        self.__output_names = output_names
        self.output_types = [get_signal_class(out[:, 1, ...]
                                              if len(out[:, 1, ...]) > 1
                                              else float(0))
                             for out in y]

    def get_output(self, t: float) -> typing.List[typing.Any]:
        """
        Get the piecewise linear interpolated value of the sequence.

        :param t: Time to get the output at.
        :return: The result of the piecewise linear interpolation of the given
            sequences.
        """
        out = [0]*len(self._t)
        for i, (t_vec, y_vec) in enumerate(zip(self._t, self._y)):
            if t < t_vec[0] or t_vec[-1] < t:
                # Use extrapolation
                match self._extrapolation:
                    case "zero":
                        out_t = y_vec[:, 0, ...]*0
                        out[i] = out_t[0] if len(out_t) == 1 else out_t
                        continue
                    case "const":
                        out_t = y_vec[:, 0, ...] \
                            if t < t_vec[0] else y_vec[:, -1, ...]
                        out[i] = out_t[0] if len(out_t) == 1 else out_t
                        continue
                    case "linear":
                        if t < t_vec[0]:
                            out_t = ((y_vec[:, 0, ...] - y_vec[:, 1, ...])
                                      / (t_vec[0] - t_vec[1])
                                      * (t - t_vec[1])) + y_vec[:, 1, ...]
                        else:
                            out_t = ((y_vec[:, -1, ...] - y_vec[:, -2, ...])
                                      / (t_vec[-1] - t_vec[-2])
                                      * (t - t_vec[-2])) + y_vec[:, -2, ...]
                        out[i] = out_t[0] if len(out_t) == 1 else out_t
                        continue
                    case "periodic":
                        t = (t - t_vec[0]) % (t_vec[-1] - t_vec[0]) + t_vec[0]
            # idx s.t. t_vec[i-1] <= t < t_vec[i]
            idx = t_vec.searchsorted(t, side='right')
            idx = idx-1 if idx == len(t_vec) else idx
            # Interpolation
            out_t = ((y_vec[:, idx, ...] - y_vec[:, idx-1, ...])
                     / (t_vec[idx] - t_vec[idx-1]) * (t - t_vec[idx-1])) \
                + y_vec[:, idx-1, ...]

            out[i] = out_t[0] if len(out_t) == 1 else out_t

        return out

    def _compile_julia_system(self) -> None:
        # Call superclass method for basic definitions
        super()._compile_julia_system()
        # Get time and output symbols
        t_jl = self.jl_macro_vars(self.jl, 't', "var")
        outputs_sp = Utilities.apply_nested_list(
            lambda x, t: sympy.Symbol(self.__output_names[t[0]]
                                      + ('_' + '_'.join([str(i)
                                                         for i in t[1:]])
                                         if len(t) > 1 else
                                         '')
                                      ),
            [y[:, 0, ...] if len(y[:, 0, ...]) > 1 else 0 for y in self._y]
        )
        output_jl = [self.jl_macro_vars(self.jl, str(outp), "var", [t_jl])
                     for outp in Utilities.flatten(outputs_sp)]

        # Get Julia Interpolation functions
        interp = []
        for i, (t, yvec) in enumerate(zip(self._t, self._y)):
            for j in range(yvec[:, 0, ...].size):
                # Get yvec index
                yvec_idx = np.unravel_index(j, shape=yvec[:, 0, ...].shape)
                interp.append(self.jl.LinearInterpolation(
                    juliacall.convert(
                        self.jl.Vector,
                        yvec[yvec_idx[0], :]
                        if yvec.ndim == 2
                        else yvec[yvec_idx[0], :, yvec_idx[1:]]
                    ),
                    juliacall.convert(self.jl.Vector, t),
                    extrapolate=True
                ))

        # Get Julia code for extrapolation
        fun = "{intp}({t})"
        match self._extrapolation:
            case "zero":
                # Set to zero if outside range
                fun = "{t} < {min} || {max} < {t} ? 0 : {intp}({t})"
            case "const":
                # Hold constant if outside range
                fun = "{t} < {min} ? {y_left} : " \
                      "{max} < {t} ? {y_right} : {intp}({t})"
            case "linear":
                # This is the default behavior
                fun = "{intp}({t})"
            case "periodic":
                # Wrap t around
                fun = "{intp}(mod({t} - {min}, {max} - {min}) + {min})"

        # Register interpolation as symbolic and add extrapolation feature
        eqs = [0]*len(interp)
        for i, intp in enumerate(interp):
            self.jl_assign_var(self.jl,
                               "interp{i}_obj_{id}".format(i=i, id=id(self)),
                               intp)
            self.jl.seval(("interp{i}_{id}({t}) = " + fun).format(
                intp="interp{i}_obj_{id}".format(i=i, id=id(self)),
                i=i, id=id(self),
                t=str(t_jl), min=intp.t[0], max=intp.t[-1],
                y_left=intp.u[0], y_right=intp.u[-1]
            ))
            self.jl.seval("@register_symbolic interp{i}_{id}({t})".format(
                i=i, id=id(self), t=str(t_jl)
            ))
            eqs[i] = self.jl.seval(str(output_jl[i])[:-2 - len(str(t_jl))] +
                                   " ~ interp" + str(i) + "_" + str(id(self))
                                   + "(" + str(t_jl) + ")")

        eqs = juliacall.convert(self.jl.Vector,eqs)
        sys = self.jl.ODESystem(
            eqs, t_jl, name=self.jl.Symbol(self.name + "_" + str(id(self)))
        )

        outputs_jl = Utilities.apply_nested_list(
            lambda x, t: getattr(sys, str(x)),
            outputs_sp
        )

        self._system_jl = sys
        self._t_jl = t_jl
        self._inputs_jl = []
        self._outputs_jl = outputs_jl
        self._ic_jl = self.jl.Dict()
        self._tstops_jl = [juliacall.convert(self.jl.Vector, set(x))
                           for x in self._t]
        self._d_discontinuities_jl = [juliacall.convert(self.jl.Vector, set(x))
                                      for x in self._t]
        if self._extrapolation == "periodic":
            self._discontinuities_period_jl = [(max(t)-min(t), min(t))
                                               for t in self._t]
        else:
            self._discontinuities_period_jl = [(math.inf, 0)]*len(self._t)


#######################################################
# A gain block for a simple multiplication
#######################################################
class Gain(JuliaModelingToolkitConvertibleStaticSystem):
    """
    A system for multiplying the input with a constant.

    This system is used for performing a simple multiplication of the input
    with a predefined output. The output has the same dimension as the input.

    The resulting system has the following inputs and outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    | Input| 0     | u0      | a                | The input to multiply with  |
    |      |       |         |                  | the given gain.             |
    +      +-------+---------+------------------+-----------------------------+
    |      | ...   | ui      | ?                | Repeated s.t. all provided  |
    |      |       |         |                  | gains have their input.     |
    +------+-------+---------+------------------+-----------------------------+
    |Output| 0     | y0      | a                | The output of u0 times its  |
    |      |       |         |                  | gain.                       |
    +      +-------+---------+------------------+-----------------------------+
    |      | ...   | yi      | ?                | Repeated for all provided   |
    |      |       |         |                  | multiplications.            |
    +------+-------+---------+------------------+-----------------------------+

    :param gain: The constant to multipy the input with
    :param name: The name of the system
    """

    __gain: typing.List[typing.Any]
    """The gain to multiply the input with"""

    def __init__(self, gain: typing.List[typing.Any] = None,
                 name: str = "Gain"):
        # Set default gain vector
        if gain is None:
            gain = []
        # Get the symbolic input and output vector
        inputs = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x),
            Utilities.apply_nested_list(
                lambda x, t: (sympy.Symbol('u' + '_'.join([str(i) for i in t]))
                              if x else []),
                gain
            )
        )
        outputs = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x),
            Utilities.apply_nested_list(
                lambda x, t: (sympy.Symbol('y' + '_'.join([str(i) for i in t]))
                              if x else []),
                gain
            )
        )
        # Init the system
        super().__init__(num_inputs=len(gain), num_outputs=len(gain),
                         input_types=[get_signal_class(obj) for obj in gain],
                         output_types=[get_signal_class(obj) for obj in gain],
                         inputs=inputs,
                         outputs=outputs,
                         name=name)
        self.__gain = gain
        self.output_formula = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x),
            sympy.sympify([g*i
                           for g, i in zip(Utilities.flatten(gain),
                                           Utilities.flatten(self.inputs))])
        )

    def get_output(self, t: float) -> typing.List[typing.Any]:
        """
        Get the output of this gain block.

        :param t: Time to get the output at. Only relevant for the input,
            as the gain of this system is constant.
        :return: The constant output of the system.
        """
        val = [inp * gain
               for inp, gain in zip(self._get_inputs(t), self.__gain)]
        return [out_typ.cast(v) for out_typ, v in zip(self.output_types, val)]


#######################################################
# A cast block to change the datatype
#######################################################
class Cast(System):
    """
    A system for typecasting, i.e. transforming a type to another.

    This system is to be used for transformation of a signal of a certain type
    to another type.

    The resulting system has the following inputs and outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    | Input| 0     | u0      | a                | The input to cast to the    |
    |      |       |         |                  | the given type.             |
    +      +-------+---------+------------------+-----------------------------+
    |      | ...   | ui      | ?                | Repeated s.t. all provided  |
    |      |       |         |                  | types have their input.     |
    +------+-------+---------+------------------+-----------------------------+
    |Output| 0     | y0      | b                | The output of u0 casted to  |
    |      |       |         |                  | the target type.            |
    +      +-------+---------+------------------+-----------------------------+
    |      | ...   | yi      | ?                | Repeated for all provided   |
    |      |       |         |                  | type casts.                 |
    +------+-------+---------+------------------+-----------------------------+

    :param in_types: The types at the input of the cast system
    :param out_types: The types to transform the inputs to
    :param name: The name of the system
    """

    def __init__(self,
                 in_types: typing.List[SignalType] = None,
                 out_types: typing.List[SignalType]
                 = None,
                 name: str = "Cast"):
        # Default arguments
        if in_types is None:
            in_types = []
        if out_types is None:
            out_types = []
        # Check if lengths are compatible
        if len(in_types) != len(out_types):
            e = ValueError("Error creating cast system: The number of input "
                           "types ({num_inp}) and output types ({num_outp}) "
                           "must be equal.".format(
                            num_inp=len(in_types),
                            num_outp=len(out_types)))
            raise e

        super().__init__(len(in_types), len(out_types), name=name)
        self.input_types = in_types
        self.output_types = out_types

    def get_output(self, t: float) -> typing.List[typing.Any]:
        """
        Get the output of this cast block.

        Gets the input of this block cast to the output type.

        :param t: Simply used to pass on as this function is time-independent
        :return: The input cast to the output types.
        """
        return [out_type.cast(inp)
                for out_type, inp in zip(self.output_types,
                                         self._get_inputs(t))]
