import copy

import numpy as np
import sympy
import typing

import peepypoo.Utilities as Utilities

from peepypoo.SignalTypes import get_signal_class
from peepypoo.Systems.Static._StaticSystems import \
    JuliaModelingToolkitConvertibleStaticSystem

__all__ = ['IdealClarifier', 'FlowUnifier', 'FlowSeparator']


#######################################################
# An ideal clarifier
#######################################################
class IdealClarifier(JuliaModelingToolkitConvertibleStaticSystem):
    """
    An ideal clarifier.

    Splits the inflow into two flows, one of which contains
    all particulate parts while the soluble parts have equal concentrations in
    both parts.

    .. warning::

        This system does not check if the inputs or outputs are reasonable,
        e.g. if all flows are positive. Thus, depending on the given flows, the
        outputs could potentially be unphysical.

    The resulting system has the following inputs and outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    | Input| 0     | q_in    | float            | The input flow rate of the  |
    |      |       |         |                  | clarifier.                  |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1     | C_in    | np.array[float]  | The concentrations in the   |
    |      |       |         | Size: n_mat x 1  | inflow.                     |
    +      +-------+---------+------------------+-----------------------------+
    |      | 2     | q_out   | float            | The output flow rate of the |
    |      |       |         |                  | clarifier. Sludge flow or   |
    |      |       |         |                  | clarified effluent flow     |
    |      |       |         |                  | depending on the            |
    |      |       |         |                  | ``provided_outflow`` value. |
    +------+-------+---------+------------------+-----------------------------+
    |Output| 0     | q_out_l | float            | The output flow rate of the |
    |      |       |         |                  | clarified effluent.         |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1     | C_out_l | np.array[float]  | The output concentrations   |
    |      |       |         | Size: n_mat x 1  | in the clarified effluent.  |
    +      +-------+---------+------------------+-----------------------------+
    |      | 2     | q_out_s | float            | The output flow rate of the |
    |      |       |         |                  | sludge effluent.            |
    +      +-------+---------+------------------+-----------------------------+
    |      | 3     | C_out_s | np.array[float]  | The output concentrations   |
    |      |       |         | Size: n_mat x 1  | in the sludge effluent.     |
    +------+-------+---------+------------------+-----------------------------+

    :param num_materials: The number of materials in the flow for which
        concentrations are given.
    :param solid_materials: Boolean array which indicates which of the
        tracked materials are solid (``True`` if solid, ``False`` if liquid).
    :param provided_outflow: Which outflow is provided. Either ``liquid`` if
        the flow rate of the liquid output is provided or ``sludge`` if the
        flow rate of the sludge is given.
    :param f_ns: The fraction of non-settleable solids. Should be between 0 and 1
    :param name: The name of the system.
    """

    _provided_outflow: typing.Literal["liquid", "sludge"]
    """ 
    The outflow rate which is provided as input, either liquid or sludge
    """
    _solid_materials: typing.List[bool]
    """ Boolean array which is true for all solid components in the inflow. """
    _f_ns: float
    """ The fraction of solids, that is non-settleable. Should be in range [0, 1]. """

    def __init__(self, num_materials: int,
                 solid_materials: typing.List[bool],
                 provided_outflow: typing.Literal["liquid", "sludge"]
                 = "sludge",
                 f_ns: float = 0,
                 name: str = "Ideal Clarifier"):

        if len(solid_materials) != num_materials:
            raise ValueError("Error initializing the ideal clarifier: The "
                             "particulate_materials vector must contain "
                             "exactly one boolean for each material, denoting "
                             "whether it is particulate or not.")

        if f_ns > 1 or f_ns < 0:
            raise ValueError("Error initializing the ideal clarifier: The fraction of non-settleable solids"
                             "must be a percentage given in the interval [0, 1]")

        # Assemble input and output symbols
        c_nums = [str(i) for i in range(num_materials)]
        inputs = [sympy.Symbol('q_in'),
                  [sympy.Symbol('C_in' + i) for i in c_nums],
                  sympy.Symbol('q_out_' + provided_outflow)]
        inputs = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x), inputs
        )
        outputs = [sympy.Symbol('q_out_liquid'),
                   [sympy.Symbol('C_out_liquid' + i) for i in c_nums],
                   sympy.Symbol('q_out_sludge'),
                   [sympy.Symbol('C_out_sludge' + i) for i in c_nums]]
        outputs = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x), outputs
        )

        super().__init__(num_inputs=3, num_outputs=4,
                         inputs=inputs, outputs=outputs,
                         input_types=
                         [get_signal_class(0.0),
                          get_signal_class(np.array([0.0]*num_materials)),
                          get_signal_class(0.0)],
                         output_types=
                         [get_signal_class(0.0),
                          get_signal_class(np.array([0.0]*num_materials)),
                          get_signal_class(0.0),
                          get_signal_class(np.array([0.0]*num_materials))],
                         name=name)

        self._provided_outflow = provided_outflow
        self._solid_materials = solid_materials
        self._f_ns = f_ns
        self.output_formula = sympy.Tuple(
            inputs[0] - inputs[2] if provided_outflow == "sludge"
            else inputs[2],
            sympy.Tuple(*(inputs[1][i] if not solid_materials[i] else
                          (inputs[1][i] * inputs[0] * f_ns / (inputs[0] - inputs[2])
                           if provided_outflow == "sludge"
                           else inputs[1][i] * inputs[0] * f_ns / inputs[2])
                          for i in range(num_materials))),
            inputs[2] if provided_outflow == "sludge"
            else inputs[0] - inputs[2],
            sympy.Tuple(*(inputs[1][i] if not solid_materials[i] else
                          (inputs[1][i] * inputs[0] * (1 - f_ns) / inputs[2]
                           if provided_outflow == "sludge"
                           else inputs[1][i] * inputs[0] * (1 - f_ns) /
                                (inputs[0] - inputs[2]))
                          for i in range(num_materials)))
        )

    def get_output(self, t: float) -> typing.List[typing.Any]:
        """
        Get the constant output of this constant block.

        :param t: Time to get the output at. Ignored as the output is constant
        :return: The constant output of the system.
        """
        # Get the input
        q_in, c_in, q_sludge = self._get_inputs(t)
        # If the liquid outflow is given, transform it to the sludge flow using
        # mass balance
        if self._provided_outflow == "liquid":
            q_sludge = q_in - q_sludge

        # Calculate output concentrations
        c_out_liquid = copy.copy(c_in)
        c_out_sludge = copy.copy(c_in)
        # Remove solids from liquid part
        c_out_liquid[self._solid_materials] *= q_in*self._f_ns/(q_in - q_sludge)
        # Increase solid concentrations in sludge
        c_out_sludge[self._solid_materials] *= q_in*(1-self._f_ns)/q_sludge

        return [q_in - q_sludge, c_out_liquid, q_sludge, c_out_sludge]


#######################################################
# A flow unifier
#######################################################
class FlowUnifier(JuliaModelingToolkitConvertibleStaticSystem):
    """
    An ideal flow unifier assuming ideal mixing.

    Combines multiple inflows into one flow, which is assumed to be ideally
    mixed.
    The resulting system has the following inputs and outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    | Input| 2*i   | q_in_i  | float            | The flow rate of the input  |
    |      |       |         |                  | flow i                      |
    +      +-------+---------+------------------+-----------------------------+
    |      | 2*i+1 | C_in_i  | np.array[float]  | The concentrations in the   |
    |      |       |         | Size: n_mat x 1  | input flow i                |
    +      +-------+---------+------------------+-----------------------------+
    |      | ...   | ...     | ...              | Repetition of the input     |
    |      |       |         |                  | above for all inflows.      |
    +------+-------+---------+------------------+-----------------------------+
    |Output| 0     | q_out   | float            | The output flow rate        |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1     | C_out   | np.array[float]  | The concentrations in  the  |
    |      |       |         | Size: n_mat x 1  | outflow.                    |
    +------+-------+---------+------------------+-----------------------------+

    :param num_materials: The number of materials in the flow for which
        concentrations are given. Must be equal for all inflows
    :param num_flows: The number of flows to unify
    :param name: The name of the system.
    """

    _num_flows: int
    """ The number of flows to unite """

    def __init__(self, num_materials: int, num_flows: int = 2,
                 name: str = "Flow Unifier"):

        # Assemble input and output symbols
        c_nums = [str(i) for i in range(num_materials)]
        f_nums = [str(i) for i in range(num_flows)]
        inputs = [[sympy.Symbol('q_in_' + j),
                   [sympy.Symbol('C_in_' + j + '_' + i) for i in c_nums]]
                  for j in f_nums]
        inputs = [i for inp in inputs for i in inp]
        inputs = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x), inputs
        )
        outputs = [sympy.Symbol('q_out'),
                   [sympy.Symbol('C_out_' + i) for i in c_nums]]
        outputs = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x), outputs
        )

        super().__init__(num_inputs=2*num_flows, num_outputs=2,
                         inputs=inputs, outputs=outputs,
                         input_types=
                         [get_signal_class(0.0),
                          get_signal_class(np.array([0.0]*num_materials))]
                         * num_flows,
                         output_types=
                         [get_signal_class(0.0),
                          get_signal_class(np.array([0.0]*num_materials))],
                         name=name)

        self._num_flows = num_flows

        q_in = inputs[0::2]
        c_in = np.array(inputs[1::2])
        self.output_formula = sympy.Tuple(
            sum(q_in),
            sympy.Tuple(*(np.sum(c_in.T*q_in, axis=1) / sum(q_in)))
        )

    def get_output(self, t: float) -> typing.List[typing.Any]:
        """
        Get the constant output of this constant block.

        :param t: Time to get the output at. Ignored as the output is constant
        :return: The constant output of the system.
        """
        # Get the input
        inp = self._get_inputs(t)

        # Split into flows and concentrations
        q_in = inp[0::2]
        c_in = np.array(inp[1::2])

        # Compute mass balance and return
        return [sum(q_in), np.sum(c_in.T*q_in, axis=1) / sum(q_in)]


#######################################################
# A flow separator
#######################################################
class FlowSeparator(JuliaModelingToolkitConvertibleStaticSystem):
    """
    An ideal flow separator.

    Separates an inflow into multiple outflows. The inflow is assumed to be
    perfectly mixed s.t. all outflows have the same concentrations.
    The resulting system has the following inputs and outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    | Input| 0     | q_in    | float            | The flow rate of the input  |
    |      |       |         |                  | flow to separate            |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1     | C_in    | np.array[float]  | The concentrations in the   |
    |      |       |         | Size: n_mat x 1  | input flow to separate      |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1+i   | q_out_i | float            | The output flow rate of     |
    |      | (i>0) |         |                  | output i. Percentage or     |
    |      |       |         |                  | absolute value, depending   |
    |      |       |         |                  | on ``use_percentage``.      |
    |      |       |         |                  | The first outflow rate (i=0)|
    |      |       |         |                  | is determined by the mass   |
    |      |       |         |                  | balance.                    |
    +      +-------+---------+------------------+-----------------------------+
    |      | ...   | ...     | ...              | Repetition of the input     |
    |      |       |         |                  | above for all but the first |
    |      |       |         |                  | outflow (for which it is    |
    |      |       |         |                  | inferred by the mass        |
    |      |       |         |                  | balance).                   |
    +------+-------+---------+------------------+-----------------------------+
    |Output| 2*i   | q_out_i | float            | The output flow rate of     |
    |      |       |         |                  | output i. Absolute value.   |
    +      +-------+---------+------------------+-----------------------------+
    |      | 2*i+1 | C_out_i | np.array[float]  | The concentrations in       |
    |      |       |         | Size: n_mat x 1  | outflow i.                  |
    +      +-------+---------+------------------+-----------------------------+
    |      | ...   | ...     | ...              | Repetition of the two above |
    |      |       |         |                  | outputs for all i as there  |
    |      |       |         |                  | are number of outflows      |
    +------+-------+---------+------------------+-----------------------------+


    :param num_materials: The number of materials in the flow for which
        concentrations are given.
    :param num_flows: The number of flows to split into
    :param use_percentages: If the outflows are given as percentages. If false,
        the outflow parameters are taken as flow rates
    :param name: The name of the system.
    """

    _num_flows: int
    """ The number of flows to split into """
    _use_percentages: bool
    """ If the outflows are given as percentages """

    def __init__(self, num_materials: int, num_flows: int = 2,
                 use_percentages: bool = False, name: str = "Flow Separator"):

        # Assemble input and output symbols
        c_nums = [str(i) for i in range(num_materials)]
        f_nums = [str(i) for i in range(num_flows)]
        inputs = [sympy.Symbol('q_in'),
                  [sympy.Symbol('C_in_' + i) for i in c_nums],
                  *[sympy.Symbol('q_out_' + j + '_ref') for j in f_nums[1:]]]
        inputs = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x), inputs
        )
        outputs = [[sympy.Symbol('q_out_' + j),
                   [sympy.Symbol('C_out_' + j + '_' + i) for i in c_nums]]
                   for j in f_nums]
        outputs = [i for outp in outputs for i in outp]
        outputs = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x), outputs
        )

        super().__init__(num_inputs=1+num_flows, num_outputs=2*num_flows,
                         inputs=inputs, outputs=outputs,
                         input_types=
                         [get_signal_class(0.0),
                          get_signal_class(np.array([0.0]*num_materials)),
                          *([get_signal_class(0.0)] * (num_flows-1))],
                         output_types=
                         [get_signal_class(0.0),
                          get_signal_class(np.array([0.0]*num_materials))]
                         * num_flows,
                         name=name)

        self._num_flows = num_flows
        self._use_percentages = use_percentages

        self.output_formula = sympy.Tuple(
            # Mass balance for first outflow
            inputs[0]*(1-sum(inputs[2:]))
            if use_percentages else inputs[0]-(sum(inputs[2:])),
            inputs[1],  # Concentrations
            # Rest of outflows as [q, C] and then flatten out
            *[val for i in range(1, num_flows) for val in
              [inputs[1+i]*(inputs[0] if use_percentages else 1), inputs[1]]])

    def get_output(self, t: float) -> typing.List[typing.Any]:
        """
        Get the constant output of this constant block.

        :param t: Time to get the output at. Ignored as the output is constant
        :return: The constant output of the system.
        """
        # Get the input
        inp = self._get_inputs(t)

        # Split into flows and concentrations
        q_in = inp[0]
        c_in = inp[1]
        q_out = np.array([0, *inp[2:]]) * (q_in
                                           if self._use_percentages else 1)
        q_out[0] = q_in - np.sum(q_out[1:])

        # Output flow rates and input concentration for each output
        return [out for q in q_out for out in [q, c_in]]
