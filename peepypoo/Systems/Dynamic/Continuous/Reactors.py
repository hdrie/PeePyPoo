import abc
import numpy as np
import os.path
import pathlib
import re
import sympy
import typing

from peepypoo.Systems.Dynamic._DynamicSystems import \
    ContinuousTimeDynamicalSystem
from peepypoo.SignalTypes import get_signal_class
from peepypoo.Systems.Dynamic.Continuous.StoichiometricMatrices \
    .readStoichiometricMatrix import *

__all__ = ['SBR', 'CSTR', 'BatchReactor']


class ReactorWithStoichiometry(ContinuousTimeDynamicalSystem, abc.ABC):
    """
    An arbitrary reactor with the transformation reaction defined as
    stoichiometric matrix within a tabular-file.

    This abstract system takes care of reading in and using the reaction as
    defined in the stoichiometry file. It mainly provides one function which
    evaluates the reaction rates.

    :param stoichiometry_file: The file in which the stoichiometric matrix is
        defined. Can either be the path to a file or the name (without ending)
        of a predefined stoichiometry. In case a file is provided, it must be
        a valid file name to a file which is readable either by
        :py:func:`pandas.read_excel` or :py:func:`pandas.read_csv`.
    :param parameters_file: The file which defines which parameters are in the
        system and can specify values for them. If no value is given, for a
        specific parameter, this parameter is assumed to be given at runtime.
        If none is given, it is assumed that the parameters are defined in the
        same file as the stoichiometric matrix.
    :param `*ct_dyn_sys_args`: Positional arguments to pass on to the
        :class:`ContinuousTimeDynamicalSystem <peepypoo.Systems.Dynamic._DynamicSystems.ContinuousTimeDynamicalSystem>` constructor
    :param `**ct_dyn_sys_kwargs`: Keyword arguments to pass on to the
        :class:`ContinuousTimeDynamicalSystem <peepypoo.Systems.Dynamic._DynamicSystems.ContinuousTimeDynamicalSystem>` constructor

    :raises FileNotFoundError: If the stoichiometry file or the parameters file
        is invalid
    """

    reaction_rates: typing.Callable[[np.array, typing.List[np.array]],
                                    np.array]
    """ A callable to evaluate the reaction rates """
    params_plugged_in: typing.Dict[str, float]
    """ 
    A dict with the parameters which have been plugged in and their values 
    """
    _mass_transfer_formula: sympy.Tuple
    """ The mass transfer as formula """
    reaction_rates_formula: sympy.Tuple
    """ The reaction rates as formula """
    _is_const: typing.List[bool]
    """ A list of bool indicating whether the state should be kept constant """
    @property
    def mass_transfer_formula(self) -> sympy.Tuple:
        """ The mass transfer as formula """
        return self._mass_transfer_formula

    @mass_transfer_formula.setter
    def mass_transfer_formula(self, value: sympy.Tuple):
        self._mass_transfer_formula = value
        self.dynamics_formula = sympy.Tuple(*[mt + rr for mt, rr in
                                              zip(self.mass_transfer_formula,
                                                  self.reaction_rates_formula)
                                              ])

    def __init__(self, *ct_dyn_sys_args, stoichiometry_file: str = None,
                 parameters_file: str = None,
                 **ct_dyn_sys_kwargs):
        # Check if the stoichiometry file is a valid file or a library
        # stoichiometry
        if stoichiometry_file is not None and \
                not os.path.isfile(stoichiometry_file):
            lib_stoichiometries = [x.name for x in
                                   (pathlib.Path(__file__).absolute().parent
                                    / 'StoichiometricMatrices').iterdir()]
            lib_stoichiometries = [x
                                   for x in lib_stoichiometries
                                   if x[-3:] != '.py']
            match = np.where([bool(re.search(stoichiometry_file, x))
                              for x in lib_stoichiometries])
            if len(match[0]) == 1:
                stoichiometry_file = str(pathlib.Path(__file__).absolute()
                                         .parent / 'StoichiometricMatrices'
                                         / lib_stoichiometries[match[0][0]])
            elif len(match[0]) == 0:
                raise FileNotFoundError("Invalid stoichiometry_file: \"" +
                                        stoichiometry_file +
                                        "\" is neither a library stoichiometry"
                                        " file nor a valid file path.")
            else:
                # Check if any file matches it exactly and choose that one
                match = np.where([bool(re.search(r"^" + stoichiometry_file +
                                                 r"\..*$", x))
                                  for x in lib_stoichiometries])

                if len(match[0]) == 1:
                    stoichiometry_file = str(pathlib.Path(__file__).absolute()
                                             .parent / 'StoichiometricMatrices'
                                             / lib_stoichiometries[
                                                 match[0][0]])
                else:
                    raise FileNotFoundError("Invalid stoichiometry_file: \"" +
                                            stoichiometry_file +
                                            "\" matches multiple library "
                                            "stoichiometry files")
        elif stoichiometry_file is None:
            raise FileNotFoundError("Invalid stoichiometry_file: No "
                                    "stoichiometry file defined")

        # Check the parameters file: if none is given, take from stoichiometry
        # file, else must be valid file.
        if parameters_file is None:
            parameters_file = stoichiometry_file
        elif not os.path.isfile(parameters_file):
            raise FileNotFoundError("Invalid parameters_file: No file \"" +
                                    parameters_file + "\" exists. " +
                                    "Specify as None (or equivalently do not "
                                    "specify) if it is defined in the same "
                                    "file as the stoichiometry.")

        # Get states, params and rates from stoichiometry
        states, params, self.reaction_rates, \
            reaction_rates_formula, self.params_plugged_in, self._is_const = \
            get_reaction_rates(stoichiometry_file, parameters_file)

        self.reaction_rates_formula = sympy.Tuple(*reaction_rates_formula)
        inputs = sympy.Tuple(sympy.Tuple(*[sympy.symbols(state + '_in')
                                           for state in states]))
        states = sympy.Array([sympy.symbols(state) for state in states])
        if len(params) == 0:
            n_params = 0
            params = []
            param_types = []
        else:
            params = sympy.Tuple(sympy.Tuple(*[sympy.symbols(param)
                                               for param in params[0]]))
            n_params = 1
            param_types = [get_signal_class(np.zeros(len(params[0])))]

        t = sympy.symbols('t')
        super().__init__(*ct_dyn_sys_args,
                         num_states=1, num_params=n_params,
                         param_types=param_types, t_sym=t, states=states,
                         inputs=inputs, params=params, **ct_dyn_sys_kwargs)

    def transformation_processes(self, x: np.array, p: np.array) -> np.array:
        # Reaction rates as defined in the stoichiometric matrix.
        return self.reaction_rates(x, p)


class SBR(ReactorWithStoichiometry):
    """
    A sequencing batch reactor (a stirred tank reactor with variable volume)
    with the transformation reaction defined as stoichiometric matrix within a
    tabular file (excel, csv or similar).

    The resulting system has the following inputs and outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    | Input| 0     | q_in    | float            | The input flow rate of the  |
    |      |       |         |                  | SBR.                        |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1     | C_in    | np.array[float]  | The concentrations in the   |
    |      |       |         | Size: n_mat x 1  | inflow. The order of the    |
    |      |       |         |                  | substances is as defined in |
    |      |       |         |                  | the stoichiometric matrix.  |
    +      +-------+---------+------------------+-----------------------------+
    |      | 2     | q_out   | float            | The output flow rate of the |
    |      |       |         |                  | SBR.                        |
    +      +-------+---------+------------------+-----------------------------+
    |      | 3     | p       | np.array[float]  | The parameters of the SBR.  |
    |      |       |         | Size: n_param x 1| The order is as defined in  |
    |      |       |         |                  | the parameters file with the|
    |      |       |         |                  | parameters for which values |
    |      |       |         |                  | were given left out.        |
    +------+-------+---------+------------------+-----------------------------+
    |Output| 0     | V       | float            | The current volume in the   |
    |      |       |         |                  | SBR.                        |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1     | q_out   | float            | The output flow rate of the |
    |      |       |         |                  | SBR.                        |
    +      +-------+---------+------------------+-----------------------------+
    |      | 2     | C_out   | np.array[float]  | The output concentrations   |
    |      |       |         | Size: n_mat x 1  | in the effluent of the SBR. |
    +------+-------+---------+------------------+-----------------------------+

    :param initial_state: The initial state of the reactor as
        ``[V_init, C_init]`` in one numpy array.
    :param initial_t: The time at which the initial state is given
    :param name: The name of the reactor
    :param stoichiometry_file: The file in which the stoichiometric matrix is
        defined. Can either be the path to a file or the name (without ending)
        of a predefined stoichiometry. In case a file is provided, it must be
        a valid file name to a file which is readable either by
        :py:func:`pandas.read_excel` or :py:func:`pandas.read_csv`.
    :param parameters_file: The file which defines which parameters are in the
        system and can specify values for them. If no value is given, for a
        specific parameter, this parameter is assumed to be given at runtime.
        If none is given, it is assumed that the parameters are defined in the
        same file as the stoichiometric matrix.
    :param `*reactor_args`: Positional arguments to pass to the
        :class:`ReactorWithStoichiometry` system
    :param `**reactor_kwargs`: Keyword arguments to pass to the
        :class:`ReactorWithStoichiometry` system

    :raises ValueError: If the initial condition has the wrong size
    """

    def __init__(self, initial_state: np.array = None,
                 initial_t: float = 0, initial_output: np.array = None, name: str = "SBR",
                 stoichiometry_file: str = None, parameters_file: str = None,
                 *reactor_args, **reactor_kwargs):
        # Get the input and output types
        ic_signal_type = get_signal_class(initial_state[1:])
        input_types = [get_signal_class(1.0),
                       ic_signal_type,
                       get_signal_class(1.0)]
        output_types = [get_signal_class(1.0),
                        get_signal_class(1.0),
                        ic_signal_type]

        super().__init__(*reactor_args, stoichiometry_file=stoichiometry_file,
                         parameters_file=parameters_file, num_inputs=3,
                         input_types=input_types, num_outputs=3,
                         output_types=output_types,
                         initial_state=initial_state, initial_t=initial_t, initial_output=initial_output,
                         name=name, **reactor_kwargs)

        # Add the volume to the states
        self.states = sympy.Array([sympy.symbols('V'), *self.states])
        # Add the inflow to the inputs
        self.inputs = [sympy.Symbol('q_in'),
                       sympy.Array(*self.inputs),
                       sympy.Symbol('q_out')]

        self.output_formula = sympy.Tuple(self.states[0],
                                          self.inputs[-1],
                                          sympy.Tuple(*self.states[1:]))
        self.reaction_rates_formula = \
            sympy.Tuple(*[0, *self.reaction_rates_formula])
        mass_transfer = (self.inputs[0]/self.states[0] * (self.inputs[1] - self.states[1:]))
        self.mass_transfer_formula = \
            sympy.Tuple(self.inputs[0] - self.inputs[2],
                        *[0 if c else m for c, m in zip(self._is_const, mass_transfer)])

        # Check the initial state size
        if len(self.states) != len(self._initial_state):
            raise ValueError("The given initial state has the wrong length " +
                             str(len(self._initial_state)) + " instead of " +
                             str(len(self.states)) + " which is inferred " +
                             "from the stoichiometry file.")

        # Set the initial output
        if initial_output is None:
            self._initial_output = self.calculate_output(self._initial_state, [x.get_zero() for x in self.input_types],
                                                         [], self._initial_t)
        self.reset()

    def f(self, x: typing.Any, u: typing.List[typing.Any],
          p: typing.List[typing.Any], t: float) \
            -> typing.Any:
        """
        SBR dynamics.

        The dynamics of the SBR. Consists of the volume dynamics and the
        concentration dynamics

        :param x: The state at time t
        :param u: The input at time t
        :param p: The parameters at time t
        :param t: The time t
        :return: The derivative of the state at the given time
        """
        dv = u[0] - u[2]
        dc = u[0]/x[0]*(u[1] - x[1:]) \
             + self.transformation_processes(x[1:], p)
        dc = [0 if c else dc for c, dc in zip(self._is_const, dc)]
        return np.concatenate(([dv], dc))

    def calculate_output(self,
                         x: typing.Any, u: typing.Any,
                         p: typing.Any, t: float) -> typing.List[typing.Any]:
        """
        Output the state as [V, C].

        :param x: State at the time to calculate the output
        :param u: Input at the time to calculate the output
        :param p: Parameters at the time to calculate the output
        :param t: Time to calculate the output at
        :return: The output at the given time
        """
        return [x[0], u[2], x[1:]]


class CSTR(SBR):
    """
    A continuously stirred tank reactor (CSTR) with the transformation reaction
    defined as stoichiometric matrix within a tabular file (excel, csv or
    similar). The CSTR is modeled as SBR with equal in- and outflow.

    The resulting system has the following inputs and outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    | Input| 0     | q_in    | float            | The input flow rate of the  |
    |      |       |         |                  | CSTR. Equal to the output   |
    |      |       |         |                  | flow rate.                  |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1     | C_in    | np.array[float]  | The concentrations in the   |
    |      |       |         | Size: n_mat x 1  | inflow. The order of the    |
    |      |       |         |                  | substances is as defined in |
    |      |       |         |                  | the stoichiometric matrix.  |
    +      +-------+---------+------------------+-----------------------------+
    |      | 2     | p       | np.array[float]  | The parameters of the CSTR. |
    |      |       |         | Size: n_param x 1| The order is as defined in  |
    |      |       |         |                  | the parameters file with the|
    |      |       |         |                  | parameters for which values |
    |      |       |         |                  | were given left out.        |
    +------+-------+---------+------------------+-----------------------------+
    |Output| 0     | q_out   | float            | The output flow rate of the |
    |      |       |         |                  | CSTR.                       |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1     | C_out   | np.array[float]  | The output concentrations   |
    |      |       |         | Size: n_mat x 1  | in the effluent of the CSTR.|
    +------+-------+---------+------------------+-----------------------------+

    :param v: The volume of the reactor
    :param initial_state: The initial state of the reactor
    :param initial_t: The time at which the initial state is given
    :param name: The name of the reactor
    :param stoichiometry_file: The file in which the stoichiometric matrix is
        defined. Can either be the path to a file or the name (without ending)
        of a predefined stoichiometry. In case a file is provided, it must be
        a valid file name to a file which is readable either by
        :py:func:`pandas.read_excel` or :py:func:`pandas.read_csv`.
    :param parameters_file: The file which defines which parameters are in the
        system and can specify values for them. If no value is given, for a
        specific parameter, this parameter is assumed to be given at runtime.
        If none is given, it is assumed that the parameters are defined in the
        same file as the stoichiometric matrix.
    :param `*reactor_args`: Positional arguments to pass to the
        :class:`SBR` system
    :param `**reactor_kwargs`: Keyword arguments to pass to the
        :class:`SBR` system
    """

    v: float
    """ The fixed volume of the CSTR. """

    def __init__(self, v: float, initial_state: np.array = None,
                 initial_t: float = 0, initial_output: np.array = None, name: str = "CSTR",
                 stoichiometry_file: str = None, parameters_file: str = None,
                 *reactor_args, **reactor_kwargs):
        initial_state_ext = np.concatenate(([v], initial_state))

        super().__init__(*reactor_args, stoichiometry_file=stoichiometry_file,
                         parameters_file=parameters_file,
                         initial_state=initial_state_ext, initial_t=initial_t,
                         name=name, **reactor_kwargs)

        # Adapt interface definitions of SBR to match CSTR
        self._remove_system_input_port([2])
        self._remove_output_port([0])
        # Adapt the signal type of the input (one dimension less)
        ic_signal_type = get_signal_class(initial_state)
        self.input_types[1] = ic_signal_type

        # Remove the volume from the states
        self.states = self.states[1:]
        self._initial_state = self._initial_state[1:]
        self.reaction_rates_formula = self.reaction_rates_formula[1:]
        # Remove the inflow from the inputs (it is equal to the outflow)
        self.inputs = [sympy.Symbol('q_in'), *self.inputs[1:-1]]
        self.v = v
        # Remove the volume from the outputs
        self.outputs = self.outputs[1:]

        self.output_formula = sympy.Tuple(self.inputs[0],
                                          sympy.Tuple(*self.states))
        mass_transfer = sympy.sympify(self.inputs[0])/sympy.sympify(self.v) * (self.inputs[1] - self.states)
        self.mass_transfer_formula = sympy.Tuple(*[0 if c else m for c, m in zip(self._is_const, mass_transfer)])

        # Set the initial output
        if initial_output is None:
            self._initial_output = self.calculate_output(self._initial_state, [x.get_zero() for x in self.input_types],
                                                         [], self._initial_t)
        else:
            self._initial_output = initial_output
        self.reset()

    def f(self, x: typing.Any, u: typing.List[typing.Any],
          p: typing.List[typing.Any], t: float) \
            -> typing.Any:
        """
        CSTR Dynamics.

        Implements the dynamics of a CSTR (i.e. SBR with equal in- and output)

        :param x: The state at time t
        :param u: The input at time t
        :param p: The parameters at time t
        :param t: The time t
        :return: The derivative of the state at the given time
        """
        u_ext = [u[0], u[1], u[0]]
        x_ext = np.array([self.v, *x])
        return super().f(x_ext, u_ext, p, t)[1:]

    def calculate_output(self,
                         x: typing.Any, u: typing.Any,
                         p: typing.Any, t: float) -> typing.List[typing.Any]:
        """
        Output the state of the system which is the concentration.

        :param x: State at the time to calculate the output
        :param u: Input at the time to calculate the output
        :param p: Parameters at the time to calculate the output
        :param t: Time to calculate the output at
        :return: The output at the given time
        """
        return [u[0], x]


class BatchReactor(CSTR):
    """
    Implements a batch reactor as a CSTR with zero flow.

    The resulting system has the following inputs and outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    | Input| 0     | p       | np.array[float]  | The parameters of the Batch |
    |      |       |         | Size: n_param x 1| reactor. The order is as    |
    |      |       |         |                  | defined in the parameters   |
    |      |       |         |                  | file with the parameters for|
    |      |       |         |                  | which values were given left|
    |      |       |         |                  | out.                        |
    +------+-------+---------+------------------+-----------------------------+
    |Output| 0     | C       | np.array[float]  | The concentrations within   |
    |      |       |         | Size: n_mat x 1  | the batch reactor (note that|
    |      |       |         |                  | the reactor does not have an|
    |      |       |         |                  | outflow.                    |
    +------+-------+---------+------------------+-----------------------------+

    :param initial_state: The initial state of the reactor
    :param initial_t: The time at which the initial state is given
    :param name: The name of the reactor
    :param stoichiometry_file: The file in which the stoichiometric matrix is
        defined. Can either be the path to a file or the name (without ending)
        of a predefined stoichiometry. In case a file is provided, it must be
        a valid file name to a file which is readable either by
        :py:func:`pandas.read_excel` or :py:func:`pandas.read_csv`.
    :param parameters_file: The file which defines which parameters are in the
        system and can specify values for them. If no value is given, for a
        specific parameter, this parameter is assumed to be given at runtime.
        If none is given, it is assumed that the parameters are defined in the
        same file as the stoichiometric matrix.
    :param `*reactor_args`: Positional arguments to pass to the
        :class:`CSTR` system
    :param `**reactor_kwargs`: Keyword arguments to pass to the
        :class:`CSTR` system
    """

    def __init__(self, initial_state: np.array = None,
                 initial_t: float = 0, initial_output: np.array = None, name: str = "Batch Reactor",
                 stoichiometry_file: str = None, parameters_file: str = None,
                 *reactor_args, **reactor_kwargs):

        super().__init__(*reactor_args, v=1,
                         stoichiometry_file=stoichiometry_file,
                         parameters_file=parameters_file,
                         initial_state=initial_state, initial_t=initial_t,
                         name=name, **reactor_kwargs)

        # Adapt interface definitions of CSTR to match the batch reactor
        self._remove_system_input_port([0, 1])  # Remove all inputs
        self._remove_output_port([0])  # Remove flow output

        # Remove the inputs
        self.inputs = []
        # Remove flow output
        self.outputs = self.outputs[1:]
        self.output_formula = self.output_formula[1:]

        # Set the mass transfer to 0 (of the appropriate dimension)
        self.mass_transfer_formula = sympy.sympify(0) * self.states

        # Set the initial output
        if initial_output is None:
            self._initial_output = self.calculate_output(self._initial_state, [], [], self._initial_t)
        else:
            self._initial_output = initial_output
        self.reset()

    def f(self, x: typing.Any, u: typing.List[typing.Any],
          p: typing.List[typing.Any], t: float) \
            -> typing.Any:
        """
        Batch reactor dynamics.

        Implements the dynamics of a batch reactor (wrap CSTR with zero flow)

        :param x: The state at time t
        :param u: The input at time t
        :param p: The parameters at time t
        :param t: The time t
        :return: The derivative of the state at the given time
        """
        u_ext = [0, np.zeros(self._num_states), *u]
        return super().f(x, u_ext, p, t)

    def calculate_output(self,
                         x: typing.Any, u: typing.Any,
                         p: typing.Any, t: float) -> typing.List[typing.Any]:
        """
        Output the state of the system which is the concentration.

        :param x: State at the time to calculate the output
        :param u: Input at the time to calculate the output
        :param p: Parameters at the time to calculate the output
        :param t: Time to calculate the output at
        :return: The output at the given time
        """
        return [x]
