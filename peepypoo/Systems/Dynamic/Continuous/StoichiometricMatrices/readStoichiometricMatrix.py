import math
import numbers
import numpy as np
import pandas
import pandas as pd
import re
import sympy
import typing


COMPOSITION_TOL = 1e-4
"""
Constant defining the tolerance on the consistency check with the composition 
matrix.
"""


def __extract_submatrices(st_mat: pd.DataFrame) \
        -> typing.Tuple[pd.DataFrame, pd.DataFrame,
                        pd.Series, pd.Series, pd.Series, pd.Series]:
    """
    Extracts the stoichiometry, the composition, the process
    rates, the state names and the state units from a dataframe containing the
    complete stoichiometric matrix.

    The stoichiometric matrix is marked by the following properties:

    * "Stoichiometric Matrix" or "Stoichiometry" (all capitals can also be
      lowercase) is written in the *top left corner*
    * The *top row* (without the "Stoichiometric Matrix" or equivalent label)
      consists of the character "i" followed by the increasing list of integers
      (starting with 1) denoting the compound number and finally an empty cell.
    * In the *second row*, the state variables are named
    * In the *third row*, the units of the state variables are added
    * Then, the following rows consist of numbers or numeric expressions which
      define the stoichiometry.
    * The *most left column* (again without the "Stoichiometric Matrix" or
      equivalent label) consists first of an empty cell, then the character "j"
      and the increasing list of process numbers (again starting with 1).
    * In the *most right column* below the empty cell after the compound
      numbers, the process rates are listed, all in the same row as the
      corresponding process
    * Below this stoichiometric matrix, the composition matrix **can** be added
      which is structured as follows:

        * In the *first column*, directly below the highest process number, the
          character "k" denotes that the composition matrix follows. Afterwards,
          the increasing numbers of the conservatives follow (again starting from
          1).
        * Right of this, in the column of the corresponding states, the
          composition matrix is added.

    * Below this stoichiometric matrix and eventually the composition matrix, a row **can** be added indicating whether
      the state dynamics are applied or if the state is to be kept constantly at a value, given as parameter.
      If not given, all states are assumed to use the dynamics. If the state is to be kept constant, the system will
      hold the initial value. This row is structured as follows:

        * In the *first column* directly below the highest process number or the highest number of conservatives,
          "is_constant" is added.
        * Right of this, in the columns of the corresponding states, it is added whether the state is to be kept at a
          constant value. Empty or "0" indicates that the dynamics are used, "1" implies that the state is to be kept
          constant.

    Note that the stoichiometric matrix does **not** have to be the only thing
    in the dataframe. It is just necessary that the top left corner, the top
    row as well as the most left column correspond to the requirements
    mentioned above and that no other entry except the top left corner of the
    matrix matches the "[sS]toichiometr(y|ic [mM]atrix)" regex. Otherwise, an
    error is thrown.

    :param st_mat: The dataframe containing the stoichiometric matrix
    :return: stoichiometry, composition, process rates, state names, state
        units and if the state should be kept constant.
        Matrices as pandas dataframe, Vectors as pandas series.
    :raises SyntaxError: If the stoichiometric matrix does not match the form
        described above
    """
    # Get matrix Starting on top left of Stoichiometric matrix
    # (denoted by "Stoichiometry" or "Stoichiometric Matrix" (see regex))
    mat_start = np.where(st_mat.map(lambda x: bool(re.search(
        "[sS]toichiometr(y|ic [mM]atrix)",
        str(x)))))
    if any([len(x) != 1 for x in mat_start]):
        raise SyntaxError("Invalid matrix format: The stoichiometric matrix "
                          "must have an entry matching \"[sS]toichiometr(y|ic "
                          "[mM]atrix)\" in the top left corner and the entry "
                          "must only appear once in the matrix.")
    st_mat = st_mat.iloc[mat_start[0][0]:, mat_start[1][0]:]

    # Get matrix stopping bottom right
    mat_end = [0, 0]
    # Walk over columns to the right
    # Second entry must be i and then 1
    if st_mat.iloc[0, 1] != "i" \
            or not (isinstance(st_mat.iloc[0, 2], (int, float))
                    or st_mat.iloc[0, 2].isnumeric())\
            or int(st_mat.iloc[0, 2]) != 1:
        raise SyntaxError("Invalid matrix format: The first row of the "
                          "stoichiometric matrix must start with \"i\", "
                          "then an increasing sequence of integers starting "
                          "at 1 and end with an empty cell.")
    mat_end[1] = 2
    # Followed by increasing row of integers
    try:
        while int(st_mat.iloc[0, mat_end[1]]) == \
                int(st_mat.iloc[0, mat_end[1] + 1]) - 1:
            mat_end[1] += 1
    except (TypeError, IndexError, ValueError):
        pass
    # Followed by a nan
    if not (isinstance(st_mat.iloc[0, mat_end[1] + 1], (int, float))
            or st_mat.iloc[0, mat_end[1] + 1].isnumeric())\
            or not math.isnan(st_mat.iloc[0, mat_end[1] + 1]):
        raise SyntaxError("Invalid matrix format: The first row of the "
                          "stoichiometric matrix must start with \"i\", "
                          "then an increasing sequence of integers starting "
                          "at 1 and end with an empty cell.")
    mat_end[1] += 1
    # Walk over rows to the bottom
    # Second entry must be NaN, then j and then 1
    if not (isinstance(st_mat.iloc[1, 0], (int, float))
            or st_mat.iloc[1, 0].isnumeric())\
            or not math.isnan(st_mat.iloc[1, 0]) \
            or st_mat.iloc[2, 0] != "j" \
            or not (isinstance(st_mat.iloc[3, 0], (int, float))
                    or st_mat.iloc[3, 0].isnumeric())\
            or int(st_mat.iloc[3, 0]) != 1:
        raise SyntaxError("Invalid matrix format: The first column of the "
                          "stoichiometric matrix must start with an empty "
                          "cell, then \"j\", an increasing sequence of "
                          "integers starting at 1. Then it either ends with "
                          "an empty cell or the composition matrix follows "
                          "with \"k\" and another increasing sequence of "
                          "integers starting at 1 or a row starting with \"is_constant\" is "
                          "added to indicate if some states are to be kept constant.")
    mat_end[0] = 3
    # Followed by increasing row of integers
    try:
        while int(st_mat.iloc[mat_end[0], 0]) == \
                int(st_mat.iloc[mat_end[0] + 1, 0]) - 1:
            mat_end[0] += 1
    except (TypeError, IndexError, ValueError):
        pass
    # Followed by k and 1 if composition matrix is added
    if st_mat.shape[0] > mat_end[0] + 1 \
            and st_mat.iloc[mat_end[0] + 1, 0] == "k":
        if int(st_mat.iloc[mat_end[0] + 2, 0]) != 1:
            raise SyntaxError("Invalid matrix format: The first column of the "
                              "stoichiometric matrix must start with an empty "
                              "cell, then \"j\", an increasing sequence of "
                              "integers starting at 1. Then it either ends "
                              "with an empty cell or the composition matrix "
                              "follows with \"k\" and another increasing "
                              "sequence of integers starting at 1 or a row starting with \"is_constant\" is "
                              "added to indicate if some states are to be kept constant.")
        # Save k-index for later
        k_ind = mat_end[0] + 1
        mat_end[0] += 2
        # Followed by increasing row of integers
        try:
            while int(st_mat.iloc[mat_end[0], 0]) == \
                    int(st_mat.iloc[mat_end[0] + 1, 0]) - 1:
                mat_end[0] += 1
        except (TypeError, IndexError, ValueError):
            pass
    elif st_mat.shape[0] == mat_end[0] + 1 \
            or not (isinstance(st_mat.iloc[mat_end[0] + 1, 0], (int, float))
                    or st_mat.iloc[mat_end[0] + 1, 0].isnumeric())\
            or math.isnan(float(st_mat.iloc[mat_end[0] + 1, 0])):
        k_ind = mat_end[0] + 1
    else:
        raise SyntaxError("Invalid matrix format: The first column of the "
                          "stoichiometric matrix must start with an empty "
                          "cell, then \"j\", an increasing sequence of "
                          "integers starting at 1. Then it either ends with "
                          "an empty cell or the composition matrix follows "
                          "with \"k\" and another increasing sequence of "
                          "integers starting at 1.")
    # Followed by a row is_constant if the states can be constant
    # Save const_ind for later
    const_ind = mat_end[0] + 1
    if st_mat.shape[0] > mat_end[0] + 1 \
            and st_mat.iloc[mat_end[0] + 1, 0] == "is_constant":
        # Is only single row
        mat_end[0] += 1

    # Crop matrix to final format
    st_mat = st_mat.iloc[:mat_end[0]+1, :mat_end[1]+1]

    state_vars = st_mat.iloc[1, 2:-1]
    state_units = st_mat.iloc[2, 2:-1].rename(index=state_vars)
    process_names = st_mat.iloc[3:k_ind, 1]
    process_rates = st_mat.iloc[3:k_ind, -1].rename(index=process_names)
    stoichiometry = st_mat.iloc[3:k_ind, 2:-1].fillna(0)\
        .rename(columns=state_vars, index=process_names)
    con_names = st_mat.iloc[k_ind + 1:const_ind, 1]
    composition = st_mat.iloc[k_ind + 1:const_ind, 2:-1].fillna(0)\
        .rename(columns=state_vars, index=con_names)
    is_const = st_mat.iloc[const_ind:, 2:-1]
    if is_const.empty:
        is_const = pd.Series([False]*len(state_vars))
    else:
        is_const = pd.to_numeric(is_const.iloc[0, :], errors='coerce').rename(state_vars) \
            .apply(
            lambda x: not math.isnan(x) and not x == 0 and x == 1
        )
    return stoichiometry, composition, process_rates, state_vars, state_units, is_const


def __extract_parameters(params_mat: pd.DataFrame) -> typing.Tuple[pd.Series,
                                                                   pd.Series]:
    """
    Extracts the parameters and the values, if set, from the given dataframe.

    The parameters and values have to be marked by the following markers:

    * It consists of two columns with headers "Parameters" (or "Parameter",
      "Params", "Param" or all letters being lowercase) and "Values" (or
      "Value", or with lowercase "v"). The "Values" column must be directly
      right of the "Parameters" column, i.e. the cell right of "Parameters"
      must contain "Values".
    * The "Parameters" column contain each a parameter, the "Values" column the
      value of the parameters on its left. If the cell is left empty, the
      parameter is considered to be provided at runtime.
    * The columns are terminated by an empty cell.

    Similar to the stoichiometric matrix, also other entries might be in the
    dataframe. The only condition is that none of them match neither the regex
     "[pP]aram(eter)?s?" nor "[vV]alues?".

    :param params_mat: Pandas dataframe containing the parameters
    :return: Strings of the parameter names and the parameter values as pandas
        series
    :raises SyntaxError: If the parameters does not match the form described
        above
    """
    # Get matrix from start (Denoted by "Parameters" and "Values" in adjacent
    # cells)
    param_idx = np.where(params_mat.map(lambda x: bool(re.search(
        "[pP]aram(eter)?s?", str(x)))))
    value_idx = np.where(params_mat.map(lambda x: bool(re.search(
        "[vV]alues?", str(x)))))
    if any([len(x) != 1 for x in param_idx]) \
            or any([len(x) != 1 for x in value_idx]) \
            or param_idx[0][0] != value_idx[0][0] \
            or param_idx[1][0] != value_idx[1][0] - 1:
        raise SyntaxError("Invalid parameters format: The parameters and "
                          "their values must be defined in two adjacent "
                          "columns with headers matching \"[pP]aram(eter)?s?\""
                          " and right of it \"[vV]alues?\".")
    params_mat = params_mat.iloc[param_idx[0][0]:,
                                 param_idx[1][0]:value_idx[1][0]+1]

    # Find end of params section (Denoted by empty row -> NaN in param column)
    end_idx = np.where(params_mat.iloc[:, 0].isna())
    if len(end_idx[0]) > 0:
        params_mat = params_mat.iloc[:min(end_idx[0]), :]

    params = params_mat.iloc[1:, 0]
    values = params_mat.iloc[1:, 1].apply(__evaluate).rename(index=params)
    if any([not isinstance(x, numbers.Number) for x in values]):
        raise SyntaxError("Invalid parameters format: All parameter values "
                          "must be numeric.")
    return params, values


def __evaluate(x: typing.Any) -> typing.Any:
    """
    Function used to evaluate the given input.

    :param x: The input to evaluate
    :return: The result of the evaluation
    """
    # TODO This is unsafe code as the input is not sanitized!
    #  Consider sanitizing it before usage, e.g. using pyparsing
    #  https://stackoverflow.com/questions/23879784/parse-mathematical-expressions-with-pyparsing
    return sympy.sympify(x)


def __get_process_rates_and_params(rates: pd.Series,
                                   params: pd.Series, param_values: pd.Series,
                                   states: pd.Series, st: pandas.DataFrame) \
        -> typing.Tuple[sympy.Matrix,
                        typing.Tuple[sympy.Symbol, ...],
                        typing.Tuple[sympy.Symbol, ...],
                        np.ndarray]:
    """
    Transforms the process rates, parameters and states to sympy.
    Substitutes and removes the parameters with given values.

    :param rates: Pandas series containing the strings with the process rates
    :param params: Pandas series containing the parameter names
    :param param_values: Pandas series containing the parameter values
    :param states: Pandas series containing the state names
    :return: rates, states and params as sympy objects. Rates is a sympy array
        with the process rate functions, states and params are tuples of sympy
        symbols
    """
    # Get sympy of parameters and state
    const = params.iloc[(param_values != sympy.nan).to_numpy()]
    const_val = param_values.iloc[(param_values != sympy.nan).to_numpy()]
    params = params.iloc[(param_values == sympy.nan).to_numpy()]

    if len(params) == 0:
        params_sp = tuple()
    else:
        params_sp = sympy.symbols(' '.join([str(x) for x in params]))
    state_sp = sympy.symbols(' '.join([str(x) for x in states]))
    # Ensure params_sp and state_sp are tuples (also if only one value)
    if not isinstance(params_sp, tuple):
        params_sp = (params_sp, )
    if not isinstance(state_sp, tuple):
        state_sp = (state_sp, )

    # TODO This is unsafe code as the input is not sanitized!
    #  Consider sanitizing it before usage, e.g. using pyparsing
    #  https://stackoverflow.com/questions/23879784/parse-mathematical-expressions-with-pyparsing
    rates_sp = sympy.sympify(rates.to_numpy())
    rates_sp = rates_sp.subs(dict(zip(const, const_val))).simplify()

    st_np = st.map(
        lambda x: x.subs(dict(zip(const, const_val))).simplify()
    ).to_numpy()
    return rates_sp, state_sp, params_sp, st_np


def get_reaction_rates(mat_path: str, param_path: str = None, **kwargs) \
        -> typing.Tuple[typing.List[str], typing.List[typing.List[str]],
                        typing.Callable[[np.array, np.array], np.array],
                        sympy.Matrix, typing.Dict[str, float], typing.List[bool]]:
    """
    Get the states, the parameters and the reaction rates from the given files.

    :param mat_path: Path to the file containing the stoichiometric matrix. All
        file types supported by :py:func:`pandas.read_excel` or
        :py:func:`pandas.read_csv` are supported.
    :param param_path: Path to the file containing the parameters. If none
        given, the same as for the stoichiometric matrix is taken. The
        supported file types are the same as for the stoichiometric matrix
        file.
    :return: The names of the states, the names of the parameters, a
        callable to evaluate the reaction rates as :math:`r = f(x, p)` where
        ``x`` is the state and ``p`` the parameter vector, the reaction rates
        as sympy equations, the parameter values and a boolean array being true
        for the states that should be kept constant.
    :raises SyntaxError: If any of the matrices are not in the required format
    """
    if param_path is None:
        param_path = mat_path
    # Read stoichiometric matrix and parameters
    try:
        st_mat = pd.read_excel(mat_path, header=None, dtype=str, **kwargs)
    except ValueError:
        st_mat = pd.read_csv(mat_path, header=None, dtype=str, **kwargs)
    try:
        params = pd.read_excel(param_path, header=None, dtype=str, **kwargs)
    except ValueError:
        params = pd.read_csv(param_path, header=None, dtype=str, **kwargs)

    # Extract matrices and states
    st, comp, rates, states, state_units, is_const = __extract_submatrices(st_mat)
    st = st.map(__evaluate)
    comp = comp.map(__evaluate)

    # Check composition matrix
    if not np.all(np.abs(st.to_numpy()@comp.to_numpy().T) < COMPOSITION_TOL):
        raise ValueError("Conservatives are not conserved.")

    # Extract parameters and values
    params, param_values = __extract_parameters(params)
    param_values = param_values.apply(__evaluate)

    # Get sympy evaluations of process rates as function of parameters and
    # states
    rates_sp, states_sp, params_sp, st_np \
        = __get_process_rates_and_params(rates, params, param_values,
                                         states, st)

    # Calculate reaction rates
    reaction_rates = sympy.sympify(st_np.T@rates_sp)

    # Set the rates of constant states to zero
    reaction_rates = sympy.sympify([0 if c else r for c, r in zip(is_const, reaction_rates)])

    # Get reaction rates as lambda function and return
    reaction_rates_lamb = sympy.lambdify(
        (states_sp, [params_sp] if len(params_sp) > 0 else []),
        reaction_rates,
        cse=True
    )

    return ([str(x) for x in states_sp],
            [[str(x) for x in params_sp]] if len(params_sp) > 0 else [],
            reaction_rates_lamb, reaction_rates,
            param_values[param_values.apply(
                lambda x: x != sympy.nan
            ).to_numpy()].to_dict(),
            is_const.tolist())
