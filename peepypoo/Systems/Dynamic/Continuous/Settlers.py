import collections.abc

import numpy as np
import sympy
import typing
import juliacall

import peepypoo.Systems
import peepypoo.Utilities as Utilities
from peepypoo.Systems.Dynamic._DynamicSystems import \
    ContinuousTimeDynamicalSystem
from peepypoo.SignalTypes import get_signal_class

__all__ = ['SettlerTakacs']


class SettlerTakacs(ContinuousTimeDynamicalSystem):
    """
    A dynamic settler according to

        Takács I., Patry G.G., Nolasco D. (1991) A dynamic model of the clarification thickening process,
        Water Research, 25(10), 1263-1271

    Splits the inflow into two flows, one of which contains
    the settled sludge while the other contains the clarified inflow.

    The resulting system has the following inputs and outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    | Input| 0     | q_in    | float            | The input flow rate of the  |
    |      |       |         |                  | clarifier.                  |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1     | C_in    | np.array[float]  | The concentrations in the   |
    |      |       |         | Size: n_mat x 1  | inflow.                     |
    +      +-------+---------+------------------+-----------------------------+
    |      | 2     | q_out   | float            | The output flow rate of the |
    |      |       |         |                  | clarifier. Sludge flow or   |
    |      |       |         |                  | clarified effluent flow     |
    |      |       |         |                  | depending on the            |
    |      |       |         |                  | ``provided_outflow`` value. |
    +------+-------+---------+------------------+-----------------------------+
    |Output| 0     | q_out_e | float            | The output flow rate of the |
    |      |       |         |                  | clarified effluent.         |
    +      +-------+---------+------------------+-----------------------------+
    |      | 1     | C_out_e | np.array[float]  | The output concentrations   |
    |      |       |         | Size: n_mat x 1  | in the clarified effluent.  |
    +      +-------+---------+------------------+-----------------------------+
    |      | 2     | q_out_s | float            | The output flow rate of the |
    |      |       |         |                  | sludge effluent.            |
    +      +-------+---------+------------------+-----------------------------+
    |      | 3     | C_out_s | np.array[float]  | The output concentrations   |
    |      |       |         | Size: n_mat x 1  | in the sludge effluent.     |
    +------+-------+---------+------------------+-----------------------------+

    :param num_materials: The number of materials in the flow for which
        concentrations are given.
    :param solid_materials: Boolean array which indicates which of the
        tracked materials are solid (``True`` if solid, ``False`` if liquid).
    :param provided_outflow: Which outflow is provided. Either ``liquid`` if
        the flow rate of the liquid output is provided or ``sludge`` if the
        flow rate of the sludge is given.
    :param num_layers: The number of layers in the settler model
    :param Xf: The function to compute the TSS concentration from the concentrations in the flow.
        Takes a list with the concentrations as input and returns the TSS concentration.
    :param feedlayer: The index of the feed layer (0-based)
    :param A: The settler area
    :param z: The height of the settler layers
    :param v0_max: Maximum settling velocity
    :param v0: Maximum Vesilind settling velocity
    :param rh: Hindered zone settling parameter
    :param rp: Flocculant zone settling parameter
    :param fns: Non-settleable fraction
    :param Xt: Threshold concentration
    :param initial_state: The initial state for the settler as numpy array. Ordering of variables is
        [X0, C0_l0, ..., Cn_l0, X1, C0_l1, ..., Cn_l1, ..., Xm, C0_lm, Cn_lm]
        where m=num_layers-1, n=num_materials-1, Xi stand for the TSS concentration in layer i, and Ci_lj is the
        concentration of the *soluble* component i in layer j.
        .. warning:

            The initial concentrations Ci_lj is only for the soluble components, as the particulate components
            are all lumped in Xj. Hereby the ordering is the same as in the input, just with the particulate
            components skipped.

    :param initial_t: The time at which the initial state holds
    :param name: The name of the system.
    """

    _provided_outflow: typing.Literal["effluent", "sludge"]
    """ 
    The outflow rate which is provided as input, either liquid or sludge
    """
    num_materials: int
    """ The number of materials in the flow """
    _solid_materials: typing.List[bool]
    """ Boolean array which is true for all solid components in the inflow. """
    num_layers: int
    """ The number of layers in the settler model """
    Xf: typing.Callable[[typing.List[typing.Any]], typing.Any]
    """ 
    The function to compute the TSS concentration from the concentrations in the flow.
    Takes a list with the concentrations as input and returns the TSS concentration
    """
    feedlayer: int
    """ The index of the feed layer (0-based) """
    A: float
    """ The settler area """
    z: typing.List[float]
    """ The height of the settler layers """
    v0_max: float
    """ Maximum settling velocity """
    v0: float
    """ Maximum Vesilind settling velocity """
    rh: float
    """ Hindered zone settling parameter """
    rp: float
    """ Flocculant zone settling parameter """
    fns: float
    """ Non-settleable fraction """
    Xt: float
    """ Threshold concentration """

    def __init__(self, *ct_dyn_sys_args,
                 num_materials: int, solid_materials: typing.List[bool],
                 provided_outflow: typing.Literal["effluent", "sludge"] = "sludge",
                 num_layers: int, Xf: typing.Callable[[typing.List[typing.Any]], typing.Any], feedlayer: int,
                 A: float, z: typing.Union[float, typing.List[float]], v0_max: float, v0: float, rh: float,
                 rp: float, fns: float, Xt: float,
                 initial_state: np.array = None, initial_t: float = 0, initial_output: np.array = None,
                 name: str = "Settler", **ct_dyn_sys_kwargs):

        if isinstance(z, collections.abc.Sized):
            if len(z) != num_layers:
                raise ValueError("Invalid input for variable z: Must be a single "
                                 "value for the heights of each layer or a vector"
                                 "of length of the number of layers containing"
                                 "the height of each individual layer.")
        else:
            z = [z] * num_layers

        if initial_state is None:
            initial_state = np.ones(num_layers*(sum(np.bitwise_not(solid_materials)) + 1))

        # Assemble input and output symbols and types
        c_nums = [str(i) for i in range(num_materials)]
        inputs = [sympy.Symbol('q_in'),
                  [sympy.Symbol('C_in' + i) for i in c_nums],
                  sympy.Symbol('q_out')]
        inputs = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x), inputs
        )
        input_types = [get_signal_class(0.0),
                       get_signal_class(np.array([0.0]*num_materials)),
                       get_signal_class(0.0)]
        outputs = [sympy.Symbol('q_out_e'),
                   [sympy.Symbol('C_out_e' + i) for i in c_nums],
                   sympy.Symbol('q_out_s'),
                   [sympy.Symbol('C_out_s' + i) for i in c_nums]]
        outputs = Utilities.apply_to_iter_in_nested_list(
            lambda x: sympy.Tuple(*x), outputs
        )
        output_types = [get_signal_class(0.0),
                        get_signal_class(np.array([0.0]*num_materials)),
                        get_signal_class(0.0),
                        get_signal_class(np.array([0.0] * num_materials))]
        # Assemble state symbols
        states = [*Utilities.flatten(
            [[sympy.Symbol('X' + str(m)), *[sympy.Symbol('C' + str(i) + '_l' + str(m))
                                            for i in range(num_materials) if not solid_materials[i]]]
             for m in range(num_layers)]
        )]

        t = sympy.symbols('t')
        super().__init__(*ct_dyn_sys_args,
                         num_states=num_materials*num_layers, num_inputs=3, num_outputs=4,
                         input_types=input_types, output_types=output_types,
                         t_sym=t, states=states, inputs=inputs, outputs=outputs,
                         initial_state=initial_state, initial_t=initial_t, initial_output=initial_output, name=name,
                         **ct_dyn_sys_kwargs)

        # Assign variables
        self._provided_outflow = provided_outflow
        self.num_materials = num_materials
        self._solid_materials = solid_materials
        self.num_layers = num_layers
        self.Xf = Xf
        self.feedlayer = feedlayer
        self.A = A
        self.z = z
        self.v0_max = v0_max
        self.v0 = v0
        self.rh = rh
        self.rp = rp
        self.fns = fns
        self.Xt = Xt

        # Set the initial output
        if initial_output is None:
            self._initial_output = self.calculate_output(initial_state, [0, np.ones(num_materials), 0],
                                                         [], initial_t)

        # Assemble sympy formula
        # Inflow and outflow velocities
        vin = inputs[0]/A
        if provided_outflow == "sludge":
            vdn = inputs[2]/A
            vup = vin - vdn
        else:
            vup = inputs[2]/A
            vdn = vin - vup

        # TSS inflow
        Xf = self.Xf(inputs[1])

        # Velocities and fluxes
        vs = np.zeros(num_layers, dtype=object)
        Js = np.zeros(num_layers, dtype=object)
        Jsc = np.zeros(num_layers, dtype=object)
        for m in range(num_layers):
            vs[m] = sympy.Max(0.0,
                              sympy.Min(v0_max,
                                        v0*(sympy.exp(-rh*(states[self._idx(m, 0)]-fns*Xf))
                                            - sympy.exp(-rp*(states[self._idx(m, 0)]-fns*Xf)))
                                        )
                              )
            Js[m] = vs[m]*states[self._idx(m, 0)]
            if m > feedlayer:
                Jsc[m] = sympy.Piecewise(
                    (sympy.Min(Js[m], Js[m-1]), states[self._idx(m-1, 0)] > Xt),
                    (Js[m], True)
                )

        # Placeholder for the derivatives
        d = np.zeros((num_materials - sum(solid_materials) + 1)*num_layers, dtype=object)
        for m in range(num_layers):
            # Particulate
            if m == feedlayer:  # Feed layer
                d[self._idx(m, 0)] = \
                    (vin * Xf + Jsc[m+1] - (vup + vdn) * states[self._idx(m, 0)] - sympy.Min(Js[m], Js[m-1])) \
                    / z[m]
            elif m == 0:  # Bottom layer
                d[self._idx(m, 0)] = \
                    (vdn * (states[self._idx(m+1, 0)] - states[self._idx(m, 0)]) + sympy.Min(Js[m], Js[m+1])) \
                    / z[m]
            elif m == self.num_layers-1:  # Top layer
                d[self._idx(m, 0)] = \
                    (vup * (states[self._idx(m-1, 0)] - states[self._idx(m, 0)]) - Jsc[m]) \
                    / z[m]
            elif m < self.feedlayer:  # Layers below feed layer
                d[self._idx(m, 0)] = \
                    (vdn * (states[self._idx(m+1, 0)] - states[self._idx(m, 0)])
                     + sympy.Min(Js[m], Js[m+1]) - sympy.Min(Js[m], Js[m-1])) \
                    / z[m]
            elif m > self.feedlayer:  # Layers above feed layer
                d[self._idx(m, 0)] = \
                    (vup * (states[self._idx(m-1, 0)] - states[self._idx(m, 0)]) + Jsc[m+1] - Jsc[m]) \
                    / z[m]
            # Soluble
            for i in range(self.num_materials):
                if not self._solid_materials[i]:
                    if m == self.feedlayer:
                        d[self._idx(m, i+1)] = (vin * inputs[1][i] - (vdn + vup)*states[self._idx(m, i+1)]) / z[m]
                    elif m < self.feedlayer:
                        d[self._idx(m, i+1)] = (vdn * (states[self._idx(m+1, i+1)] - states[self._idx(m, i+1)])) / z[m]
                    elif m > self.feedlayer:
                        d[self._idx(m, i+1)] = (vup * (states[self._idx(m-1, i+1)] - states[self._idx(m, i+1)])) / z[m]

        self.dynamics_formula = sympy.Tuple(*d)

        # The output formula
        # Effluent flow
        C_out_e = np.zeros(num_materials, dtype=object)
        C_out_e[np.bitwise_not(solid_materials)] = states[self._idx(num_layers - 1, 1):]
        gamma = states[self._idx(num_layers - 1, 0)] / Xf
        C_out_e = [C_out_e[i] if not solid_materials[i] else inputs[1][i] * gamma for i in range(num_materials)]

        # Underflow sludge
        C_out_s = np.zeros(num_materials, dtype=object)
        C_out_s[np.bitwise_not(solid_materials)] = states[1:num_materials-sum(solid_materials)+1]
        gamma = states[self._idx(0, 0)] / Xf
        C_out_s = [C_out_s[i] if not solid_materials[i] else inputs[1][i] * gamma for i in range(num_materials)]

        if self._provided_outflow == "sludge":
            sludge_out = inputs[2]
            liquid_out = inputs[0] - sludge_out
        else:
            liquid_out = inputs[2]
            sludge_out = inputs[0] - liquid_out
        self.output_formula = sympy.Tuple(liquid_out, sympy.Tuple(*C_out_e), sludge_out, sympy.Tuple(*C_out_s))

    def f(self, x: typing.Any, u: typing.List[typing.Any],
          p: typing.List[typing.Any], t: float) \
            -> typing.Any:
        """
        Settler dynamics.

        The dynamics of the Settler.

        :param x: The state at time t
        :param u: The input at time t
        :param p: The parameters at time t
        :param t: The time t
        :return: The derivative of the state at the given time
        """

        # Inflow and outflow velocities
        vin = u[0]/self.A
        if self._provided_outflow == "sludge":
            vdn = u[2]/self.A
            vup = vin - vdn
        else:
            vup = u[2]/self.A
            vdn = vin - vup
        # TSS inflow
        Xf = self.Xf(u[1])

        # Velocities and fluxes
        vs = np.zeros(self.num_layers)
        Js = np.zeros(self.num_layers)
        Jsc = np.zeros(self.num_layers)
        for m in range(self.num_layers):
            vs[m] = max(0.0, min(self.v0_max,
                                 self.v0*(np.exp(-self.rh*(x[self._idx(m, 0)]-self.fns*Xf))
                                          - np.exp(-self.rp*(x[self._idx(m, 0)]-self.fns*Xf)))
                                 )
                        )
            Js[m] = vs[m]*x[self._idx(m, 0)]
            if m > self.feedlayer:
                if x[self._idx(m-1, 0)] > self.Xt:
                    Jsc[m] = min(Js[m], Js[m-1])
                else:
                    Jsc[m] = Js[m]

        # Placeholder for the derivatives
        d = np.zeros((self.num_materials - sum(self._solid_materials) + 1)*self.num_layers)
        for m in range(self.num_layers):
            # Particulate
            if m == self.feedlayer:  # Feed layer
                d[self._idx(m, 0)] = \
                    (vin * Xf + Jsc[m+1] - (vup + vdn) * x[self._idx(m, 0)] - min(Js[m], Js[m-1])) \
                    / self.z[m]
            elif m == 0:  # Bottom layer
                d[self._idx(m, 0)] = \
                    (vdn * (x[self._idx(m+1, 0)] - x[self._idx(m, 0)]) + min(Js[m], Js[m+1])) \
                    / self.z[m]
            elif m == self.num_layers-1:  # Top layer
                d[self._idx(m, 0)] = \
                    (vup * (x[self._idx(m-1, 0)] - x[self._idx(m, 0)]) - Jsc[m]) \
                    / self.z[m]
            elif m < self.feedlayer:  # Layers below feed layer
                d[self._idx(m, 0)] = \
                    (vdn * (x[self._idx(m+1, 0)] - x[self._idx(m, 0)]) + min(Js[m], Js[m+1]) - min(Js[m], Js[m-1])) \
                    / self.z[m]
            elif m > self.feedlayer:  # Layers above feed layer
                d[self._idx(m, 0)] = \
                    (vup * (x[self._idx(m-1, 0)] - x[self._idx(m, 0)]) + Jsc[m+1] - Jsc[m]) \
                    / self.z[m]

            # Soluble
            for i in range(self.num_materials):
                if not self._solid_materials[i]:
                    if m == self.feedlayer:
                        d[self._idx(m, i+1)] = (vin * u[1][i] - (vdn + vup)*x[self._idx(m, i+1)]) / self.z[m]
                    elif m < self.feedlayer:
                        d[self._idx(m, i+1)] = (vdn * (x[self._idx(m+1, i+1)] - x[self._idx(m, i+1)])) / self.z[m]
                    elif m > self.feedlayer:
                        d[self._idx(m, i+1)] = (vup * (x[self._idx(m-1, i+1)] - x[self._idx(m, i+1)])) / self.z[m]

        return d

    def calculate_output(self,
                         x: typing.Any, u: typing.Any,
                         p: typing.Any, t: float) -> typing.List[typing.Any]:
        """
        Output the outflows as [q_out_e, C_out_e, q_out_s, C_out_s].

        :param x: State at the time to calculate the output
        :param u: Input at the time to calculate the output
        :param p: Parameters at the time to calculate the output
        :param t: Time to calculate the output at
        :return: The output at the given time
        """
        # TSS inflow
        Xf = self.Xf(u[1])

        # Effluent flow
        C_out_e = np.zeros(self.num_materials)
        C_out_e[np.bitwise_not(self._solid_materials)] = x[self._idx(self.num_layers-1, 1):]
        gamma = x[self._idx(self.num_layers-1, 0)] / Xf
        C_out_e[self._solid_materials] = u[1][self._solid_materials] * gamma

        # Underflow sludge
        C_out_s = np.zeros(self.num_materials)
        C_out_s[np.bitwise_not(self._solid_materials)] = x[1:self.num_materials-sum(self._solid_materials)+1]
        gamma = x[self._idx(0, 0)] / Xf
        C_out_s[self._solid_materials] = u[1][self._solid_materials] * gamma

        if self._provided_outflow == "sludge":
            sludge_out = u[2]
            liquid_out = u[0] - sludge_out
        else:
            liquid_out = u[2]
            sludge_out = u[0] - liquid_out

        return [liquid_out, C_out_e, sludge_out, C_out_s]

    def _idx(self, m, i):
        return m * (self.num_materials - sum(self._solid_materials) + 1) + (i - sum(self._solid_materials[:max(i-1, 0)]))

    def _compile_julia_system(self) -> None:
        # Call superclass method for basic definitions
        # (not of the DynamicalSystem as there it would infer the dynamics
        # from the symbolics which we redefine here)
        (super(peepypoo.Systems.Dynamic.DynamicalSystem, self)
         ._compile_julia_system())
        # Get the dynamics subsystem
        d_state_jl = [self._Dt_jl(s)
                      for s in Utilities.flatten(self.system_states_jl)]
        # Inflow and outflow velocities
        vin = self.system_inputs_jl[0] / self.A
        if self._provided_outflow == "sludge":
            vdn = self.system_inputs_jl[2] / self.A
            vup = vin - vdn
        else:
            vup = self.system_inputs_jl[2] / self.A
            vdn = vin - vup
        # TSS inflow
        Xf = self.Xf(self.system_inputs_jl[1])

        # Velocities and fluxes
        vs = np.zeros(self.num_layers, dtype=object)
        Js = np.zeros(self.num_layers, dtype=object)
        Jsc = np.zeros(self.num_layers, dtype=object)
        for m in range(self.num_layers):
            vs[m] = self.jl.max(
                0.0,
                self.jl.min(
                    self.v0_max,
                    self.v0 * (self.jl.exp(
                        -self.rh * (self.system_states_jl[self._idx(m, 0)]
                                    - self.fns * Xf))
                               - self.jl.exp(
                        -self.rp * (self.system_states_jl[self._idx(m, 0)]
                                    - self.fns * Xf)))
                )
            )
            Js[m] = vs[m] * self.system_states_jl[self._idx(m, 0)]
            if m > self.feedlayer:
                Jsc[m] = self.jl.ifelse(
                    self.system_states_jl[self._idx(m - 1, 0)] > self.Xt,
                    self.jl.min(Js[m], Js[m - 1]),
                    Js[m]
                )

        # Placeholder for the derivatives
        dyn_jl = np.zeros((self.num_materials - sum(
            self._solid_materials) + 1) * self.num_layers, dtype=object)
        for m in range(self.num_layers):
            # Particulate
            if m == self.feedlayer:  # Feed layer
                dyn_jl[self._idx(m, 0)] = \
                    (vin * Xf + Jsc[m + 1] - (vup + vdn) * self.system_states_jl[self._idx(m, 0)]
                     - self.jl.min(Js[m], Js[m - 1])) \
                    / self.z[m]
            elif m == 0:  # Bottom layer
                dyn_jl[self._idx(m, 0)] = \
                    (vdn * (self.system_states_jl[self._idx(m + 1, 0)] - self.system_states_jl[self._idx(m, 0)])
                     + self.jl.min(Js[m], Js[m + 1])) \
                    / self.z[m]
            elif m == self.num_layers - 1:  # Top layer
                dyn_jl[self._idx(m, 0)] = \
                    (vup * (self.system_states_jl[self._idx(m - 1, 0)] - self.system_states_jl[self._idx(m, 0)])
                     - Jsc[m]) \
                    / self.z[m]
            elif m < self.feedlayer:  # Layers below feed layer
                dyn_jl[self._idx(m, 0)] = \
                    (vdn * (self.system_states_jl[self._idx(m + 1, 0)] - self.system_states_jl[self._idx(m, 0)])
                     + self.jl.min(Js[m], Js[m + 1]) - self.jl.min(Js[m], Js[m - 1])) \
                    / self.z[m]
            elif m > self.feedlayer:  # Layers above feed layer
                dyn_jl[self._idx(m, 0)] = \
                    (vup * (self.system_states_jl[self._idx(m - 1, 0)] - self.system_states_jl[self._idx(m, 0)])
                     + Jsc[m + 1] - Jsc[m]) \
                    / self.z[m]
            # Soluble
            for i in range(self.num_materials):
                if not self._solid_materials[i]:
                    if m == self.feedlayer:
                        dyn_jl[self._idx(m, i + 1)] = \
                            (vin * self.system_inputs_jl[1][i]
                             - (vdn + vup) * self.system_states_jl[self._idx(m, i + 1)]) \
                            / self.z[m]
                    elif m < self.feedlayer:
                        dyn_jl[self._idx(m, i + 1)] = \
                            (vdn * (self.system_states_jl[self._idx(m + 1, i + 1)]
                                    - self.system_states_jl[self._idx(m, i + 1)])) \
                            / self.z[m]
                    elif m > self.feedlayer:
                        dyn_jl[self._idx(m, i + 1)] = \
                            (vup * (self.system_states_jl[self._idx(m - 1, i + 1)]
                                    - self.system_states_jl[self._idx(m, i + 1)])) \
                            / self.z[m]

        # get the dynamics
        dyn_eqs = [self.jl.Equation(ds, dyn)
                   for ds, dyn in zip(Utilities.flatten(d_state_jl),
                                      Utilities.flatten(dyn_jl))]

        # Get the outputs subsystem
        # Flatten everything
        system_outp_vec = [*Utilities.flatten(self.system_outputs_jl)]
        # Make multiple output variables if the output is vectorial
        if (len(system_outp_vec) != len([*Utilities.flatten(
                self._output_lambda(self.states,
                                    self.inputs,
                                    self.params,
                                    self.t))])):
            system_outp_vec = self.jl_symvec_pad_vectorial(
                self.jl, system_outp_vec,
                self._output_lambda(self.states,
                                    self.inputs,
                                    self.params,
                                    self.t), self.t_jl)
        outp_jl = self._output_lambda(self.system_states_jl,
                                      self.system_inputs_jl,
                                      self.system_params_jl,
                                      self.t_jl)

        outp_eqs = [self.jl.Equation(y, outp)
                    for y, outp in zip(Utilities.flatten(system_outp_vec),
                                       Utilities.flatten(outp_jl))]

        # Full system
        eqs = juliacall.convert(self.jl.Vector, [*dyn_eqs, *outp_eqs])
        states = juliacall.convert(self.jl.Vector, [*Utilities.flatten(self.system_states_jl),
                                                    *Utilities.flatten(self.system_inputs_jl),
                                                    *Utilities.flatten(self.system_params_jl),
                                                    *Utilities.flatten(self.system_outputs_jl)])
        sys = self.jl.ODESystem(
            eqs, self.t_jl, states, self.jl.Vector(),
            name=self.jl.Symbol(self.name + "_" + str(id(self)))
        )

        # Assemble the IC
        states_jl = [getattr(sys, str(x))
                     for x in Utilities.flatten(self.states)]
        ic_jl = [juliacall.convert(self.jl.Tuple, (s, v))
                 for s, v in zip(states_jl,
                                 Utilities.flatten(self._initial_state))]
        ic_jl = juliacall.convert(self.jl.Vector, ic_jl)
        ic_jl = self.jl.Dict(ic_jl)

        # Assign to variables
        self._inputs_jl = Utilities.apply_nested_list(
            lambda x, t: getattr(sys, str(x)) if x else [],
            [*self.inputs, *self.params]
        )
        self._outputs_jl = Utilities.apply_nested_list(  # Get output variables
            lambda x, t: getattr(sys, str(x)[:-2 - len(str(self.t_jl))])
            if x else [],
            system_outp_vec
        )
        # Rearrange output variables to match the output definition
        self._outputs_jl = Utilities.apply_nested_list(
            lambda x, t: self._outputs_jl[
                Utilities.nested_idx_to_linear_idx(self.outputs, t)
            ],
            self.outputs
        )
        self._system_jl = sys
        self._ic_jl = ic_jl
