import abc
import math
import typing
import cachetools
import juliacall
import numpy as np
import sympy

import peepypoo.Utilities as Utilities

from peepypoo.SignalTypes import SignalType, get_signal_class
from peepypoo.Systems._System import System, \
    JuliaModelingToolkitConvertibleSystem
import peepypoo.Integrators
from peepypoo.Integrators import Integrator

__all__ = ['DynamicalSystem',
           'DiscreteTimeDynamicalSystem', 'ContinuousTimeDynamicalSystem']


#######################################################
# A dynamic system
#######################################################
class DynamicalSystem(JuliaModelingToolkitConvertibleSystem, abc.ABC):
    """
    A generic dynamical system.

    This system is an abstract representation of a dynamical system. It defines
    all common properties shared between dynamical systems. Note that the
    inputs

    :param num_inputs: The number of inputs of the dynamical system
    :param num_outputs: The number of outputs of the dynamical system
    :param num_states: The number of states of the dynamical system
    :param num_params: The number of parameters of the dynamical system
    :param initial_state: The initial state of the dynamical system
    :param initial_t: The time of the initial condition of the dynamical system
    :param input_types: The types of the input signals
    :param param_types: The types of the parameter signals
    :param output_types: The types of the output signals
    :param t_sym: Sympy symbol denoting the time variable. Defaults to t.
    :param states: Sympy symbols for the system states. Defaults to x0 to xn,
        where n is the given number of states.
    :param inputs: Sympy symbols for the system inputs. Defaults to u0 to un,
        where n is the given number of inputs.
    :param params: Sympy symbols for the system params. Defaults to p0 to pn,
        where n is the given number of params.
    :param outputs: Sympy symbols for the system outputs. Defaults to y0 to yn,
        where n is the given number of outputs.
    :param name: The name of the system
    """

    _t: float
    """ The current simulation time in the system """
    _initial_t: float
    """ The initial time """
    _num_states: int
    """ The number of states in the system """
    _state: typing.Any
    """ The current state of the system """
    _output: typing.Any
    """ The current output of the system """
    _initial_state: typing.Any
    """ The initial state of the dynamical system """
    _initial_output: typing.Any
    """ 
    The initial output of the dynamical system. 
    Needed for initialization for Python simulation.
    """
    _num_system_inputs: int
    """
    The number of inputs of this system (the total number of inputs consists of
    this as well as of the number of parameters)
    """
    _num_params: int
    """
    The number of parameters for this system (the total number of inputs 
    consists of this as well as of the number of system inputs)
    """
    _state_cache: cachetools.Cache
    """
    A cache for caching the state. Used for significant speedups if using 
    states at decreasing times, e.g. from a following dynamic system with 
    variable step integration.
    """
    # Symbolic values
    _t_sym: sympy.Symbol
    """ Sympy symbol for time """
    @property
    def t(self) -> sympy.Symbol:
        """ Sympy symbol for time """
        return self._t_sym

    @t.setter
    def t(self, value: sympy.Symbol) -> None:
        self._t_sym = value
        self._t_jl = self.jl_macro_vars(self.jl, str(value), "var")
        self._Dt_jl = self.jl.Differential(self.t_jl)

    _inputs: typing.List[sympy.Array]
    """ The names of the inputs """
    @property
    def inputs(self) -> typing.List[sympy.Array]:
        """ The names of the inputs """
        return self._inputs

    @inputs.setter
    def inputs(self, value: typing.List[sympy.Array]) -> None:
        self._inputs = value
        if value:
            self.system_inputs_jl = Utilities.apply_nested_list(
                lambda x, t:
                    self.jl_macro_vars(self.jl, str(x), "var", [self.t_jl])
                    if x else [],
                value
            )
        else:
            self.system_inputs_jl = []

    _outputs: typing.List[sympy.Array]
    """ The names of the outputs """
    @property
    def outputs(self) -> typing.List[sympy.Array]:
        """ The names of the outputs """
        return self._outputs

    @outputs.setter
    def outputs(self, value: typing.List[sympy.Array]) -> None:
        self._outputs = value
        if value:
            self.system_outputs_jl = Utilities.apply_nested_list(
                lambda x, t:
                    self.jl_macro_vars(self.jl, str(x), "var", [self.t_jl])
                    if x else [],
                value
            )
        else:
            self.system_outputs_jl = []

    _states: sympy.Array
    """ The names of the states """
    @property
    def states(self) -> sympy.Array:
        """ The names of the states """
        return self._states

    @states.setter
    def states(self, value: sympy.Array) -> None:
        self._states = value
        if value:
            self.system_states_jl = Utilities.apply_nested_list(
                lambda x, t:
                    self.jl_macro_vars(self.jl, str(x), "var", [self.t_jl])
                    if x else [],
                value
            )
        else:
            self.system_states_jl = []

    _params: typing.List[sympy.Array]
    """ The names of the parameters """
    @property
    def params(self) -> typing.List[sympy.Array]:
        """ The names of the parameters """
        return self._params

    @params.setter
    def params(self, value: typing.List[sympy.Array]) -> None:
        self._params = value
        if value:
            self.system_params_jl = Utilities.apply_nested_list(
                lambda x, t:
                    self.jl_macro_vars(self.jl, str(x), "var", [self.t_jl])
                    if x else [],
                value
            )
        else:
            self.system_params_jl = []

    _dynamics_formula: sympy.Tuple
    """ The symbolic dynamics formula """
    _dynamics_lambda: typing.Callable[[typing.List, typing.List,
                                       typing.List, float], np.ndarray]
    """ Lambda of the symbolic dynamics formula """
    @property
    def dynamics_formula(self) -> sympy.Tuple:
        """
        Sympy equations for calculating the derivative/next timestep depending
        on the current state, parameters, input and time
        """
        return self._dynamics_formula

    @dynamics_formula.setter
    def dynamics_formula(self, value: sympy.Tuple) -> None:
        if not value.free_symbols.issubset({self.t}.union(
                                            *[v.free_symbols
                                                for v in self.states]).union(
                                            *[v.free_symbols
                                                for v in self.inputs]).union(
                                            *[v.free_symbols
                                                for v in self.params])):
            raise ValueError("The symbolic equation contains symbols which "
                             "are not in the state, input or parameter vector "
                             "nor time.")
        self._dynamics_lambda = sympy.lambdify([self.states, self.inputs, self.params, self.t], value, modules=None)
        self._dynamics_formula = value

    _output_formula: sympy.Tuple
    """ The symbolic output formula """
    _output_lambda: typing.Callable[[typing.List, typing.List,
                                     typing.List, float], np.ndarray]
    """ Lambda of the symbolic output formula """
    @property
    def output_formula(self) -> sympy.Tuple:
        """
        Sympy equations for calculating the derivative/next timestep depending
        on the current state, parameters and input
        """
        return self._output_formula

    @output_formula.setter
    def output_formula(self, value: sympy.Tuple) -> None:
        if not value.free_symbols.issubset({self.t}.union(
                                            *[v.free_symbols
                                                for v in self.states]).union(
                                            *[v.free_symbols
                                                for v in self.inputs]).union(
                                            *[v.free_symbols
                                                for v in self.params])):
            raise ValueError("The symbolic output equation contains symbols "
                             "which are not in the state, input or parameter "
                             "vector nor time.")
        self._output_lambda = sympy.lambdify([self.states, self.inputs, self.params, self.t], value, modules=None)
        self._output_formula = value

    # Parameters for the Julia system
    _Dt_jl: juliacall.AnyValue
    """ The julia symbol for a time derivative """
    system_inputs_jl: typing.List[juliacall.VectorValue]
    """ The julia symbols of the system inputs"""
    system_outputs_jl: typing.List[juliacall.VectorValue]
    """ The julia symbols of the system outputs"""
    system_states_jl: typing.List[juliacall.VectorValue]
    """ The julia symbols of the system states"""
    system_params_jl: typing.List[juliacall.VectorValue]
    """ The julia symbols of the system parameters"""

    def __init__(self,
                 num_inputs: int = 0,
                 num_outputs: int = 0,
                 num_states: int = 0,
                 num_params: int = 0,
                 initial_state: typing.Any = None,
                 initial_t: float = 0,
                 initial_output: typing.Any = None,
                 input_types: typing.List[SignalType] = None,
                 param_types: typing.List[SignalType] = None,
                 output_types: typing.List[SignalType] = None,
                 cache_type: typing.Type[cachetools.Cache]
                 = cachetools.LRUCache,
                 cache_maxsize: int | float = math.inf,
                 t_sym: sympy.Symbol = sympy.symbols('t'),
                 states: typing.List[sympy.Symbol] | None = None,
                 inputs: typing.List[sympy.Symbol] | None = None,
                 params: typing.List[sympy.Symbol] | None = None,
                 outputs: typing.List[sympy.Symbol] | None = None,
                 name: str = "Dynamical System"):
        if initial_state is None:
            initial_state = []
        if input_types is None:
            input_types = [get_signal_class(None)] * num_inputs
        if param_types is None:
            param_types = [get_signal_class(None)] * num_params
        if output_types is None:
            output_types = [get_signal_class(None)] * num_outputs
        if initial_output is None:
            initial_output = [x.get_zero() for x in output_types]
        if states is None:
            states = sympy.Array([sympy.Symbol('x'+str(i))
                                  for i in range(num_states)])
        if inputs is None:
            inputs = sympy.Array([sympy.Symbol('u'+str(i))
                                  for i in range(num_inputs)])
        if params is None:
            params = sympy.Array([sympy.Symbol('p'+str(i))
                                  for i in range(num_params)])
        if outputs is None:
            outputs = sympy.Array([sympy.Symbol('y'+str(i))
                                   for i in range(num_outputs)])
        System.__init__(self, num_inputs + num_params, num_outputs, name=name)
        self.input_types = [*input_types, *param_types]
        self.output_types = output_types
        self._initial_state = initial_state
        self._initial_t = initial_t
        self._initial_output = initial_output
        self._num_states = num_states
        self._num_system_inputs = num_inputs
        self._num_params = num_params
        self._state_cache = cache_type(cache_maxsize)
        self.t = t_sym
        self.states = states
        self.inputs = inputs
        self.params = params
        self.outputs = outputs
        self.reset()

        # Reset the system if the input connection tree changes
        self.on_input_connection_tree_change_callbacks\
            .append(lambda sys, event: self.reset())

    """ The propagation rule as formula (derivative or next timestep) """

    def get_output(self, t: float) -> typing.List[typing.Any]:
        """
        Get the output of the dynamical system at the given time.

        Propagates the state through time and calculates the output. Takes care
        of restoring the state from the cache.
        :param t: The time to get the output at
        :return: The output at time t
        """
        if t == self._t:  # If we are already there, just output
            return self._output
        if not self.restore_state(t):  # If we cannot restore: Go back to initial state
            self._state = self._initial_state
            self._t = self._initial_t
            return self._initial_output
        self.propagate_state(t)
        inp = self._get_inputs(self._t)
        inpu = inp[0:self._num_system_inputs]
        params = inp[self._num_system_inputs:
                     self._num_system_inputs + self._num_params]
        self._output = self.calculate_output(self._state, inpu, params, self._t)
        return self._output

    def reset(self) -> None:
        """
        Resets the dynamical system to the initial state
        :return: Nothing
        """
        self._state = self._initial_state
        self._output = self._initial_output
        self._t = self._initial_t
        self._state_cache.clear()
        self._state_cache[self._initial_t] = self._initial_state

    def _add_system_input_port(self, n: int, input_types: typing.List[peepypoo.
                               SignalTypes.SignalType] | peepypoo.SignalTypes.
                               SignalType = None) -> None:
        """
        Adds system input ports to the dynamical system. Note that this
        implementation of dynamical systems differentiates the inputs into
        system inputs and parameters. This function then adds system inputs.

        .. warning::

            Changing the ports of the system might break the system. Thus
            use this functionality carefully and adapt the system code
            accordingly to match the new connection configuration.

        :param n: The number of inputs to add
        :param input_types: The types of the inputs to add
        :return: Nothing
        """
        # Remove parameter ports
        param_port_types = self.input_types[
                           self._num_system_inputs:
                           self._num_system_inputs + self._num_params]
        n_params = self._num_params
        param_connections = []
        for i in range(self._num_system_inputs,
                       self._num_system_inputs + self._num_params):
            input_map = self.input_mapping[i, :]
            if np.all(input_map != -1):
                sys = self.input_systems[input_map[0]]
                inp_port = [i + n]  # Directly adapted for with new inputs
                out_port = [input_map[1]]
                param_connections.append([sys, inp_port, out_port])
            self._remove_param_port([i - self._num_system_inputs])

        # Add new ports
        self._num_system_inputs += n
        super()._add_input_port(n, input_types)

        # Add parameter ports back
        self._add_param_port(n_params, param_port_types)
        for conn in param_connections:
            self.add_input_connection(*conn)

    def _remove_system_input_port(self, input_ports: typing.List[int]) -> None:
        """
        Removes the system input ports at the given positions from the
        dynamical system. Note that this implementation of dynamical systems
        differentiates the inputs into system inputs and parameters. This
        function then removes system inputs.

        .. warning::

            Changing the ports of the system might break the system. Thus
            use this functionality carefully and adapt the system code
            accordingly to match the new connection configuration.

        :param input_ports: A list of integers with the port numbers to remove
        :return: Nothing
        """
        self._num_system_inputs -= len(input_ports)
        super()._remove_input_port(input_ports)

    def _add_param_port(self, n: int, param_types: typing.List[peepypoo.
                        SignalTypes.SignalType] | peepypoo.SignalTypes.
                        SignalType = None):
        """
        Adds parameter ports to the dynamical system. Note that this
        implementation of dynamical systems differentiates the inputs into
        system inputs and parameters. This function then adds parameter ports.

        .. warning::

            Changing the ports of the system might break the system. Thus
            use this functionality carefully and adapt the system code
            accordingly to match the new connection configuration.

        :param n: The number of parameter ports to add
        :param param_types: The type of the parameter ports to add
        :return: Nothing
        """
        self._num_params += n
        super()._add_input_port(n, param_types)

    def _remove_param_port(self, param_ports: typing.List[int]) -> None:
        """
        Removes the parameter ports at the given positions from the dynamical
        system. Note that this implementation of dynamical systems
        differentiates the inputs into system inputs and parameters. This
        function then removes parameter ports, which are numbered by zero
        starting after the inputs.

        .. warning::

            Changing the ports of the system might break the system. Thus
            use this functionality carefully and adapt the system code
            accordingly to match the new connection configuration.

        :param param_ports: Integer list of the ports to remove
        :return: Nothing
        """
        param_ports = \
            (np.array(param_ports) + self._num_system_inputs).tolist()
        self._num_params -= len(param_ports)
        super()._remove_input_port(param_ports)

    def _remove_input_port(self, input_ports: typing.List[int]) -> None:
        """
        Removes the input ports at the given ports from the system,
        without any other changes in the system. Differentiates appropriately
        between system inputs and parameter ports.

        .. warning::

            Changing the ports of the system might break the system. Thus
            use this functionality carefully and adapt the system code
            accordingly to match the new connection configuration.

        :param input_ports: The input ports to remove. Must be a list of
            integers with the indices of the ports to remove.
        :return: None
        """
        sys_inputs = [x for x in input_ports if x < self._num_system_inputs]
        param_inputs = [x - self._num_system_inputs
                        for x in input_ports if x >= self._num_system_inputs]
        self._remove_system_input_port(sys_inputs)
        self._remove_param_port(param_inputs)

    def _add_input_port(self, n: int, input_types: typing.List[peepypoo.
                        SignalTypes.SignalType] | peepypoo.SignalTypes.
                        SignalType = None) -> None:
        """
        Adds input ports to the existing system, without any other
        changes in the system. The new inputs are appended in the end and
        configured as parameter ports.

        .. warning::

            Changing the ports of the system might break the system. Thus
            use this functionality carefully and adapt the system code
            accordingly to match the new connection configuration.

        :param n: The number of input ports to add.
        :param input_types: The types of the inputs to add. Must be a single
            signal type which is applied to every new input or have an element
            for each new input. Default is ``None`` which makes all new inputs
            the type matching only ``None``
        :return: None
        """
        self._add_param_port(n, input_types)

    @abc.abstractmethod
    def propagate_state(self, t: float) -> None:
        """
        Propagate the state to the given time.

        :param t: The time to propagate to
        :return:
        """

    @abc.abstractmethod
    def restore_state(self, t: float) -> bool:
        """
        Restores the state from the cache captured at the largest time smaller
        than or equal to t.

        :param t: The time to restore the state at
        :return:
        """

    @abc.abstractmethod
    def calculate_output(self,
                         x: typing.Any, u: typing.Any, p: typing.Any,
                         t: float | sympy.Symbol) -> typing.List[typing.Any]:
        """
        Calculate the output of the system from the given state etc.

        :param x: State at the time to calculate the output
        :param u: Input at the time to calculate the output
        :param p: Parameters at the time to calculate the output
        :param t: Time to calculate the output at
        :return: The output at the given time
        """

    @abc.abstractmethod
    def f(self, x: typing.Any, u: typing.List[typing.Any],
          p: typing.List[typing.Any], t: float | sympy.Symbol) -> typing.Any:
        """
        The system dynamics.

        Returns the system dynamics, either the state at the next timestep if
        discrete or the current derivative of the states if continuous time.
        In case of a continuous time system, the equation is evaluated as

        .. math::

            \\dot{x} = f(x, u, p, t),

        while in case of discrete time, the dynamics are given as

        .. math::

            x_{k+1} = f(x, u, p, t).

        :param x: The state at time t
        :param u: The input at time t
        :param p: The parameters at time t
        :param t: The time t
        :return: The state at the next timestep or the current derivative
            of the state depending on the
        """

    def _compile_julia_system(self) -> None:
        """
        Get the Julia ODESystem of the Dynamical System.

        Compiles the Julia ODESystem representing this dynamical system and the
        two vectors for the inputs to and outputs from the systems.
        :return: None
        """
        # Call superclass method for basic definitions
        super()._compile_julia_system()
        # Get the dynamics subsystem
        d_state_jl = [self._Dt_jl(s)
                      for s in Utilities.flatten(self.system_states_jl)]
        dyn_jl = self._dynamics_lambda(self.system_states_jl,
                                       self.system_inputs_jl,
                                       self.system_params_jl,
                                       self.t_jl)
        dyn_eqs = [self.jl.Equation(ds, dyn)
                   for ds, dyn in zip(Utilities.flatten(d_state_jl),
                                      Utilities.flatten(dyn_jl))]

        # Get the outputs subsystem
        # Flatten everything
        system_outp_vec = [*Utilities.flatten(self.system_outputs_jl)]
        # Make multiple output variables if the output is vectorial
        if (len(system_outp_vec) != len([*Utilities.flatten(
                self._output_lambda(self.states,
                                    self.inputs,
                                    self.params,
                                    self.t))])):
                system_outp_vec = self.jl_symvec_pad_vectorial(
                    self.jl, system_outp_vec,
                    self._output_lambda(self.states,
                                        self.inputs,
                                        self.params,
                                        self.t), self.t_jl)
        outp_jl = self._output_lambda(self.system_states_jl,
                                      self.system_inputs_jl,
                                      self.system_params_jl,
                                      self.t_jl)

        outp_eqs = [self.jl.Equation(y, outp)
                    for y, outp in zip([*Utilities.flatten(system_outp_vec)],
                                       [*Utilities.flatten(outp_jl)])]

        # Full system
        eqs = juliacall.convert(self.jl.Vector, [*dyn_eqs, *outp_eqs])
        states = juliacall.convert(self.jl.Vector, [*Utilities.flatten(self.system_states_jl),
                                                    *Utilities.flatten(self.system_inputs_jl),
                                                    *Utilities.flatten(self.system_params_jl),
                                                    *Utilities.flatten(system_outp_vec)])
        sys = self.jl.ODESystem(
            eqs, self.t_jl, states, self.jl.Vector(),
            name=self.jl.Symbol(self.name + "_" + str(id(self)))
        )

        # Assemble the IC
        states_jl = [getattr(sys, str(x))
                     for x in Utilities.flatten(self.states)]
        ic_jl = [juliacall.convert(self.jl.Tuple, (s, v))
                 for s, v in zip(states_jl,
                                 Utilities.flatten(self._initial_state))]
        ic_jl = juliacall.convert(self.jl.Vector, ic_jl)
        ic_jl = self.jl.Dict(ic_jl)

        # Assign to variables
        self._inputs_jl = Utilities.apply_nested_list(
            lambda x, t: getattr(sys, str(x)) if x else [],
            [*self.inputs, *self.params]
        )
        self._outputs_jl = Utilities.apply_nested_list(  # Get output variables
            lambda x, t: getattr(sys, str(x)[:-2 - len(str(self.t_jl))])
            if x else [],
            system_outp_vec
        )
        # Rearrange output variables to match the output definition
        self._outputs_jl = Utilities.apply_nested_list(
            lambda x, t: self._outputs_jl[
                Utilities.nested_idx_to_linear_idx(self.outputs, t)
            ],
            self.outputs
        )
        self._system_jl = sys
        self._ic_jl = ic_jl


class DiscreteTimeDynamicalSystem(DynamicalSystem, abc.ABC):
    """
    A generic discrete time dynamical system.

    Takes care of propagating the state to the time given the dynamics.


    :param dt: Timestep of the discrete time system
    :param name: The name of the system
    :param tol: The tolerance on floating point comparisons
    :param `*dyn_sys_args`: Positional arguments to pass on to the
        :class:`DynamicalSystem <peepypoo.Systems.Dynamic._DynamicSystems.DynamicalSystem>` constructor
    :param `**dyn_sys_kwargs`: Keyword arguments to pass on to the
        :class:`DynamicalSystem <peepypoo.Systems.Dynamic._DynamicSystems.DynamicalSystem>` constructor
    """

    dt: float
    """ Timestep """
    tol: float
    """ Floating point tolerance for comparisons """

    def __init__(self, *dyn_sys_args,
                 name: str = "Discrete Time Dynamical System",
                 dt: float = 0.125,
                 tol: float = 1e-6, **dyn_sys_kwargs):
        super().__init__(*dyn_sys_args, name=name, **dyn_sys_kwargs)
        self.dt = dt
        self.tol = tol

    def propagate_state(self, t: float) -> None:
        """
        Propagate the state to the given time.

        This function propagates the state of the discrete time system from the
        initial state and time to the given time according to the inputs. Note
        that if ``t`` is smaller than the initial time, the final state will
        simply be the initial state.

        :param t: The time to propagate to
        :return:
        """
        while t >= self._t + self.dt - self.tol:
            self._t += self.dt
            inp = self._get_inputs(self._t)
            sys_input = inp[0:self._num_system_inputs]
            sys_params = inp[self._num_system_inputs:
                             self._num_system_inputs + self._num_params]
            self._state = self.f(self._state, sys_input,
                                 sys_params, self._t)
            self._state_cache[self._t] = self._state

    def restore_state(self, t: float) -> bool:
        """
        Restores the state from the cache captured at the largest time smaller
        than or equal to t.

        :param t: The time to restore the state at
        :return:
        """
        if len(self._state_cache) > 0 and min(self._state_cache.keys()) <= t:
            self._t = max([tk for tk in self._state_cache.keys() if tk <= t])
            self._state = self._state_cache.get(self._t)
            return True
        return False


class ContinuousTimeDynamicalSystem(DynamicalSystem, abc.ABC):
    """
    A generic continuous time dynamical system.

    Takes care of integrating the state through time given the dynamics.

    :param name: The name of the system
    :param integrator: Type of the integrator to use
    :param discretize_dt: If the value is bigger than zero, discretize the
        system input using zero order hold on the given value. Default is 0,
        i.e. continuous integration.
        Continuous integration (this parameter being 0) results in higher
        accuracy while this parameter being larger improves simulation
        performance.
        This parameter has to be set to a value bigger than zero if the system is involved in a loop.
        (For one system in the loop suffices, but it is recommended to pass it to all)
    :param integrator_args: Positional arguments to pass to the integrator as a
        list.
    :param integrator_kwargs: Keyword arguments to pass to the integrator as a
        dictionary.
    :param `*dyn_sys_args`: Positional arguments to pass on to the
        :class:`DynamicalSystem <peepypoo.Systems.Dynamic._DynamicSystems.DynamicalSystem>`
        constructor
    :param `**dyn_sys_kwargs`: Keyword arguments to pass on to the
        :class:`DynamicalSystem <peepypoo.Systems.Dynamic._DynamicSystems.DynamicalSystem>`
        constructor

    :raises ValueError: If the discretization time is invalid (i.e. negative)
    """

    integrator: Integrator
    """ The integrator object used to integrate the dynamics """
    __discretize_dt: float
    """ The timestep for discretization of the input """

    def __init__(self, *dyn_sys_args,
                 name: str = "Continuous Time Dynamical System",
                 integrator: typing.Type[Integrator]
                 = peepypoo.Integrators.Python.SciPySolveIVP,
                 discretize_dt: float = 0,
                 integrator_args: typing.List[typing.Any] = None,
                 integrator_kwargs: typing.Dict[str, typing.Any] = None,
                 **dyn_sys_kwargs):
        if integrator_args is None:
            integrator_args = []
        if integrator_kwargs is None:
            integrator_kwargs = {}
        super().__init__(*dyn_sys_args, name=name, **dyn_sys_kwargs)
        self.__discretize_dt = discretize_dt
        if discretize_dt == 0:  # Integrate with continuous input
            self.integrator = integrator(self._integration_function,
                                         *integrator_args, **integrator_kwargs)
        elif discretize_dt > 0:  # Integrate with discrete input
            self.integrator = integrator(
                self._integration_function_discrete_input,
                *integrator_args, **integrator_kwargs)
        else:
            raise ValueError("Invalid value {v} for discretization sampling "
                             "time. Must be non-negative (0 for no "
                             "discretization)".format(v=discretize_dt))

    def propagate_state(self, t: float) -> None:
        """
        Propagate the state to the given time.

        This function propagates the state of the discrete time system from the
        initial state and time to the given time according to the inputs. Note
        that if ``t`` is smaller than the initial time, the output will simply
        be the initial state.

        :param t: The time to propagate to
        :return:
        """
        if t > self._t:
            self._state, fcn = \
                self.integrator.integrate(self._state, self._t, t)
            self._t = t
            self._state_cache[self._t] = fcn

    def _integration_function(self, x, t) -> typing.Any:
        inp = self._get_inputs(t)
        inpu = inp[0:self._num_system_inputs]
        params = inp[self._num_system_inputs:
                     self._num_system_inputs + self._num_params]
        return self.f(x, inpu, params, t)

    def _integration_function_discrete_input(self, x, t) -> typing.Any:
        # Floor the time to the nearest multiple of dt (bust must be lower than time)
        t_fl = (t // self.__discretize_dt) * self.__discretize_dt
        if t_fl >= t:
            t_fl -= self.__discretize_dt
        t_fl = max(t_fl, self._initial_t)  # Do not go below initial time
        inp = self._get_inputs(t_fl)
        inpu = inp[0:self._num_system_inputs]
        params = inp[self._num_system_inputs:
                     self._num_system_inputs + self._num_params]
        return self.f(x, inpu, params, t)  # use current time here, only input is discretized

    def restore_state(self, t: float) -> bool:
        """
        Restores the state from the cache captured at the largest time smaller
        than or equal to t.

        :param t: The time to restore the state at
        :return:
        """
        if len(self._state_cache) > 1 and self._initial_t <= t:
            if t > max(self._state_cache.keys()):
                t_pick = max(self._state_cache.keys())
                self._t = t_pick
            else:
                t_pick = min([tk
                              for tk in self._state_cache.keys()
                              if tk >= t and tk > self._initial_t])
                self._t = t
            self._state = self._state_cache.get(t_pick)(self._t)
            return True
        elif len(self._state_cache) == 1 and self._initial_t <= t:
            self._t = self._initial_t
            self._state = self._initial_state
            return True
        return False
