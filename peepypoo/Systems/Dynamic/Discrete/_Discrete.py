import typing

import sympy

from peepypoo.Systems.Dynamic._DynamicSystems \
    import DiscreteTimeDynamicalSystem
import peepypoo.SignalTypes

__all__ = ['Integrator']


class Integrator(DiscreteTimeDynamicalSystem):
    """
    A discrete time integrator system.

    A system that has one input, integrates it and outputs the result. The type
    of the input and output is inferred from the initial condition. Currently
    only supports forward euler integration.

    The resulting system has the following inputs and outputs:

    +------+-------+---------+------------------+-----------------------------+
    | Type | Index | Name    | Datatype         | Description                 |
    +======+=======+=========+==================+=============================+
    | Input| 0     | u       | any              | The input to be integrated. |
    +------+-------+---------+------------------+-----------------------------+
    |Output| 0     | y       | any              | The integrated input.       |
    +------+-------+---------+------------------+-----------------------------+

    :param initial_state: The initial state of the dynamical system
    :param initial_t: The time of the initial condition of the dynamical system
    :param name: The name of the system
    :param `*dt_dyn_sys_args`: Positional arguments to pass on to the
        :class:`DiscreteTimeDynamicalSystem <peepypoo.Systems.Dynamic._DynamicSystems.DiscreteTimeDynamicalSystem>` constructor
    :param `**dt_dyn_sys_kwargs`: Keyword arguments to pass on to the
        :class:`DiscreteTimeDynamicalSystem <peepypoo.Systems.Dynamic._DynamicSystems.DiscreteTimeDynamicalSystem>` constructor
    """
    # TODO: Add other, more advanced, integration methods

    def __init__(self, initial_state: typing.Any = None, initial_t: float = 0,
                 name: str = "Discrete integrator",
                 *dt_dyn_sys_args, **dt_dyn_sys_kwargs):
        signal_type = peepypoo.SignalTypes.get_signal_class(initial_state)
        x = sympy.Array([sympy.symbols('x')])
        u = sympy.Array([sympy.symbols('u')])
        y = sympy.Array([sympy.symbols('y')])
        super().__init__(*dt_dyn_sys_args, inputs=[u], outputs=[y],
                         states=[x], params=[], num_inputs=1, num_outputs=1,
                         num_states=1, num_params=0,
                         initial_state=initial_state, initial_t=initial_t,
                         input_types=[signal_type], param_types=[],
                         output_types=[signal_type], name=name,
                         **dt_dyn_sys_kwargs)
        self.dynamics_formula = sympy.Tuple(x + u*self.dt)
        self.output_formula = sympy.Tuple(x)

    def f(self, x: typing.Any, u: typing.List[typing.Any],
          p: typing.List[typing.Any], t: float) \
            -> typing.Any:
        """
        Integrator dynamics.

        This function integrates the input using forward euler.

        :param x: The state at time t
        :param u: The input at time t
        :param p: The parameters at time t
        :param t: The time t
        :return: The state at the next timestep
        """
        return x + u[0] * self.dt

    def calculate_output(self,
                         x: typing.Any, u: typing.Any,
                         p: typing.Any, t: float) -> typing.List[typing.Any]:
        """
        Output the state of the integrator.

        :param x: State at the time to calculate the output
        :param u: Input at the time to calculate the output
        :param p: Parameters at the time to calculate the output
        :param t: Time to calculate the output at
        :return: The output at the given time
        """
        return [x]
