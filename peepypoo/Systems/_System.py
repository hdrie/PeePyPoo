import abc
import collections
import copy
from collections.abc import Iterable

import juliacall
import typing

import peepypoo.Utilities as Utilities
import peepypoo.SignalTypes
import numpy as np

__all__ = ['System', 'JuliaModelingToolkitConvertibleSystem']


#######################################################
# Abstract System class as base of all systems
#######################################################
class System(abc.ABC):
    """
    An abstract class which serves as a superclass of all possible systems.

    The System class is designed to serve as a superclass of all systems used
    in this framework. It provides the basic functionality for connecting
    different systems and defines the interface used for the simulator.
    """

    ##########################################################################
    # ############################# Attributes ###############################
    ##########################################################################
    name: str
    """The name of the system"""
    # Input attributes
    num_inputs: int
    """The number of inputs"""
    input_types: typing.List[peepypoo.SignalTypes.SignalType]
    """The types of the input ports"""
    input_systems: typing.List['System']
    """All systems connected to the input"""
    input_mapping: np.ndarray[int]
    """
    Input mapping of the system.

    A :attr:`num_inputs` x 2 numpy array, each row corresponds to one
    input. The first column is the index in :attr:`input_systems` of the
    system connected to this input and the second column is the index of the
    output of this system which is connected to this input. If an input is
    not connected, the corresponding row takes the value of the constant
    :attr:`NOT_CONNECTED`. 
    """
    __adding_input_connection: bool
    """
    State variable to keep track if the system is currently adding an input
    connection
    """
    __removing_input_connection: bool
    """
    State variable to keep track if the system is currently removing an input
    connection
    """
    # Output attributes
    num_outputs: int
    """The number of outputs"""
    output_types: typing.List[peepypoo.SignalTypes.SignalType]
    """The types of the output ports"""
    output_systems: typing.List['System']
    """All systems the output is connected to"""
    # Bookkeeping variables
    __adding_output_connection: bool
    """
    State variable to keep track if the system is currently adding an output
    connection
    """
    __removing_output_connection: bool
    """
    State variable to keep track if the system is currently removing an output
    connection
    """
    # Callback Functions
    on_input_connection_addition_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on input connection addition """
    on_output_connection_addition_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on output connection addition """
    on_connection_addition_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on connection addition (input and output) """
    on_input_connection_removal_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on input connection removal """
    on_output_connection_removal_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on output connection removal """
    on_connection_removal_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on connection removal (input and output) """
    on_input_connection_change_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on input connection change """
    on_output_connection_change_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on output connection change """
    on_connection_change_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on connection change (input and output) """
    on_input_connection_tree_change_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on input connection tree change """
    on_output_connection_tree_change_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on output connection tree change """
    on_connection_tree_change_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on connection tree change (input and output) """
    on_deletion_callbacks: \
        typing.List[typing.Callable[['System',
                                     typing.Dict[str, typing.Any]], None]]
    """ Callbacks to be called on deletion of the object """

    NOT_CONNECTED: typing.ClassVar[int] = -1
    """
    Constant for input mapping to take if nothing is connected to this port.
    """

    ##########################################################################
    # ################## Constructor and Destructor ##########################
    ##########################################################################
    def __init__(self,
                 num_inputs: int = 0,
                 num_outputs: int = 0,
                 name: str = "System") -> None:
        """
        Constructor for the system class.

        Cannot be used directly as the system class is abstract. However,
        initializes all generic data for the system and is to be used as a
        helper in derived classes which thus do not need to implement this all
        by themselves.

        :param name: The name of the system
        :param num_inputs: The number of inputs
        :param num_outputs: The number of outputs
        """
        self.name = str(name)
        self.num_inputs = num_inputs
        self.input_systems = []
        self.input_types = \
            [peepypoo.SignalTypes.get_signal_class(None)] * self.num_inputs
        self.input_mapping = \
            self.NOT_CONNECTED * np.ones((self.num_inputs, 2), dtype=int)
        self.__adding_input_connection = False
        self.__removing_input_connection = False
        self.num_outputs = num_outputs
        self.output_types = \
            [peepypoo.SignalTypes.get_signal_class(None)] * self.num_outputs
        self.output_systems = []
        self.__adding_output_connection = False
        self.__removing_output_connection = False

        # Initialize callbacks
        self.on_input_connection_addition_callbacks = []
        self.on_output_connection_addition_callbacks = []
        self.on_connection_addition_callbacks = []
        self.on_input_connection_removal_callbacks = []
        self.on_output_connection_removal_callbacks = []
        self.on_connection_removal_callbacks = []
        self.on_input_connection_change_callbacks = []
        self.on_output_connection_change_callbacks = []
        self.on_connection_change_callbacks = []
        self.on_input_connection_tree_change_callbacks = []
        self.on_output_connection_tree_change_callbacks = []
        self.on_connection_tree_change_callbacks = []
        self.on_deletion_callbacks = []

    def __del__(self) -> None:
        """
        Destructor for the system class.

        Destructs the System object and takes care of removing all connections
        to other systems.
        """
        # Try-except for Attribute error since this is also called when failed
        # during creation of object (in __init__) and then not all attributes
        # may be defined.
        try:
            self.remove_connections()
        except AttributeError:
            pass

    def delete(self) -> None:
        """
        Removes all connections from the system and executes the deletion
        callbacks. Note that the system is only actually deleted if all
        references to this object are removed in the deletion callbacks and the
        original reference is deleted as well.
        :return:
        """
        self.remove_connections()
        self._on_system_deletion({
            "type": "system_deletion"
        })

    ##########################################################################
    # ############### Get Inputs from connected systems ######################
    ##########################################################################
    def _get_inputs(self, t: float) -> typing.List[typing.Any]:
        """
        Protected function which gets the system input at the specified time.

        Queries the systems connected to the inputs to get their output and
        assembles the input of this system using it. Can be used within the
        implementation of :meth:`get_output` to get the input of the system at
        the given time needed to calculate the output. For all inputs which do
        not have an output of another system matched to, the input is set to 0.

        :param t: The time to get the inputs at
        :return: A numpy array containing the input vector at the specified
            time
        """
        # Get zero placeholder vector for input
        input_vect = [typ.get_zero() for typ in self.input_types]
        # Loop though systems connected to the input and assign their outputs
        # to the correct position of the input vector
        for i in range(0, len(self.input_systems)):
            out = self.input_systems[i].get_output(t)
            for j in np.flatnonzero(self.input_mapping[:, 0] == i):
                input_vect[j] = out[self.input_mapping[j, 1]]

        return input_vect

    @abc.abstractmethod
    def get_output(self, t: float) -> typing.List[typing.Any]:
        """
        Abstract function as interface to return the output of this system.

        Abstract function which calculates the output of this system and
        returns it. The output may have arbitrary type. It is to be overriden
        in all classes deriving from this one.

        :param t: The time to calculate the output at
        :return: The output of the system at the given time
        """

    ##########################################################################
    # ################## Adapt number of Connections #########################
    ##########################################################################
    def _add_input_port(self, n: int, input_types: typing.List[peepypoo.
                        SignalTypes.SignalType] | peepypoo.SignalTypes.
                        SignalType = None) -> None:
        """
        Adds input ports to the existing system, without any other
        changes in the system. The new inputs are appended in the end.

        .. warning::

            Changing the ports of the system might break the system. Thus
            use this functionality carefully and adapt the system code
            accordingly to match the new connection configuration.

        :param n: The number of input ports to add.
        :param input_types: The types of the inputs to add. Must be a single
            signal type which is applied to every new input or have an element
            for each new input. Default is ``None`` which makes all new inputs
            the type matching only ``None``
        :return: None

        :raises ValueError: If the size of ``input_types`` does not match.
        """
        if input_types is None:
            input_types = [peepypoo.SignalTypes.get_signal_class(None)] * n
        if isinstance(input_types, peepypoo.SignalTypes.SignalType):
            input_types = [input_types] * n
        if len(input_types) != n:
            raise ValueError("Input types must either be a single signal type "
                             "which is the same for each new input or a list "
                             "with the length of the signal types to add.")
        self.num_inputs += n
        self.input_types += input_types
        self.input_mapping = np.append(self.input_mapping,
                                       np.ones((n, 2), dtype=int)
                                       * self.NOT_CONNECTED, axis=0)

    def _remove_input_port(self, input_ports: typing.List[int]) -> None:
        """
        Removes the input ports at the given ports from the system,
        without any other changes in the system. This will most likely break
        the system unless special care is taken, as the system probably relies
        on these inputs and accessing them afterwards will lead to an error.

        Removing input ports will remove the connections on the specified ports
        but all other connections will remain the same, but shifted
        appropriately.

        .. warning::

            Changing the ports of the system might break the system. Thus
            use this functionality carefully and adapt the system code
            accordingly to match the new connection configuration.

        :param input_ports: The input ports to remove. Must be a list of
            integers with the indices of the ports to remove.
        :return: None
        """
        self.remove_input_connection(input_ports)
        self.num_inputs -= len(input_ports)
        [self.input_types.pop(x) for x in sorted(input_ports, reverse=True)]
        self.input_mapping = np.delete(self.input_mapping,
                                       input_ports, axis=0)

    def _add_output_port(self, n: int, output_types: typing.List[
            peepypoo.SignalTypes.SignalType] | peepypoo.SignalTypes.SignalType
            = None) -> None:
        """
        Adds output connections to the existing system, without any other
        changes in the system. The new outputs are appended in the end.

        .. warning::

            Changing the connections of the system might break the system. Thus
            use this functionality carefully and adapt the system code
            accordingly to match the new connection configuration.

        :param n: The number of connections to add.
        :param output_types: The types of the outputs to add. Must be a single
            signal type which is applied to every new output or have an element
            for each new output. Default is ``None`` which makes all new
            outputs the type outputting only ``None``
        :return: None

        :raises ValueError: If the size of ``output_types`` does not match.
        """
        if output_types is None:
            output_types = [peepypoo.SignalTypes.get_signal_class(None)] * n
        if isinstance(output_types, peepypoo.SignalTypes.SignalType):
            output_types = [output_types] * n
        if len(output_types) != n:
            raise ValueError("Output types must either be a single signal type"
                             "which is the same for each new output or a list"
                             "with the length of the signal types to add.")
        self.num_outputs += n
        self.output_types += output_types

    def _remove_output_port(self, output_ports: typing.List[int]) -> None:
        """
        Removes the output ports at the given ports from the system,
        without any other changes in the system.

        Removing output ports will remove the connections on the specified
        ports but all other connections will remain the same, but shifted
        appropriately.

        .. warning::

            Changing the ports of the system might break the system. Thus
            use this functionality carefully and adapt the system code
            accordingly to match the new connection configuration.

        :param output_ports: The input ports to remove. Must be a list of
            integers with the indices of the ports to remove.
        :return: None
        """
        self.remove_output_connection(output_ports)
        # Get all outgoing connections
        output_conn = []
        for sys in self.output_systems:
            # Get current port connections
            self_idx = np.where([x is self
                                 for x in sys.input_systems])[0][0]
            in_ports = np.where(sys.input_mapping[:, 0]
                                == self_idx)[0].tolist()
            out_ports = sys.input_mapping[in_ports, 1]
            # Adapt out_ports to match output ports after removal
            out_ports = (out_ports - [np.sum(output_ports < out_ports[i])
                                      for i in range(len(out_ports))]).tolist()
            output_conn.append([sys, in_ports, out_ports])
            # Remove the connections
            sys.remove_input_connection(in_ports)
        # Remove Ports
        self.num_outputs -= len(output_ports)
        [self.output_types.pop(x) for x in sorted(output_ports, reverse=True)]
        # Add outgoing connections back
        for conn in output_conn:
            self.add_output_connection(*conn)

    ##########################################################################
    # ###################### Connection handling #############################
    ##########################################################################
    def add_input_connection(self, system: 'System',
                             input_ports: typing.List[int],
                             output_ports: typing.List[int]) -> None:
        """
        Maps another systems output as input of this system.

        Adds a subset of the outputs of the given system as input of this
        system. Takes care of adding this connection as well as output
        connection of the given system.

        .. warning::

            The connections are very strict about the type, for example `int` is
            incompatible with `float`.

        :param system: The system providing the outputs which are used as
            input to this system.
        :param input_ports: A list with the indices of the input ports of this
            system to which the outputs of the other system are to be matched
            to.
        :param output_ports: A list with the indices of the output ports of
            the other system which are mapped to the inputs of this system.
            The other system's output at the first index in this list is mapped
            to the input of this system at the first index of the input_ports
            list.
        :raises ValueError: Thrown if the input ports are not conforming, i.e.
            if they're either out of range (not in [0, num_inputs)), the
            inputs are already connected or some of the input ports are
            repeated.
        :raises TypeError: Thrown if the input ports list is not consisting of
            integers or if the ports to be connected have incompatible types.
        """
        # Check if all input ports are integer
        if any(int(input_ports[i]) != input_ports[i]
               for i in range(0, len(input_ports))):
            e = TypeError(
                "Error adding input connection: All input ports must be "
                "integer.")
            raise e
        # Check if input ports are in range [0, self.num_inputs)
        if min(input_ports) < 0 or max(input_ports) >= self.num_inputs:
            e = ValueError("Error adding input connection: The input ports "
                           "{ports} are out of range.".format(
                            ports=np.array(input_ports)[
                                       [elem < 0 or elem >= self.num_inputs
                                        for elem in input_ports]]
                            ))
            raise e
        # Check that no input ports are repeated
        if len(input_ports) > len(set(input_ports)):
            e = ValueError("Error adding input connection: The input ports "
                           "must be unique.")
            raise e
        # Check if the inputs are already connected
        if any(elem != -1 for elem in self.input_mapping[input_ports, 0]):
            e = ValueError("Error adding input connection: The input ports "
                           "{ports} are already connected.".format(
                            ports=np.array(input_ports)[
                                [*(elem != -1 for elem in
                                   self.input_mapping[input_ports, 0])]]))
            raise e
        # Add this system to other systems output (first as this checks if
        # output ports are valid). Skip if already in the process of adding
        # this connection
        if not self.__adding_input_connection:
            self.__adding_input_connection = True
            try:
                system.add_output_connection(self, input_ports, output_ports)
            except Exception:
                self.__adding_input_connection = False
                raise
        else:
            return
        # Check port types to check if connection is possible
        if not all(self.input_types[j].is_compatible(system.output_types[i])
                   for i, j in zip(output_ports, input_ports)):
            #self.__adding_input_connection = False
            e = TypeError("Error adding input connection: The types at input"
                          " ports {ports} do not match. The outputs {outpPorts} of system \"{outpSys}\" have types "
                          "{outpTypes} while the corresponding inputs {inpPorts} of system \"{inpSys}\" have types "
                          "{inpTypes}".format(
                           ports=np.array(output_ports)[
                               [*(not self.input_types[j].is_compatible(
                                   system.output_types[i])
                                for i, j in zip(output_ports, input_ports))]
                           ],
                           outpSys=system, outpPorts=output_ports,
                           outpTypes=[str(system.output_types[i]) for i in output_ports],
                           inpSys=self, inpPorts=input_ports,
                           inpTypes=[str(self.input_types[i]) for i in input_ports]))
            raise e
        # Add system to list
        if any([sys is system for sys in self.input_systems]):
            idx = np.flatnonzero([sys is system
                                  for sys in self.input_systems]).tolist()[0]
        else:
            idx = len(self.input_systems)
            self.input_systems.append(system)
        # Add input mapping
        self.input_mapping[input_ports, 0] = idx
        self.input_mapping[input_ports, 1] = output_ports
        # Evaluate event function _on_input_connection_addition
        self._on_input_connection_addition({
            "type": "input_connection_addition",
            "input_ports": input_ports,
            "output_ports": output_ports,
            "output_system": system
        })
        # Reset adding connection state
        self.__adding_input_connection = False

    def add_output_connection(self, system: 'System',
                              input_ports: typing.List[int],
                              output_ports: typing.List[int]):
        """
        Maps the output of this system as input of another given system.

        Adds a subset of the outputs of this system as input of the given
        system. Takes care of adding this connection as well as input
        connection of the given system.

        .. warning::

            The connections are very strict about the type, for example `int` is
            incompatible with `float`.

        :param system: The system which takes this system's outputs as inputs.
        :param input_ports: A list with the indices of the input ports of the
            other system to which the outputs of this system are to be matched
            to.
        :param output_ports: A list with the indices of the output ports of
            this system which are mapped to the inputs of the other system.
            The output of this system at the first index in this list is mapped
            to the input of the other system at the first index of the
            input_ports list.
        :raises ValueError: Thrown if the output ports are out of range
            (not in [0, num_outputs)).
        :raises TypeError: Thrown if the input ports list is not consisting of
            integers or if the ports to be connected have incompatible types.
        """
        # Check if all output ports are integer
        if any(int(output_ports[i]) != output_ports[i]
               for i in range(0, len(output_ports))):
            e = TypeError(
                "Error adding output connection: All output ports must be "
                "integer.")
            raise e
        # Check if output ports are in range [0, self.num_outputs)
        if min(output_ports) < 0 or max(output_ports) >= self.num_outputs:
            e = ValueError("Error adding output connection: The output ports "
                           "{ports} are out of range.".format(
                            ports=np.array(output_ports)[
                                       [elem < 0 or elem >= self.num_outputs
                                        for elem in output_ports]]
                            ))
            raise e
        # Add to other systems input (Here to have checking if input is valid
        # before adding). Skip if already in the process of adding this
        # connection.
        if not self.__adding_output_connection:
            self.__adding_output_connection = True
            try:
                system.add_input_connection(self, input_ports, output_ports)
            except Exception:
                self.__adding_output_connection = False
                raise
        else:
            return
        # Check port types to check if connection is possible
        if not all(system.input_types[j].is_compatible(self.output_types[i])
                   for i, j in zip(output_ports, input_ports)):
            self.__adding_output_connection = False
            e = TypeError("Error adding output connection: The types at output"
                          " ports {ports} do not match. The outputs {outpPorts} of system \"{outpSys}\" have types "
                          "{outpTypes} while the corresponding inputs {inpPorts} of system \"{inpSys}\" have types "
                          "{inpTypes}".format(
                           ports=np.array(output_ports)[
                               [*(not system.input_types[j].is_compatible(
                                   self.output_types[i])
                                for i, j in zip(output_ports, input_ports))]
                           ],
                           outpSys=self, outpPorts=output_ports,
                           outpTypes=[str(self.output_types[i]) for i in output_ports],
                           inpSys=system, inpPorts=input_ports,
                           inpTypes=[str(system.input_types[i]) for i in input_ports]))
            raise e
        # Check if this system is already connected as output and add it if not
        if not any([sys is system for sys in self.output_systems]):
            # Append the system to the output systems
            self.output_systems.append(system)
        # Evaluate event function _on_connection_removal
        self._on_output_connection_addition({
            "type": "output_connection_addition",
            "input_ports": input_ports,
            "output_ports": output_ports,
            "input_system": system
        })
        # Reset adding connection state
        self.__adding_output_connection = False

    def remove_input_connection(self, input_ports: typing.List[int]) -> None:
        """
        Removes the input connections at the specified ports.

        This function is used to remove the incoming connections at the given
        ports. Further, takes care to remove the output connection of the
        corresponding system as well.

        :param input_ports: The indices of the input connections to remove.
        :raises ValueError: Thrown if the inputs ports are out of range
            (not in [0, num_inputs)).
        :raises TypeError: Thrown if the input ports list is not consisting of
            integers.
        """
        # Check if anything to remove and exit if not
        if not input_ports:
            return
        # Check if input ports are in range [0, self.num_inputs)
        if min(input_ports) < 0 or max(input_ports) >= self.num_inputs:
            e = ValueError("Error removing input connection: The input ports "
                           "{ports} are out of range.".format(
                            ports=np.array(input_ports)[
                                       [elem < 0 or elem >= self.num_inputs
                                        for elem in input_ports]]
                            ))
            raise e

        # Remove output connection in connected system if not already in
        # process of removing this connection
        if not self.__removing_input_connection:
            self.__removing_input_connection = True

            # Get input mapping and indexes of systems
            mapping = self.input_mapping[input_ports, :]
            mapping = mapping[mapping[:, 0] != self.NOT_CONNECTED, :]

            removed_systems = []
            removed_mappings = {}
            # Get removed mappings
            for in_port, (sys_idx, port) in enumerate(mapping):
                removed_mappings[self.input_systems[sys_idx]] = [in_port, port]

            # Remove connections from input mapping
            self.input_mapping[input_ports, :] = self.NOT_CONNECTED
            # Remove systems in input_systems not connected anymore and update
            # indices
            for sys_idx in np.sort(np.unique(mapping[:, 0]))[::-1]:
                if np.all(self.input_mapping[:, 0] != sys_idx):
                    # Remove system from list and reduce indices of following
                    # systems in the input mapping by one.
                    removed_systems.append(self.input_systems[sys_idx])
                    self.input_systems.pop(sys_idx)
                    self.input_mapping[
                        self.input_mapping[:, 0] > sys_idx, 0
                    ] -= 1
            # Remove output connections from other systems
            for sys, (in_port, port) in removed_mappings.items():
                sys.remove_output_connection([port], [self])
            # Evaluate event function _on_input_connection_removal
            self._on_input_connection_removal({
                "type": "input_connection_removal",
                "ports": input_ports,
                "fully_removed_systems": removed_systems,
                "removed_mappings": removed_mappings
            })
            # Reset removing connection state
            self.__removing_input_connection = False

    def remove_output_connection(self,
                                 output_ports: typing.List[int],
                                 systems: typing.List['System'] = None) \
            -> None:
        """
        Removes the output connections at the specified ports.

        This function is used to remove the outgoing connections at the given
        ports. Further, takes care to remove the output connection of the
        corresponding system as well.

        :param output_ports: The indices of the output connections to remove.
        :param systems: List of the systems the output connection is going to.
            Default value ``None`` leads to all systems at this port being
            disconnected.
        :raises ValueError: Thrown if the output ports are out of range
            (not in [0, num_outputs)) or if any of the given systems is not
            connected to the output.
        :raises TypeError: Thrown if the output ports list is not consisting of
            integers.
        """
        # Check if anything to remove and exit if not
        if not output_ports:
            return
        # Take connections to all systems if not given
        if systems is None:
            systems = self.output_systems.copy()
        # Check if output ports are in range [0, system.num_outputs)
        if min(output_ports) < 0 or max(output_ports) >= self.num_outputs:
            e = ValueError("Error removing output connection: The output ports"
                           " {ports} are out of range.".format(
                            ports=np.array(output_ports)[
                                       [elem < 0 or elem >= self.num_outputs
                                        for elem in output_ports]]
                            ))
            raise e
        # Check if systems are in output systems
        if any([sys not in self.output_systems for sys in systems]):
            e = ValueError("Error removing output connection: The systems "
                           "{system} are not connected.".format(
                            system=[str(sys) for sys in systems
                                    if sys not in self.output_systems]
                            ))
            raise e

        # Remove input connection in connected system if not already in
        # process of removing this connection
        if not self.__removing_output_connection:
            self.__removing_output_connection = True
            removed_systems = []
            removed_mappings = {}
            for sys in systems:
                input_systems = sys.input_systems.copy()
                mapping = sys.input_mapping.copy()
                for i in range(0, len(input_systems)):
                    if input_systems[i] is self:
                        mapping_idxs = mapping[:, 0] == i
                        rm_ports = np.logical_or.reduce(
                            np.concatenate([
                                np.array(mapping[:, 1] == i,
                                         ndmin=2)
                                for i in output_ports
                            ]))
                        rm_ports = np.logical_and(mapping_idxs, rm_ports)
                        rm_ports = np.flatnonzero(rm_ports).tolist()
                        sys.remove_input_connection(rm_ports)
                        removed_mappings[sys] = \
                            [mapping[rm_ports, 1].tolist(), rm_ports]
                # Remove the system from the list of output systems if not
                # connected through other connection
                if not (self in sys.input_systems):
                    removed_systems.append(sys)
                    self.output_systems.remove(sys)
            # Evaluate event function _on_output_connection_removal
            self._on_output_connection_removal({
                "type": "output_connection_removal",
                "ports": output_ports,
                "fully_removed_systems": removed_systems,
                "removed_mappings": removed_mappings
            })
            # Reset removing connection state
            self.__removing_output_connection = False

    def remove_connections(self) -> None:
        """
        Function to remove all connections from or to this system.

        This function is to be called when to remove all incoming and outgoing
        connections of this system.
        """
        self.remove_input_connection([*range(0, self.num_inputs)])
        self.remove_output_connection([*range(0, self.num_outputs)])

    ##########################################################################
    # ########################## Event functions #############################
    ##########################################################################
    # ### Connection Events
    # ## Connection Addition
    def _on_input_connection_addition(self,
                                      event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate on addition of an input connection.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the logic
        behind the propagation of the events.

        :param event: Parameters of the processed event. Has fields:
            type
                The type of the event (Here "input_connection_addition")
            input_ports
                The input ports of this system to which the connection have
                been added
            output_ports
                The output ports of the other system which have been connected
                to this systems input
            output_system
                The system of which the outputs have been connected to this
                systems input
        :return: None
        """
        for cbk in self.on_input_connection_addition_callbacks:
            cbk(self, event)
        self._on_input_connection_change(event)
        self._on_connection_addition(event)

    def _on_output_connection_addition(self,
                                       event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate on addition of an output connection.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the logic
        behind the propagation of the events.


        :param event: Parameters of the processed event. Has fields:
            type
                The type of the event (Here "output_connection_addition")
            input_ports
                The input ports of the other system which have been connected
                to this system's outputs
            output_ports
                The output ports of this system which have been connected to
                the other system's input
            input_system
                The system to which the outputs of this system have been
                connected
        :return: None
        """
        for cbk in self.on_output_connection_addition_callbacks:
            cbk(self, event)
        self._on_output_connection_change(event)
        self._on_connection_addition(event)

    def _on_connection_addition(self,
                                event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate on addition of connection.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the logic
        behind the propagation of the events.


        :param event: Parameters of the processed event. Has fields:
            type
                The type of the event (either input or output connection
                addition)
            input_ports
                The input ports connected
            output_ports
                The output ports connected
            input_system or output_system
                The system to which the connection from this system has been
                added. The name depends on whether an input or an output
                connection has been added
        :return: None
        """
        for cbk in self.on_connection_addition_callbacks:
            cbk(self, event)

    # ## Connection Removal
    def _on_input_connection_removal(self,
                                     event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate on removal of an input connection.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the logic
        behind the propagation of the events.


        :param event: Parameters of the processed event. Has fields:
            type
                The type of the event (Here "input_connection_removal")
            ports
                The input ports on which the connections have been removed
            fully_removed_systems
                List of the systems which have been fully removed
            removed_mappings
                Mappings which have been removed. Is a dict with keys being the
                systems and values being a list of [input ports, output ports]
                which defines which mappings have been removed
        :return: None
        """
        for cbk in self.on_input_connection_removal_callbacks:
            cbk(self, event)
        self._on_input_connection_change(event)
        self._on_connection_removal(event)

    def _on_output_connection_removal(self,
                                      event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate on removal of an output connection.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the logic
        behind the propagation of the events.


        :param event: Parameters of the processed event. Has fields:
            type
                The type of the event (Here "output_connection_removal")
            ports
                The output ports on which the connections have been removed
            fully_removed_systems
                List of the systems which have been fully removed
            removed_mappings
                Mappings which have been removed. Is a dict with keys being the
                systems and values being a list of [input ports, output ports]
                which defines which mappings have been removed
        :return: None
        """
        for cbk in self.on_output_connection_removal_callbacks:
            cbk(self, event)
        self._on_output_connection_change(event)
        self._on_connection_removal(event)

    def _on_connection_removal(self,
                               event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate on removal of connection.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the logic
        behind the propagation of the events.

        :param event: Parameters of the processed event. Has fields:
            type
                The type of the event
            ports
                The input or output ports on which the connections have been
                removed, depending on the event type
            fully_removed_systems
                List of the systems which have been fully removed
            removed_mappings
                Mappings which have been removed. Is a dict with keys being the
                systems the connection was going to and values being a list of
                [input ports, output ports] which defines which connections
                have been removed
        :return: None
        """
        for cbk in self.on_connection_removal_callbacks:
            cbk(self, event)

    # ## Connection Change
    def _on_input_connection_change(self,
                                    event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate on input connection change.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the logic
        behind the propagation of the events.

        :param event: Parameters of the processed event. See the corresponding
            definitions in the dedicated event functions.
        :return: None
        """
        for cbk in self.on_input_connection_change_callbacks:
            cbk(self, event)
        tree_change_event = event.copy()
        tree_change_event["type"] = "input_connection_tree_change"
        tree_change_event["connection_tree"] = []
        self._on_input_connection_tree_change(tree_change_event)
        self._on_connection_change(event)

    def _on_output_connection_change(self,
                                     event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate on output connection change.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the logic
        behind the propagation of the events.

        :param event: Parameters of the processed event. See the corresponding
            definitions in the dedicated event functions.
        :return: None
        """
        for cbk in self.on_output_connection_change_callbacks:
            cbk(self, event)
        tree_change_event = event.copy()
        tree_change_event["type"] = "output_connection_tree_change"
        tree_change_event["connection_tree"] = []
        self._on_output_connection_tree_change(tree_change_event)
        self._on_connection_change(event)

    def _on_connection_change(self,
                              event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate on connection change.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the logic
        behind the propagation of the events.

        :param event: Parameters of the processed event. See the corresponding
            definitions in the dedicated event functions.
        :return: None
        """
        for cbk in self.on_connection_change_callbacks:
            cbk(self, event)

    # ## On changes in the connection tree
    def _on_input_connection_tree_change(self,
                                         event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate if any connection in the input tree changes
        including the inputs of this system. Takes care of propagating the
        event forwards through the tree.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the
        propagation through the tree.

        :param event: Parameters of the processed event. See the dedicated
            function for a description. Additionally, it has the field
            *connection_tree* which represents the sequence of systems which
            were called for this connection tree change event before this
            system.
        :return: None
        """
        # Handle event only once -> skip if self is already in the connection
        # tree
        if self in event["connection_tree"]:
            return
        for cbk in self.on_input_connection_tree_change_callbacks:
            cbk(self, event)
        self._on_connection_tree_change(event)
        event["connection_tree"].append(self)
        for sys in self.output_systems:
            sys._on_input_connection_tree_change(event)

    def _on_output_connection_tree_change(self,
                                          event: typing.Dict[str, typing.Any])\
            -> None:
        """
        Function to evaluate if any connection in the output tree changes
        including the outputs of this system. Takes care of propagating it
        backwards through the tree.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the
        propagation through the tree.

        :param event: Parameters of the processed event. See the dedicated
            function for a description. Additionally, it has the field
            *connection_tree* which represents the sequence of systems which
            were called for this connection tree change event before this
            system.
        :return: None
        """
        # Handle event only once -> skip if self is already in the connection
        # tree
        if self in event["connection_tree"]:
            return
        for cbk in self.on_output_connection_tree_change_callbacks:
            cbk(self, event)
        self._on_connection_tree_change(event)
        event["connection_tree"].append(self)
        for sys in self.input_systems:
            sys._on_output_connection_tree_change(event)

    def _on_connection_tree_change(self,
                                   event: typing.Dict[str, typing.Any]) \
            -> None:
        """
        Function to evaluate if a connection in the tree changes.

        Defaults to no action, but can be changed by systems. Each system
        overriding this function should call the overriden function in
        the end to preserve the action of the overridden system and the
        propagation through the tree.

        :param event: Parameters of the processed event. See the dedicated
            function for a description. Additionally, it has the field
            *connection_tree* which represents the sequence of systems which
            were called for this connection tree change event before this
            system.
        :return: None
        """
        for cbk in self.on_connection_tree_change_callbacks:
            cbk(self, event)

    def _on_system_deletion(self, event: typing.Dict[str, typing.Any]):
        """
        Function to evaluate if the system is to be deleted.

        :param event: Parameters of the processed event. It is a dict with the
            single field *type* which evaluates to `system_deletion`
        :return:
        """
        for cbk in self.on_deletion_callbacks:
            cbk(self, event)

    ##########################################################################
    # ######################### Printing Functions ###########################
    ##########################################################################
    def __str__(self) -> str:
        """
        Returns a string representing this system.

        This function is used to represent this system as string. Currently, it
        simply returns the systems name.
        """
        return self.name


##########################################################################
# ############## Base Class for Julia-convertible systems ################
##########################################################################
class JuliaModelingToolkitConvertibleSystem(System, abc.ABC):
    # Get julia module
    jl: juliacall.ModuleValue \
        = juliacall.newmodule('JuliaConvertibleSystem_module')
    """ The julia module used in this class"""
    # Load Julia packages
    jl.seval("using ModelingToolkit")
    jl.seval("using DifferentialEquations")

    _t_jl: juliacall.RealValue
    """ The time of the julia model """
    @property
    def t_jl(self) -> juliacall.RealValue:
        """ The time of the julia model """
        if not hasattr(self, '_t_jl'):
            self._compile_julia_system()
        return self._t_jl
    _inputs_jl: typing.List
    """ The julia symbols of the inputs """
    _outputs_jl: typing.List
    """ The julia symbols of the outputs """
    _system_jl: juliacall.AnyValue
    """ The julia ODESystem """
    _ic_jl: juliacall.DictValue
    """ The julia dict with the initial condition """
    _tstops_jl: typing.List[juliacall.VectorValue]
    """ 
    Extra times the julia solver must step into. Can be assigned for improved 
    accuracy, e.g. to point of discontinuities or singularities. 
    """
    _d_discontinuities_jl: typing.List[juliacall.VectorValue]
    """ 
    Locations of discontinuities in low order derivatives. 
    """
    _discontinuities_period_jl: typing.List[typing.Tuple[float, float]]
    """ 
    (Period, phase) of the tstops and d_discontinuities. Set to inf if none. 
    """
    @property
    def inputs_jl(self) -> typing.List:
        """ The julia symbols of the inputs """
        if not hasattr(self, '_inputs_jl'):
            self._compile_julia_system()
        return self._inputs_jl

    @property
    def outputs_jl(self) -> typing.List:
        """ The julia symbols of the outputs """
        if not hasattr(self, '_outputs_jl'):
            self._compile_julia_system()
        return self._outputs_jl

    @property
    def system_jl(self) -> juliacall.AnyValue:
        """ The julia ODESystem equivalent """
        if not hasattr(self, '_system_jl'):
            self._compile_julia_system()
        return self._system_jl

    @property
    def initial_condition_jl(self) -> juliacall.DictValue:
        """ The julia dict containing the initial conditions """
        if not hasattr(self, '_ic_jl'):
            self._compile_julia_system()
        return self._ic_jl

    @property
    def tstops_jl(self) -> typing.List[juliacall.VectorValue]:
        """
        The julia vector containing extra times the timestepping algorithm
        must step into
        """
        if not hasattr(self, '_tstops_jl'):
            self._compile_julia_system()
        return self._tstops_jl

    @property
    def d_discontinuities_jl(self) -> typing.List[juliacall.VectorValue]:
        """
        The julia vector containing locations of low order discontinuities.
        """
        if not hasattr(self, '_d_discontinuities_jl'):
            self._compile_julia_system()
        return self._d_discontinuities_jl

    @property
    def discontinuities_period_jl(self) -> typing.List[typing.Tuple[float,
                                                                    float]]:
        """ Period of the tstops and d_discontinuities. """
        if not hasattr(self, '_discontinuities_period_jl'):
            self._compile_julia_system()
        return self._discontinuities_period_jl

    @staticmethod
    def jl_macro_vars(jl, name: str,
                      type: typing.Literal["var", "param", "const"],
                      dep: typing.List[typing.Any] | None = None,
                      val: float | int | None = None) -> juliacall.RealValue:
        """
        Generate Julia symbolics variables.

        :param jl: juliaCall Module to use for julia evaluations
        :param name: the name of the variable
        :param type: The type of the variable: "var" for variables, "param" for
            parameters and "const" for constants.
        :param dep: The dependencies of the variable, if the type is "var"
        :param val: The value of the constant, if the type is "const"
        :return: Handles to the julia symbolic variables
        """
        if type == "var":
            macro = "@variables"
            annex = ""
            if dep is not None:
                annex = "(" + ', '.join([str(d) for d in dep]) + ')'
        elif type == "param":
            macro = "@parameters"
            annex = ""
        elif type == "const":
            if val is None:
                raise ValueError(
                    "Error creating Julia ModelingToolkit constant: "
                    "Value must be given.")
            macro = "@constants"
            annex = " = " + str(val)
        else:
            raise ValueError("Error creating Julia ModelingToolkit variable: "
                             "Invalid type given.")
        return jl.seval(macro + " " + name + annex)[0]

    @staticmethod
    def jl_assign_var(jl, name: str,
                      value: juliacall.AnyValue) -> juliacall.AnyValue:
        """
        Assign a julia value to the julia workspace under the given name, s.t.
        it can be used from within julia e.g. for usage within macros.

        :param jl: juliaCall Module to assign the variable to
        :param name: The name of the variable in the julia module
        :param value: The Julia value for the variable to contain
        :return: The assigned value, equal to the provided value
        """
        return jl.seval("x->global " + name + " = x")(value)

    @staticmethod
    def jl_symvec_pad_vectorial(jl, symvec: typing.Any, formvec: typing.Any,
                                indep_var: juliacall.RealValue) -> typing.Any:
        """
        Pad a symbolic vector to have the same structure as another nested
        vector.

        Pads the symbols in the vector ``symvec`` if the corresponding element
        in ``formvec`` is vectorial s.t. ``symvec`` contains a vector with the
        same structure and number of elements as ``formvec`` but as Julia
        symbols. The used names for the newly generated symbols is the already
        existing name appended by ``_`` and a number corresponding to the index
        in the position in the vector. For each additional level of nesting,
        an additional underscore and number is added.

        :param jl: juliaCall Module to use
        :param symvec: The vector of symbolics to pad
        :param formvec: The (nested) vector with the shape that the vector of
            symbolics is to be padded to
        :param indep_var: The independent variable for the new symbols
        :return: A vector of julia symbols with the same shape as ``formvec``
        """
        # Go through each pair
        for i, (y, eq) in enumerate(zip(copy.copy(symvec), formvec)):
            if (isinstance(y, collections.abc.Sized)
                and len(y) > 1) \
                    and (isinstance(eq, collections.abc.Sized)
                         and len(eq) > 1):
                # If both are vectorial and the vectors have same size, apply
                # the same to the lower level. If the size is different, throw
                # an error
                if len(y) == len(eq):
                    symvec[i] = JuliaModelingToolkitConvertibleSystem.\
                        jl_symvec_pad_vectorial(jl, y, eq, indep_var)
                else:
                    raise ValueError("Invalid symbolic vector padding: Symbol "
                                     "and formvec are both vectorial but they "
                                     "have different lengths.")
                continue
            elif isinstance(eq, collections.abc.Sized):
                # If only the formvec is vectorial, add the symbols to match
                # the shape
                try:
                    # Check if equations are actually subscriptable if length
                    # is 1 or if it is just a sized. E.g. the juliacall objects
                    # are fulfilling this criterion
                    if len(eq) == 1:
                        eq = [eq[0]]
                    symvec[i] = Utilities.apply_nested_list(
                        lambda x, t: JuliaModelingToolkitConvertibleSystem.
                        jl_macro_vars(jl,
                                      str(y)[:-2 - len(str(indep_var))]
                                      + ('_' + '_'.join([str(j) for j in t])),
                                      "var", [indep_var]),
                        eq)
                    continue
                except:
                    pass

            # If only the symbol is vectorial but not the equations, throw an
            # error
            if isinstance(y, collections.abc.Sized):
                y_vec = True
                try:
                    y = [y[0]]
                except:
                    y_vec = False
                if y_vec:
                    raise ValueError("Invalid symbolic vector padding: "
                                     "Expected vectorial form as the symbols "
                                     "are vectorial but got only one entry in "
                                     "the formvec.")
        return symvec

    @abc.abstractmethod
    def _compile_julia_system(self) -> None:
        """
        Compiles the system to get the julia ODESystem equivalent.

        This function generates the julia ODESystem equivalent of the system.
        It has to assign the following private variables:
        _system_jl: Holds the ODESystem
        _t_jl: Holding a reference to the independent variable of the system
        _inputs_jl: Holding references to the ODESystem inputs
        _outputs_jl: Holding references to the ODESystem outputs
        _ic_jl: Dict with keys being the julia variables and values the
            initial conditions on them.

        Additionally, the following private variables can be assigned:
        _tstops_jl: Additional times the solver has to stop at. Can improve
            accuracy if assigned properly
        _d_discontinuities_jl: Locations of discontinuities in low order
            derivatives.
        _discontinuities_period_jl: The period and phase shift of each tstops
            and d_discontinuities vector.

        Further, when overriding this function, the superclass should always be
        called in the beginning to ensure that default values for the optional
        settings are loaded.
        """
        self._tstops_jl = []
        self._d_discontinuities_jl = []
        self._discontinuities_period_jl = []

    def get_equations(self) -> str:
        """
        Get the differential equations of the resulting system as string representing LaTeX code for the resulting
        equations.

        .. note::

            This function returns a string containing the LaTeX code of the equations, which is not easily readable.
            Thus, it is recommended to compile the output string before having a look at it. There are a multitude of
            methods to do this.

            For example, if you use a Jupyter Notebook to run the code, you can directly use Jupyter to display the
            equations nicely using:

            .. code-block::

                from IPython.display import Math
                Math(eqs)

        :return: A string containing the latex code of the differential equations associated with the system.
        """

        return str(self.jl.ModelingToolkit.latexify(self.jl.equations(self.system_jl)))
