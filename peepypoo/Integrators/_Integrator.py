import abc
import typing

__all__ = ['Integrator']


#######################################################
# Abstract Integrator class for integrating CT systems
#######################################################
class Integrator(abc.ABC):
    """
    An abstract class for defining the interface for integrating continuous
    time systems.

    :param f: A handle to the function to be integrated. It has to be given as

        .. math::

            \\dot{x} = f(x, t)
    """
    f: typing.Callable[
        [typing.Any, float], typing.Any]
    """ 
    The function to be integrated in the from of :math:`\\dot{x}=f(x, t)`
    """

    def __init__(self, f: typing.Callable[[typing.Any, float], typing.Any],
                 *args, **kwargs) -> None:
        self.f = f

    @abc.abstractmethod
    def integrate(self, x_init: typing.Any, t_init: float, t_final: float) \
            -> typing.Tuple[typing.Any, typing.Callable[[float], typing.Any]]:
        """
        Definition of the interface to integrate the function from ``t_init``
        to ``t_final`` with initial state ``x_init``.

        :param x_init: Initial state
        :param t_init: Initial time
        :param t_final: Final time
        :return:
        """
