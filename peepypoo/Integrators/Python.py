import typing
import numpy as np
from scipy import integrate
from peepypoo.Integrators._Integrator import Integrator

__all__ = ['SciPySolveIVP']


class SciPySolveIVP(Integrator):
    """
    Integrate using the :py:func:`scipy.integrate.solve_ivp` method.

    Wrapper around the :py:func:`scipy.integrate.solve_ivp` method to use this
    for integration.

    :param f: The function to integrate as :math:`\\dot{x} = f(x, t)`
    :param method: The integration method to use. Available methods are the
        same as for :py:func:`scipy.integrate.solve_ivp`.
    :param `**options`: Options passed to the chosen solver. See
        :py:func:`scipy.integrate.solve_ivp` for reference
    """

    method: str
    """ The integration method """
    options: typing.Dict[str, typing.Any]
    """ The Arguments to the solver """

    def __init__(self, f: typing.Callable[[np.array, float], np.array],
                 method: str = 'RK45', vectorized: bool = False, **options):
        super().__init__(f)
        self.method = method
        self.options = options
        # Set vectorized in options
        self.options["vectorized"] = vectorized
        # Enable dense output
        self.options["dense_output"] = True

    def integrate(self, x_init: np.array, t_init: float, t_final: float) \
            -> typing.Tuple[typing.Any, typing.Callable[[float], typing.Any]]:
        """
        Integrate the function using :py:func:`scipy.integrate.solve_ivp`.

        :param x_init: Initial state
        :param t_init: Initial time
        :param t_final: Final time
        :return: Tuple containing the state at the final time and a function to
            interpolate the state within (t_init, t_final).
        """
        # The initial condition must be 1D vector
        non_vec = False
        if not hasattr(x_init, "__len__"):
            non_vec = True
            x_init = [x_init]
        y = integrate.solve_ivp(lambda t, x: self.f(x, t), [t_init, t_final],
                                x_init, first_step=min(t_final-t_init, 0.1), method=self.method, **self.options)
        if non_vec:
            return y.y[:, -1][0], lambda t: y.sol(t)[0]
        else:
            return y.y[:, -1], y.sol
