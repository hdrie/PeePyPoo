import math
import warnings

import juliacall
import numpy as np
import pandas as pd
import peepypoo
import peepypoo.Utilities as Utilities
import typing

__all__ = ["BDWrapper"]


class BDWrapper:
    """
    A class to wrap a connection of systems to a block diagram. It provides
    functionality which includes the whole systems graph e.g. analysing the
    connections to detect problems and improve simulation performance or for
    simulating all systems up to a given time.
    Note that all systems which are newly connected to any of the systems
    already in the block diagram get added to the block diagram as well.
    However, if connections are removed, the now possibly disconnected systems
    are *not* removed from the block diagram.

    :param systems: One or more systems in the blockdiagram. All systems
        connected to any of those is added to the block diagram. However, the
        given systems do not necessarily need to be connected to a single
        diagram.
    """

    _all_systems: typing.List[peepypoo.Systems.System]
    """ All systems in the block diagram """
    _tails: typing.List[peepypoo.Systems.System]
    """ 
    All tails in the block diagram, i.e. the systems without connected inputs. 
    """
    _heads: typing.List[peepypoo.Systems.System]
    """ 
    All heads in the block diagram, i.e. the systems without connected outputs. 
    """
    # Get julia module
    jl: juliacall.ModuleValue \
        = juliacall.newmodule('BDWrapper_module')
    """ The julia module used in this class"""
    # Load Julia packages
    jl.seval("using ModelingToolkit")
    jl.seval("using DifferentialEquations")

    _julia_system_uptodate: bool
    """ If the julia system is up to date """
    _julia_system: juliacall.AnyValue
    """ The julia representation of the full system """
    _julia_ics: juliacall.DictValue
    """ The julia representation of the initial conditions """
    _julia_tstops: juliacall.VectorValue
    """ Additional timesteps the julia solver must step into """
    _julia_d_discontinuities: juliacall.VectorValue
    """ 
    Locations of positions where the discontinuities in low order derivatives 
    occur.
    """
    _julia_discontinuities_periodic: typing.List[
        typing.Dict[str, float | juliacall.VectorValue]
    ]
    """ 
    tstops and d_discontinuities which are periodic. Stored as dict with 
    fields:
    period: The period length
    phase: The phase
    tstops: The periodic tstops
    d_disc: The periodic d_discontinuities
    """

    def __init__(self, systems: typing.List[peepypoo.Systems.System]) -> None:
        self._all_systems = []
        self._tails = []
        self._heads = []
        self._system_callbacks = {}
        self.add_systems_tree(systems)
        # Set that the julia system is not up-to-date
        self._julia_system_uptodate = False

    def add_systems_tree(self, systems: typing.List[peepypoo.Systems.System]) \
            -> None:
        """
        Adds new system trees.
        Adds the given systems as well as all systems connected to them to the
        block diagram.

        :param systems: One or more systems to add to the blockdiagram. All
            systems connected to any of those is added to the block diagram.
            However, the given systems do not necessarily need to be connected
            to a single diagram.
        :return:
        """
        self._add_systems(self._get_systems(systems))

    def remove_systems_tree(self,
                            systems: typing.List[peepypoo.Systems.System]) \
            -> None:
        """
        Removes a system trees.
        Removes the given systems as well as all systems connected to them from
        the block diagram.

        :param systems: One or more systems to remove from the blockdiagram.
            All systems connected to any of those are removed from the block
            diagram. However, the given systems do not necessarily need to be
            connected to a single diagram.
        :return:
        """
        self._remove_systems(self._get_systems(systems))

    def _add_systems(self, systems: typing.List[peepypoo.Systems.System]) \
            -> None:
        """
        Adds the given systems to the block diagram and to the heads/tails
        if they have no out-/inputs
        :param systems: The systems to add.
        :return:
        """
        systems = [sys for sys in systems if sys not in self._all_systems]
        # Add callback functions for automatic updating of the block diagram
        for sys in systems:
            sys.on_input_connection_addition_callbacks \
                .append(self._on_system_input_addition)
            sys.on_input_connection_removal_callbacks \
                .append(self._on_system_input_removal)
            sys.on_output_connection_addition_callbacks \
                .append(self._on_system_output_addition)
            sys.on_output_connection_removal_callbacks \
                .append(self._on_system_output_removal)
            sys.on_deletion_callbacks.append(self._on_system_deletion)
        self._all_systems.extend(systems)
        self._tails.extend([sys for sys in systems if not sys.input_systems])
        self._heads.extend([sys for sys in systems if not sys.output_systems])
        # Set that the julia system is not up-to-date
        self._julia_system_uptodate = False

    def _remove_systems(self, systems: typing.List[peepypoo.Systems.System]) \
            -> None:
        """
        Removes the given systems from the block diagram.

        :param systems: The systems to remove.
        :return:
        """
        for sys in systems:
            # Remove callbacks
            sys.on_input_connection_addition_callbacks \
                .remove(self._on_system_input_addition)
            sys.on_input_connection_removal_callbacks \
                .remove(self._on_system_input_removal)
            sys.on_output_connection_addition_callbacks \
                .remove(self._on_system_output_addition)
            sys.on_output_connection_removal_callbacks \
                .remove(self._on_system_output_removal)
            sys.on_deletion_callbacks.remove(self._on_system_deletion)
            # Remove the system from the bd
            self._all_systems.remove(sys)
            if sys in self._tails:
                self._tails.remove(sys)
            if sys in self._heads:
                self._heads.remove(sys)
        # Set that the julia system is not up-to-date
        self._julia_system_uptodate = False

    @staticmethod
    def _get_systems(systems: typing.List[peepypoo.Systems.System]) \
            -> typing.List[peepypoo.Systems.System]:
        """
        Get all systems connected to any of the given systems.

        :param systems: The systems to which
        :return:
        """
        added = False
        # Get all systems in graph
        all_sys: typing.List[peepypoo.Systems.System] = systems.copy()
        for sys in systems:
            conn_sys: typing.List[peepypoo.Systems.System] = \
                [s for s in sys.input_systems if s not in all_sys]
            conn_sys.extend([s for s in sys.output_systems
                             if s not in all_sys and s not in conn_sys])
            all_sys.extend(conn_sys)
            if len(conn_sys) > 0:
                added = True

        if added:
            return BDWrapper._get_systems(all_sys)
        else:
            return all_sys

    def _on_system_input_addition(self,
                                  sys: peepypoo.Systems.System,
                                  event: dict):
        """
        Callback to add the following systems if an input connection is added
        to a system in the block diagram.

        :param sys: The system to which the input connection was added
        :param event: Dict containing information on the occurred event
        :return:
        """
        if sys in self._tails:
            self._tails.remove(sys)
        self._add_systems(self._get_systems(sys.input_systems))

    def _on_system_input_removal(self,
                                 sys: peepypoo.Systems.System,
                                 event: dict):
        """
        Callback to change the tails of the block diagram if an input
        connection is removed from a system in the block diagram.

        :param sys: The system from which the input connection was removed
        :param event: Dict containing information on the occurred event
        :return:
        """
        if not sys.input_systems and sys not in self._tails:
            self._tails.append(sys)
        # Set that the julia system is not up-to-date
        self._julia_system_uptodate = False

    def _on_system_output_addition(self,
                                   sys: peepypoo.Systems.System,
                                   event: dict):
        """
        Callback to add the following systems if an output connection is added
        to a system in the block diagram.

        :param sys: The system to which the output connection was added
        :param event: Dict containing information on the occurred event
        :return:
        """
        if sys in self._heads:
            self._heads.remove(sys)
        self._add_systems(self._get_systems(sys.output_systems))

    def _on_system_output_removal(self,
                                  sys: peepypoo.Systems.System,
                                  event: dict):
        """
        Callback to change the heads of the block diagram if an output
        connection is removed from a system in the block diagram.

        :param sys: The system from which the output connection was removed
        :param event: Dict containing information on the occurred event
        :return:
        """
        if not sys.output_systems and sys not in self._heads:
            self._heads.append(sys)
        # Set that the julia system is not up-to-date
        self._julia_system_uptodate = False

    def _on_system_deletion(self, sys: peepypoo.Systems.System, event: dict):
        """
        Callback to remove the system from the block diagram on system
        deletion.

        :param sys: The system from which the output connection was removed
        :param event: Dict containing information on the occurred event
        :return:
        """
        self._remove_systems([sys])

    def has_cycles(self) -> bool:
        """
        Check if there are any cycles in the block diagram

        :return: If there are cycles
        """
        visited = {sys: False for sys in self._all_systems}
        finished = {sys: False for sys in self._all_systems}
        for sys in self._all_systems:
            if self._cycle_detection_dfs(sys, visited, finished):
                return True
        return False

    def has_algebraic_loop(self) -> bool:
        """
        Check if there are any algebraic loops in the block diagram. An
        algebraic loop is a loop without any dynamics, i.e. only consisting of
        algebraic relations.

        :return: If there is an algebraic loop in the system
        """
        visited = {sys: False for sys in self._all_systems}
        finished = {sys: isinstance(sys,
                                    peepypoo.Systems.Dynamic.DynamicalSystem)
                    for sys in self._all_systems}
        for sys in self._all_systems:
            if self._cycle_detection_dfs(sys, visited, finished):
                return True
        return False

    def _cycle_detection_dfs(
            self, system: peepypoo.Systems.System,
            visited: typing.Dict[peepypoo.Systems.System, bool],
            finished: typing.Dict[peepypoo.Systems.System, bool]) -> bool:
        """
        Perform a depth first search (DFS) through the graph to detect cycles
        in the block diagram.

        :param system: The current system to search in the DFS
        :param visited: A dict mapping all systems in the block diagram to a
            boolean if they were already visited by the algorithm.
        :param finished: A dict mapping all systems in the block diagram to a
            boolean if the following systems were already completely searched
            for loops by the algorithm.
        :return:
        """
        if finished[system]:
            return False
        if visited[system]:
            return True
        visited[system] = True
        for adj in system.output_systems:
            if self._cycle_detection_dfs(adj, visited, finished):
                return True
        finished[system] = True
        return False

    def _compile_julia_system(self) -> None:
        """
        Compiles the BDWrapper to a Julia ModelingToolkit ODESystem.

        Collects all systems and connects them to a large system comprising
        all.
        :return: Nothing
        """

        # Check if all systems are convertible to Julia ModelingToolkit
        all_systems: typing.List[peepypoo.Systems
                                 .JuliaModelingToolkitConvertibleSystem] = []
        for sys in self._all_systems:
            # Check if the system is julia convertible
            if not isinstance(
                    sys, peepypoo.Systems.JuliaModelingToolkitConvertibleSystem
            ):
                raise ValueError("Error compiling the Julia System: All "
                                 "systems have to inherit from the "
                                 "JuliaModelingToolkitConvertibleSystem class "
                                 "for compiling them to Julia and computing "
                                 "them there. System {} does not.".format(sys))
            all_systems.append(sys)

        # Compile, collect and connect all systems
        connections = []
        systems_jl = []
        ic_jl = []
        tstops_jl = []
        d_discontinuities = []
        discontinuities_period = []
        indep_variables = []
        for sys in all_systems:
            # Append the system and ic
            systems_jl.append(sys.system_jl)
            ic_jl.append(sys.initial_condition_jl)
            tstops_jl.extend(sys.tstops_jl)
            d_discontinuities.extend(sys.d_discontinuities_jl)
            discontinuities_period.extend(sys.discontinuities_period_jl)
            indep_variables.append(sys.t_jl)
            # Get the connections between the systems
            for i in range(sys.num_inputs):
                inp_map = sys.input_mapping[i, :]
                if inp_map[0] == sys.NOT_CONNECTED:
                    raise ValueError("Error compiling the Julia "
                                     "ModelingToolkit system: Input {nInp} "
                                     "of system {sys} in the wrapper is not connected."
                                     " Only systems with all inputs connected are supported.".format(sys=sys, nInp=i))
                inp_sys = sys.input_systems[inp_map[0]]
                if inp_sys not in all_systems:
                    raise ValueError("Error compiling the Julia "
                                     "ModelingToolkit system: The system tree is not fully contained in the BDWrapper."
                                     " This is currently not supported. System {inSys} in the wrapper is connected to "
                                     "system {outSys} which is not in the wrapper.".format(inSys=sys, outSys=inp_sys))
                inp_sys = typing.cast(peepypoo.Systems
                                      .JuliaModelingToolkitConvertibleSystem,
                                      inp_sys)
                outp = inp_sys.outputs_jl[inp_map[1]]
                inp = sys.inputs_jl[i]
                for outp, inp in zip(peepypoo.Utilities.flatten(outp),
                                     peepypoo.Utilities.flatten(inp)):
                    connections.append(self.jl.Equation(outp, inp))

        # Convert connections, systems and ics to julia
        connections = juliacall.convert(self.jl.Vector, connections)
        systems_jl = juliacall.convert(self.jl.Vector, systems_jl)
        indep_variables = self.jl.unique(self.jl.vcat(*indep_variables))
        self._julia_ics = self.jl.merge(*ic_jl)

        # Separate tstops and d_discontinuities into periodic and non-periodic
        # ones
        self._julia_tstops = self.jl.Vector()
        self._julia_d_discontinuities = self.jl.Vector()
        self._julia_discontinuities_periodic = []
        for tstop, d_disc, period in zip(tstops_jl,
                                         d_discontinuities,
                                         discontinuities_period):
            if math.isinf(period[0]):
                self.jl.append_b(self._julia_tstops, tstop)
                self.jl.append_b(self._julia_d_discontinuities, d_disc)
            else:
                self._julia_discontinuities_periodic.append(
                    {'period': period[0],
                     'phase': period[1],
                     'tstops': tstop,
                     'd_disc': d_disc}
                )

        # Check if only one independent variable
        if len(indep_variables) > 1:
            raise ValueError("Converting systems to the Julia ModelingToolkit "
                             "with different independent variables is "
                             "currently not supported: All systems must have "
                             "the same independent variable.")
        indep_var = indep_variables[0]

        # Assemble the full system and simplify it
        full_system = self.jl.compose(
            self.jl.ODESystem(connections, indep_var,
                              name=self.jl.Symbol('full_system')),
            systems_jl
        )
        self._julia_system = self.jl.structural_simplify(full_system)

        self._julia_system_uptodate = True

    def evaluate_blockdiagram_julia(self,
                                    t_start: float,
                                    t_end: float,
                                    t_eval: typing.Iterable[float] = None,
                                    atol: float = None,
                                    rtol: float = None) -> pd.DataFrame:
        """
        Evaluate the wrapped blockdiagram in Julia.

        Compiles the Julia system if not yet done and computes the solution of
        the system in the given timeinterval at each timestep.

        :param t_start: Start time for the evaluation
        :param t_end: Stop time for the evaluation
        :param t_eval: Iterable with the times to evaluate. Set to ``None``
            empty for automatic decision
        :param atol: The absolute tolerance for the solver. None for default. The default is set by the Julia dependency
            `DifferentialEquations.jl <https://docs.sciml.ai/DiffEqDocs/stable/>`_. See `solver docs there <https://docs.sciml.ai/DiffEqDocs/stable/basics/common_solver_opts/#CommonSolve.solve-Tuple{SciMLBase.AbstractDEProblem,%20Vararg{Any}}>`_.
        :param rtol: The relative tolerance for the solver. None for default. The default is set by the Julia dependency
            `DifferentialEquations.jl <https://docs.sciml.ai/DiffEqDocs/stable/>`_. See `solver docs there <https://docs.sciml.ai/DiffEqDocs/stable/basics/common_solver_opts/#CommonSolve.solve-Tuple{SciMLBase.AbstractDEProblem,%20Vararg{Any}}>`_.
        :return: A pandas dataframe containing the solution of the blockdiagram, i.e. the output of each system at
            the requested timestamps.
            The dataframe has row indices being the timesteps at which the solution is evaluated and column indices
            being the *systems* (yes, the actual system objects) in the blockdiagram. Thus, the output trajectory of
            a specific system is obtained by directly indexing the output by the system object.
            The elements of the dataframe, i.e. the output of a specific system at a specific time, are each a numpy
            array of objects containing all outputs of the system at that time (note that depending on the outputs of
            the system, the elements of this array might again be numpy arrays themselves).
        """
        if t_eval is None:
            t_eval = []

        # Assemble tolerance dict
        toldict = {}
        if atol is not None:
            toldict['abstol'] = atol
        if rtol is not None:
            toldict['reltol'] = rtol

        # Compile the system if not up-to-date
        if not self._julia_system_uptodate:
            self._compile_julia_system()

        # Assemble Periodic discontinuities
        tstops_all = self._julia_tstops
        d_disc_all = self._julia_d_discontinuities

        def periodic_continuation(period, phase, vec, t_start, t_end):
            # Extend the value to [t_start, t_end]
            i = 0
            start = phase
            end = phase + period
            vec_ext = vec
            while end < t_end:
                i += 1
                end += period
                vec_ext = np.append(vec_ext, vec + i * period)
            i = 0
            while start > t_start:
                i += 1
                start -= period
                vec_ext = np.append(vec - i * period, vec_ext)
            # Crop vec_ext to only contain values in the range
            vec_ext = vec_ext[np.bitwise_and(t_start <= vec_ext,
                                             vec_ext <= t_end)]
            return vec_ext

        for disc in self._julia_discontinuities_periodic:
            # Repeat tstops to simulation range
            self.jl.append_b(
                tstops_all, juliacall.convert(
                    self.jl.Vector,
                    periodic_continuation(disc['period'], disc['phase'],
                                          disc['tstops'].to_numpy(),
                                          t_start, t_end)
                ))

            # Repeat d_disc to simulation range
            self.jl.append_b(
                d_disc_all, juliacall.convert(
                    self.jl.Vector,
                    periodic_continuation(disc['period'], disc['phase'],
                                          disc['d_disc'].to_numpy(),
                                          t_start, t_end)
                ))

        # Make the ODAEProblem
        problem = self.jl.ODAEProblem(
            # Basic system
            self._julia_system, self._julia_ics, (t_start, t_end),
            # Saving and stepping options
            saveat=juliacall.convert(self.jl.Vector, t_eval),
            tstops=tstops_all,
            d_discontinuities=d_disc_all,
            # Use symbolics
            jac=True, sparse=True,
            # Solver tolerance
            **toldict
        )
        # Solve the problem
        solution = self.jl.solve(problem)

        # Assign the solution to a pandas dataframe
        full_solution = pd.DataFrame(index=solution.t,
                                     columns=self._all_systems)
        for sys in self._all_systems:
            sys = typing.cast(peepypoo.Systems
                              .JuliaModelingToolkitConvertibleSystem, sys)
            state = np.array([self.jl.getindex(solution, outp).to_numpy()
                              for outp in Utilities.flatten(sys.outputs_jl)]).T
            state = Utilities.apply_nested_list(
                lambda x, t:
                state[:,
                      Utilities.nested_idx_to_linear_idx(sys.outputs_jl, t)],
                sys.outputs_jl)
            state = np.array(
                [[*np.array(v, ndmin=1).transpose().tolist(), []] for v in
                 state], dtype=object).transpose()
            state = Utilities.convert_lists_to_numpy(state[:-1, :])
            full_solution[sys] = list(state)

        return full_solution

    def get_equations(self) -> str:
        """
        Get the differential equations of the resulting system as string representing LaTeX code for the resulting
        equations. The equations are simplified to the minimum amount, including all fixed connections directly in
        the equations. Further, all fixed parameters are directly inserted into the equations.

        .. warning::

            This function relies on the Julia translation of the systems and thus only works with the systems that
            can be converted to Julia.

        All variables are named after 'SYSTEM_NAME'_{+}'VARIABLE_NAME', where 'SYSTEM_NAME' is the name given to the
        system and 'VARIABLE_NAME' is the name of the variable. If multiple systems have the same name, their names are
        added a subscript with the id of the system.

        .. note::

            System names being numeric are supported, but they are discouraged to use because in the resulting equations
            the multiplication is not shown by a dot but the convention of simply concatenating it is followed. If now
            the system name starts with a numeric value, the resulting equations are confusing because it is not
            directly clear which digit is part of the system name or of the factor multiplying it.

        .. note::

            The returned equations are the result of a symbolic simplification of the system. Thus, some symbols that
            are expected might have been optimized away. See the documentation of structural_simplify in
            `ModelingToolkit.jl <https://docs.sciml.ai/ModelingToolkit/stable/>`_ for more info about the
            simplification.

        .. note::

            This function returns a string containing the LaTeX code of the equations, which is not easily readable.
            Thus, it is recommended to compile the output string before having a look at it. There are a multitude of
            methods to do this.

            For example, if you use a Jupyter Notebook to run the code, you can directly use Jupyter to display the
            equations nicely using:

            .. code-block::

                from IPython.display import Math
                Math(eqs)

        :return: A string containing the latex code of the equations solved for the system.
        """

        # Compile the system if not up-to-date
        if not self._julia_system_uptodate:
            self._compile_julia_system()

        # Get the resulting equations as Latex
        eqs = str(self.jl.ModelingToolkit.latexify(self.jl.full_equations(self._julia_system)))

        # Get all systems, which have a unique name
        sys_names = np.array([s.name for s in self._all_systems])  # All system names
        dupl_sys = []  # Get all systems which have the same names as others
        for s in self._all_systems:
            if np.sum(s.name == sys_names) > 1:
                dupl_sys.append(s)
            if s.name[0].isdigit():
                warnings.warn("The system name {} starts with a number. This produces the correct result, but is "
                              "discouraged because this makes multiplication is implied by concatenation the result "
                              "might be misleading (not directly clear if the number is part of the name or of the "
                              "multiplying constant).")

        # Systems with unique names
        sys_unique = list(set(self._all_systems).difference(dupl_sys))

        # Unique namestrings
        unique_names = (
            dict([(s.name, str(typing.cast(peepypoo.Systems.JuliaModelingToolkitConvertibleSystem, s).system_jl.name))
                  for s in sys_unique]))

        # Replace names in equation
        # unique ones remove the timestamp
        for (new, old) in unique_names.items():
            if old.split('_')[0].isdigit():
                eqs = eqs.replace(old.replace('_', '_{'), '{' + new)
            else:
                eqs = eqs.replace(old.replace('_', '}_{'), new)

        # Non_unique ones make only the timestamp subscript
        for s in dupl_sys:
            name = str(typing.cast(peepypoo.Systems.JuliaModelingToolkitConvertibleSystem, s).system_jl.name)
            eqs = eqs.replace(name.replace('_', '}_{'), name.replace('_', '_{') + '}')

        eqs.replace('}_{+}', '+}')

        return eqs

    @classmethod
    def convert_df_systems_to_df_signals(cls, df: pd.DataFrame) -> pd.DataFrame:
        """
        Convert the dataframe with columns being :class:`systems<peepypoo.Systems.System>`, as provided by
        :meth:`peepypoo.BDWrapper.evaluate_blockdiagram_julia`, to a dataframe where each column corresponds to a signal
        in the system.

        :param df: The dataframe with columns being systems to be converted.
        :return: A pandas dataframe containing the dataframe with columns being systems transformed to a dataframe
            with signals as columns.
            The dataframe has the same row indices as `df` and column indices being the signals in the blockdiagram.
            Hereby, as the signals do generally not have names specified, the columns are strings containing
            'SYSTEM_NAME: Output i' where SYSTEM_NAME is the name of the system and i the number of the output.
            If the same system name occurs multiple times in the dataframe, the systems are enumerated and a warning
            shown.
            The elements of the dataframe are then the values of the signal at the given time.
        """

        t = df.index
        sys = df.columns
        if any([not isinstance(s, peepypoo.Systems.System) for s in sys]):
            raise ValueError("Error converting the given dataframe: All columns must be indexed using PeePyPoo "
                             "systems, the columns with numeric indices {c} are not."
                             .format(c=np.asarray([not isinstance(s, peepypoo.Systems.System)
                                                   for s in sys]).nonzero()[0]))
        sys_names = np.array([s.name for s in sys], dtype=object)  # Get system names, dtype=object for non-fixed length
        sys_names_enum = sys_names.copy()  # Operating copy to allow counting while modifying
        for i in range(len(sys_names)):  # Append the number of previous occurrences to the system name
            if np.sum(sys_names[:i] == sys_names[i]) > 0:
                warnings.warn("There are multiple systems named '{n}' in the dataframe. Enumerating them."
                              .format(n=sys_names[i]))
                sys_names_enum[i] = sys_names[i] + str(np.sum(sys_names[:i] == sys_names[i]))
        sys_names = sys_names_enum

        # Build the column names
        cols = [name + ": Output " + str(n) for s, name in zip(sys, sys_names) for n in range(s.num_outputs)]

        df_signals = pd.DataFrame(index=t, columns=cols)
        for i, s in enumerate(sys):
            outp = np.array(df[s].tolist())
            if len(outp.shape) != 2:
                raise ValueError("Error converting the given dataframe: The output data for each system at a specific"
                                 "time must be a list for the output of each system. This is not the case for "
                                 "system {s}.".format(s=s))
            if outp.shape[1] != s.num_outputs:
                raise ValueError("Error converting the given dataframe: The output data in the dataframe for system {s}"
                                 " has {n_df} elements at each timestep but the system outputs {n_s} signals."
                                 .format(s=s, n_df=outp.shape[1], n_s=s.num_outputs))
            for sig in range(s.num_outputs):
                df_signals[cols[i]] = outp[:, sig]

        return df_signals
