import copy
import typing

from collections.abc import Iterable, Sized

__all__ = ["flatten", "apply_nested_list", "apply_to_iter_in_nested_list",
           "convert_lists_to_numpy"]

import numpy as np


def flatten(xs: typing.Any) -> typing.Generator:
    """
    Helper function for iterating through an arbitrarily nested list.

    :param xs: The nested list
    :return: Generator to walk through the nested list
    """
    if not isinstance(xs, Iterable) or isinstance(xs, (str, bytes)):
        yield xs
    for x in xs:
        if isinstance(x, Iterable) \
                and not isinstance(x, (str, bytes)) \
                and len(x) > 1:
            yield from flatten(x)
        else:
            try:
                yield from flatten([x[0]])
            except:
                yield x


def nested_idx_to_linear_idx(lst: typing.Iterable,
                             nested_idx: typing.List[int] = None) -> int:
    """
    Converts a nested index into the linear index needed if the list would be
    flattened out.

    :param lst: The nested list in which the nested index is applied
    :param nested_idx: The nested index
    :return: The linear index to get to the same element in a fully flattened
        version of the nested list
    """
    # Length 1 it hasn't any length
    if not hasattr(lst, '__len__'):
        return 1
    else:
        # If it has the length attribute but is not indexable, skip anyway
        # (needed for juliacall objects)
        try:
            lst[0]
        except:
            return 1
    if nested_idx is None or not nested_idx:
        # Count elements in the possibly nested list if no index given
        cnt = 0
        for elem in lst:
            cnt += nested_idx_to_linear_idx(elem)
        return cnt

    # Return the sum of element of the previous lists plus the nested idx to
    # linear idx of the next lower level. If at the lowest level, subtract 1
    # because python is 0-based
    return sum([nested_idx_to_linear_idx(elem)
                for elem in lst[0:nested_idx[0]]]) + \
        nested_idx_to_linear_idx(lst[nested_idx[0]], nested_idx[1:]) \
        - (1 if not nested_idx[1:] else 0)


def apply_nested_list(
        fun: typing.Callable[[typing.Any, typing.List[int]], typing.Any],
        lst: typing.Any,
        _tree: typing.List[int] | None = None
) -> typing.List:
    """
    Applies the given function to each element in the nested list.

    Recursively iterates through the given nested list and applies the
    function to each element.

    .. warning::

        If an empty list is in the nested list, it is treated as a final
        element and the function is called on it. Therefore, the function
        must be robust to handle this case if it is possible that it
        occurs.

    :param fun: The function to apply to each element. Takes two arguments,
        the first one is the element to process, the second one a list with
        the indices used to index to the passed element.
    :param lst: The nested list to apply the function to
    :param _tree: The tree to reach the current element.
    :return: A nested list with the same structure as ``lst`` with elements
        being the returned values from the callback applied to this
        element.
    """
    if _tree is None:
        _tree = []
    if isinstance(lst, Sized) and isinstance(lst, Iterable) and len(lst) > 1:
        ret = []
        for i, e in enumerate(lst):
            ret.append(apply_nested_list(fun, e, [*_tree, i]))
        return ret
    else:
        try:  # Try if the first element can be accessed.
            # Needed as all julia values are iterable.
            return [apply_nested_list(fun, lst[0], [*_tree, 0])]
        except:
            return fun(lst, _tree)


def apply_to_iter_in_nested_list(
        fun: typing.Callable[[typing.Iterable], typing.Any],
        lst: typing.Any
) -> typing.Any:
    """
    Applies the given function to each iterable in the given nested list.
    Non-iterable elements are left as they were. Therefore, the given function
    must accept any iterable.

    Recursively iterates through the given nested list and applies the
    conversion to each iterable found.

    .. warning::

        If an empty list is in the nested list, it is treated as a final
        element and the function is called on it. Therefore, the function
        must be robust to handle this case if it is possible that it
        occurs.

    :param fun: The function to apply to each iterable in the nested. Takes an
        arbitrary iterable as input and returns another iterable preferably
        with the same elements
    :param lst: The nested list to apply the function to
    :return: The transform of the given nested iterable where the function has
        been applied to each iterable within it.
    """
    if isinstance(lst, Sized) and isinstance(lst, Iterable) and len(lst) > 1:
        ret = []
        for i, e in enumerate(lst):
            ret.append(apply_to_iter_in_nested_list(fun, e))
        return fun(ret)
    elif isinstance(lst, Sized) and isinstance(lst, Iterable):
        try:
            return apply_to_iter_in_nested_list(fun, [lst[0]])
        except:
            return fun(lst)
    else:
        return lst


def convert_lists_to_numpy(lst: np.ndarray) -> np.ndarray:
    """
    Converts all iterables in the given numpy array to numpy arrays.

    :param lst: The Iterable to traverse
    :return: Numpy array with the contents of the given iterable but all
        iterables within it replaced by numpy arrays.
    """
    out = copy.deepcopy(lst)
    for i, val in enumerate(lst):
        if isinstance(val, Iterable):
            out[i] = convert_lists_to_numpy(np.array(val))
    return out
