import typing
import numpy as np
import pandas as pd
from peepypoo.SignalTypes._SignalType import get_signal_class, SignalType

__all__ = ['ListSignals', 'NumpySignals', 'DictSignals', 'PandasDFSignals']


#######################################################
# A signal type for lists
#######################################################
class ListSignals(SignalType):
    """
    :class:`SignalType` for lists.
    """

    __shape: int
    __dtypes: typing.List[SignalType]

    def __init__(self, val: typing.Any):
        super().__init__(val)
        self.__shape = len(val)
        self.__dtypes = [get_signal_class(v) for v in val]

    def supports_value(self, val: typing.Any) -> bool:
        if type(val) is not list:
            return False
        if not hasattr(self, '_ListSignals__shape') \
                or not hasattr(self, '_ListSignals__dtypes'):
            return True
        else:
            return len(val) == self.__shape and all(
                [st.is_compatible(get_signal_class(v))
                 for st, v in zip(self.__dtypes, val)])

    def get_zero(self) -> typing.List:
        return [st.get_zero() for st in self.__dtypes]

    def is_compatible(self, other: 'SignalType') -> bool:
        return self.supports_value(other.get_zero())

    def __str__(self):
        return str(type(self.get_zero())) + '[' + ', '.join([str(s) for s in self.__dtypes]) + ']'


#######################################################
# A signal type for numpy arrays
#######################################################
class NumpySignals(ListSignals):
    """
    :class:`SignalType` for numpy arrays.
    """

    __shape: typing.Tuple
    __dtypes: np.dtype

    def __init__(self, val: typing.Any):
        super().__init__(val)
        self.__shape = val.shape
        self.__dtypes = val.dtype

    def supports_value(self, val: typing.Any) -> bool:
        if type(val) is not np.ndarray:
            return False
        if not hasattr(self, '_NumpySignals__shape') \
                or not hasattr(self, '_NumpySignals__dtypes'):
            return True
        else:
            return val.shape == self.__shape and val.dtype == self.__dtypes

    def get_zero(self) -> np.ndarray:
        return np.zeros(self.__shape, dtype=self.__dtypes)

    def __str__(self):
        return str(type(self.get_zero())) + '(shape=' + str(self.__shape) + ', dtype=' + str(self.__dtypes) + ')'


#######################################################
# A signal type for dictionaries
#######################################################
class DictSignals(SignalType):
    """
    :class:`SignalType` for dictionaries.
    """

    __keys: typing.List
    __types: typing.List[SignalType]

    def __init__(self, val: typing.Any):
        super().__init__(val)
        self.__keys = list(val.keys())
        self.__types = [get_signal_class(v) for v in val.values()]

    def supports_value(self, val: typing.Any) -> bool:
        if type(val) is not dict:
            return False
        if not hasattr(self, '_DictSignals__keys') \
                or not hasattr(self, '_DictSignals__types'):
            return True
        else:
            return len(val) == len(self.__keys) and \
                   all([a == b for a, b in
                        zip(list(val.keys()), self.__keys)]) \
                   and all([st.is_compatible(get_signal_class(v))
                            for st, v in zip(self.__types, val.values())])

    def get_zero(self) -> dict:
        return {k: v.get_zero()
                for k, v in zip(list(self.__keys), self.__types)}

    def is_compatible(self, other: 'SignalType') -> bool:
        return self.supports_value(other.get_zero())

    def __str__(self):
        return (str(type(self.get_zero())) + '{' +
                ', '.join([str(k) + ': ' + str(v) for k, v in zip(list(self.__keys), self.__types)]) + '}')


#######################################################
# A signal type for pandas dataframes
#######################################################
class PandasDFSignals(SignalType):
    """
    :class:`SignalType` for pandas dataframes.
    """

    __shape: typing.Tuple
    __columns: pd.Index
    __index: pd.Index
    __types: pd.Series

    def __init__(self, val: typing.Any):
        super().__init__(val)
        self.__shape = val.shape
        self.__columns = val.columns
        self.__index = val.index
        self.__types = val.dtypes

    def supports_value(self, val: typing.Any) -> bool:
        if type(val) is not pd.DataFrame:
            return False
        if not hasattr(self, '_PandasDFSignals__shape') \
                or not hasattr(self, '_PandasDFSignals__columns') \
                or not hasattr(self, '_PandasDFSignals__index') \
                or not hasattr(self, '_PandasDFSignals__types'):
            return True
        else:
            return val.shape == self.__shape and \
                   all(val.columns == self.__columns) and \
                   all(val.index == self.__index) and \
                   all(val.dtypes == self.__types)

    def get_zero(self) -> pd.DataFrame:
        return pd.DataFrame(data=0, index=self.__index,
                            columns=self.__columns).astype(self.__types)

    def is_compatible(self, other: 'SignalType') -> bool:
        return self.supports_value(other.get_zero())

    def __str__(self):
        return (str(type(self.get_zero())) + '(shape=' + str(self.__shape) + ', index=' + str(self.__index.tolist()) +
                ', columns=' + str(self.__columns.tolist()) + ', types=' + str(self.__types.tolist()) + ')')
