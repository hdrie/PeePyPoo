import typing

__all__ = ['get_signal_class', 'SignalType']


def get_signal_class(val: typing.Any) -> 'SignalType':
    """
    Returns the signal class for the given value.

    Searches all available subclasses of ``SignalType`` for one supporting
    the given value. For this, precedence is given to the most specialized
    type that matches.

    :param val: The value to be in the signal class
    :return: A ``SignalType`` instance for the type of this value
    """
    def find_supporting(cls: typing.Type[SignalType]) \
            -> typing.Optional[SignalType]:
        for c in cls.__subclasses__():
            c_sup = find_supporting(c)
            if c_sup is not None:
                return c_sup
            else:
                try:
                    return c(val)
                except TypeError:
                    continue
        return None

    c_sup = find_supporting(SignalType)
    if c_sup is not None:
        return c_sup
    else:
        try:
            return SignalType(val)
        except TypeError:
            raise TypeError("Error instantiating a signal of type {typ}: "
                            "This type is currently not supported. Either "
                            "implement a subclass of ``SignalType`` "
                            "supporting this variable type or use a "
                            "different variable type.")


class SignalType:
    """
    A class for defining a signal's type.

    This class serves as a base class for any class that holds a value to
    transmit between systems and defines the interface for it. Further it
    serves as a generic signal type for all types that admit an empty
    constructor.

    :param val: An exemplary value of this signal. It's type must accept an
        empty constructor
    :raises TypeError: Throws a TypeError if the given value is not supported
        by this ``SignalType`` class.
    """

    __type: type
    """ The type of this signal """

    def __init__(self, val: typing.Any) -> None:
        if not self.supports_value(val):
            raise TypeError("Error instantiating a signal of type {typ}: "
                            "The type must have a valid empty constructor or "
                            "a subclass of ``SignalType`` must explicitly "
                            "support this type. Either implement a subclass "
                            "of ``SignalType`` supporting this variable type "
                            "or use a different variable type.")
        self.__type = type(val)

    def supports_value(self, val: typing.Any) -> bool:
        """
        If this ``SignalType`` class supports the given value.

        :param val: The value to check the support of
        :return: Boolean if this type is supported by this class.
        """
        try:
            type(val)()
        except TypeError:
            return False
        return True

    def get_zero(self) -> typing.Any:
        """
        Gets the zero/neutral element of this type.

        :return: Returns the neutral element of this type
        """
        return self.__type()

    def is_compatible(self, other: 'SignalType') -> bool:
        """
        Returns if the other signal type is compatible with this one.

        For the generic system, it is all that are subclasses of the type.

        :param other: The signal type to check compatibility
        :return: If the two ``SignalType`` are compatible
        """
        return issubclass(other.__type, self.__type)

    def cast(self, val: typing.Any) -> typing.Any:
        """
        Casts the given value to the type in this signal.

        .. warning::

            This only casts it to the new type using typing, without any actual
            change in the data

        :param val: The value to cast
        :return: The value cast to the type of this signal
        """
        return typing.cast(self.__type, val)

    def __str__(self):
        """
        Returns a string representing this signal type.

        This function is used to represent this signal type as string.
        It simply returns the string representation of the signal type.
        """
        return str(type(self.get_zero()))
